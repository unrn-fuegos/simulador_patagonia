﻿#ifdef WIN32
	#include <windows.h>
#endif



#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <GL/freeglut.h>


//#include "superinclude.h"
#include "GIS/codigo_entrada_salida_mapas.cu"

// Utilities and timing functions
#include "cuda_helpers/helper_functions.h"    // includes cuda.h and cuda_runtime_api.h
#include "cuda_helpers/timer.h"               // timing functions

// CUDA helper functions
#include "cuda_helpers/helper_cuda.h"         // helper functions for CUDA error check
#include "cuda_helpers/helper_cuda_gl.h"      // helper functions for CUDA/GL interop
#include <vector_types.h>

#include "imgui/imgui.h"
//#include "imgui/imgui_draw.cpp"
//#include "imgui/imgui.cpp"
//#include "imgui/imgui_demo.cpp"
//#include "imgui_sig.c"

#include "Singleton.h"

#include "main.h"
#include "initgl.h"
#include "entrada.h"
#include "vertices.h"
#include "trigonometria.h"
#include "includes_del_ejemplo.h"
#include "my_imgui_draw.h"

#include <cmath>
#include <assert.h>

#ifdef LINUX
#include <sys/time.h>	/* gettimeofday */
#endif

#include <ctime>
#include <time.h>
#include <cuda.h>
// includes, cuda
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>


#include <stdio.h>	/* print */
#include <stdlib.h>
#include <iostream>	/* print */
#include <math.h>   /*math functions stl library*/
#include "Simulador.h"
#include "Parameters.h"
#include "curso.h"
#include "Manejador_Proyectos.h"
#include <stdio.h>

const char* execname="main";
const char *sSDKsample = "simpleGL (VBO)";

StopWatchInterface *timer = NULL;

/* Datos del proyecto, como primer opcion
   las hago variables globales del simulador */

int verificar_propagacion = 1;
char opcion;

int paso_historicos =  10;  //es la cantidad de simulaciones entre cada almacenamiento de los historicos
////////



int automode=0;
float automode_speed=1.0f;
float g_t=0.0f;

int frame_cnt=0;

int x,y;

extern bool show_barrapaso_window;


////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    time_t now = time(0);
    char* dt = ctime(&now);
    print_debug("*************************************************************** \n");
    print_debug("Inicializacion del programa %s",dt);

#if defined(__linux__)
    setenv ("DISPLAY", ":0", 0);
#endif

    if (argc != 2) {
		//printf("Parametros Incorrectos, lo correcto es:\n%s carpeta_proyecto\\\n",execname);
        printf("Si desea cargar directamente un proyecto escriba:\n%s carpeta_proyecto\\\n",execname);
		//exit (-1);
	}

    if(argc==2)
    {
        Singleton::getInstance()->proyecto_cargado=1;
    }

    if(Singleton::getInstance()->proyecto_cargado)
    {
    	//proyecto = argv[1];
    	memcpy(Singleton::getInstance()->PROJECT_FOLDER_P,argv[1],1+strlen(argv[1]));

    	CP_1();
    }

    if(!Singleton::getInstance()->proyecto_cargado)
    {
        Singleton::getInstance()->PROJECT_FOLDER_P[0]=0;
    }

	Singleton::getInstance()->peli_col[0]=vec3(0,0,0);//COLORES DE PELIGROSIDAD
    Singleton::getInstance()->peli_col[1]=vec3(146,208,80);
    Singleton::getInstance()->peli_col[2]=vec3(85,142,213);
    Singleton::getInstance()->peli_col[3]=vec3(255,255,0);
    Singleton::getInstance()->peli_col[4]=vec3(255,192,0);
    Singleton::getInstance()->peli_col[5]=vec3(255,0,0);

	/*Singleton::getInstance()->FWI_T.init(Singleton::getInstance()->PROJECT_FOLDER_P,(char*)"indices_iniciales.txt",(char*)"datos_meteorologicos.txt",(char*)"salida_FWI.txt");
	Singleton::getInstance()->FWI_T.load_files();//PROCESADO DATOS METEOROLOGICOS Y CALCULO FWI*/
    sdkCreateTimer(&timer);
    initGLUT(Singleton::getInstance()->PROJECT_FOLDER_P);//REGISTRO DE CALLBACKS OPENGL
    cudaGLSetGLDevice(gpuGetMaxGflopsDeviceId());
    glewInit();
    installShaders();//CARGA DE ARCHIVOS TXT CON EL CODIGO DE SHADERS y COMPILADO
    ImGui_ImplGlfwGL3_Init();//INICIALIZADO IMGUI

    if(Singleton::getInstance()->proyecto_cargado){
        CP_2();
    }


    if(!Singleton::getInstance()->proyecto_cargado){
        glutFullScreenToggle();
    }


    print_debug("Entrando a glutMainLoop()\n");
    glutMainLoop();

    printf("%s completed, returned %s\n", sSDKsample, (Singleton::getInstance()->g_TotalErrors == 0) ? "OK" : "ERROR!");
    fclose(Singleton::getInstance()->logfile_sigdbg);
    exit(Singleton::getInstance()->g_TotalErrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}

void CP_1(void)
{
    Singleton::getInstance()->proyectow.Cargar_config(Singleton::getInstance()->PROJECT_FOLDER_P);
    Singleton::getInstance()->proyectow.Cargar_paleta();
    Singleton::getInstance()->XIGNI=Singleton::getInstance()->proyectow.XIGNI;
    Singleton::getInstance()->YIGNI=Singleton::getInstance()->proyectow.YIGNI;
}

void CP_2(void)
{
    print_debug("por entrar a cargar_mapas_de_archivo ok\n");
    cargar_mapas_de_archivo();//CARGA MAPAS RASTER E INICIALIZA LAS ESTRUCTURAS RASTERMAP
    print_debug("cargar_mapas_de_archivo ok\n");
    print_debug("por entrar a initglbuf\n");
    initGLBUF();//CREADO DE VERTICES Y TEXTURAS
    print_debug("initglbuf ok\n");


    print_debug("Proximo a entrar a main_simulador()\n");

    Singleton::getInstance()->Simulador.inicializar(2);//1=AutomataCelular


    Singleton::getInstance()->vegetacion.Load_Paleta(Singleton::getInstance()->paleta_vegetacion,100);
    print_debug("Paleta cargada OK\n");


    Singleton::getInstance()->proyectow.mover_grafico_a_paleta();
    print_debug("mover_grafico_a_paleta OK\n");

    regenerar_textura_final();
    print_debug("regenerar_textura_final OK\n");

    show_barrapaso_window=1;

    if(Singleton::getInstance()->proyectow.start_fullscreen)glutFullScreen();
}

void CP_3(void)
{

}

void CP_4(void)
{

}


void Cargar_proyecto(void)
{
    Singleton::getInstance()->proyecto_cargado=1;
    CP_1();
    CP_2();
}

void set_layer(int idx,bool state,bool sop,float alpha,RasterMap* rmap,const char * nombre)
{
	Singleton::getInstance()->layer_list[idx].ptr_RasterMap = rmap;
    Singleton::getInstance()->layer_list[idx].enabled = state;
    sprintf(Singleton::getInstance()->layer_list[idx].name,"%s",nombre);
    Singleton::getInstance()->layer_list[idx].alpha = &(rmap->map_a);
    rmap->map_a=alpha;
    Singleton::getInstance()->layer_list[idx].show_on_panel=sop;
}


void cargar_mapas_de_archivo(void)//carga mapas para parte visual
{
	Singleton::getInstance()->vert_gis_x=Singleton::getInstance()->proyectow.COLS;
	Singleton::getInstance()->vert_gis_y=Singleton::getInstance()->proyectow.ROWS;
	int vert_gis_x=Singleton::getInstance()->vert_gis_x;
	int vert_gis_y=Singleton::getInstance()->vert_gis_y;
	Singleton::getInstance()->tex_x=vert_gis_x;
	Singleton::getInstance()->tex_y=vert_gis_y;
	int tex_x=Singleton::getInstance()->tex_x;
	int tex_y=Singleton::getInstance()->tex_y;


	Singleton::getInstance()->celdas_prom=max(Singleton::getInstance()->vert_gis_x,Singleton::getInstance()->vert_gis_y)/60;
    if(Singleton::getInstance()->celdas_prom<=0)Singleton::getInstance()->celdas_prom=1;

	print_debug("celdas_prom=%d\n",Singleton::getInstance()->celdas_prom);

	Singleton::getInstance()->camspd=0.1f*(float)vert_gis_x/800.0f;

	int aux;

    Singleton::getInstance()->h_mapaalti=GetMap(Singleton::getInstance()->proyectow.ELEV_MAP_P, vert_gis_y, vert_gis_x,&aux);

    Singleton::getInstance()->verticesT=(vert_gis_y-1)*(vert_gis_x-1)*2*3;
    Singleton::getInstance()->alto=vert_gis_y-1;
    Singleton::getInstance()->ancho=vert_gis_x-1;
    Singleton::getInstance()->vegetacion.h_map=GetMapi(Singleton::getInstance()->proyectow.VEGE_MAP_P, vert_gis_y, vert_gis_x,&Singleton::getInstance()->vegetacion.nodata_value);
    Singleton::getInstance()->vegetacion.is_in_cpu=1;Singleton::getInstance()->vegetacion.is_gpu_utd=0;


	Singleton::getInstance()->vegetacionSIM.h_map=(int*)malloc(sizeof(int)*vert_gis_y*vert_gis_x);
    for(int qi=0;qi<vert_gis_x*vert_gis_y;qi++)
    {
    	//vegetacion.h_map[qi]=0;
    	if(Singleton::getInstance()->vegetacion.h_map[qi]==Singleton::getInstance()->vegetacion.nodata_value){Singleton::getInstance()->vegetacionSIM.h_map[qi]=0;Singleton::getInstance()->vegetacion.h_map[qi]=0;continue;}
    	Singleton::getInstance()->vegetacionSIM.h_map[qi]=Singleton::getInstance()->proyectow.paleta_conversion_vege[Singleton::getInstance()->vegetacion.h_map[qi]];

    	//vegetacionSIM.h_map[qi]=0;
    }
    Singleton::getInstance()->vegetacionSIM.is_in_cpu=1;
    Singleton::getInstance()->vegetacionSIM.set_size(Singleton::getInstance()->tex_x,Singleton::getInstance()->tex_y);


    Singleton::getInstance()->quemado.h_map=(int*)malloc(sizeof(int)*vert_gis_x*vert_gis_y);
    Singleton::getInstance()->selcorf.h_map=(int*)malloc(sizeof(int)*vert_gis_x*vert_gis_y);
    Singleton::getInstance()->h_mapaselcorft=(int*)malloc(sizeof(int)*vert_gis_x*vert_gis_y);
    Singleton::getInstance()->h_mapavegecorf=(float*)malloc(sizeof(float)*vert_gis_x*vert_gis_y);

    Singleton::getInstance()->quemado.is_in_cpu=1;Singleton::getInstance()->quemado.is_gpu_utd=0;
    Singleton::getInstance()->selcorf.is_in_cpu=1;Singleton::getInstance()->selcorf.is_gpu_utd=0;

    Singleton::getInstance()->mapaaux.h_map=(int*)malloc(sizeof(int)*vert_gis_x*vert_gis_y);Singleton::getInstance()->mapaaux.is_in_cpu=1;Singleton::getInstance()->mapaaux.is_gpu_utd=0;
    Singleton::getInstance()->levelmap.h_map=(int*)malloc(sizeof(int)*vert_gis_x*vert_gis_y);Singleton::getInstance()->levelmap.is_in_cpu=1;Singleton::getInstance()->levelmap.is_gpu_utd=0;

    print_debug("OU x=%d y=%d\n",tex_x,tex_y);

    Singleton::getInstance()->quemado.set_size(tex_x,tex_y);
    Singleton::getInstance()->selcorf.set_size(tex_x,tex_y);
    Singleton::getInstance()->vegetacion.set_size(tex_x,tex_y);
    Singleton::getInstance()->mapaaux.set_size(tex_x,tex_y);
    Singleton::getInstance()->levelmap.set_size(tex_x,tex_y);
    Singleton::getInstance()->windImap.set_size(tex_x,tex_y);
    Singleton::getInstance()->aspectmap.set_size(tex_x,tex_y);
    Singleton::getInstance()->altimap.set_size(tex_x,tex_y);
    Singleton::getInstance()->peligmap.set_size(tex_x,tex_y);




    for(int g=0;g<MAX_NUM_LAYERS;g++)
    {
    	Singleton::getInstance()->layer_list[g].show_on_panel=0;
    }

    set_layer(8,true,true,1.0f,&Singleton::getInstance()->vegetacion,"Vegetacion");
    set_layer(7,true,true,1.0f,&Singleton::getInstance()->quemado,"Quemado");
    set_layer(6,true,true,0.8f,&Singleton::getInstance()->selcorf,"Cortafuego");
    set_layer(5,false,true,0.8f,&Singleton::getInstance()->mapaaux,"Inc. Real");
    set_layer(4,false,true,0.8f,&Singleton::getInstance()->levelmap,"Curvas Nivel");
    set_layer(3,false,true,1.0f,&Singleton::getInstance()->aspectmap,"AspectMap");
    set_layer(2,false,true,1.0f,&Singleton::getInstance()->altimap,"Altitud");
    set_layer(1,false,true,1.0f,&Singleton::getInstance()->windImap,"Int. Viento");
    set_layer(0,false,true,1.0f,&Singleton::getInstance()->peligmap,"Peligrosidad");

    Singleton::getInstance()->peligmap.has_tooltip=1;
    sprintf(Singleton::getInstance()->peligmap.tooltiptext,"Sin Definir fecha");

    Singleton::getInstance()->h_mapaaspect=GetMap(Singleton::getInstance()->proyectow.ASPECT_MAP_P, vert_gis_y, vert_gis_x,&aux);
    Singleton::getInstance()->h_mapaslope=GetMap(Singleton::getInstance()->proyectow.SLOPE_MAP_P, vert_gis_y, vert_gis_x,&aux);

    Singleton::getInstance()->h_mapawindANG=GetMap(Singleton::getInstance()->proyectow.WIND_MAP_P, vert_gis_y, vert_gis_x,&Singleton::getInstance()->mapawindANG_ND);
    Singleton::getInstance()->h_mapawindINT=GetMap(Singleton::getInstance()->proyectow.WINDI_MAP_P, vert_gis_y, vert_gis_x,&Singleton::getInstance()->mapawindINT_ND);

    //h_mapaaspect=(unsigned char*)malloc(4*sizeof(unsigned char*)*(vert_gis_x)*vert_gis_y);
    for(int y=0;y<vert_gis_y;y++)
    {
        for(int x=0;x<vert_gis_x;x++)
        {
            Singleton::getInstance()->quemado.h_map[x+y*vert_gis_x]=0;
            Singleton::getInstance()->selcorf.h_map[x+y*vert_gis_x]=0;
            Singleton::getInstance()->h_mapaselcorft[x+y*vert_gis_x]=0;
            //printf("%.01f ",h_mapaalti[x+y*vert_gis_x]);
        }
        //printf("\n");
    }
    Singleton::getInstance()->quemado.is_gpu_utd=0;
    Singleton::getInstance()->selcorf.is_gpu_utd=0;
    print_debug("cargar_mapas_de_archivo() OK\n");

}

////////////////////////////////////////////////////////////////////////////////
//! Display callback
////////////////////////////////////////////////////////////////////////////////

void display()
{
	//sdkStopTimer(&timer);
    computeFPS();
    sdkStartTimer(&timer);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, Singleton::getInstance()->window_width, Singleton::getInstance()->window_height);
    glClearColor(0.0, 0.0f, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    if(Singleton::getInstance()->proyecto_cargado){
        if(Singleton::getInstance()->arrowstate[0]){paso_atras_simulador();}
        else if(Singleton::getInstance()->arrowstate[1]){paso_adelante_simulador();}

        /*if(automode)
        {
        	for(int cnt=0;cnt<PARAL;cnt++){
        		M[cnt].Evaluar_Individuo_FW_noh(d_individuo, 0, 1, 0,frame_cnt);
        		//M[cnt].Evaluar_Individuo_FW(d_individuo, 0, 1, 0);
        		//M[cnt].nuevomapa=0;
        	}

            recorrer_floats();
            regenerar_textura_final();
        }*/

        /*RENDER AL FRAMEBUFFER*/
        /*glActiveTexture(GL_TEXTURE0);
        //glBindTexture(GL_TEXTURE_2D, textureID);
        glBindTexture(GL_TEXTURE_2D, texture_flecha_norte_ID);
    	glBindFramebuffer(GL_FRAMEBUFFER, flechaFBO);
    	glViewport(0, 0, 256, 256);
    	glClearColor(0.0, 0.0f, 0.0, 0.0);
    	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);*/

    	/*glUseProgram(programID);//esto es si queremos imprimir un mapa igual al de verdad
        sendUniformsToOpenGL();
        glBindVertexArray(globalVAOID);
    	glDrawElements(GL_TRIANGLES,numindices,GL_UNSIGNED_INT,0);*/

        /*glUseProgram(programHUDID);
        sendHUDUniformsToOpenGL();
        glBindVertexArray(VAOHUDID);
        glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);*/





        /*RENDER DEL MAPA NORMAL*/
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->textureID);
    	glUseProgram(Singleton::getInstance()->programID);
        sendUniformsToOpenGL(Singleton::getInstance()->programID);
        glBindVertexArray(Singleton::getInstance()->globalVAOID);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->textureID);



        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_flecha_ID);

        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_colormap_ID);

        if(Singleton::getInstance()->drawwiremesh)
        {
    	   glDrawElements(GL_LINE_STRIP,Singleton::getInstance()->numindices,GL_UNSIGNED_INT,0);
        }
        else
        {
           glDrawElements(GL_TRIANGLES,Singleton::getInstance()->numindices,GL_UNSIGNED_INT,0);
        }
    	/*RENDER DE CURVAS DE NIVEL*/
        /*glClear(GL_DEPTH_BUFFER_BIT);//Si borro el buffer de profundidad las lineas se ven aunque esten atras de las montañas
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_flecha_norte_ID);
    	glUseProgram(Singleton::getInstance()->programLINESID);
        sendUniformsToOpenGL(Singleton::getInstance()->programLINESID);
        glBindVertexArray(Singleton::getInstance()->globalVAOID);
    	glDrawElements(GL_LINES,Singleton::getInstance()->numindices,GL_UNSIGNED_INT,0);*/



    	/*RENDER DEL GUI 2D DE LA FLECHA*/
    	/*glBindFramebuffer(GL_FRAMEBUFFER, 0);
    	glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, flechatexture);
    	glUseProgram(program2dID);
        //sendUniformsToOpenGL(Singleton::getInstance()->program2dID);
        glBindVertexArray(VAO2DID);
    	glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);*/




        glClear(GL_DEPTH_BUFFER_BIT);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_flecha_norte_ID);

        glUseProgram(Singleton::getInstance()->programHUDID);
        sendHUD2UniformsToOpenGL(Singleton::getInstance()->programHUDID);
        glBindVertexArray(Singleton::getInstance()->VAOHUDID);
        glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);
    }

    ImGui_DrawAll();

    if(Singleton::getInstance()->proyecto_cargado){
        Singleton::getInstance()->CORTAFUEGOS.actualizar_mapa(Singleton::getInstance()->selcorf.h_map,Singleton::getInstance()->h_mapaselcorft);
        Singleton::getInstance()->SIMUSCRIPT.evaluar_ingnitionpoints(Singleton::getInstance()->Simulador.paso_simulacion,0);
    }

    glutSwapBuffers();

    frame_cnt++;
}





/*
 * http://www.gnu.org/software/libtool/manual/libc/Elapsed-Time.html
 * Subtract the `struct timeval' values X and Y,
 * storing the result in RESULT.
 * Return 1 if the difference is negative, otherwise 0.
 */

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y) {
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait. tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}





int Imprimir_Datos_Placa_Grafica() {

	int card;
	cudaGetDevice(&card);
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, card);
	std::cout << "\nDevice Selected " << card << " " << deviceProp.name << "\n";

	return 0;
}



//PARA HACER:
/*
-HECHO Agregar ventana con lista de puntos de ignicion (poder activar o desactivarlos, borrar)
-HECHO Quiza utilizar un keyframe cada tanto para guardar las celdas quemadas en relacion al mapa
-HECHO Incluir FWI
-HECHO Colores peligrosidad del mapa segun FWI diario
-HECHO Pasar todos los mapas distintos (aspect, height, FWI) al sistema de capas con alpha, que permita subir, bajar, habilitar, etc cada capa, usando fondo negro.
-HECHO Pasar global y global_ext a un singleton.
-Pasar los nombres de peligrosidad y colores a un archivo cfg de configuracion
-HECHO Hacer que se pueda guardar el mapa de incendio de distintas formas:
	Por parches de quemado al paso actual;
	El numero de paso en que se quemo cada celda;

*/
