//#include "superinclude.h"
#include "Singleton.h"
#include "entrada.h"

#include "trigonometria.h"
#include "Simulador.h"

void regenerar_textura_final(void);
void left_btn_down(void);

void paso_adelante_simulador(void);
void paso_atras_simulador(void);
void volver_a_cero_simulador(void);


using namespace glm;
vec3 vector;
//extern int ancho;
extern int nuevomapa;
extern float* nuevomapaptr;

int pru=Singleton::getInstance()->ancho+5;

int ni,nj;

vec2 lnsta,lnend,lnprev;
int corfid;
bool linea_temp=0;


extern bool imguicontrol;

void reposition_north_arrow(void)
{
    float imgui_winwidth=400.0f;
    float fle_wide=200.0f;
    //float scale=2.0f*fle_wide/window_width;

    vec2 a1,a2,a3,a4;
    a1=vec2(Singleton::getInstance()->window_width-imgui_winwidth-fle_wide,0.0f);
    a2=vec2(Singleton::getInstance()->window_width-imgui_winwidth,0.0f);
    a3=vec2(Singleton::getInstance()->window_width-imgui_winwidth-fle_wide,fle_wide);
    a4=vec2(Singleton::getInstance()->window_width-imgui_winwidth,fle_wide);

    a1.x=(2.0f*a1.x-Singleton::getInstance()->window_width)/Singleton::getInstance()->window_width;
    a2.x=(2.0f*a2.x-Singleton::getInstance()->window_width)/Singleton::getInstance()->window_width;
    a3.x=(2.0f*a3.x-Singleton::getInstance()->window_width)/Singleton::getInstance()->window_width;
    a4.x=(2.0f*a4.x-Singleton::getInstance()->window_width)/Singleton::getInstance()->window_width;

    a1.y=(Singleton::getInstance()->window_height-2.0*a1.y)/Singleton::getInstance()->window_height;
    a2.y=(Singleton::getInstance()->window_height-2.0*a2.y)/Singleton::getInstance()->window_height;
    a3.y=(Singleton::getInstance()->window_height-2.0*a3.y)/Singleton::getInstance()->window_height;
    a4.y=(Singleton::getInstance()->window_height-2.0*a4.y)/Singleton::getInstance()->window_height;

    vec3 vpos[4]={
        vec3(a1,0.0),
        vec3(a2,0.0),
        vec3(a3,0.0),
        vec3(a4,0.0)
    };

    glBindVertexArray(Singleton::getInstance()->VAO2DID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->VBOP2DID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*4, vpos, GL_STATIC_DRAW);
}

//**********************************************************************************ENTRADA
void reshape(int width, int height)
{
	Singleton::getInstance()->window_width=width;
	Singleton::getInstance()->window_height=height;
	glViewport(0, 0, Singleton::getInstance()->window_width, Singleton::getInstance()->window_height);

    reposition_north_arrow();
}

void calc_camera_front(void)
{
    Singleton::getInstance()->MainCam.front.x=-cos(radians(Singleton::getInstance()->MainCam.rotation.x))*sin(radians(Singleton::getInstance()->MainCam.rotation.y));
    Singleton::getInstance()->MainCam.front.y=sin(radians(Singleton::getInstance()->MainCam.rotation.x));
    Singleton::getInstance()->MainCam.front.z=-cos(radians(Singleton::getInstance()->MainCam.rotation.x))*cos(radians(Singleton::getInstance()->MainCam.rotation.y));
    Singleton::getInstance()->MainCam.front=normalize(Singleton::getInstance()->MainCam.front);

    vec3 SideVector=vec3(cos(radians(Singleton::getInstance()->MainCam.rotation.y)),0,-sin(radians(Singleton::getInstance()->MainCam.rotation.y)));
    Singleton::getInstance()->MainCam.side=normalize(SideVector);
}

void mouse_click(int x,int y){
    Singleton::getInstance()->click_si=1;
    Singleton::getInstance()->click_x=x;
    Singleton::getInstance()->click_y=y;
}

void timerEvent(int value)
{
    if(!imguicontrol)
    {
        if(Singleton::getInstance()->keys['w'])
    	{
    		Singleton::getInstance()->MainCam.position=Singleton::getInstance()->MainCam.position+Singleton::getInstance()->MainCam.front*Singleton::getInstance()->camspd;
    	}
    	if(Singleton::getInstance()->keys['s'])
    	{
    		Singleton::getInstance()->MainCam.position=Singleton::getInstance()->MainCam.position-Singleton::getInstance()->MainCam.front*Singleton::getInstance()->camspd;
    	}
    	if(Singleton::getInstance()->keys['a'])
    	{
    		Singleton::getInstance()->MainCam.position=Singleton::getInstance()->MainCam.position-Singleton::getInstance()->MainCam.side*Singleton::getInstance()->camspd;
    	}
    	if(Singleton::getInstance()->keys['d'])
    	{
    		Singleton::getInstance()->MainCam.position=Singleton::getInstance()->MainCam.position+Singleton::getInstance()->MainCam.side*Singleton::getInstance()->camspd;
    	}
    }
    
    if (glutGetWindow())
    {
        glutPostRedisplay();
        glutTimerFunc(REFRESH_DELAY, timerEvent,0);
    }
}

////////////////////////////////////////////////////////////////////////////////
//! Keyboard events handler
////////////////////////////////////////////////////////////////////////////////
void ImGui_ImplGlfwGL3_CharCallback(unsigned int c);
void ImGui_ImplGlfwGL3_KeyCallback(int key, int action, int mods);

void keyboard_dn(unsigned char key, int x, int y)
{
    Singleton::getInstance()->keys[key]=true;
    if(!imguicontrol)
    {
        switch (key)
        {
        case 27:
            glutLeaveMainLoop();

            break;

        case 'l':
        case 'L':
            Singleton::getInstance()->drawwiremesh=!Singleton::getInstance()->drawwiremesh;
            
            break;


        case 'f':
        case 'F':
            //printf("main window toggle fullscreen\n");
            glutFullScreenToggle();
            Singleton::getInstance()->fullscreen=!Singleton::getInstance()->fullscreen;

            break;

        default:
            break;
        }
    }
    //printf("TECLA=%d\n",key);
    //if(key==8){
        ImGui_ImplGlfwGL3_KeyCallback(key,1,0);

    //}
    
    ImGui_ImplGlfwGL3_CharCallback(key);
}

void special_keyboard_dn(int key, int x, int y)
{
    if(!imguicontrol)
    {
        int modf=glutGetModifiers();
        switch (key)
        {
        case 100://flecha izq
            if(modf==GLUT_ACTIVE_CTRL){paso_atras_simulador();}
            else{Singleton::getInstance()->arrowstate[0]=1;}
            //paso_atras_simulador();
            break;
        case 102://flecha der
            //paso_adelante_simulador();
            if(modf==GLUT_ACTIVE_CTRL){paso_adelante_simulador();}
            else{Singleton::getInstance()->arrowstate[1]=1;}
            break;
        default:
            break;
        }
    }

    //ImGui_ImplGlfwGL3_KeyCallback(key,1,0);
    //printf("STECLA=%d\n",key);
}

void special_keyboard_up(int key, int x, int y)
{
    if(!imguicontrol)
    {
        switch (key)
        {
        case 100://flecha izq
            Singleton::getInstance()->arrowstate[0]=0;
            //paso_atras_simulador();
            break;
        case 102://flecha der
            //paso_adelante_simulador();
            Singleton::getInstance()->arrowstate[1]=0;
            break;
        default:
            break;
        }
    }

    //ImGui_ImplGlfwGL3_KeyCallback(key,1,0);
    //printf("STECLA=%d\n",key);
}

void keyboard_up(unsigned char key, int x, int y)
{
    //printf("Keyrelease:%d x=%d y=%d\n",key,x,y);
    ImGui_ImplGlfwGL3_KeyCallback(key,0,0);
    Singleton::getInstance()->keys[key]=0;
}

////////////////////////////////////////////////////////////////////////////////
//! Mouse event handlers
////////////////////////////////////////////////////////////////////////////////
void mouse(int button, int state, int x, int y)
{

    // Wheel reports as button 3(scroll up) and button 4(scroll down)
    if ((button == 3) || (button == 4)) // It's a wheel event
    {
       // Each wheel event reports like a button click, GLUT_DOWN then GLUT_UP
       if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events

       if(button==3){
            Singleton::getInstance()->g_MouseWheel+=0.2f;

       }
       
       if(button==4){
            Singleton::getInstance()->g_MouseWheel-=0.2f;

       }

       //printf("Scroll %s At %d %d\n", (button == 3) ? "Up" : "Down", x, y);
    }else{  // normal button event
       //printf("Button %s At %d %d\n", (state == GLUT_DOWN) ? "Down" : "Up", x, y);
    }

    if (state == GLUT_DOWN)
    {
        Singleton::getInstance()->mouse_buttons |= 1<<button;
        //printf("x=%d y=%d\n",x,y);
    }
    else if (state == GLUT_UP)
    {
        Singleton::getInstance()->mouse_buttons = 0;
    }

    

    if(Singleton::getInstance()->mouse_buttons&4){
        mouse_click(x,y);

    }
	
    Singleton::getInstance()->mouse_x=x;
    Singleton::getInstance()->mouse_y=y;

    Singleton::getInstance()->mouse_old_x = x;
    Singleton::getInstance()->mouse_old_y = y;

    if(!Singleton::getInstance()->proyecto_cargado)return;

    intersection_calc(&ni,&nj);//devuelve la celda que apunta el mouse

    if(Singleton::getInstance()->mouse_buttons&1 && button==0)
    {
        left_btn_down();

    }
}

void motion(int x, int y)
{
    float dx, dy;
    dx = (float)(x - Singleton::getInstance()->mouse_old_x);
    dy = (float)(y - Singleton::getInstance()->mouse_old_y);

    if (Singleton::getInstance()->mouse_buttons & 1)//boton izquierdo
    {
		

		//printf("x=%.02f y=%.02f z=%.02f\n",MainCam.side.x,MainCam.side.y,MainCam.side.z);
		//printf("rx=%.02f ry=%.02f\n",MainCam.rotation.x,MainCam.rotation.y);
    }
    else if (Singleton::getInstance()->mouse_buttons & 4)//boton derecho
    {
		//posz=(float)-x*5.0f/window_width;
		//printf("posz=%f\n",posz);
        Singleton::getInstance()->MainCam.rotation.x-=dy * 0.2f;
        Singleton::getInstance()->MainCam.rotation.y-=dx * 0.2f;

        if(Singleton::getInstance()->MainCam.rotation.x>90.0){Singleton::getInstance()->MainCam.rotation.x=90.0;}
        if(Singleton::getInstance()->MainCam.rotation.x<-90.0){Singleton::getInstance()->MainCam.rotation.x=-90.0;}
        if(Singleton::getInstance()->MainCam.rotation.y>=360.0){Singleton::getInstance()->MainCam.rotation.y-=360.0;}
        if(Singleton::getInstance()->MainCam.rotation.y<=-360.0){Singleton::getInstance()->MainCam.rotation.y+=360.0;}

        calc_camera_front();
    }
	else if (Singleton::getInstance()->mouse_buttons & 2)//apretar ruedita
    {
        //translate_x += dx * 0.01f;
		//translate_y -= dy * 0.01f;
    }
    Singleton::getInstance()->mouse_x=x;
    Singleton::getInstance()->mouse_y=y;
//printf("btn=%d\n",mouse_buttons);

    Singleton::getInstance()->mouse_old_x = x;
    Singleton::getInstance()->mouse_old_y = y;
    //printf("M\n");

    if(!Singleton::getInstance()->proyecto_cargado)return;

    intersection_calc(&ni,&nj);//devuelve la celda que apunta el mouse
    if(ni!=-1 && Singleton::getInstance()->btnS_poner_cortafuego_multilinea && Singleton::getInstance()->draw_line_state==1)
    {
        if(ni!=(int)lnprev.x || nj!=(int)lnprev.y)
        {
            //int id=CORTAFUEGOS.nueva_entrada(0);
            lnsta.x=ni;
            lnsta.y=nj;
            int id=corfid;
            Singleton::getInstance()->CORTAFUEGOS.ENTRADA[id].add_new((int)lnprev.x,(int)lnprev.y,(int)lnsta.x,(int)lnsta.y);
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
            lnprev=lnsta;
        }
    }
    if(ni!=-1 && Singleton::getInstance()->btnS_poner_cortafuego && Singleton::getInstance()->draw_line_state==1)
    {
        if(ni!=(int)lnsta.x || nj!=(int)lnsta.y)
        {
            lnend.x=ni;
            lnend.y=nj;
            linea_temp=1;
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio_t=1;
        }
    }
}

extern int paso_simulacion;

void recorrer_floats(void);

void SetPixelForAll(int x, int y){

    Singleton::getInstance()->Simulador.SetPixelForAll(x,y);
}

void left_btn_down(void)
{

    if(ni!=-1 && Singleton::getInstance()->mouse_buttons&1)
    {
        static vec2 linesta,lineend;
        if(Singleton::getInstance()->btnS_poner_cortafuego)
        {
            if(Singleton::getInstance()->draw_line_state==0)
            {
                linesta.x=ni;
                linesta.y=nj;
                lnsta.x=ni;
                lnsta.y=nj;
                Singleton::getInstance()->draw_line_state=1;
            }
            else if(Singleton::getInstance()->draw_line_state==1)
            {
                lineend.x=ni;
                lineend.y=nj;
                Singleton::getInstance()->btnS_poner_cortafuego=0;
                Singleton::getInstance()->draw_line_state=0;
                int id=Singleton::getInstance()->CORTAFUEGOS.nueva_entrada(1);
                Singleton::getInstance()->CORTAFUEGOS.ENTRADA[id].add_new((int)linesta.x,(int)linesta.y,(int)lineend.x,(int)lineend.y);
                Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
                linea_temp=0;
            }  
        }
        else if(Singleton::getInstance()->btnS_poner_cortafuego_multilinea)
        {
            if(Singleton::getInstance()->draw_line_state==0)
            {
                linesta.x=ni;
                linesta.y=nj;
                lnsta.x=ni;
                lnsta.y=nj;
                Singleton::getInstance()->draw_line_state=1;
                lnprev=lnsta;
                corfid=Singleton::getInstance()->CORTAFUEGOS.nueva_entrada(0);
            }
            else if(Singleton::getInstance()->draw_line_state==1)
            {
                lineend.x=ni;
                lineend.y=nj;
                Singleton::getInstance()->btnS_poner_cortafuego_multilinea=0;
                Singleton::getInstance()->draw_line_state=0;
                //int id=CORTAFUEGOS.nueva_entrada(0);
                //CORTAFUEGOS.ENTRADA[corfid].add_new((int)linesta.x,(int)linesta.y,(int)lineend.x,(int)lineend.y);//contorno cerrado
                Singleton::getInstance()->CORTAFUEGOS.ENTRADA[corfid].add_new((int)lnprev.x,(int)lnprev.y,(int)lineend.x,(int)lineend.y);//contorno abierto
                Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
                linea_temp=0;
            } 
        }
        else if(Singleton::getInstance()->btnS_IGNI)//Punto de ignicion manual
        {

            /*for(int cnt=0;cnt<PARAL;cnt++){ 
                M[cnt].Set_Pixel(ni+nj*vert_gis_x);
            }

            recorrer_floats();
            regenerar_textura_final();*/

            Singleton::getInstance()->SIMUSCRIPT.add_ignitionpoint(Singleton::getInstance()->Simulador.paso_simulacion,ni,nj,1);

            Singleton::getInstance()->btnS_IGNI=0;
            Singleton::getInstance()->SIMUSCRIPT.evaluar_ingnitionpoints(Singleton::getInstance()->Simulador.paso_simulacion,1);

            /*XIGNI=ni;
            YIGNI=nj;
            
            volver_a_cero_simulador();*/
        }
        else if(Singleton::getInstance()->btnS_IGNI_redef>-1)
        {
            Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[Singleton::getInstance()->btnS_IGNI_redef].x=ni;
            Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[Singleton::getInstance()->btnS_IGNI_redef].y=nj;
            Singleton::getInstance()->btnS_IGNI_redef=-1;
            Singleton::getInstance()->SIMUSCRIPT.evaluar_ingnitionpoints(Singleton::getInstance()->Simulador.paso_simulacion,1);
        }
    }
}