#pragma once

class Historico_Entrada{
public:
	int paso, cant_celdas_tot,cant_celdas_dif;
	int * celdas_dif;
	float * celdas_dif_V;
};

class Historicos{
public:
	Historico_Entrada * HEntry[10000];
	int cols,rows;
	int act_HEntry;
	float * h_actmap,*d_actmap;
	float *h_difmap,*d_difmap;
	float *h_initialmap;

	int last_celda_dif;
	int derivada_celda_dif;

	int numfloats;

	void init(int cols,int rows,int numfloats);
	void add_pixel(int x, int y);
	void save_new_map(float*d_map,int paso);
	void step_BW(int&paso,float*h_mapa_aux,float*d_tn,float*d_t);
	~Historicos();
	void destruir(void);
	int* count_ones_store_array(float*h_map,int max,int&numones,int&ovf,float**vecV,int estim);
	void load_initial_map(float * textura);
};