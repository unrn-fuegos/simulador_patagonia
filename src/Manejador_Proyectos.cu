#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "glm/glm.hpp"
#include "Manejador_Proyectos.h"
#include "Singleton.h"

int preproceso(char linea[],char * post)
{
	int i=0;
	int st=-1,end=-1;
	i=strlen(linea);
	while(true)
	{
		end=i;
		if(linea[i]!='\r' && linea[i]!='\n' && linea[i]!=' ' && linea[i]!='#' && linea[i]!='\t')
		{
			end--;
			break;
		}
		if(i<=0){end--;break;}
		i--;
	}

	if(i<=0){return 0;}

	i=0;
	while(true)
	{
		st=i;
		if(linea[i]!='\r' && linea[i]!='\n' && linea[i]!=' ' && linea[i]!='\t')
		{
			break;
		}
		if(i>=end){break;}
		i++;
	}

	if(st<end)
	{

	}
	else
	{
		return 0;	
	}

	i = st;
	int comment=0;
	while(true)
	{
		if(linea[i]=='#' && !comment){comment=1;end=i;break;}
		i++;
		if(i>=end){break;}
	}

	if(st==end){return 0;}

	memcpy(post,&linea[st],end-st);
	post[end-st]=0;
	return 1;
}

using namespace glm;

void Proyecto::mover_paleta_a_grafico(void){
	vec3* paleta_vegetacion=&Singleton::getInstance()->paleta_vegetacion[0];
	for(int i=0;i<100;i++)
	{
		if(paleta_conversion_vege[i]==99)continue;
		paleta_vegetacion[i].r=(float)vegeR[i]/255.0f;
    	paleta_vegetacion[i].g=(float)vegeG[i]/255.0f;
    	paleta_vegetacion[i].b=(float)vegeB[i]/255.0f;
	}
}

void Proyecto::mover_grafico_a_paleta(void){
	vec3* paleta_vegetacion=&Singleton::getInstance()->paleta_vegetacion[0];
	for(int i=0;i<100;i++)
	{
		if(paleta_conversion_vege[i]==99)continue;
		vegeR[i]=(int)(paleta_vegetacion[i].r*255.0f);
		vegeG[i]=(int)(paleta_vegetacion[i].g*255.0f);
		vegeB[i]=(int)(paleta_vegetacion[i].b*255.0f);
	}
}

int Proyecto::Cargar_paleta(void)
{
	//int sizeStr;
	char *pala, *clave,*valor;
	//char aux[20], *nada;
	//char proyectoOriginal[50];

	char linea[100],lineapostP[50];

	char*path=this->PALETA_F;
	int rasteridx,simuidx;

	for(int i=0;i<100;i++){
		paleta_conversion_vege[i]=99;
		fuelnames[i]=NULL;
	}

	
	FILE *file_pointer = fopen(path, "r");
	if ( file_pointer == NULL ) {
	       print_debug("File could not be opened. *%s*\n",path);
	       exit(-1);  }
	else
    {
     	  //printf("The words available in the dictionary are :\n");
		  
          while(fgets(linea, 100, file_pointer)!=NULL)
          {
          	if(!preproceso(linea,lineapostP))continue;
          	//printf("-%s-\n",linea);
          	//printf("-%s-\n",lineapostP);
            pala = lineapostP;

            //continue;

        	clave = strtok(pala, "=");
        	valor = strtok(NULL, "\0\r\n");
        	//sizeStr = strlen(valor);

        	//printf("-%s-%s-\n",clave,valor);

           	switch (clave[0]) {
				case 'R':  //si tengo dos que comienzan igual, hago un if
					// en el config REAL_FIRE_MAP y ROWS comienzan con R
					if ((clave[1]>='0') && (clave[1]<='9'))
					{
						
						simuidx = (int) atoi(valor);
						if (clave[2] >= '0' && clave[2] <= '9')
						{
							rasteridx=clave[2]-'0'+(clave[1]-'0')*10;
						}
						else
						{
							rasteridx=(clave[1]-'0');
						}

						paleta_conversion_vege[rasteridx]=simuidx;
						//printf("RS -%d- %d=%d\n",clave[1]-'0',rasteridx,simuidx);	


					}else{print_debug("SALE POR EL ELSE\n");}
					
					break;

				case 'I':  //si tengo dos que comienzan igual, hago un if
					// en el config REAL_FIRE_MAP y ROWS comienzan con R
					if (clave[1] >= '0' && clave[1] <= '9'){
						//simuidx = (int) atoi(valor);
						if (clave[2] >= '0' && clave[2] <= '9')
						{
							rasteridx=clave[2]-'0'+(clave[1]-'0')*10;
						}
						else
						{
							rasteridx=(clave[1]-'0');
						}

						fuelnames[rasteridx]=(char*)malloc(48*sizeof(int));

						char * pch = strtok(valor,"\"");

						strcpy(fuelnames[rasteridx],pch);


						pch = strtok (NULL, ",");
						sscanf(pch,"%d",&vegeR[rasteridx]);

						pch = strtok (NULL, ",");
						sscanf(pch,"%d",&vegeG[rasteridx]);

						pch = strtok (NULL, ",\0\r\n");
						sscanf(pch,"%d",&vegeB[rasteridx]);
						

						//printf("ESTE-%s-%d %d %d\n",fuelnames[rasteridx],vegeR[rasteridx],vegeG[rasteridx],vegeB[rasteridx]);

						//memcpy()

					}
						
					break;
				default: 
					print_debug("Clave no encontrada: %s=%s \n", clave,valor);
					break;

			} //switch case
          }
    

 	   fclose(file_pointer);
	}

	//mover_paleta_a_grafico();
	
	return 0;
}

void char_repl(char * string)
{
    int i=0;
    while(string[i]!=0)
    {
        if(string[i]=='\r'){
            string[i]=0;
            return;
        }
        i++;
    }
}

// carga los datos del proyecto pasado por parametro
int Proyecto::Cargar_config(char* proyecto)
{
 
	
	//FILE *file_pointer;
	//int /*input_counter = 0,*/ sizeStr;
	//int array_size = 50;
	char *pala, *clave,*valor/*, filas[10], cols[10]*/;
	//char dictionary_array[array_size][20];
	char /*aux[20], */*nada;
	char proyectoOriginal[50];

	char linea[100],lineapostP[50];
	start_fullscreen=0;

	strncpy(proyectoOriginal, proyecto, strlen(proyecto)+1);
	strncpy(this->proyecto, proyecto, strlen(proyecto)+1);

	char copyproj[256];
	memcpy(copyproj,proyecto,sizeof(char)*(1+strlen(proyecto)));

	char *path = strcat(copyproj,"config.txt");

	
	FILE *file_pointer = fopen(path, "r");
	if ( file_pointer == NULL ) {
	       print_debug("File could not be opened. %s\n",path);
	       exit(-1);  }
	else
    {
     	  //printf("The words available in the dictionary are :\n");
		  
          while(fgets(linea, 100, file_pointer)!=NULL)
          {
          	if(!preproceso(linea,lineapostP))continue;
          	//printf("-%s-\n",linea);
          	//printf("-%s-\n",lineapostP);
            pala = lineapostP;

            //continue;

        	clave = strtok(pala, "=");
        	valor = strtok(NULL, " \r");
        	//sizeStr = strlen(valor);

        	//printf("-%s-%s-\n",clave,valor);

    		// esto lo tengo que hacer porque el strcat modifica el primer valor
    		// con lo que en cada entrada al switch case estoy modificando 
    		// la variable proyectoOriginal
    	   	nada = strtok(proyectoOriginal, "/");
	        strcat(nada,"/");
	        strcpy(proyectoOriginal, nada);
	        // al proyecto le uno el valor 
	        strcat(proyectoOriginal,valor);

	        char_repl(proyectoOriginal);

           	switch (clave[0]) {
				case 'R':  //si tengo dos que comienzan igual, hago un if
					// en el config REAL_FIRE_MAP y ROWS comienzan con R
					if (clave[1] == 'O')
						this->ROWS = (int) atoi(valor);
					else
						strcpy(REAL_FIRE_MAP_P, proyectoOriginal);
					break;
				case 'C':  						
					this->COLS = (int) atoi(valor);
					break;
				case 'X':  						
					XIGNI = (int) atoi(valor);
					break;
				case 'Y':  						
					YIGNI = (int) atoi(valor);
					break;
				case 'W':
					strcpy(WIND_MAP_P, proyectoOriginal);
					break;
				case 'I':
					strcpy(WINDI_MAP_P, proyectoOriginal);
					break;
				case 'S':
					strcpy(SLOPE_MAP_P, proyectoOriginal);
					break;
				case 'E':
					strcpy(ELEV_MAP_P, proyectoOriginal);
					break;
				case 'V':
					if (clave[1] == 'E')
						strcpy(VEGE_MAP_P, proyectoOriginal);
					else 
						strcpy(VEGE_PAL_P, valor);	//La paleta esta solo afecta a la simulacion, la parte 
													//visual sigue viendose en los mismos colores.			
					break;
				case 'F':
					strcpy(FOREST_MAP_P, proyectoOriginal);
					break;
				case 'P':
					strcpy(PALETA_F, proyectoOriginal);
					break;
				case 'c':
					celdas_prom = (int) atoi(valor);
					break;
				case 'f':
					start_fullscreen = (int) atoi(valor);
					break;
				case 'A':
					// en el config ASPECT_MAP y ALTI_MAP comienzan con A
					if (clave[1] == 'S')
						strcpy(ASPECT_MAP_P, proyectoOriginal);
					else 
						strcpy(ALTI_MAP_P, proyectoOriginal);					
					break;
				default: 
					print_debug("Clave no encontrada: %s=%s \n", clave,valor);
					break;

			} //switch case
          }
    

 	   fclose(file_pointer);
	}
	
	return 0;
}
