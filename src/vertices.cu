//#include "superinclude.h"
#include "stdio.h"
#include "Singleton.h"
#include "vertices.h"

using namespace glm;

int leer_matriz_de_archivo(char *nombre, float *mat, int fi, int co);
//int inicializar_matriz(float *mat, int fi, int co);
int armar_histograma(float *mat,int fi, int co, int *histo, int nval);
int mostrar_histograma(int *histo, int nval);
int guardar_histograma(char *nombre, int *histo, int nval);
int calcular_maximo_valor(float *mat, int fi, int co);
int escribir_matriz_de_archivo(char *nombre, float *mat, int fi, int co);
void histograma_viento(void);

void GenerarVertices(void)
{
    int x,z,ptr=0;
    int cnt=0;

    Singleton::getInstance()->altitud_min=50000.0f;
    Singleton::getInstance()->altitud_max=-50000.0f;
    float* h_mapaalti=Singleton::getInstance()->h_mapaalti;
    float* h_mapaaltir=Singleton::getInstance()->h_mapaaltir;
    float* h_mapawindANGr=Singleton::getInstance()->h_mapawindANGr;
    float* h_mapawindINTr=Singleton::getInstance()->h_mapawindINTr;
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    int vert_x=Singleton::getInstance()->vert_x;
    int vert_y=Singleton::getInstance()->vert_y;
    for(int i=0;i<(vert_gis_x-1)*(vert_gis_y-1);i++)
    {

        if(h_mapaalti[i]<Singleton::getInstance()->altitud_min)
        {
            Singleton::getInstance()->altitud_min=h_mapaalti[i];
        }
        if(h_mapaalti[i]>Singleton::getInstance()->altitud_max)
        {
            Singleton::getInstance()->altitud_max=h_mapaalti[i];
        }
    }
    tex_x=vert_gis_x;
    tex_y=vert_gis_y;
    //printf("/ %dx%d /",tex_x,tex_y);
    //tex_x=800;
    //tex_y=800;
    print_debug("Altitud minima %f\n",Singleton::getInstance()->altitud_min);
    print_debug("Altitud maxima %f\n",Singleton::getInstance()->altitud_max);
    float dpx,dpz,dx=0.0125f*(float)Singleton::getInstance()->celdas_prom,dz=0.0125f*(float)Singleton::getInstance()->celdas_prom;
    vec3 vp;

    vec3* vpinternal=(vec3*)malloc(sizeof(vec3)*Singleton::getInstance()->numvertices/4);
    vec3* vuvinternal=(vec3*)malloc(sizeof(vec3)*Singleton::getInstance()->numvertices/4);
    vec3* vninternal=(vec3*)malloc(sizeof(vec3)*Singleton::getInstance()->numvertices/4);

    int celdasx=(Singleton::getInstance()->vert_x-1)*Singleton::getInstance()->celdas_prom;
    int celdasy=(Singleton::getInstance()->vert_y-1)*Singleton::getInstance()->celdas_prom;



    for(z=0;z<vert_y;z++)
    {
        for(x=0;x<vert_x;x++)
        {

            vp.x=dx*x;
            vp.y=(h_mapaaltir[x+z*vert_x]-Singleton::getInstance()->altitud_min)*0.0125f/30.0f;
            vp.z=dz*z;

            vpinternal[x+z*vert_x]=vp;
            vuvinternal[x+z*vert_x].x=(float)x*Singleton::getInstance()->celdas_prom/(vert_gis_x);
            vuvinternal[x+z*vert_x].y=(float)z*Singleton::getInstance()->celdas_prom/(vert_gis_y);//+(float)1.0f/(float)(vert_gis_y%Singleton::getInstance()->celdas_prom);
            //vuvinternal[x+z*vert_x].z=(float)vert_x*z+x;

            /*verticesUV[x+z*vert_x].x=(float)x/(vert_x-1);
            verticesUV[x+z*vert_x].y=(float)z/(vert_y-1);
            verticesUV[x+z*vert_x].z=(float)vert_x*z+x;*/
            //printf("%.03f ",verticesUV[x+z*vert_x].x);
            cnt++;
        }

    }

    //vec3* tempNormals=(vec3*)malloc(vert_gis_y*vert_gis_x*sizeof(vec3));

    for(z=0;z<vert_y;z++)
    {
        for(x=0;x<vert_x;x++)
        {
            if(z<=0 || x<=0)
            {
                vninternal[x+z*vert_x]=vec3(0,1.0f,0);
            }
            else if(z>=(vert_y-1))
            {
                vninternal[x+z*vert_x]=vninternal[x+(z-1)*vert_x];
            }
            else if (x>=(vert_x-1))
            {
                vninternal[x+z*vert_x]=vninternal[(x-1)+z*vert_x];
            }else{
                dpx=-(vpinternal[x+1+z*vert_x].y-vpinternal[x-1+z*vert_x].y)/(2*dx);
                dpz=-(vpinternal[x+(z+1)*vert_x].y-vpinternal[x+(z-1)*vert_x].y)/(2*dz);
                //px2=-(verticesPosicion[x+2+z*801].y-verticesPosicion[x-2+z*801].y)/(4*dx);
                //dpz2=-(verticesPosicion[x+(z+2)*801].y-verticesPosicion[x+(z-2)*801].y)/(4*dx);
                vninternal[x+z*vert_x]=normalize(vec3(dpx,1.0f,dpz));
                //cnt++;
            }
        }
    }

    float flecha_scale=0.9f;

    vec2 pos[4];
    pos[0]=vec2(0,0)-vec2(0.5f,0.5f);
    pos[1]=vec2(0,1)-vec2(0.5f,0.5f);
    pos[2]=vec2(1,0)-vec2(0.5f,0.5f);
    pos[3]=vec2(1,1)-vec2(0.5f,0.5f);
    /*pos[0]*=flecha_scale;
    pos[1]*=flecha_scale;
    pos[2]*=flecha_scale;
    pos[3]*=flecha_scale;*/

    for(z=0;z<vert_y-1;z=z+1)
    {
        for(x=0;x<vert_x-1;x=x+1)
        {
            Singleton::getInstance()->verticesPosicion[4*(x+z*vert_x)+0]=vpinternal[(x+z*vert_x)];
            Singleton::getInstance()->verticesPosicion[4*(x+z*vert_x)+1]=vpinternal[(x+(z+1)*vert_x)];
            Singleton::getInstance()->verticesPosicion[4*(x+z*vert_x)+2]=vpinternal[(x+1+z*vert_x)];
            Singleton::getInstance()->verticesPosicion[4*(x+z*vert_x)+3]=vpinternal[(x+1+(z+1)*vert_x)];

            Singleton::getInstance()->verticesUV[4*(x+z*vert_x)+0]=vuvinternal[(x+z*vert_x)];
            Singleton::getInstance()->verticesUV[4*(x+z*vert_x)+1]=vuvinternal[(x+(z+1)*vert_x)];
            Singleton::getInstance()->verticesUV[4*(x+z*vert_x)+2]=vuvinternal[(x+1+z*vert_x)];
            Singleton::getInstance()->verticesUV[4*(x+z*vert_x)+3]=vuvinternal[(x+1+(z+1)*vert_x)];

            Singleton::getInstance()->verticesNormals[4*(x+z*vert_x)+0]=vninternal[(x+z*vert_x)];
            Singleton::getInstance()->verticesNormals[4*(x+z*vert_x)+1]=vninternal[(x+(z+1)*vert_x)];
            Singleton::getInstance()->verticesNormals[4*(x+z*vert_x)+2]=vninternal[(x+1+z*vert_x)];
            Singleton::getInstance()->verticesNormals[4*(x+z*vert_x)+3]=vninternal[(x+1+(z+1)*vert_x)];

            /*verticesUVF[4*(x+z*vert_x)+0]=vec3(0,0,0);//aca hay que cargar las cordenadas uv rotadas para las flechas del viento
            verticesUVF[4*(x+z*vert_x)+1]=vec3(0,1,0);
            verticesUVF[4*(x+z*vert_x)+2]=vec3(1,0,0);
            verticesUVF[4*(x+z*vert_x)+3]=vec3(1,1,0);*/

            
            //static float wind_ang_aux=0.0f;
            float wind_angle=h_mapawindANGr[x+z*vert_x];
            //float wind_angle=0.0f;
            float wind_angle_rad=radians(-wind_angle);//CW, el 0grados es el norte. Se indica el angulo desde donde viene el viento.

            //wind_ang_aux+=1.0f;
            mat2 rm;
            vec2 fp[4];
            rm=mat2(cos(wind_angle_rad),-sin(wind_angle_rad),sin(wind_angle_rad),cos(wind_angle_rad));
            fp[0]=pos[0]*rm;
            fp[1]=pos[1]*rm;
            fp[2]=pos[2]*rm;
            fp[3]=pos[3]*rm;

            /*fp[0]+=vec2(0.5f*flecha_scale,0.5f*flecha_scale);
            fp[1]+=vec2(0.5f*flecha_scale,0.5f*flecha_scale);
            fp[2]+=vec2(0.5f*flecha_scale,0.5f*flecha_scale);
            fp[3]+=vec2(0.5f*flecha_scale,0.5f*flecha_scale);*/

            fp[0]+=vec2(0.5f,0.5f);
            fp[1]+=vec2(0.5f,0.5f);
            fp[2]+=vec2(0.5f,0.5f);
            fp[3]+=vec2(0.5f,0.5f);

            fp[0]=fp[0]*flecha_scale;
            fp[1]=fp[1]*flecha_scale;
            fp[2]=fp[2]*flecha_scale;
            fp[3]=fp[3]*flecha_scale;
            //windi_min=15.0f;
            //windi_max=25.0f;

            float windi=(h_mapawindINTr[x+z*vert_x]-Singleton::getInstance()->windi_min)/(Singleton::getInstance()->windi_max-Singleton::getInstance()->windi_min);

            //windi=0.9f;

            Singleton::getInstance()->verticesUVF[4*(x+z*vert_x)+0]=vec3(fp[0],1.0f-windi);//aca hay que cargar las cordenadas uv rotadas para las flechas del viento
            Singleton::getInstance()->verticesUVF[4*(x+z*vert_x)+1]=vec3(fp[1],1.0f-windi);
            Singleton::getInstance()->verticesUVF[4*(x+z*vert_x)+2]=vec3(fp[2],1.0f-windi);
            Singleton::getInstance()->verticesUVF[4*(x+z*vert_x)+3]=vec3(fp[3],1.0f-windi);


            //y setear el area de trabajo en maximo 45x45px, para que al rotar la flecha nunca caiga fuera de la textura
            /*
            matriz de rotacion:(tita grados en sentido antihorario)
            cos     -sin
            sin     cos
            */

            /************************************************************************************************************/
            //LA ROTACION YA FUNCIONA, SOLO FALTA CARGAR EL BMP DE LA FLECHA Y TOMAR LOS ANGULOS DEL MAPA DE VIENTO
            /************************************************************************************************************/


        }
    }



    for(z=0;z<vert_y-1;z=z+1)
    {
        for(x=0;x<vert_x-1;x=x+1)
        {
            /*verticesIndex[ptr]=z*vert_x+x+0;ptr++;
            verticesIndex[ptr]=z*vert_x+x+vert_x;ptr++;
            verticesIndex[ptr]=z*vert_x+x+1;ptr++;
            verticesIndex[ptr]=z*vert_x+x+vert_x;ptr++;
            verticesIndex[ptr]=z*vert_x+x+1+vert_x;ptr++;
            verticesIndex[ptr]=z*vert_x+x+1;ptr++;*/
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+0;ptr++;
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+1;ptr++;
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+2;ptr++;
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+1;ptr++;
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+3;ptr++;
            Singleton::getInstance()->verticesIndex[ptr]=4*(z*vert_x+x)+2;ptr++;
        }
    }
    print_debug("Generados %d vertices y %d indices\n",cnt,ptr);

}

float promedio_vecinas(float* map,int x, int y,float nd){
    int div=0;
    float suma=0;
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    for(int j=y;j<y+Singleton::getInstance()->celdas_prom;j++)
    {
        for(int i=x;i<x+Singleton::getInstance()->celdas_prom;i++)
        {
            if(i<0 || j<0 || i>=vert_gis_x || j>=vert_gis_y){continue;}
            if(map[i+j*vert_gis_x]==nd){continue;}
            div++;
            suma+=map[i+j*vert_gis_x];
        }

    }
    if(div==0){return 0.0f;}
    return (float)suma/div;
}

glm::vec2 promedio_vecinas_vec2(vec2* map,int x, int y,vec2 nd){
    int div=0;
    vec2 suma=vec2(0.0f,0.0f);
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    for(int j=y;j<y+Singleton::getInstance()->celdas_prom;j++)
    {
        for(int i=x;i<x+Singleton::getInstance()->celdas_prom;i++)
        {
            if(i<0 || j<0 || i>=vert_gis_x || j>=vert_gis_y){continue;}
            if(map[i+j*vert_gis_x]==nd){continue;}
            div++;
            suma=suma+map[i+j*vert_gis_x];
        }

    }
    if(div==0){div=1;}
    return (vec2)suma*((float)1.0f/div);
}

float* promediar_mapa(float* map,float nd)
{
    float* d;
    int vert_x=Singleton::getInstance()->vert_x;
    int vert_y=Singleton::getInstance()->vert_y;
    d=(float*)malloc(vert_x*vert_y*sizeof(float));
    int y;
    for(y=0;y<vert_y-1;y++)
    {
        int x;
        for(x=0;x<vert_x-1;x++)
        {
            d[x+y*vert_x]=promedio_vecinas(map,x*Singleton::getInstance()->celdas_prom,y*Singleton::getInstance()->celdas_prom,nd);
        }
        x=vert_x-1;
        d[x+y*vert_x]=d[(x-1)+y*vert_x];
    }

    for(int x=0;x<vert_x;x++)
    {
        y=vert_y-1;
        d[x+y*vert_x]=d[x+(y-1)*vert_x];
    }

    print_debug("promediar_mapa() OK \n");


    return d;
}

void promediar_mapa_polar(float** dr,float** da,float* o_r,float* o_a,float ndr,float nda)
{
    float* drt,*dat;
    vec2 * temp_rect,*prom_rect;
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    int vert_x=Singleton::getInstance()->vert_x;
    int vert_y=Singleton::getInstance()->vert_y;
    print_debug("LL0.4\n");
    temp_rect=(vec2*)malloc(vert_gis_x*vert_gis_y*sizeof(vec2));
    prom_rect=(vec2*)malloc(vert_x*vert_y*sizeof(vec2));
    drt=(float*)malloc(vert_x*vert_y*sizeof(float));
    dat=(float*)malloc(vert_x*vert_y*sizeof(float));
    int el;
    print_debug("LL0.5\n");
    for(int y=0;y<vert_gis_y;y++)
    {
        for(int x=0;x<vert_gis_x;x++)
        {
            el=x+y*vert_gis_x;
            temp_rect[el].x=o_r[el]*cos(radians(o_a[el]));
            temp_rect[el].y=o_r[el]*sin(radians(o_a[el]));
            if(o_r[el]==ndr || o_a[el]==nda)temp_rect[el]=vec2(0.0f,0.0f);
        }
    }

    print_debug("LL1\n");

    for(int y=0;y<vert_y-1;y++)
    {
        for(int x=0;x<vert_x-1;x++)
        {
            el=x+y*vert_x;
            prom_rect[el]=promedio_vecinas_vec2(temp_rect,x*Singleton::getInstance()->celdas_prom,y*Singleton::getInstance()->celdas_prom,vec2(0.0f,0.0f));
            drt[el]=length(prom_rect[el]);
            dat[el]=(float)180.0f*atan2(prom_rect[el].y,prom_rect[el].x)/3.141592f;
        }
    }

    print_debug("LL2\n");

    *dr=drt;
    *da=dat;

    print_debug("LL3\n");

    free(temp_rect);
    print_debug("LL4\n");
    free(prom_rect);

    print_debug("promediar_mapa_polar() OK \n");
}

void generar_textura(float* map,unsigned char** texdata,vec3 * paleta)
{
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    if((*texdata)==NULL){(*texdata)=(unsigned char*)realloc(*texdata,sizeof(unsigned char)*tex_x*tex_y*4);}
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            vec3 aux=paleta[(int)map[x+y*tex_x]];
            //if(x>200){aux=vec3(0,1,0);}
            texdata[0][(x+y*tex_x)*4+0]=(unsigned char)(aux.x*255);
            texdata[0][(x+y*tex_x)*4+1]=(unsigned char)(aux.y*255);
            texdata[0][(x+y*tex_x)*4+2]=(unsigned char)(aux.z*255);
            texdata[0][(x+y*tex_x)*4+3]=255;
        }
    }
}

float encontrar_maximo(float * map,int msize)
{
    float act_max=-99999.0f;
    int i;
    for(i=0;i<msize;i++)
    {
        //printf("%.02f ",map[i]);
        if(map[i]>act_max)
        {
            act_max=map[i];
        }
    }
    return act_max;
}

float encontrar_minimo(float * map,int msize)
{
    float act_min=99999.0f;
    int i;
    for(i=0;i<msize;i++)
    {
        //printf("%.02f ",map[i]);
        if(map[i]<act_min && map[i]>0.0f)
        {
            act_min=map[i];
        }
    }
    return act_min;
}

void promediar_mapas(void){
    Singleton::getInstance()->vert_x=(Singleton::getInstance()->vert_gis_x+Singleton::getInstance()->celdas_prom)/Singleton::getInstance()->celdas_prom;
    Singleton::getInstance()->vert_y=(Singleton::getInstance()->vert_gis_y+Singleton::getInstance()->celdas_prom)/Singleton::getInstance()->celdas_prom;

    //if(Singleton::getInstance()->vert_gis_x%Singleton::getInstance()->celdas_prom!=0){Singleton::getInstance()->vert_x++;}
    //if(Singleton::getInstance()->vert_gis_y%Singleton::getInstance()->celdas_prom!=0){Singleton::getInstance()->vert_y++;}
    //Singleton::getInstance()->vert_x++;
    //Singleton::getInstance()->vert_y++;

    int celdasx=(Singleton::getInstance()->vert_x-1)*Singleton::getInstance()->celdas_prom;
    int celdasy=(Singleton::getInstance()->vert_y-1)*Singleton::getInstance()->celdas_prom;
    if(celdasx<Singleton::getInstance()->vert_gis_x)Singleton::getInstance()->vert_x++;
    if(celdasy<Singleton::getInstance()->vert_gis_y)Singleton::getInstance()->vert_y++;


    print_debug("celdasx=%d\n",celdasx);
    print_debug("celdasy=%d\n",celdasy);

    print_debug("vert_x=%d\n",Singleton::getInstance()->vert_x);
    print_debug("vert_y=%d\n",Singleton::getInstance()->vert_y);

    //COMPROBAR ACÁ Y CARGAR LAS VARIABLES SOBRA_X y SOBRA_Y, también SCALEF_X SCALEF_Y



    Singleton::getInstance()->h_mapaaltir=promediar_mapa(Singleton::getInstance()->h_mapaalti,-9999.0f);
    //h_mapawindMODr=promediar_mapa(h_mapawindMOD);
    promediar_mapa_polar(&Singleton::getInstance()->h_mapawindINTr,&Singleton::getInstance()->h_mapawindANGr,Singleton::getInstance()->h_mapawindINT,Singleton::getInstance()->h_mapawindANG,Singleton::getInstance()->mapawindINT_ND,Singleton::getInstance()->mapawindANG_ND);
    //h_mapawindANGr=promediar_mapa(h_mapawindANG,mapawindANG_ND);
    //h_mapawindINTr=promediar_mapa(h_mapawindINT,mapawindINT_ND);
    //h_mapaveger=promediar_mapa(h_mapavege);

    Singleton::getInstance()->windi_max=encontrar_maximo(Singleton::getInstance()->h_mapawindINTr,Singleton::getInstance()->vert_x*Singleton::getInstance()->vert_y);
    Singleton::getInstance()->windi_min=encontrar_minimo(Singleton::getInstance()->h_mapawindINTr,Singleton::getInstance()->vert_x*Singleton::getInstance()->vert_y);
    print_debug("windi_max=%.02f\n",Singleton::getInstance()->windi_max);
    print_debug("windi_min=%.02f\n",Singleton::getInstance()->windi_min);


    histograma_viento();
    print_debug("promediar_mapas() OK \n");
}

void histograma_viento(void)
{
    int vert_x=Singleton::getInstance()->vert_x;
    int vert_y=Singleton::getInstance()->vert_y;
    int size = vert_x*vert_y*sizeof(float);
    int *histo;

    int nbins=1+(int)Singleton::getInstance()->windi_max;

    Singleton::getInstance()->h_mapawindINTprom = (float*)malloc(size);

    histo = (int*)malloc(nbins * sizeof(int));
    print_debug("El histograma tendrá %d bins.\n",nbins);

    
    armar_histograma(Singleton::getInstance()->h_mapawindINTr, vert_y, vert_x, histo, nbins);
    //mostrar_histograma(histo, (int)Singleton::getInstance()->windi_max);
    //guardar_histograma("histo.out", histo, (int)windi_max);

    int i,max=0,binmax=-1,total=0;
    for (i = 0; i < (int)Singleton::getInstance()->windi_max; i++)
    {
        if(histo[i]>max){max=histo[i];binmax=i;}
        total+=histo[i];
    }

    print_debug("total=%d\n",total);

    int centro=binmax;

    int fuera_de_rango=0;
    int bini=centro;
    int binf=centro;
    
    int totales=histo[centro];
    float incluidas=(float)totales/total;
    while(!fuera_de_rango && incluidas<0.9f)
    {
        bini--;
        binf++;
        if(bini<0||binf>=((int)Singleton::getInstance()->windi_max))
        {
            fuera_de_rango=1;
            bini++;binf--;
        }
        else
        {
            totales+=histo[bini]+histo[binf];
            incluidas=(float)totales/total;
        }
    }

    print_debug("Incluidas %f%% de las celdas\n",incluidas*100.0f);

    float*h_mapawindINTr=Singleton::getInstance()->h_mapawindINTr;
    float*h_mapawindINTprom=Singleton::getInstance()->h_mapawindINTprom;


    int x;
    for(x=0;x<vert_x*vert_y;x++)
    {
        if(h_mapawindINTr[x]<(float)bini)
        {
            h_mapawindINTprom[x]=(float)bini;
        }
        else if(h_mapawindINTr[x]>(float)binf)
        {
            h_mapawindINTprom[x]=(float)binf;
        }
        else
        {
            h_mapawindINTprom[x]=h_mapawindINTr[x];
        }
    }
    Singleton::getInstance()->windi_max=(float)binf;
    Singleton::getInstance()->windi_min=(float)bini;

    print_debug("windi_max=%.02f\n",Singleton::getInstance()->windi_max);
    print_debug("windi_min=%.02f\n",Singleton::getInstance()->windi_min);
    //print_debug("LLA1 %d\n",(long int)histo);
    free(histo);
    print_debug("LLA2\n");
}



/*int inicializar_matriz(float *mat, int fi, int co) {

    int i;

    srand(time(NULL));

    for (i=0; i < fi*co; i++)
        mat[i] = (float)(rand() % NVALORES);

    return 0;
}*/
//armar_histograma(Singleton::getInstance()->h_mapawindINTr, vert_y, vert_x, histo, (int)Singleton::getInstance()->windi_max);
int armar_histograma(float *mat,int fi, int co, int *histo, int nval) {

    int i, j, nulos=0;

    for (i = 0; i < nval; i++)
        histo[i] = 0;

    //printf("fi=%d\n",fi);
    //printf("co=%d\n",co);
    for (j = 0; j < fi*co; j ++) 
        if (mat[j] > 0.0f)
            histo[(int)mat[j]] ++;
        else 
            nulos++;
            
    print_debug("nulos: %d \n", nulos);

    return 0;
}


int mostrar_histograma(int *histo, int nval){

    int i;

    for (i = 0; i < nval; i++)
        print_debug(" %d ", histo[i]);
    print_debug("\n");

    return 0;
}



int guardar_histograma(char *nombre, int *histo, int nval) {

    FILE *fdes;

    fdes = fopen(nombre, "w");

    if (!fdes) {print_debug("No abre archivo \n"); exit (-1);}

    int i;

    for (i=0; i < nval; i++)
        fprintf(fdes, "%d %d \n",i, histo[i]);

    fclose(fdes);

    return 0;
}


int escribir_matriz_de_archivo(char *nombre, float *mat, int fi, int co) {

    FILE *fdes;

    fdes = fopen(nombre, "w");

    if (!fdes) {print_debug("No abre archivo \n"); exit (-1);}

    int i, j;
    //float dato;

    for (i=0; i < fi; i++){
        for (j=0; j < co; j++) {
            fprintf(fdes, "%f ", mat[i*co + j]);    
        }
        fprintf(fdes, "\n");
    }

    fclose(fdes);

    return 0;   
}