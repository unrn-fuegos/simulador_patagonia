#pragma once

void reshape(int width, int height);
void calc_camera_front(void);
void mouse_click(int x,int y);
void timerEvent(int value);
void keyboard_dn(unsigned char key, int x, int y);
void keyboard_up(unsigned char key, int x, int y);
void special_keyboard_dn(int key, int x, int y);
void special_keyboard_up(int key, int x, int y);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
