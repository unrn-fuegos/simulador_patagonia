//#include "superinclude.h"

#include "Singleton.h"

#include "cortafuego.h"

#ifdef WIN32
#include "direntW.h"
#else
#include "dirent.h"
#endif

#include "imgui/imgui.h" 

#include "Simulador.h"


extern bool imguicontrol;
extern bool imguicontrol2;

using namespace glm;


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

void dibujar_linea(int* mapa, vec2 p0, vec2 p1,int val)
{
	int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int dx = abs(p1.x-p0.x), sx = p0.x<p1.x ? 1 : -1;
    int dy = abs(p1.y-p0.y), sy = p0.y<p1.y ? 1 : -1; 
    int err = (dx>dy ? dx : -dy)/2, e2;

    for(;;){
       // setPixel(p0.x,p0.y);
        mapa[(int)p0.x+(int)p0.y*vert_gis_x]=val;
        if (p0.x==p1.x && p0.y==p1.y) break;
        e2 = err;
        if (e2 >-dx) { err -= dy; p0.x += sx; }
        if (e2 < dy) { err += dx; p0.y += sy; }
    }
}

void generar_mapa_bosquecorf(void)
{
	int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
	float* h_mapavegecorf=Singleton::getInstance()->h_mapavegecorf;
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            h_mapavegecorf[x+y*vert_gis_x]=Singleton::getInstance()->vegetacionSIM.h_map[x+y*vert_gis_x];
            //h_mapavegecorf[x+y*vert_gis_x]=vegetacionSIM.h_map[x+y*vert_gis_x];
            if(Singleton::getInstance()->selcorf.h_map[x+y*vert_gis_x]>0)
            {
                h_mapavegecorf[x+y*vert_gis_x]=0.0f;
            }
        }
    
    }
    Singleton::getInstance()->Simulador.Update_vege_map(h_mapavegecorf);
    
    //CpyHostToDevice(d_vege, h_mapavegecorf, vert_gis_y, vert_gis_x);
}

CORTAFUEGOS_LINEA::CORTAFUEGOS_LINEA(int x0,int y0, int x1, int y1){
	this->x0=x0;
	this->y0=y0;
	this->x1=x1;
	this->y1=y1;
}

void CORTAFUEGOS_LINEA::set_pos(int x0,int y0, int x1, int y1){
	this->x0=x0;
	this->y0=y0;
	this->x1=x1;
	this->y1=y1;
}

void CORTAFUEGOS_LINEA::get_pos(int* x0,int *y0, int *x1, int *y1){
	(*x0)=this->x0;
	(*y0)=this->y0;
	(*x1)=this->x1;
	(*y1)=this->y1;
}

int CORTAFUEGOS_LISTA::nueva_entrada(bool linea_Narb){
	cant_corf++;
	ENTRADA=(CORTAFUEGOS_ENTRADA*)realloc(ENTRADA,cant_corf*sizeof(CORTAFUEGOS_ENTRADA));
	listbox_items=(char**)realloc(listbox_items,cant_corf*sizeof(char*));
	listbox_visible=(int*)realloc(listbox_visible,cant_corf*sizeof(int));
	ENTRADA[cant_corf-1].linea_Narb=linea_Narb;
	ENTRADA[cant_corf-1].LINEA=NULL;
	ENTRADA[cant_corf-1].cant_lineas=0;
	ENTRADA[cant_corf-1].visible=1;
	if(linea_Narb)
	{
		sprintf(ENTRADA[cant_corf-1].nombre,"Linea %d",cant_corf);
	}
	else
	{
		sprintf(ENTRADA[cant_corf-1].nombre,"Multi %d",cant_corf);
	}

	for(int i=0;i<cant_corf;i++)
	{
		listbox_items[i]=ENTRADA[i].nombre;
		listbox_visible[i]=ENTRADA[i].visible;
	}
	//listbox_items[cant_corf-1]=ENTRADA[cant_corf-1].nombre;
	return cant_corf-1;
}

CORTAFUEGOS_LISTA::CORTAFUEGOS_LISTA(){
	cant_corf=0;
	ENTRADA=NULL;
	listbox_items=NULL;
	listbox_visible=NULL;
	show_save=0;
	show_open=0;
	show_map_open=0;
	listbox_file_cant=0;
	listbox_file_list=NULL;
}

void CORTAFUEGOS_LISTA::borrar_entrada(int id){
	if(id<cant_corf && id>=0)
	{
		ENTRADA[id].remove();
		for(int i=id+1;i<cant_corf;i++)
		{
			ENTRADA[i-1]=ENTRADA[i];
			listbox_items[i-1]=listbox_items[i];
			listbox_visible[i-1]=listbox_visible[i];
		}
		cant_corf--;
		ENTRADA=(CORTAFUEGOS_ENTRADA*)realloc(ENTRADA,cant_corf*sizeof(CORTAFUEGOS_ENTRADA));
		listbox_items=(char**)realloc(listbox_items,cant_corf*sizeof(char*));
		listbox_visible=(int*)realloc(listbox_visible,cant_corf*sizeof(int));
		for(int i=0;i<cant_corf;i++)
		{
			listbox_items[i]=ENTRADA[i].nombre;
			listbox_visible[i]=ENTRADA[i].visible;
		}
	}
}

void CORTAFUEGOS_ENTRADA::add_new(int x0,int y0, int x1, int y1){
	cant_lineas++;
	LINEA=(CORTAFUEGOS_LINEA*)realloc(LINEA,cant_lineas*sizeof(CORTAFUEGOS_LINEA));
	LINEA[cant_lineas-1].set_pos(x0,y0,x1,y1);
}

void CORTAFUEGOS_ENTRADA::remove(){
	if(LINEA!=NULL)
	{
		free(LINEA);
		cant_lineas=0;
		LINEA=NULL;
	}
}

CORTAFUEGOS_ENTRADA::CORTAFUEGOS_ENTRADA(){
	cant_lineas=0;
	LINEA=NULL;
	linea_Narb=1;
}

void dibujar_linea(int* mapa, glm::vec2 p0, glm::vec2 p1,int val);
void generar_mapa_bosquecorf(void);
void regenerar_textura_final(void);

extern bool linea_temp;
extern glm::vec2 lnsta,lnend;



void CORTAFUEGOS_LISTA::actualizar_mapa(int* mapa,int* mapat){
	int selec=-1;
	int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
	if(!algun_cambio && algun_cambio_t)
	{
		memcpy(mapa,mapat,sizeof(int)*vert_gis_x*vert_gis_y);
		if(linea_temp)
		{
			dibujar_linea(mapa,lnsta,lnend,1);	
		}
		algun_cambio_t=0;
		regenerar_textura_final();
        generar_mapa_bosquecorf();
        Singleton::getInstance()->selcorf.is_gpu_utd=0;
        //printf("T\n");
	}

	if(algun_cambio)
	{
		//printf("N\n");
		int x0,y0,x1,y1;
		glm::vec2 linesta,lineend;
		memset(mapa,0,vert_gis_x*vert_gis_y*sizeof(int));
		for(int i=0;i<cant_corf;i++)
		{
			if(i==selected)
			{
				selec=i; continue;
			}
			if(!ENTRADA[i].visible){continue;}
			for(int j=0;j<ENTRADA[i].cant_lineas;j++)
			{
				ENTRADA[i].LINEA[j].get_pos(&x0,&y0,&x1,&y1);
				linesta.x=x0;
				linesta.y=y0;
				lineend.x=x1;
				lineend.y=y1;
				dibujar_linea(mapa,linesta,lineend,2);
				//this.ENTRADA[i].LINEA[j].
			}

		}
		if(selec>=0)
		{
			int i=selec;
			for(int j=0;j<ENTRADA[i].cant_lineas;j++)
			{
				ENTRADA[i].LINEA[j].get_pos(&x0,&y0,&x1,&y1);
				linesta.x=x0;
				linesta.y=y0;
				lineend.x=x1;
				lineend.y=y1;
				dibujar_linea(mapa,linesta,lineend,1);
			}
		}
		memcpy(mapat,mapa,sizeof(int)*vert_gis_x*vert_gis_y);
		algun_cambio=0;
		regenerar_textura_final();
        generar_mapa_bosquecorf();
        Singleton::getInstance()->selcorf.is_gpu_utd=0;
	}
}

void CORTAFUEGOS_LISTA::borrar_todo(void){
	for(int i=0;i<cant_corf;i++)
	{
		ENTRADA[i].remove();
	}
	free(listbox_visible);
	listbox_visible=NULL;
	free(listbox_items);
	listbox_items=NULL;
	free(ENTRADA);
	ENTRADA=NULL;
	cant_corf=0;
}

#define MAX_LN 128

int strtodelim(char* str,char delim)
{
	int i=0;
	while(str[i]!=0 && str[i]!='\r' && str[i]!='\n' && str[i]!=delim)
	{
		i++;
	}
	return i;

}

void CORTAFUEGOS_LISTA::cargar_archivo(char* filename){
	FILE * pFile;
	pFile = fopen (filename,"r");
	if (pFile!=NULL)
	{
		char line[128];
		bool blockmode=0;
		int blockmodeid=-1;
		while(fgets(line, MAX_LN, pFile))
		{
			int stpos=0;
			char ext[64];
			int extlen;

			extlen=strtodelim(&line[stpos],',');
			memcpy(ext,&line[stpos],extlen);
			ext[extlen]=0;
			stpos+=extlen+1;

			if(!blockmode)
			{
				if(!strcmp(ext,"L"))
				{
					int id=nueva_entrada(1);
					int x0,y0,x1,y1;
					int visible;

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&visible);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&x0);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&y0);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&x1);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&y1);

					extlen=strtodelim(&line[stpos],0);


					memcpy(ENTRADA[id].nombre,&line[stpos],extlen);
					ENTRADA[id].nombre[extlen]=0;

	                ENTRADA[id].add_new(x0,y0,x1,y1);
	                ENTRADA[id].visible=(bool)visible;
	                algun_cambio=1;
				}
				else if(!strcmp(ext,"LBSTART"))
				{
					blockmode=1;
					int id=nueva_entrada(0);
					blockmodeid=id;
					int cant_li,visible;

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&visible);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&cant_li);


					extlen=strtodelim(&line[stpos],0);


					memcpy(ENTRADA[id].nombre,&line[stpos],extlen);
					ENTRADA[id].nombre[extlen]=0;

					ENTRADA[id].visible=(bool)visible;

	                algun_cambio=1;
				}
				else
				{
					print_debug("Comando *%s* incorrecto en linea %s.\n",ext,line);
				}
			}
			else
			{
				if(!strcmp(ext,"L"))
				{
					int id=blockmodeid;
					int x0,y0,x1,y1;

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&x0);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&y0);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&x1);

					extlen=strtodelim(&line[stpos],',');
					memcpy(ext,&line[stpos],extlen);
					ext[extlen]=0;
					stpos+=extlen+1;
					sscanf(ext,"%d",&y1);

	                ENTRADA[id].add_new(x0,y0,x1,y1);
	                algun_cambio=1;
				}
				else if(!strcmp(ext,"LBEND"))
				{
					blockmode=0;

				}
				else
				{
					print_debug("Comando *%s* incorrecto en linea %s.\n",ext,line);
				}

			}
    	}
		fclose (pFile);
	}

}



void CORTAFUEGOS_LISTA::guardar_archivo(char* filename){
	FILE * pFile;
	pFile = fopen (filename,"w");
	if (pFile!=NULL)
	{
		char str[128];
		for(int i=0;i<cant_corf;i++)
		{
			if(ENTRADA[i].linea_Narb)//si es solo una linea
			{
				sprintf(str,"L,%d,%d,%d,%d,%d,%s\r\n",ENTRADA[i].visible,ENTRADA[i].LINEA[0].x0,ENTRADA[i].LINEA[0].y0,ENTRADA[i].LINEA[0].x1,ENTRADA[i].LINEA[0].y1,ENTRADA[i].nombre);
				fputs(str,pFile);
			}
			else// si es dibujo arbitrario
			{
				sprintf(str,"LBSTART,%d,%d,%s\r\n",ENTRADA[i].visible,ENTRADA[i].cant_lineas,ENTRADA[i].nombre);
				fputs(str,pFile);
				for(int j=0;j<ENTRADA[i].cant_lineas;j++)
				{
					sprintf(str,"L,%d,%d,%d,%d\r\n",ENTRADA[i].LINEA[j].x0,ENTRADA[i].LINEA[j].y0,ENTRADA[i].LINEA[j].x1,ENTRADA[i].LINEA[j].y1);
					fputs(str,pFile);
				}
				sprintf(str,"LBEND\r\n");
				fputs(str,pFile);
			}
		}
		fclose (pFile);
	}
}

void CORTAFUEGOS_LISTA::show_cortafuego_saveas(void){
	//bool show_another_window=1;
	ImGui::SetNextWindowSize(ImVec2(500,600), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-300,200),ImGuiSetCond_Always);
    //ImGui::Begin("Guardar cortafuego como...", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
    
    if (ImGui::BeginPopupModal("Guardar cortafuego como..."))
    {
	    static int listbox_item_current = 1;
	    int old_listbox_item_current = listbox_item_current;

	    ImGui::ListBox("selecciona \narchivo \ndonde \nguardar", &listbox_item_current, listbox_file_list, listbox_file_cant, 15);

	    if(listbox_item_current!=old_listbox_item_current)
	    {
	    	pers_filename=0;
	    	memcpy(save_filename,listbox_file_list[listbox_item_current],1+strlen(listbox_file_list[listbox_item_current]));
	    }
	    ImGui::Text("Nombre:");
	    ImGui::SameLine();

		if(ImGui::InputText("", save_filename, 32)){
			pers_filename=1;

		}
	            

	    if (ImGui::Button("Guardar")) {

	    	char str[128];
	    	sprintf(str,"%scortafuego/%s",Singleton::getInstance()->PROJECT_FOLDER_P,save_filename);
	    	guardar_archivo(str);
	    	show_save=0;
	    	ImGui::CloseCurrentPopup();
	    }
	    ImGui::SameLine();
	    if (ImGui::Button("Cancelar")) {
	    	show_save=0;
	    	ImGui::CloseCurrentPopup();
	    }
	    if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
	    ImGui::EndPopup();
	}
}

void CORTAFUEGOS_LISTA::show_cortafuego_open(void){
	//bool show_another_window=1;
	ImGui::SetNextWindowSize(ImVec2(500,600), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-300,200),ImGuiSetCond_Always);
    //ImGui::Begin("Abrir cortafuego...", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
    
    if (ImGui::BeginPopupModal("Abrir cortafuego..."))
    {
	    static int listbox_item_current = 1;
	    int old_listbox_item_current = listbox_item_current;

	    ImGui::ListBox("selecciona \narchivo \npara \nabrir", &listbox_item_current, listbox_file_list, listbox_file_cant, 15);

	    if(listbox_item_current!=old_listbox_item_current)
	    {
	    	pers_filename=0;
	    	memcpy(save_filename,listbox_file_list[listbox_item_current],1+strlen(listbox_file_list[listbox_item_current]));
	    }
	    ImGui::Text("Nombre:");
	    ImGui::SameLine();

		if(ImGui::InputText("", save_filename, 32)){
			pers_filename=1;

		}
	            

	    if (ImGui::Button("Abrir")) {

	    	char str[128];
	    	sprintf(str,"%scortafuego/%s",Singleton::getInstance()->PROJECT_FOLDER_P,save_filename);
	    	cargar_archivo(str);
	    	show_open=0;
	    	ImGui::CloseCurrentPopup();
	    }
	    ImGui::SameLine();
	    if (ImGui::Button("Cancelar")) {
	    	show_open=0;
	    	ImGui::CloseCurrentPopup();
	    }
	    if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
	    ImGui::EndPopup();
	}
}

int* GetMapi(const char * nombre, int filas, int cols,int *nodata);
void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v );

void CORTAFUEGOS_LISTA::show_map_openf(void){
	ImGui::SetNextWindowSize(ImVec2(500,600), ImGuiSetCond_Always);
	int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;

	
    if (ImGui::BeginPopupModal("Cargar mapa fuego real..."))
    {
        
	    //ImGui::SetNextWindowPos(ImVec2(window_width-300,200),ImGuiSetCond_Always);
	    //ImGui::Begin("Cargar mapa fuego real...", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
	    
	    static int listbox_item_current = 1;
	    int old_listbox_item_current = listbox_item_current;

	    ImGui::ListBox("selecciona \narchivo \npara \nabrir", &listbox_item_current, listbox_file_list, listbox_file_cant, 15);

	    static char myfilename[32];
	    if(listbox_item_current!=old_listbox_item_current)
	    {
	    	pers_filename=0;
	    	memcpy(myfilename,listbox_file_list[listbox_item_current],1+strlen(listbox_file_list[listbox_item_current]));
	    }
	    ImGui::Text("Nombre:");
	    ImGui::SameLine();

		if(ImGui::InputText("", myfilename, 32)){
			pers_filename=1;

		}
	            

	    if (ImGui::Button("Abrir")) {

	    	char str[128];
	    	sprintf(str,"%s%s",Singleton::getInstance()->PROJECT_FOLDER_P,myfilename);
	    	Singleton::getInstance()->mapaaux.h_map=GetMapi(str, vert_gis_y, vert_gis_x,&Singleton::getInstance()->mapaaux.nodata_value);
	    	int min=99999;
	    	int max=-99999;

	    	for (int i=0;i<vert_gis_y*vert_gis_x;i++)
	    	{
	    		if(Singleton::getInstance()->mapaaux.h_map[i]<min){min=Singleton::getInstance()->mapaaux.h_map[i];}
	    		if(Singleton::getInstance()->mapaaux.h_map[i]>max){max=Singleton::getInstance()->mapaaux.h_map[i];}
	    	}
	    	if(min==0 && max<=10){
	    		int*maparaw=Singleton::getInstance()->mapaaux.h_map;
	    		Singleton::getInstance()->mapaaux.SinPaleta(0);//REVISAR
	    		Singleton::getInstance()->mapaaux.h_map=maparaw;
	    		Singleton::getInstance()->mapaaux.is_in_cpu=1;Singleton::getInstance()->mapaaux.is_gpu_utd=0;
	    		Singleton::getInstance()->mapaaux.nodata_value=0;
	    	}
	    	else
	    	{
	    		int*maparaw=Singleton::getInstance()->mapaaux.h_map;
	    		Singleton::getInstance()->mapaaux.SinPaleta(1);

	    		for (int i=0;i<vert_gis_y*vert_gis_x;i++)
	    		{
		    		float r,g,b;
	            	float hval=(float)(maparaw[i]-(float)min)/((float)(max-min));
	            	HSVtoRGB(&r,&g,&b,360.0f*hval,1.0f,1.0f);

		            /*textura_alti[0][(x+y*tex_x)*4+0]=(unsigned char)((float)r*255.0f);
		            textura_alti[0][(x+y*tex_x)*4+1]=(unsigned char)((float)g*255.0f);
		            textura_alti[0][(x+y*tex_x)*4+2]=(unsigned char)((float)b*255.0f);
		            textura_alti[0][(x+y*tex_x)*4+3]=255;

		    		maparaw[i]=*/
		            if(maparaw[i]==0){maparaw[i]=0;}else{
		    			maparaw[i]=256*256*((int)((float)r*255.0f))+256*((int)((float)g*255.0f))+((int)((float)b*255.0f));
		    		}
	    		}
	    		Singleton::getInstance()->mapaaux.h_map=maparaw;
	    		Singleton::getInstance()->mapaaux.is_in_cpu=1;Singleton::getInstance()->mapaaux.is_gpu_utd=0;
	    		Singleton::getInstance()->mapaaux.nodata_value=0;
	    	}

	    	//cargar_archivo(str);
	    	ImGui::CloseCurrentPopup();
	    	algun_cambio=1; //
	    	this->show_map_open=0;
	    }
	    ImGui::SameLine();
	    if (ImGui::Button("Cancelar")) {
	    	ImGui::CloseCurrentPopup();
	    	this->show_map_open=0;
	    }
	    if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
	    ImGui::EndPopup();
	}

}

#define MAX_DIR 32

const char *get_filename_ext(const char * filename){
	const char *ext=strrchr(filename,'.');
	if(!ext || ext==filename)return "";
	return ext+1;

}


void CORTAFUEGOS_LISTA::recalcular_filelist(char* proyecto,const char*subd,const char* exten){
	for(int i=0;i<listbox_file_cant;i++)
	{
		free(listbox_file_list[i]);
		listbox_file_list[i]=NULL;
	}
	if(listbox_file_list!=NULL)free(listbox_file_list);
	listbox_file_list=NULL;
	listbox_file_cant=0;
	//AGREGAR SUBDIRECTORIO CORTAFUEGOS****************************************************************
	DIR *d;
	struct dirent *dir;
	char str[256];
	sprintf(str,"%s%s",proyecto,subd);
	d=opendir(str);
	if(d)
	{
		while((dir=readdir(d))!=NULL){
			if(!strcmp(get_filename_ext(dir->d_name),exten)){
				listbox_file_list=(char**)realloc(listbox_file_list,(1+listbox_file_cant)*sizeof(char*));
				listbox_file_list[listbox_file_cant]=(char*)malloc(32);
				memcpy(listbox_file_list[listbox_file_cant],dir->d_name,1+strlen(dir->d_name));
				listbox_file_cant++;
			}

		}
		closedir(d);

	}
}