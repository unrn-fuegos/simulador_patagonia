#pragma once
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

void printvec3(const char* txt, glm::vec3* vec);
float triangle_intersection(glm::vec3* orig,
                      glm::vec3* dir,
                      glm::vec3* v0,
                      glm::vec3* v1,
                      glm::vec3* v2);
void raycast_calc(glm::vec3* ray_WCS);
void si_intersecta(int x,int y);
int intersection_calc(int *ni,int *nj);

