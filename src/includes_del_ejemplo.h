extern StopWatchInterface *timer;
#include "Singleton.h"

void computeFPS()
{
    Singleton::getInstance()->frameCount++;
    Singleton::getInstance()->fpsCount++;

    //if (fpsCount == fpsLimit)
    //{
        Singleton::getInstance()->avgFPS = 1.f / (sdkGetTimerValue(&timer) / 1000.f);
        Singleton::getInstance()->fpsCount = 0;
        //fpsLimit = (int)MAX(avgFPS, 1.f);

        sdkResetTimer(&timer);
    //}
    //REVISAR ESTA PARTE DEL CODIGO PARA VER QUE ANDA MAL
    //char fps[256];
    //sprintf(fps, "Simulador de Incendios Forestales. - %3.0f FPS", avgFPS);
    //glutSetWindowTitle(fps);
}



__global__ void kernel_inicial(float4 *d_tri, float3 *d_col, float* alti, float* colo, unsigned int width, unsigned int height, float time,int gisx,int gisy,int step)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

    if(x<width && y<height)
    {
    }

    
} 

////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
bool runTest(int argc, char **argv, char *ref_file)
{
    // Create the CUTIL timer
    sdkCreateTimer(&timer);

    // command line mode only
    if (ref_file != NULL)
    {
        // This will pick the best possible CUDA capable device
        int devID = findCudaDevice(argc, (const char **)argv);

        // create VBO
        //checkCudaErrors(cudaMalloc((void **)&d_vbo_buffer, verticesT*sizeof(float4)));

        // run the cuda part
        //runAutoTest(devID, argv, ref_file);

        // check result of Cuda step
        //checkResultCuda(argc, argv, vbo);

        //cudaFree(d_vbo_buffer);
        //d_vbo_buffer = NULL;
    }
    else
    {
       /* initGLUT();
        cudaGLSetGLDevice(gpuGetMaxGflopsDeviceId());
        glewInit();
        installShaders();

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        cargar_mapas_de_archivo();
        initGLBUF();

        ImGui_ImplGlfwGL3_Init();*/

       // ImGuiIO& io = ImGui::GetIO();

        //io.Fonts->AddFontFromFileTTF("./extra_fonts/ProggyClean.ttf", 13.0f);

        //glutMainLoop();
    } 

    return true;
}


////////////////////////////////////////////////////////////////////////////////
//! Run the Cuda part of the computation
////////////////////////////////////////////////////////////////////////////////
void runCuda(struct cudaGraphicsResource **vbo_resource)
{
    // map OpenGL buffer object for writing from CUDA
    float4 *dptr;





    checkCudaErrors(cudaGraphicsMapResources(1, vbo_resource, 0));
    size_t num_bytes;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes,
                                                         *vbo_resource));
    //printf("CUDA mapped VBO: May access %ld bytes\n", num_bytes);

    // execute the kernel
    //    dim3 block(8, 8, 1);
    //    dim3 grid(mesh_width / block.x, mesh_height / block.y, 1);
    //    kernel<<< grid, block>>>(dptr, mesh_width, mesh_height, g_fAnim);

    //launch_kernel(dptr, mesh_width, mesh_height, g_fAnim);

    // unmap buffer object
    checkCudaErrors(cudaGraphicsUnmapResources(1, vbo_resource, 0));
}

#ifdef _WIN32
#ifndef FOPEN
#define FOPEN(fHandle,filename,mode) fopen_s(&fHandle, filename, mode)
#endif
#else
#ifndef FOPEN
#define FOPEN(fHandle,filename,mode) (fHandle = fopen(filename, mode))
#endif
#endif

void sdkDumpBin2(void *data, unsigned int bytes, const char *filename)
{
    printf("sdkDumpBin: <%s>\n", filename);
    FILE *fp;
    FOPEN(fp, filename, "wb");
    fwrite(data, bytes, 1, fp);
    fflush(fp);
    fclose(fp);
}

////////////////////////////////////////////////////////////////////////////////
//! Run the Cuda part of the computation
////////////////////////////////////////////////////////////////////////////////
/*void runAutoTest(int devID, char **argv, char *ref_file)
{
    char *reference_file = NULL;
    void *imageData = malloc(mesh_width*mesh_height*sizeof(float));
    

    // execute the kernel
   // launch_kernel((float4 *)d_vbo_buffer, mesh_width, mesh_height, g_fAnim);

    cudaDeviceSynchronize();
    getLastCudaError("launch_kernel failed");

    //checkCudaErrors(cudaMemcpy(imageData, d_vbo_buffer, mesh_width*mesh_height*sizeof(float), cudaMemcpyDeviceToHost));

    sdkDumpBin2(imageData, mesh_width*mesh_height*sizeof(float), "simpleGL.bin");
    reference_file = sdkFindFilePath(ref_file, argv[0]);

    if (reference_file &&
        !sdkCompareBin2BinFloat("simpleGL.bin", reference_file,
                                mesh_width*mesh_height*sizeof(float),
                                MAX_EPSILON_ERROR, THRESHOLD, pArgv[0]))
    {
        g_TotalErrors++;
    }
}*/

void cleanup()
{
    sdkDeleteTimer(&timer);

    /*if (vbo)
    {
        //deleteVBO(&vbo, cuda_vbo_resource);
        //deleteVBO(&vbc, cuda_vbc_resource);
    }*/
    //free(h_triang);
    free(Singleton::getInstance()->h_color);
    free(Singleton::getInstance()->h_mapaalti);
    //cudaFree(d_triang);
    //cudaFree(d_mapaalti);
    //cudaFree(d_mapavege);
}


////////////////////////////////////////////////////////////////////////////////
//! Check if the result is correct or write data to file for external
//! regression testing
////////////////////////////////////////////////////////////////////////////////
void checkResultCuda(int argc, char **argv, const GLuint &vbo)
{
    /*if (!d_vbo_buffer)
    {
        checkCudaErrors(cudaGraphicsUnregisterResource(cuda_vbo_resource));

        // map buffer object
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        float *data = (float *) glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY);

        // check result
        if (checkCmdLineFlag(argc, (const char **) argv, "regression"))
        {
            // write file for regression test
            sdkWriteFile<float>("./data/regression.dat",
                                data, mesh_width * mesh_height * 3, 0.0, false);
        }

        // unmap GL buffer object
        if (!glUnmapBuffer(GL_ARRAY_BUFFER))
        {
            fprintf(stderr, "Unmap buffer failed.\n");
            fflush(stderr);
        }

        checkCudaErrors(cudaGraphicsGLRegisterBuffer(&cuda_vbo_resource, vbo,
                                                     cudaGraphicsMapFlagsWriteDiscard));

        SDK_CHECK_ERROR_GL();
    }*/
}


void inicializar_mapa(struct cudaGraphicsResource **vbo_resource,struct cudaGraphicsResource **vbc_resource)
{
    //int x,y,ptr=0;
    //h_triang=(float4*)malloc(sizeof(float4)*verticesT);
    //h_color=(float3*)malloc(sizeof(float3)*verticesT);

    //cudaMalloc((void**)&d_triang,sizeof(float4)*verticesT);

    float4 *dptr;
    float4 *dptrc;
    //cudaMalloc((void**)&d_mapaalti,sizeof(float)*verticesT);
    //cudaMalloc((void**)&d_mapavege,sizeof(float)*verticesT);
 

    //cudaMemcpy(d_triang,h_triang,sizeof(float4)*801*2*800,cudaMemcpyHostToDevice);
    checkCudaErrors(cudaGraphicsMapResources(1, vbo_resource, 0));
    size_t num_bytes;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&dptr, &num_bytes,
                                                         *vbo_resource));
    printf("CUDA mapped VBO: May access %ld vertices\n", num_bytes/(sizeof(float4)));
    printf("En host alocamos para %i vertices\n", Singleton::getInstance()->verticesT);


    checkCudaErrors(cudaGraphicsMapResources(1, vbc_resource, 0));
    size_t num_bytesc;
    checkCudaErrors(cudaGraphicsResourceGetMappedPointer((void **)&dptrc, &num_bytesc,
                                                         *vbc_resource));
    printf("Y alocamos para color %ld vertices cuda\n", num_bytes/(sizeof(float4)));
    printf("Y alocamos para color %i vertices host\n", Singleton::getInstance()->verticesT);

    // execute the kernel
    //    dim3 block(8, 8, 1);
    //    dim3 grid(mesh_width / block.x, mesh_height / block.y, 1);
    //    kernel<<< grid, block>>>(dptr, mesh_width, mesh_height, g_fAnim);

    //launch_kernel(dptr, mesh_width, mesh_height, g_fAnim);

    //cudaMemcpy(dptr,h_triang,sizeof(float4)*verticesT,cudaMemcpyHostToDevice);
    //cudaMemcpy(dptrc,h_color,sizeof(float3)*verticesT,cudaMemcpyHostToDevice);
    //cudaMemcpy(d_mapaalti,h_mapaalti,sizeof(float)*vert_gis_x*vert_gis_y,cudaMemcpyHostToDevice);
    //cudaMemcpy(d_mapavege,h_mapavege,sizeof(float)*vert_gis_x*vert_gis_y,cudaMemcpyHostToDevice);
    int dimx = 32;
    int dimy = 32;
    dim3 block(dimx, dimy);
    dim3 grid ((Singleton::getInstance()->ancho+31)/dimx , (Singleton::getInstance()->alto+31)/dimy);
   
    //kernel_inicial<<<grid, block>>>((float4*)dptr,(float3*)dptrc,d_mapaalti,d_mapavege, ancho, alto, g_fAnim,ancho_puntos_GIS,alto_puntos_GIS,step1);
    cudaDeviceSynchronize();

    // unmap buffer object
    checkCudaErrors(cudaGraphicsUnmapResources(1, vbo_resource, 0));
    checkCudaErrors(cudaGraphicsUnmapResources(1, vbc_resource, 0));

    
}