#pragma once
#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <GL/freeglut.h>


#include <math.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "manejo_archivos.h"
#include "Manejador_Proyectos.h"
#include "Modelo_AutomataCelular.h"
#include "Modelo_RDC.h"

class Simulador{
public:
	int modelo;//0=NINGUNO  1=AutomataCelular 2=RDC
	Modelo_AutomataCelular * M;
	Modelo_RDC * RDC;
	int paso_simulacion;
	Simulador();
	void adelante(void);
	void atras(void);
	void volver_a_cero(void);
	void inicializar(int tipo);
	float* export_fire_map(void);
	void SetPixelForAll(int x, int y);
	void Update_vege_map(float * newmap);
	void recorrer_floats(void);
	void convertir_IS_a_quemado(void);


private:
	

};
