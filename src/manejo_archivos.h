#pragma once



#include "constantes.h"



// rutinas para procesar datos desde un archivo
int procesar_datos_desde_archivo();
int procesar_dato_diario();
int procesar_archivo(char in[48], char out[48]);
int obtener_datos(char line[256],  int *date, float *temp, float *rh, char windS[8], float *windV, float *rain);

class METEOR{
public:
	int aa,mm,dd;
	float temp,rh,ws,wv,rain;
	char ws_str[4];
};

class FWI_OUT{
public:
	METEOR met;
	bool valid;
	float ffmc,dmc,dc,isi,bui,fwi,dsr;
	int pelig[5];
	FWI_OUT();
};

class FWI_TABLE{
public:
	FWI_OUT* fwi;
	int cant_fwi;
	char indice_filename[128];
	char meteor_filename[128];
	char output_filename[128];
	FWI_TABLE();
	void init(char* project_folder,char*filenamei,char*filenamem,char*filenameo);
	void init(char* project_folder,char*filenamem,char*filenameo);
	void load_files(float FFMC,float DMC,float DC);
	void calculate_FWI(void);
	void show_load_mfile(bool*showw);
	void clear(void);
};

