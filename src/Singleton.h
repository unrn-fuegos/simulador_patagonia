#pragma once
#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <GL/freeglut.h>

#define print_debug(...); {if(debug){printf(__VA_ARGS__);}{fprintf(Singleton::getInstance()->logfile_sigdbg,__VA_ARGS__);}}
//#define print_debug(...); {printf(__VA_ARGS__);}
//#define print_debug(...); ;

#include <math.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "manejo_archivos.h"
#include "Manejador_Proyectos.h"
#include "cortafuego.h"
#include "RasterMap.h"
#include "Simulador.h"


using namespace glm;

#define REFRESH_DELAY     20 //ms
#define PARAL 10
#define MAX_NUM_LAYERS 20
#define SIZE_CHAR 40
#define debug 0
#define MICROSEC (1E-6)

struct Camera{
	vec3 position;
	vec3 front;
	vec3 rotation;
	vec3 side;
};



class LAYER{
public:
	bool enabled;
	bool show_on_panel;
	float* alpha;
	char name[24];
	void * ptr_RasterMap;
	LAYER(){ptr_RasterMap=NULL;}
	bool SetAlpha(float newalpha);
};

class ProjectFileList{
public:
	int pers_filename;
	char **listbox_file_list;
	int listbox_file_cant;
};


class Singleton
{
private:
	Singleton( );
	static Singleton* instance;
	int visualizar_pelig_indice;int old_visualizar_pelig_indice;




public:
	FILE * logfile_sigdbg;
	Simulador Simulador;
	GLuint vertexBufferID;
	GLuint uvBufferID;
	GLuint uvfBufferID;
	GLuint indexBufferID;
	GLuint normalBufferID;
	GLuint globalVAOID;
	GLuint programID,program2dID,programHUDID,programLINESID;
	GLuint flechaFBO,flechatexture;
	GLuint VAOHUDID,VBOPHUDID,VBOUVHUDID,VBOIHUDID;

	GLuint textureID,texture_altitud_ID,texture_pendiente_ID,texture_aspect_ID,texture_flecha_ID,texture_colormap_ID,texture_windi_ID;
	GLuint texture_alti_ID,texture_Calti_ID,texture_pelig_ID;
	GLuint texture_flecha_norte_ID;

	GLuint VAO2DID,VBOP2DID,VBOUV2DID,VBOI2DID;

	struct cudaGraphicsResource *cuda_vertexBuffer_res;
	struct cudaGraphicsResource *cuda_colorBuffer_res;

	ProjectFileList projectfilelist;

	float *h_mapaaltir;
	float *h_mapaveger;
	float *h_mapawindANGr;
	float *h_mapawindANG;
	float *h_mapawindINTr;
	float *h_mapawindINTprom;
	float *h_mapawindINT;
	float *h_mapaalti;
	float *h_mapavegecorf;
	float *h_mapaaspect;
	float *h_mapaslope;
	int mapawindANG_ND;
	int mapawindINT_ND;

	bool drawwiremesh;
	bool proyecto_cargado;

	int window_width;
	int window_height;

	int mouse_x,mouse_y;
	float g_MouseWheel;

	float near_plane;

	int vert_x,vert_y;
	int vert_gis_x,vert_gis_y;
	int tex_x,tex_y;
	int sobra_x,sobra_y;
	float scalef_x,scalef_y;

	int celdas_prom;

	float windi_max;
	float windi_min;

	float ambient_mult;
	float specular_mult;
	float diffuse_mult;
	float diffuseC_mult;

	float wind_a,wind_pot;
	float camspd;

	int verticesT;

	int click_si;
	int click_x;
	int click_y;

	float altitud_min;
	float altitud_max;

	bool fullscreen;


	int mouse_buttons;
	float rotate_x, rotate_y, rotate_z;
	float translate_x;
	float translate_y;
	float translate_z;

	// Auto-Verification Code
	int fpsCount;        // FPS count for averaging
	int fpsLimit;        // FPS limit for sampling
	int g_Index;
	float avgFPS;
	unsigned int frameCount;
	unsigned int g_TotalErrors;
	bool g_bQAReadback;

	unsigned char* textura_vegetacion,*textura_windi,*textura_quemado,*textura_final,*textura_selcorf,*textura_aspect,*textura_pelig,*textura_alti,*textura_Calti;
	bool btnS_poner_cortafuego;
	bool btnS_poner_cortafuego_multilinea;

	int *pArgc;
	char **pArgv;

	int update_Calti;

	int mapa_Calti_cant,mapa_Calti_pow;

	bool btnS_IGNI;
	int btnS_IGNI_redef;

	int *h_mapaselcorft;

	vec3 *paleta_vegetacion;
	vec3 *paleta_selcorf;
	vec3 *paleta_quemado;

	float3 *h_color;

	int XIGNI,YIGNI;

	int alto,ancho;

	bool keys[1024];

	vec3* verticesPosicion;
	vec3* verticesUV;
	vec3* verticesUVF;
	vec3* verticesNormals;
	int* verticesIndex;

	int numvertices,numindices;

	mat4 projectionMatrix;
	mat4 matrixVTP;
	mat4 matrixWTV;

	int draw_line_state;

	mat4 g_fullTransfWTP;

	int mouse_old_x, mouse_old_y;

	bool arrowstate[2];

	int tabla_pelig[5];
	vec3 peli_col[6];

	char PROJECT_FOLDER_P[SIZE_CHAR];
	char ALTI_MAP_P[SIZE_CHAR];

	int colormap_width;
	unsigned char* colormap_bmp;

	Camera MainCam;
	LAYER layer_list[MAX_NUM_LAYERS];
	Proyecto proyectow;
	CORTAFUEGOS_LISTA CORTAFUEGOS;
	SimuScript SIMUSCRIPT;
	RasterMap vegetacion,vegetacionSIM,selcorf,quemado,mapaaux,levelmap;
	RasterMap aspectmap,altimap,peligmap,windImap;
	FWI_TABLE FWI_T;

public:
	static Singleton* getInstance( );
	static Singleton* gI( );
	~Singleton( );
	int Get_visualizar_pelig_indice();
	bool Set_visualizar_pelig_indice(int val);


};
