#include "Simulador.h"

#include "Singleton.h"

#include <math.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

using namespace glm;


extern void regenerar_textura_final(void);


Simulador::Simulador()
{
	modelo=0;
	//RDC=(Modelo_RDC*)malloc(sizeof(Modelo_RDC));
	RDC=NULL;
	M=NULL;
}

void Simulador::adelante(void)
{
	if(!Singleton::getInstance()->proyecto_cargado){
		print_debug("NO HAY PROYECTO CARGADO: ADELANTE\n");
		return;
	}
	switch (modelo)
	{
		case 1:{//MODELO AUTOMATA CELULAR

		    //print_debug(">\n");//https://bitbucket.org/monicamalen/fire-simulator
		    for(int cnt=0;cnt<PARAL;cnt++){ 
		        M[cnt].Evaluar_Individuo_FW(0, 1, 0);
		    }
		    paso_simulacion=M[0].paso_simulacion;


		    recorrer_floats(); //Funcion de clase simulador

		    regenerar_textura_final(); 

		    print_debug("AUTOMATA CELULAR %d ",paso_simulacion);
			break;
		}

		case 2:{//MODELO RDC

			RDC->paso_adelante();

			convertir_IS_a_quemado();

			regenerar_textura_final(); 
			print_debug("RDC");
			break;
		}


		default:{
		print_debug("NO HAY MODELO SELECCIONADO");
		}
	} 

	print_debug(": adelante\n");
}

float * Simulador::export_fire_map(void){
	return M[0].h_mapa_aux;
}

void Simulador::SetPixelForAll(int x, int y){

	switch (modelo)
	{
		case 1:{//MODELO AUTOMATA CELULAR

		    for(int cnt=0;cnt<PARAL;cnt++){ 
		        M[cnt].Set_Pixel(x+y*Singleton::getInstance()->vert_gis_x);
		    }

		    print_debug("AUTOMATA CELULAR");
			break;
		}

		case 2:{//MODELO RDC

			print_debug("RDC");
			RDC->Set_Pixel(x+y*Singleton::getInstance()->vert_gis_x);
			break;
		}


		default:{
			print_debug("NO HAY MODELO SELECCIONADO");
		}
	} 
	print_debug(": SetPixelForAll\n");
    
}

void Simulador::Update_vege_map(float * newmap)
{
	switch (modelo)
	{
		case 1:{//MODELO AUTOMATA CELULAR

		    for(int cnt=0;cnt<PARAL;cnt++){	
		    	M[cnt].Update_vege_map(newmap);
			}

		    print_debug("AUTOMATA CELULAR");
			break;
		}

		case 2:{//MODELO RDC
			RDC->Update_vege_map(newmap);
			print_debug("RDC");
			break;
		}


		default:{
			print_debug("NO HAY MODELO SELECCIONADO");
		}
	} 
	print_debug(": Update_vege_map\n");
	
}


void Simulador::atras(void)
{
	if(!Singleton::getInstance()->proyecto_cargado){
		print_debug("NO HAY PROYECTO CARGADO: ATRAS\n");
		return;
	}
	switch (modelo)
	{
		case 1:{//MODELO AUTOMATA CELULAR

			//print_debug(">\n");//https://bitbucket.org/monicamalen/fire-simulator
			for(int cnt=0;cnt<PARAL;cnt++){ 
		        M[cnt].Evaluar_Individuo_BW(0, 1, 0);
		    }

		    paso_simulacion=M[0].paso_simulacion;

		    recorrer_floats(); //Funcion de clase simulador

		    regenerar_textura_final(); 

		    print_debug("AUTOMATA CELULAR");
			break;
		}

		case 2:{//MODELO RDC

			RDC->atras();
			convertir_IS_a_quemado();
			regenerar_textura_final();
			print_debug("RDC");
			break;
		}


		default:{
			print_debug("NO HAY MODELO SELECCIONADO");
		}
	} 
	print_debug(": atras. NuevoPaso=%d\n",paso_simulacion);
}

void Simulador::volver_a_cero(void)
{
	if(!Singleton::getInstance()->proyecto_cargado){
		print_debug("NO HAY PROYECTO CARGADO: A CERO\n");
		return;
	}
	switch (modelo)
	{
		case 1:{//MODELO AUTOMATA CELULAR

			//print_debug(">\n");//https://bitbucket.org/monicamalen/fire-simulator
			for(int cnt=0;cnt<PARAL;cnt++){ 
		        M[cnt].Evaluar_Individuo_A_CERO(0, 1, 0);
		    }

		    paso_simulacion=M[0].paso_simulacion;

		    recorrer_floats(); //Funcion de clase simulador

		    regenerar_textura_final();

		    print_debug("AUTOMATA CELULAR"); 

			break;
		}

		case 2:{//MODELO RDC


			print_debug("RDC");
			break;
		}


		default:{
			print_debug("NO HAY MODELO SELECCIONADO");
		}
	} 
	print_debug(": volver a cero\n");
}

int inicializar_betas(float *beta_cero_p, float *beta_forest_p, float *beta_aspect_p, float *beta_wind_p, float *beta_slope_p){

	/*int opcion;


	printf("Elija una opcion: \n");
	printf("0 -  para valores por defecto \n");
	printf("1 -  para ingresar valores \n");
	scanf("%d\n", &opcion);
	fflush(NULL);

	if (opcion ){
		printf("Ingrese valor para beta_cero: ");
		scanf("%f", beta_cero_p);

		printf("\nIngrese valor para beta_forest: ");
		scanf("%f", beta_forest_p);

		printf("\nIngrese valor para beta_aspect: ");
		scanf("%f", beta_aspect_p);

		printf("\nIngrese valor para beta_wind: ");
		scanf("%f", beta_wind_p);

		printf("\nIngrese valor para beta_slope: ");
		scanf("%f", beta_slope_p);
	} else {*/

		*beta_cero_p = beta_cero_best;
		*beta_forest_p = beta_forest_best;
		*beta_aspect_p = beta_aspect_best;
		*beta_wind_p = beta_wind_best;
		*beta_slope_p = beta_slope_best;

	//}



	return 0;
}

void Simulador::inicializar(int tipo)
{
	print_debug("Inicializando con tipo %d \n",tipo);
	modelo=tipo;
	paso_simulacion=0;

	switch(modelo)
	{
		case 1:{//Automata celular
			//M=(Modelo_AutomataCelular*)malloc(PARAL*sizeof(Modelo_AutomataCelular));
			if(RDC!=NULL){delete RDC;}

			print_debug("POR ENTRAR A NEW(Modelo_AutomataCelular)\n");
			M=new Modelo_AutomataCelular[PARAL];

			print_debug("POR ENTRAR Al for inicializar betas\n");

			for(int cnt=0;cnt<PARAL;cnt++){
				inicializar_betas(&M[cnt].beta_cero, &M[cnt].beta_forest, &M[cnt].beta_aspect, &M[cnt].beta_wind, &M[cnt].beta_slope);
			}
		    //if(debug)print_debug("BETAS HOST %f %f %f %f %f\n",b_cero, b_forest, b_aspect, b_wind, b_slope);

			print_debug("SIZE %d %d\n",Singleton::getInstance()->proyectow.ROWS,Singleton::getInstance()->proyectow.COLS);

			for(int cnt=0;cnt<PARAL;cnt++){
				print_debug("POR ENTRAR A .SIZE\n");
				if(cnt==0)M[cnt].size(Singleton::getInstance()->proyectow.ROWS, Singleton::getInstance()->proyectow.COLS);
				print_debug("POR ENTRAR A .alocar_compartidas\n");
				if(cnt==0)M[cnt].alocar_compartidas();
				print_debug("POR ENTRAR A .alocar\n");
				M[cnt].alocar();

				print_debug("POR ENTRAR A .Init_All_Maps\n");

				if(cnt==0)M[cnt].Init_All_Maps(Singleton::getInstance()->proyectow.ROWS, Singleton::getInstance()->proyectow.COLS, Singleton::getInstance()->proyectow.WIND_MAP_P, Singleton::getInstance()->proyectow.ELEV_MAP_P, Singleton::getInstance()->proyectow.SLOPE_MAP_P, Singleton::getInstance()->proyectow.REAL_FIRE_MAP_P, Singleton::getInstance()->proyectow.ALTI_MAP_P, Singleton::getInstance()->proyectow.ASPECT_MAP_P, Singleton::getInstance()->proyectow.VEGE_MAP_P, Singleton::getInstance()->proyectow.FOREST_MAP_P);
				
				print_debug("POR ENTRAR A .Evaluar_Individuo_INIT\n");
				
				M[cnt].Evaluar_Individuo_INIT(0, 1, 0);print_debug("Evaluar_Individuo_INIT ok, %d\n",cnt);
				M[cnt].Init_Initial_Map(NULL, NULL);print_debug("Init_Initial_Map ok, %d\n",cnt);
				M[cnt].indice_matriz=cnt;

		    }

		    Singleton::getInstance()->Simulador.paso_simulacion = 0;
		    Singleton::getInstance()->quemado.reset();



			Singleton::getInstance()->SIMUSCRIPT.load_ignitionpoint_file((char*)"puntos_ignicion.txt");

			Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
			Singleton::getInstance()->CORTAFUEGOS.actualizar_mapa(Singleton::getInstance()->selcorf.h_map,Singleton::getInstance()->h_mapaselcorft);

			regenerar_textura_final(); 

			/*SIMUSCRIPT.add_ignitionpoint(0,proyectow.XIGNI,proyectow.YIGNI,1);
			SIMUSCRIPT.add_ignitionpoint(97,583,383,1);
			SIMUSCRIPT.add_ignitionpoint(145,260,576,1);*/

			//M.Evaluar_Individuo(d_individuo, 0, 1, 0);



				/* toma de tiempos */
				/*gettimeofday(&end,NULL);
				timeval_subtract(&elapsed, &end, &start);
				secs = (double)elapsed.tv_sec + ((double)elapsed.tv_usec*MICROSEC);
		  		print_debug("Terrain size: nx=%d NY=%d. Fire Simulation execution time (in hours): %lf, TRUN=%d \n", ROWS_P, COLS_P, secs/3600,T_RUN);*/



		    print_debug("AUTOMATA CELULAR"); 
			break;
		}

		case 2://RDCpaso_adelante
		{
			print_debug("BORRANDO M...\n");
			if(M!=NULL){M[0].liberar_compartidas(); delete[] M;}
			print_debug("BORRADO M OK\n");
			RDC=new Modelo_RDC();
			RDC->inicializar();
			Singleton::getInstance()->quemado.reset();

			Singleton::getInstance()->SIMUSCRIPT.load_ignitionpoint_file((char*)"puntos_ignicion.txt");

			Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
			Singleton::getInstance()->CORTAFUEGOS.actualizar_mapa(Singleton::getInstance()->selcorf.h_map,Singleton::getInstance()->h_mapaselcorft);

			//print_debug("C=%d R=%d\n",RDC->COLS,RDC->ROWS);

			RDC->Histo.init(RDC->COLS,RDC->ROWS,4);
			RDC->Histo.load_initial_map(RDC->texIS);

			//print_debug("C=%d R=%d\n",RDC->Histo.cols,RDC->Histo.rows);

			regenerar_textura_final(); 
			print_debug("RDC"); 
			break;
		}

		default:{
			print_debug("NO HAY MODELO SELECCIONADO");
		}
	}



	print_debug(": inicializar\n");
}

__global__ void kernel_recorrer_floats(int vert_gis_x, int* mapa_salida,
        float* mapa0,
        float* mapa1,
        float* mapa2,
        float* mapa3,
        float* mapa4,
        float* mapa5,
        float* mapa6,
        float* mapa7,
        float* mapa8,
        float* mapa9) 
{

            int y = (blockIdx.y * blockDim.y) + threadIdx.y ;
            int x =  (blockIdx.x * blockDim.x) + threadIdx.x ;

            int pos=x+y*vert_gis_x;
            mapa_salida[pos]=0;
            mapa_salida[pos]+=(int)(mapa0[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa1[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa2[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa3[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa4[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa5[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa6[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa7[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa8[pos]>0.0f ? 1.0f : 0.0f);
            mapa_salida[pos]+=(int)(mapa9[pos]>0.0f ? 1.0f : 0.0f);
}

void Simulador::recorrer_floats(void)
{
	if(modelo==1)
	{
	    //print_debug("ingresa a recorrer floats\n");
	    dim3 dimBlock(16,16);//OJO CON PONER 32x32 porque cuda da errores raros!!!!!!
	    dim3 dimGrid((Singleton::getInstance()->vert_gis_x + dimBlock.x-1) / dimBlock.x, (Singleton::getInstance()->vert_gis_y + dimBlock.y -1) / dimBlock.y,1);

	    static int* d_mapa_salida;
	    static bool ftcm=1;
	    if(ftcm){
	        cudaMalloc((void**)&d_mapa_salida, Singleton::getInstance()->vert_gis_x*Singleton::getInstance()->vert_gis_y*sizeof(int));
	        ftcm=0;
	    }

	    kernel_recorrer_floats<<<dimGrid, dimBlock>>>(Singleton::getInstance()->vert_gis_x,d_mapa_salida,
	        M[0].d_terrainnew,
	        M[1].d_terrainnew,
	        M[2].d_terrainnew,
	        M[3].d_terrainnew,
	        M[4].d_terrainnew,
	        M[5].d_terrainnew,
	        M[6].d_terrainnew,
	        M[7].d_terrainnew,
	        M[8].d_terrainnew,
	        M[9].d_terrainnew
	    );

	    cudaDeviceSynchronize();

	    cudaMemcpy(Singleton::getInstance()->quemado.h_map, d_mapa_salida, sizeof(int) * Singleton::getInstance()->vert_gis_x*Singleton::getInstance()->vert_gis_y, cudaMemcpyDeviceToHost);
	    Singleton::getInstance()->quemado.is_gpu_utd=0;
	    //print_debug("terminado recorrer floats\n");
	}

    
}

void Simulador::convertir_IS_a_quemado(void)
{
	//cudaMemcpy(, d_mapa_salida, sizeof(int) * Singleton::getInstance()->vert_gis_x*Singleton::getInstance()->vert_gis_y, cudaMemcpyDeviceToHost);
    float tempS,tempI;

	for(int y=0;y<Singleton::getInstance()->vert_gis_y;y++)
	{
		for(int x=0;x<Singleton::getInstance()->vert_gis_x;x++)
		{
			
			tempI=RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)+0];
			tempS=RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)+1];
			
			float tempF=(10.0f*(1.0f-tempS+tempI));


			

			if(tempF>0.0001f && tempF<1.0f){tempF=1.0f;}//PARA PINTAR DE AMARILLO CLARO LA ZONA DE MUY POCA ACTIVIDAD

			Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=(int)tempF;//(tempI*10.0f);
			//ESTA LINEA SUMA LOS SITIOS INCENDIANDOSE CON LOS (1.0-SUCEPTIBLES)


			//Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=(int)(10.0f*(1.0f-RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)+1]));
			
			//Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=(int)(10.0f*(RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)+0]));
			
			//Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=max(Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x],(int)(10.0f*(RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)+0])));


			if(RDC->texAlReVe[(y*Singleton::getInstance()->vert_gis_x+x)*4+2]<=2.5f){Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=0;}

			if(Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]>10)
			{
				Singleton::getInstance()->quemado.h_map[y*Singleton::getInstance()->vert_gis_x+x]=10;
			}
			/*if(RDC->floattex1[4*(y*Singleton::getInstance()->vert_gis_x+x)]!=0.0f)
			{
				print_debug("Q\n");
			}*/
		}
	}

    Singleton::getInstance()->quemado.is_gpu_utd=0;

}

