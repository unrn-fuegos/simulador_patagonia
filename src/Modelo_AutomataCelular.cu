﻿//#pragma once
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <math.h>   /*math functions stl library*/
#include <algorithm>  /* std::min_element, std::max_element*/
#include <curand.h>
#include <curand_kernel.h>

//#include "Parameters.h"
#include "Modelo_AutomataCelular.h"


//#pragma once
//#include "Individuo.h"
#include <vector>
#include <thrust/device_vector.h>
#include "Parameters.h"


/*#include <thrust/swap.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>*/
#include "curso.h"


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

//#include "cortafuego.h"

extern int verificar_propagacion;
extern char opcion;

extern int paso_historicos;


#define MAX 100

#define WITHIN_BOUNDS( x ) ( x > -1 ) // por como está la función get_index, si no esta entre filas y cols retrona -1


#include "Singleton.h"

#define SEED 1332277027LLU   /*semilla para Philox*/

int Modelo_AutomataCelular::ROWS = 0;
int Modelo_AutomataCelular::COLS = 0;
int Modelo_AutomataCelular::map_size = 0;
float *Modelo_AutomataCelular::h_slope=NULL, *Modelo_AutomataCelular::h_aspect=NULL, *Modelo_AutomataCelular::h_wind=NULL, *Modelo_AutomataCelular::h_forest=NULL, *Modelo_AutomataCelular::h_alti=NULL, *Modelo_AutomataCelular::h_vege=NULL;
float *Modelo_AutomataCelular::d_slope=NULL, *Modelo_AutomataCelular::d_aspect=NULL, *Modelo_AutomataCelular::d_wind=NULL, *Modelo_AutomataCelular::d_forest=NULL, *Modelo_AutomataCelular::d_alti=NULL, *Modelo_AutomataCelular::d_vege=NULL;
curandState *Modelo_AutomataCelular::d_state = NULL;




/* Genera solo 1 vez un seed por cada thread. Se llama solo al comenzar y luego 
   se pasa en cada paso de simulacion el arreglo state  */
__global__ void setup_kernel(curandState *state, int cols){

  int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
  int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;
  int idx = fila * cols + col;

  curand_init(clock64(), idx, 0, &state[idx]);
}




__device__ int get_index(int fila, int col, int filas, int cols){
    // indice en 1 dimension para el elemento (fila,col) en un arreglo bidimensional de (filas x cols)
    int index=-1;
    if ( fila < filas && col < cols){
        index = fila*cols + col;
    }
    return index;
}


/* calcula la pendiente en base a los mapas de pendiente y de altitud y como lo explica el paper  */
__device__
float calculate_sigma_value(float *d_slope, float *d_alti, int target_index, int neighbor_index)
{
	float slope = 0.0;

	if (WITHIN_BOUNDS(neighbor_index)) {

		float neighbor_slope = (float) d_slope[neighbor_index];

	//slope = ((d_alti[neighbor_index] <= d_alti[target_index]) || (neighbor_slope <= 0) ) ? 0.0 : neighbor_slope;
	//Cambio el peso que le doy a la pendiente para que tenga el mismo peso que las otras variables que valen 1 o -1...
	  slope = ((d_alti[neighbor_index] <= d_alti[target_index]) || (neighbor_slope <= 0) ) ? 0.0 : 1;

	}

	return slope;
}


/* calculo del valor psi como esta en el paper Modificar para que lo calcule 1 sola vez*/
/*For slopes greater than 5: Driest conditions facing NW psi=1, humid conditions facing SE psi=-1*/
__global__
void KERNEL_calculate_psi_value(float *d_aspect, float *d_slope, int filas, int cols)
{
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;

	if ((fila < filas) && (col < cols)) {

		d_aspect[fila * cols + col] = (d_slope[fila * cols + col] > 5) ? cos(d_aspect[fila * cols + col] - 365.0) : 0.0;

	}
}



__device__
int calculate_angle_between_cells(int target_index, int neighbor_index, int filas, int cols)
{
	// lo calculo como el eje cartesiano
	int angulo =  -1;

	if (WITHIN_BOUNDS(target_index) && WITHIN_BOUNDS(target_index))	{

		int target_fila, target_col;
		int neighbor_fila, neighbor_col;

		int matriz[3][3] = {{315,0,45}, {270,-1,90}, {225,180,135}};  // segun explicacion de mm: 0 es el N. clockwise

		target_fila = target_index / cols;
		target_col = target_index % cols;

		neighbor_fila = neighbor_index / cols;
		neighbor_col = neighbor_index % cols;

		int diff_x = (target_col - neighbor_col) * -1 ; // multiplico por menos 1 porque es T-N y si N está a la izq necesito -1 y me queda 1 y al revés
		int diff_y = (target_fila - neighbor_fila) * -1 ; // lo mismo que para las filas

		angulo = matriz[diff_y+1][diff_x+1];	// para pasar los rangos -1..1 a 0..2 que son los indices de la matriz
	}

	return angulo;
}

/* calculo de omega segun paper omega = cos (fi_w - fi_c) que es la diferencia entre el angulo de la direccion del viento y el angulo formado por celda target y celda neighbor*/
/* Monica dice que falta verificar si es la dirección desde donde viene el viento o hacia donde va el viento. */
__device__
float calculate_omega_value(float *d_wind, int target_index, int neighbor_index, int filas, int cols)
{
	float wind_dir = 0.0;

	if (WITHIN_BOUNDS(target_index))	{

		float fi_w = (d_wind[target_index] >= 180) ? (d_wind[target_index] - 180) : (d_wind[target_index] + 180) ;  // direccion del viento, ver si es esto o d_wind - 180
		float fi_c = calculate_angle_between_cells(target_index, neighbor_index, filas, cols);
		wind_dir = cos(fi_w - fi_c) ;

	}
	return wind_dir;
}


/*Ignition probability of the target cell*/
__device__ float IgnitionProbability(float * d_mapa, float * d_forest, float * d_wind, float * d_aspect, float * d_slope, float *d_alti, int target_index,int neighbor_index,
					      int filas, int cols,float beta_cero, float beta_forest,float beta_aspect,float beta_slope, float beta_wind)

{
    /*Ignition Probability from manuscript Morales, Mermoz, Kitzberger*/
//    float prob_ignition=0.14; //con esta prob y T_RUN=5mil el fuego no llega a los bordes

 int valor_forest = ((d_forest[target_index] >= 3) && (d_forest[target_index] <= 5))? 1 : 0 ; // valores 3, 4 y 5 corresponden a tipos de bosque, el resto NO
     float prob_ignition=1.0 /(1 + exp(-(beta_cero +			// baseline ignition probability for matorral
 		    beta_forest   * valor_forest +													// forest
 		    beta_wind     * calculate_omega_value(d_wind, target_index, neighbor_index, filas, cols) +	// wind
 		    beta_aspect   * d_aspect[target_index] +		    // aspect ya transformado por direccion,humedad,pendiente
		    beta_slope    * calculate_sigma_value(d_slope, d_alti, target_index, neighbor_index) 		// slope
  		    ))) ;

    if(Kdebug/*&&prob_ignition>0.0000001f*/)
    {
      printf("Prob_igni=%f\n",prob_ignition);
      float auxs=(beta_cero +			// baseline ignition probability for matorral
  		    beta_forest   * valor_forest +													// forest
  		    beta_wind     * calculate_omega_value(d_wind, target_index, neighbor_index, filas, cols) +	// wind
  		    beta_aspect   * d_aspect[target_index] +		    // aspect ya transformado por direccion,humedad,pendiente
 		    beta_slope    * calculate_sigma_value(d_slope, d_alti, target_index, neighbor_index) 		// slope
      );
      //printf("Beta DEVICE %f %f %f %f %f \n",beta_cero,beta_forest,beta_wind,beta_aspect,beta_slope);
      printf("factors DEVICE %f %f %f %f \n",valor_forest,calculate_omega_value(d_wind, target_index, neighbor_index, filas, cols),d_aspect[target_index],calculate_sigma_value(d_slope, d_alti, target_index, neighbor_index) );
      printf("%f parcial %f %f %f %f %f\n",auxs,beta_cero,beta_forest  * valor_forest, beta_wind     * calculate_omega_value(d_wind, target_index, neighbor_index, filas, cols),beta_aspect   * d_aspect[target_index],beta_slope    * calculate_sigma_value(d_slope, d_alti, target_index, neighbor_index));
    }


    return prob_ignition;
}

__global__
void KERNEL_MapaProbaMaxIgnition(float * d_pmap, float * d_mapa, float * d_vege, float * d_forest, float * d_wind, float * d_aspect,
					  float * d_slope, float *d_alti, int filas, int cols,float beta_cero,
					  float beta_forest,float beta_aspect,float beta_slope, float beta_wind, DatosCelda * d_mapa_datos)
{
//Suponiendo que todas las celdas de alrededor tienen fuego cual es la probabilidad (Maxima) de que la celda central se queme
/*un thread para cada celda*/
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;

	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = get_index(fila, col, filas,cols);

	/* Test para ver si la coordenada del thread cae dentro de las celdas que actualizamos */
	int within_bounds = (fila < filas-1) && (col < cols-1) && (fila > 0) && (col>0);
	if (within_bounds)
	{
		int x=fila;
		int y=col;

		// reduzco las llamadas a get_index, lo calculo 1 vez y paso esto como parametro
		int target_index = get_index(x,y,filas,cols);
		int neighbor_index;
		//miro a mis 8 vecinos si no esta quemado
		float proba_noignition=1.0;
	  	for (int f = -1; f <= 1; f++)
	  	{
	   		for(int c = -1; c <= 1; c++)
	    		{
	      			neighbor_index = get_index(x+c, y+f, filas, cols);
	      			proba_noignition = proba_noignition*(1.0-IgnitionProbability(d_mapa, d_forest, d_wind, d_aspect, d_slope, d_alti, target_index, neighbor_index, filas, cols, beta_cero,
		      										 beta_forest, beta_aspect, beta_slope, beta_wind));
	    		}
	  	}
	   	d_pmap[target_index]=1.0-proba_noignition;
	}

}


/*********************************************************************/
/*********************************************************************/
/* Agregado por Moni */

/*
0	1	2
7	x 	3
6	5	4
*/

/* Por cada celda, se asume los vecinos numerados del 0..7 desde el -1,-1 en sentido del reloj.
   Con esto puedo determinar si el vecino es par o impar y a partir de ahi determinar si los
   vecinos +-1 son no combustible se asume que desde esa celda no se propaga. Esto es porque
   por la resolucion de los mapas, a veces quedan lineas de no combustible y el fuego se propaga
   igual. Las resoluciones trabajadas son 30m con lo que no deberia propagar. */
   __device__ int calcular_numero_vecino (int fila, int columna) {

   		/*Precondicion: nunca se llama con 0,0 por el if que hay en la fc
   		  llamadora */

   		int valor = -9;

   		valor = (fila == -1) ? 1 + columna : valor;

   		valor = ((valor == -9) && (fila == 1)) ? (5 - (columna)) : valor;

   		valor = ((valor == -9) && (fila == 0)) ? (5 - (2 * columna)) : valor;

   		return valor;

   		/* como es una funcion ejecutada por cada thread, para evitar saltos
   		 divergentes lo programo asi. */


   }

__device__ int puedeQuemar(int neighbor_index, int numVecino, int cols, float *d_vege) {


	int vecino1 = -99;
	int vecino2 = -99;


	// vecinos PARES
	vecino1 = (numVecino == 0)? neighbor_index + 1    : vecino1;
	vecino2 = (numVecino == 0)? neighbor_index + cols : vecino2;

	vecino1 = (numVecino == 2)? neighbor_index + cols : vecino1;
	vecino2 = (numVecino == 2)? neighbor_index - 1    : vecino2;//a estos no le da bola (numVecino=2)

	vecino1 = (numVecino == 4)? neighbor_index - cols : vecino1;
	vecino2 = (numVecino == 4)? neighbor_index - 1    : vecino2;

	vecino1 = (numVecino == 6)? neighbor_index - cols : vecino1;//a estos no le da bola (numVecino=6)
	vecino2 = (numVecino == 6)? neighbor_index + 1    : vecino2;


	// vecinos IMPARES
	/*vecino1 = (numVecino == 1)? neighbor_index + 1    : vecino1;
	vecino2 = (numVecino == 1)? neighbor_index - 1    : vecino2;

	vecino1 = (numVecino == 3)? neighbor_index + cols : vecino1;
	vecino2 = (numVecino == 3)? neighbor_index - cols : vecino2;

	vecino1 = (numVecino == 5)? neighbor_index - 1    : vecino1;
	vecino2 = (numVecino == 5)? neighbor_index + 1    : vecino2;

	vecino1 = (numVecino == 7)? neighbor_index - cols : vecino1;
	vecino2 = (numVecino == 7)? neighbor_index + cols : vecino2;*/

	//modificado por sigfrido, probando
	/*vecino1 = (numVecino == 1)? neighbor_index : vecino1;
	vecino2 = (numVecino == 1)? neighbor_index : vecino2;

	vecino1 = (numVecino == 3)? neighbor_index : vecino1;
	vecino2 = (numVecino == 3)? neighbor_index : vecino2;

	vecino1 = (numVecino == 5)? neighbor_index : vecino1;
	vecino2 = (numVecino == 5)? neighbor_index : vecino2;

	vecino1 = (numVecino == 7)? neighbor_index : vecino1;
	vecino2 = (numVecino == 7)? neighbor_index : vecino2;*/


	if(vecino1!=-99)
	{
		if((d_vege[vecino1] >= 0.0 && d_vege[vecino1] <=2.0) && (d_vege[vecino2] >= 0.0 && d_vege[vecino2] <=2.0)){return 0;}
		return 1;
	}
	else
	{
		return 1;
	}

}






/*se quema o no se quema la celda (x,y) en donde estoy parada?*/
__global__ void KERNEL_Fire_Propagation(float * d_mapa, float * d_vege, float * d_forest, float * d_wind, float * d_aspect,float * d_slope, float *d_alti, int filas, int cols,int semilla, float * d_mapanew,
		DatosCelda * d_mapa_datos, int *d_fuego_propaga, int verificar_propagacion, int paso_simulacion, curandState *my_curandstate,float beta_cero,float beta_forest,float beta_aspect,float beta_wind, float beta_slope)
{
	/*un thread para cada celda*/
	int y = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int x =  (blockIdx.x * blockDim.x) + threadIdx.x ;


	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = x+cols*y;//get_index(y, x, filas,cols);

		

	if ((x >= cols) || (y >= filas))
		return;

	/* Test para ver si la coordenada del thread cae dentro de las celdas que actualizamos */
	int within_bounds = (y < filas-1) && (x < cols-1) && (y > 0) && (x>0);
	if (within_bounds)
	{
		//	d_mapanew[index]= 1.0;
	//	return ;

		int numVecino;


    if(Kdebug && ::isnan(beta_cero)){
      printf("BETAS_KERNEL %f %f %f %f %f\n",beta_cero, beta_forest, beta_aspect, beta_wind, beta_slope);

    }


		// reduzco las llamadas a get_index, lo calculo 1 vez y paso esto como parametro
		int target_index = index;
		int neighbor_index;
		//miro a mis 8 vecinos si no esta quemado
		float prob_no_ignition = 1.0;
		//int DuracionFuego; //Duracion del fuego en pasos, definida segun tipo de vegetacion en Parameters.h
		//int tiempoEncendida =  0;  // VER ESTOOOOO //d_mapa_datos[get_index(x, y, filas,cols)].timeFireOn; //acumulo el tiempo que dura encendida una celda
		//Defino los tiempos de apagado de celdas sgun el tipo de vegetacion

		// obtengo la vegetacion que hay en la celda target
		float target_vege = d_vege[target_index];
		if(target_vege<=2.0){target_vege=0.0;}
		else if(target_vege<=5.0){target_vege=1.0;}
		else if(target_vege<=7.0){target_vege=2.0;}


		//DuracionFuego = (target_vege == 1.0) ? TIEMPO_MATORRAL : ((target_vege == 2.0) ? TIEMPO_BOSQUE : 0);

		// solo proceso celdas NO quemadas
		if(target_vege != 0.0) //la celda es combustible
		{
			if(d_mapa[target_index]==0.0) //la celda no esta quemada
			{
				int f,c;

				/*if(fila==6 && col==5)
				{
					printf();
				}*/

				// con los for de -1 a 1 reviso las 8 celdas que rodean a la celda target. Cubro todas las combinaciones de +1 y -1
				for (f = -1; f <= 1; f++)
				{
					for(c = -1; c <= 1; c++)
					{
						if (!(f==0 && c==0))  // si f==0 && c==0 es que estoy en la celda que analizo, no en una vecina
						{
						    neighbor_index = get_index( y+f,x+c, filas, cols);
						   	if(d_mapa[neighbor_index] >= 1.0)
							{

								// agrego para no quemar si la celda vecina evaluada tiene vecinos
								// diagonales no no combustible (el fuego salta barreras y no esta bien)
								numVecino = calcular_numero_vecino(f,c);
								//numVecino=0;

								// si numVecino es impar puede quemar, ya que no es diagonal al target
								//printf("VECINOS");
								if (puedeQuemar(neighbor_index, numVecino, cols, d_vege)) {

								    prob_no_ignition = prob_no_ignition *(1.0-IgnitionProbability(d_mapa, d_forest, d_wind, d_aspect,
										d_slope, d_alti, target_index, neighbor_index, filas, cols, beta_cero,  beta_forest, beta_aspect, beta_slope, beta_wind));
                    //prob_no_ignition=0.5f;
								    if(Kdebug){
								  //  printf("prob no ignition %f\n",prob_no_ignition);
								    //acumulo la probabilidad de que no se queme la celda en la que estoy
								    }
								}
							}
						}
					}
				}
				
				//if(prob_no_ignition==0)printf("proba no ingnic: %f \n", prob_no_ignition);
				float prob_total_ignition = 1.0 - prob_no_ignition; //proba que se queme=1-la proba de que no se queme


				if(prob_total_ignition == 0.0)  d_mapanew[target_index]= 0.0; //no quema
				else
				{


    			    float myrandf = curand_uniform(my_curandstate+index);
	
					if(myrandf < prob_total_ignition){
						// SE QUEMA LA CELDA HAGO TODO LO QUE TENGO QUE HACER
						d_mapanew[target_index]= (float)paso_simulacion;//ESTO QUEMA LA CELDA


						if ((verificar_propagacion) && (!d_fuego_propaga[0])) {
							//atomicAdd(&d_fuego_propaga[0],1);

						}

					//	d_mapa_datos[target_index].timeFireOn++;  // si se quema
					}
						//d_mapanew[target_index]= 1.0; //Para propagar siempre
				}
			}
			else {  // celda ya estaba quemada veo si la apago o no segun el tipo de vegetacion

				//d_mapanew[target_index] =   ( tiempoEncendida > DuracionFuego) ? -1.0 : 1.0;
				d_mapanew[target_index] =d_mapa[target_index];//COPIA LAS CELDAS QUEMADAS AL NUEVO MAPA

			//	d_mapa_datos[target_index].timeFireOn += (tiempoEncendida <= DuracionFuego) ? 1 : 0;

			}
		}
		else {
			d_mapanew[target_index]=0.0; //la celda seguira siendo no combustible
		}


	}else
	{
		d_mapanew[index] = 0.0;  // pongo 0s en los bordes??*/
	}


}

__global__ void KERNEL_Diferencia(float * d_mapaA,float * d_mapaB,float * d_mapaAmB, int filas, int cols)
{
	/*un thread para cada celda*/
	int y = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int x =  (blockIdx.x * blockDim.x) + threadIdx.x ;


	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = x+cols*y;

	if ((x >= cols) || (y >= filas))return;

	d_mapaAmB[index] = d_mapaA[index]-d_mapaB[index];  // calculo mapa diferencia
}


void Modelo_AutomataCelular::Fire_Propagation(int semilla, int *h_fuego_propaga, int *d_fuego_propaga, int verificar_propagacion)
{

	dim3 dimBlock(16,16);
	dim3 dimGrid((COLS + dimBlock.x-1) / dimBlock.x, (ROWS + dimBlock.y -1) / dimBlock.y,1);


	/* envio puntero a la poblacion e indice del individuo */
	KERNEL_Fire_Propagation<<<dimGrid, dimBlock>>>(d_terrain,d_vege, d_forest,d_wind,d_aspect,d_slope, d_alti, ROWS,COLS,semilla,d_terrainnew,
						       d_mapa_datos, d_fuego_propaga, verificar_propagacion, paso_simulacion, d_state, beta_cero, beta_forest, beta_aspect,beta_wind, beta_slope);


	checkCUDAError("Launch failure: 1 ");

	cudaDeviceSynchronize();

//if (verificar_propagacion)
	//	HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));



	//wrap raw pointers d_terrain and d_terrainnew to use with thrust
	thrust::device_ptr<float> d_terrainnew_ptr(d_terrainnew);
	thrust::device_ptr<float> d_terrain_ptr(d_terrain);

	try{
	    thrust::swap_ranges(d_terrainnew_ptr,d_terrainnew_ptr+(ROWS*COLS),d_terrain_ptr);
	}
	catch(thrust::system_error &e){
	  std::cout << "error calculando swap: " << e.what() << std::endl;
	}

};



// constructor vacio
Modelo_AutomataCelular::Modelo_AutomataCelular()
{
	// esto es para almacenar los historicos
	indice_historicos = -1; // para incrementar o decrementar y luego trabajar
	paso_simulacion=0;
	nuevomapa=0;
}

int map_sizeabc;

void Modelo_AutomataCelular::size(int filas_, int cols_)
{

	ROWS = filas_;
	COLS = cols_;

	map_size = ROWS * COLS * sizeof(float);
	map_sizeabc=map_size;

//	h_reference_map = (float *)malloc(sizeof(float) * filas * cols );

/*	h_reference_map= (float**)malloc(sizeof(float*)*CANT_MAPAS_REFERENCIA);
	for(int nref=0; nref<CANT_MAPAS_REFERENCIA;nref++) //voy a promediar sobre CANT_MAPAS_REFERENCIA mapas iniciales
	{
	  if (( h_reference_map[nref] = (float*)malloc(sizeof(float)*ROWS*COLS)) == NULL) {
	    printf("No aloca %d \n", nref);
	    exit (-1);
	  }
	}*/

	//h_pmap = (float *)malloc(sizeof(float) * filas * cols );

	// mapas en device


}

Modelo_AutomataCelular::Modelo_AutomataCelular(int filas_, int cols_)
{

	ROWS = filas_;
	COLS = cols_;

	int map_size = ROWS * COLS * sizeof(float);
	map_sizeabc=map_size;

	paso_simulacion=0;

	 // mapas en host
	//h_terrain = (float *)malloc(map_size);
	/*h_wind = (float*)malloc(map_size);
	h_aspect = (float *)malloc(map_size);
	h_slope = (float*) malloc(map_size);
	h_forest = (float *)malloc(map_size);   // ojo que hay un mapa "bosque" y un mapa "vegetacion"
	h_alti = (float *)malloc(map_size);
	h_vege = (float *)malloc(map_size);*/
//	h_reference_map = (float *)malloc(sizeof(float) * filas * cols );

/*	h_reference_map= (float**)malloc(sizeof(float*)*CANT_MAPAS_REFERENCIA);
	for(int nref=0; nref<CANT_MAPAS_REFERENCIA;nref++) //voy a promediar sobre CANT_MAPAS_REFERENCIA mapas iniciales
	{
	  if (( h_reference_map[nref] = (float*)malloc(sizeof(float)*ROWS*COLS)) == NULL) {
	    printf("No aloca %d \n", nref);
	    exit (-1);
	  }
	}*/

	//h_pmap = (float *)malloc(sizeof(float) * filas * cols );

	// mapas en device


}

void Modelo_AutomataCelular::alocar_compartidas(void)
{
	print_debug("entrando a alocar_compartidas() mapsize=%d\n",map_sizeabc);
	int map_size=map_sizeabc;
	print_debug("Mapsize=%d\n",map_size);

	HANDLE_ERROR(cudaMalloc((void**)&d_wind, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_aspect, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_slope, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_forest, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_alti, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_vege, map_size));
	HANDLE_ERROR(cudaMalloc((void**)&d_state, ROWS * COLS * sizeof(curandState)));

	print_debug("Alocado en GPU listo, ahora se alocará en CPU\n");

	 // mapas en host
	h_wind = (float*)malloc(map_size);
	h_aspect = (float *)malloc(map_size);
	h_slope = (float*) malloc(map_size);
	h_forest = (float *)malloc(map_size);   // ojo que hay un mapa "bosque" y un mapa "vegetacion"
	h_alti = (float *)malloc(map_size);
	h_vege = (float *)malloc(map_size);
	print_debug("Alocado en CPU listo.\n");

	if (!h_aspect || !h_wind || !h_forest || !h_slope || !h_alti || !h_vege) //
	{
		print_debug("Mapa Constructor: Error alocando matrices en host \n");
		exit(-1);
	}

	if (!d_wind || !d_aspect || !d_slope || !d_forest || !d_alti || !d_vege)
	{
		print_debug("Mapa Constructor: Error alocando matrices en device AB \n");
		exit(-1);
	}
	print_debug("OK\n");
}

void Modelo_AutomataCelular::alocar(void)
{

	int map_size=map_sizeabc;
	//printf("Mapsize=%d\n",map_size);

	HANDLE_ERROR(cudaMalloc((void**)&d_terrain, map_size));
	// solo en device, hace de mapa auxiliar para la actualización
	HANDLE_ERROR(cudaMalloc((void**)&d_terrainnew, map_size));

	h_terrain = (float *)malloc(map_size);


	/* Agrego aca el FLAG de fuego_propaga*/
	h_fuego_propaga = (int*)malloc(sizeof(int));
	HANDLE_ERROR(cudaMalloc((void**)&d_fuego_propaga, sizeof(int)));

	if (!h_terrain || !h_fuego_propaga) //
	{
		print_debug("Mapa Constructor: Error alocando matrices en host \n");
		exit(-1);
	}

	if (!d_terrain || !d_terrainnew || !d_fuego_propaga)
	{
		print_debug("Mapa Constructor: Error alocando matrices en device AB \n");
		exit(-1);
	}
}


// uso el mapa de referencia: h_reference_map, que fue o random o sintentico
void Modelo_AutomataCelular::Obtener_coordenadas_fuego_referencia(int nref)
{
   int f, c ;
   h_indicesfuegoRef.clear();
   for (f=0; f < ROWS; f++)
	   for(c=0; c < COLS; c++)
		   if (h_reference_map[nref][f*COLS + c] == 1.0 || h_reference_map[nref][f*COLS + c] == -1.0)
		   {
			   h_indicesfuegoRef.push_back(f*COLS+c);
		   }

//std::cout << "Num sitios quemados..."<<h_indicesfuegoRef.size()<< std::endl;
   if(h_indicesfuegoRef.size()==0)
	   std::cout << "no hay sitios quemados...exit!" << std::endl;exit(1);

}


void Modelo_AutomataCelular::Update_vege_map(float * newmap)
{
	CpyHostToDevice(d_vege, newmap, ROWS, COLS);
}

// Inicializa todos los mapas necesarios y el mapa inicial.
void Modelo_AutomataCelular::Init_All_Maps(int filas, int cols, char *WIND_MAP_P, char *ELEV_MAP_P, char *SLOPE_MAP_P, char *REAL_FIRE_MAP_P, char *ALTI_MAP_P, char *ASPECT_MAP_P, char *VEGE_MAP_P, char *FOREST_MAP_P)
{

  	ROWS = filas;
  	COLS = cols;
	// leo de disco todos los mapas que describen el medio ambiente
	//GetMap(VEGETATION_MAP, h_vege, filas, cols);
	//GetMap(VEGE_MAP_P, h_vege, filas, cols);
	for(int i=0;i<filas*cols;i++)
	{
		h_vege[i]=(float)Singleton::getInstance()->vegetacionSIM.h_map[i];
		/*if(h_vege[i]<0.0f || h_vege[i]>2.0f)
		{
			print_debug("%.01f\n",h_vege[i]);
		}*/
		//h_vege[30]=2.0f;
	}

	print_debug("Init all maps start:...");
	CpyHostToDevice(d_vege, h_vege, filas, cols);

    /* Inicializo el arreglo d_state con los seeds para curand. Lo pongo aca que esto se ejecuta solo 1 vez. */
    dim3 dimBlock1(16,16);
    dim3 dimGrid1((cols + dimBlock1.x-1) / dimBlock1.x, (filas + dimBlock1.y -1) / dimBlock1.y,1);
    setup_kernel<<<dimGrid1,dimBlock1>>>(d_state, (cols + dimBlock1.x-1) / dimBlock1.x);


       // leo mapas en cpu y paso a gpu MAPA de VIENTO
	GetMap(WIND_MAP_P, h_wind, filas, cols);
	CpyHostToDevice(d_wind, h_wind, filas, cols);

	// leo mapas en cpu y paso a gpu MAPA de COMBUSTIBLE
	GetMap(FOREST_MAP_P, h_forest, filas, cols);
	CpyHostToDevice(d_forest, h_forest, filas, cols);

	// leo mapas en cpu y paso a gpu MAPA de ORIENTACION DE LA PENDIENTE
	GetMap(ASPECT_MAP_P, h_aspect, filas, cols);
	CpyHostToDevice(d_aspect, h_aspect, filas, cols);

	// leo mapas en cpu y paso a gpu MAPA de PENDIENTE
	GetMap(SLOPE_MAP_P, h_slope, filas, cols);
	CpyHostToDevice(d_slope, h_slope, filas, cols);

	// leo mapas en cpu y paso a gpu MAPA de ALTITUD
	GetMap(ALTI_MAP_P, h_alti, filas, cols);
	CpyHostToDevice(d_alti, h_alti, filas, cols);

	dim3 dimBlock(NTHREADS_X, NTHREADS_Y);
	dim3 dimGrid(COLS / NTHREADS_X, ROWS / NTHREADS_Y);

	//cudaDeviceSynchronize();

	KERNEL_calculate_psi_value<<<dimGrid, dimBlock>>>(d_aspect, d_slope,filas,cols);
	checkCUDAError("Launch failure Q: ");

	/*Inicializo mapa de probabilidades en cero*/
	/*for (int fi = 0; fi < ROWS; fi++)
		for(int col = 0; col < COLS; col++)
		    h_pmap[fi * COLS + col] = 0.0; //inicializo todas las probabilidades en cero
	 */

	//CpyHostToDevice(d_pmap, h_pmap, filas, cols);
	print_debug("DONE\n");

}



void Modelo_AutomataCelular::Inicializar_datos_celda(DatosCelda *h_mapa_datos)
{
	for (int fi = 0; fi < ROWS; fi++)
	 	for(int col = 0; col < COLS; col++)
        	  h_mapa_datos[fi * COLS + col].timeFireOn = 0.0; //inicializo todos los tiempos en cero

}



void Modelo_AutomataCelular::Init_Initial_Map(int *x, int *y) //Esta funcion se refiere a la inicializacion de las SIMULACIONES
{
	// MAPA INICIAL EN h_terrain HAY DOS POSIBILIDADES: son excluyentes!
	//las simulaciones comienzan desde un punto fijo determinado (XIGNI,YIGNI) o comienzan desde un punto elegido aleatoriamente dentro del area quemada

	//if (IGNICION_SIMS_DETERMINISTA)   // el fuego comienza en un punto conocido (en parameters.h)
	//	inicializar_Mapa_Determinista(h_terrain, h_vege, ROWS, COLS,x,y);
	//else  // se inicializa un mapa random pero dentro de los puntos quemados en el de referencia
	//	inicializar_Mapa_Random(h_terrain, h_vege, ROWS, COLS, h_indicesfuegoRef,x,y);
	// por el momento no, ya que no tengo mapa de referencia

	//Inicializar_datos_celda(h_mapa_datos);

   //	HANDLE_ERROR(cudaMemcpy(d_mapa_datos,h_mapa_datos,sizeof(DatosCelda)*filas*cols, cudaMemcpyHostToDevice));

	//Set_Pixel((*x)+COLS*(*y));

	memset(h_mapa_aux, 0.0, sizeof(float)*ROWS*COLS);
	memset(h_terrain, 0.0, sizeof(float)*ROWS*COLS);

	CpyHostToDevice(d_terrain, h_terrain, ROWS, COLS);
	CpyHostToDevice(d_terrainnew, h_terrain, ROWS, COLS);


	//CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
	//Almacenar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);
	paso_simulacion=0;

	
	//aca hay que cargarle un mapa que no tenga ninguna celda quemada
	//indice_historicos++;
	Histo.init(COLS,ROWS,1);
	Histo.save_new_map(d_terrainnew,paso_simulacion);

}




int Modelo_AutomataCelular::celda_quemable(float *h_vege, int celdax, int celday)
{
	return (h_vege[celday * COLS + celdax] == 1.0);
}



// quema una celda para iniciar la quema del mapa sintetico de referencia
void Modelo_AutomataCelular::Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y, int nref)
{
	if (IGNICION_REFERENCIA_DETERMINISTA)
	{
		// inicializar el punto de ignición usando XIGNI YIGNI
		inicializar_Mapa_Determinista(h_reference_map[nref], h_vege, ROWS, COLS,x,y);

	}
	else {
		// random x e y y le pongo 1.0 en esa celda
		int celdax, celday;

		srand(time(NULL));
		celdax = rand() % COLS;
		celday = rand() % ROWS;

		while (!celda_quemable(h_vege, celdax, celday)) {
			celdax = rand() % COLS;
			celday = rand() % ROWS;
		}


		for (int fi = 0; fi < ROWS; fi++)
		{
			for(int col = 0; col < COLS; col++){
				h_reference_map[nref][fi * COLS + col] = ((celday == fi) && (celdax == col)) ? 1.0: 0.0;
			}
		}

		// seteo los valores de la celda de inicio ya que los necesito fuera
		*x = celdax;
		*y = celday;
	}
}









// Copy Functions

/* transfer from CPU to GPU memory */
void Modelo_AutomataCelular::CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols )
{
     HANDLE_ERROR(cudaMemcpy(d_mapa,h_mapa, sizeof(float)*filas*cols, cudaMemcpyHostToDevice));
}


/* transfer from GPU to CPU memory */
void Modelo_AutomataCelular::CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols )
{
    HANDLE_ERROR(cudaMemcpy(h_mapa, d_mapa, sizeof(float)*filas*cols, cudaMemcpyDeviceToHost));
}



/* ENTRADA Y SALIDA DE MAPAS */
//void Mapa::GetMap(const char * nombre, float *mapa, int filas, int cols)
void Modelo_AutomataCelular::GetMap(char * nombre, float *mapa, int filas, int cols)
{
	FILE *input = fopen(nombre,"r");

	if (input == NULL)
	{
		print_debug("Get Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j, dato;

	// son archivos raster con 6 lineas de metadata
	char line[MAX];
	char identif[24];
	int rfilas,rcols;

	fscanf(input,"%s%d",identif,&rcols);
	fscanf(input,"%s%d",identif,&rfilas);

	print_debug("Cargado archivo GIS %s con ALTO=%d ANCHO=%d\n",nombre,filas,cols);
	if(rfilas!=filas || rcols!=cols)
	{
		print_debug("Get Map error: el archivo %s es de otro tamaño diferente a (%d,%d) \n", nombre,filas,cols);
		exit(-1);
	}

	//mapa = (float*)malloc(sizeof(float)*filas*cols);

    //fscanf(input,"%s",identif,cols);

	for(i=0; i < 4; i++) { //aca deberia leer solo 4 lineas, pero si le pongo 4 lee mal (revisar)
		fgets(line, MAX, input);
		//printf("%s",line);
	}
	int nodata=-9999;
	fscanf(input,"%s%d",identif,&nodata);
	//printf("NODATA=%d\n",nodata);

	for(i = 0; i < (filas); i++)
	{
		for(j = 0; j < (cols); j++)
		{
			fscanf(input, "%d", &dato);
			if(dato==nodata){dato=0;}
			mapa[i*(cols) + j] = (float)dato;
			//mapa[i*(cols) + j] = 0.0f;
			//printf("%.01f ",(float)dato);
		}

	}

	fclose(input);

}






void Modelo_AutomataCelular::SaveMap(char * nombre, float *mapa, int filas, int cols)
{
	FILE *output = fopen(nombre,"w");

	if (output == NULL)
	{
		print_debug("Save Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	if (IMPRIMIR)
		print_debug("Writing final map: %s \n", nombre);

	int i,j;
	float dato;
	for(i = 0; i < filas; i++) {
		for(j = 0; j < cols; j++) {
			dato = mapa[i*cols + j];
			fprintf(output,"%.2f ",dato);
		}
		fprintf(output, "\n");
	}
	fclose(output);

}




void Modelo_AutomataCelular::PrintParamsRef(char * nombre,int x,int y)
{

	FILE *output = fopen(nombre,"w");

        if (output == NULL)
	{
		print_debug("SaveList error: no abre archivo %s \n", nombre);
		exit(-1);
	}


	fprintf(output,"MAPA de REFERENCIA\n");
	fprintf(output,"%s %s %s %s %s %s %s\n", "beta_cero","beta_forest", "beta_aspect","beta_slope","beta_wind", "xinicial","yinicial");
 	fprintf(output," %f %f %f %f %f %d %d\n",beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind,x,y);


	fclose(output);

}




/*para imprimir los mapas de probabilidad en funcion dl tiempo*/
void Modelo_AutomataCelular::print_probability_progress(int iteration)
{
    char nombre1[] = "proba";
    char vector[5], vector2[5] ;
    int temporal, iteration2 = iteration;
    int Base = 48,  cont=0, i, j;

    // armo un vector INVERTIDO con los numeros, caracteres
    for(i=0; iteration2!=0; i++){
      temporal = iteration2%10;
      iteration2 = (iteration2-temporal)/10;
      vector[i]= (char)(temporal+Base);
      cont++;
    }
    for (i = cont; i < 4; i++)
      vector[i] = '0';
    // invierto el vector
    for (j = 0; j < 4 ; j++)
      vector2[j] = vector[4-1-j];

    vector2[4] = '\0';
    strcat(nombre1, vector2);


    // tengo que traer desde device a host d_pmap
    CpyDeviceToHost(h_pmap, d_pmap, ROWS, COLS);

    // le pego al nombre creado el nombre del directorio donde quiero los mapas almacenados
    char nombre2[] = DIR_FIRE_PROGRESS;
    strcat(nombre2, nombre1);
    SaveMap(nombre2,h_pmap, ROWS, COLS);

}


/* para obtener las distintas lineas de avance del fuego */
void Modelo_AutomataCelular::print_fire_progress(int iteration)
{
    char nombre1[] = "mapa";
    char vector[5], vector2[5] ;
    int temporal, iteration2 = iteration;
    int Base = 48,  cont=0, i, j;

    // armo un vector INVERTIDO con los numeros, caracteres
    for(i=0; iteration2!=0; i++){
      temporal = iteration2%10;
      iteration2 = (iteration2-temporal)/10;
      vector[i]= (char)(temporal+Base);
      cont++;
    }
    for (i = cont; i < 4; i++)
      vector[i] = '0';
    // invierto el vector
    for (j = 0; j < 4 ; j++)
      vector2[j] = vector[4-1-j];

    vector2[4] = '\0';
    strcat(nombre1, vector2);


    // tengo que traer desde device a host d_terrain
    CpyDeviceToHost(h_terrain, d_terrain, ROWS, COLS);

    // le pego al nombre creado el nombre del directorio donde quiero los mapas almacenados
    char nombre2[] = DIR_FIRE_PROGRESS;
    strcat(nombre2, nombre1);
    SaveMap(nombre2,h_terrain, ROWS, COLS);

}

void Modelo_AutomataCelular::Set_Pixel(int celda)
{
	if(h_vege[celda]<=2.0f)return;
	h_mapa_aux[celda]=(float)paso_simulacion;
	h_terrain[celda]=(float)paso_simulacion;
	CpyHostToDevice(d_terrain, h_mapa_aux, ROWS, COLS);
	CpyHostToDevice(d_terrainnew, h_mapa_aux, ROWS, COLS);
	//Histo.add_pixel(celda,0);
	//h_mapas_historicos[indice_historicos-1].push_back(celda);
	//memcpy(h_mapa_aux,h_terrain,sizeof(float)*ROWS*COLS);
	nuevomapa=1;

}

void Modelo_AutomataCelular::Evaluar_Individuo_INIT(int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{
	//h_fuego_propaga = (int*)malloc(sizeof(int));
	//HANDLE_ERROR(cudaMalloc((void**)&d_fuego_propaga, sizeof(int)));
	h_mapa_aux = (float*)malloc(sizeof(float) * ROWS * COLS);

	if (!h_fuego_propaga || !d_fuego_propaga || !h_mapa_aux) {

		print_debug("Error con fuego propaga \n");
		exit (-1);
	}
	if (verificar_propagacion) {
		h_fuego_propaga[0] = 1;
		HANDLE_ERROR(cudaMemcpy(d_fuego_propaga, h_fuego_propaga, sizeof(int), cudaMemcpyHostToDevice));
	}

}



void Modelo_AutomataCelular::Evaluar_Individuo_FW(int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{
	
	Histo.save_new_map(d_terrainnew,paso_simulacion);

	while (true) {

        	paso_simulacion++;

			verificar_propagacion = (paso_simulacion % FUEGO_PROPAGA_NUMERO) == 0;

			/*El primer arg es para que no sean iguales las sims*/
			Fire_Propagation(paso_simulacion, h_fuego_propaga, d_fuego_propaga, verificar_propagacion);

			// verifico si hay que almacenar el paso en historicos
			if (paso_simulacion % paso_historicos == 0) {
				CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
				//print_vector(h_mapas_historicos,indice_historicos);
				//Almacenar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);
				indice_historicos++;
				//printf("indice: %d, cant celdas: %d paso simulacion: %d \n", indice_historicos, (int)h_mapas_historicos[indice_historicos].size(), paso_simulacion);
				nuevomapa=1;
				break;
			}

			/*if (verificar_propagacion) {
				HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));
			}*/

	}
	//printf("Paso result: %d\n",paso_simulacion);

	
}

void Modelo_AutomataCelular::Evaluar_Individuo_FW_noh(int indice, int flag_imprimir_mejor_mapa, int numero_generacion,int paso_final)
{
	/*if(paso_simulacion==0){
		indice_historicos=0;
		int x,y;
		Init_Initial_Map(&x, &y);
		//memset(h_mapa_aux, 0.0, sizeof(float)*ROWS*COLS);
		//CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
		Almacenar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);
		//aca hay que cargarle un mapa que no tenga ninguna celda quemada
	}*/
	while (true) {
        	paso_simulacion++;
			//verificar_propagacion = (paso_simulacion % FUEGO_PROPAGA_NUMERO) == 0;
			/*El primer arg es para que no sean iguales las sims*/
			Fire_Propagation(paso_simulacion, h_fuego_propaga, d_fuego_propaga, verificar_propagacion);
			// verifico si hay que almacenar el paso en historicos
			if (paso_simulacion >=paso_final) {
				//indice_historicos++;
				//CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
				CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
				//Almacenar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);
				nuevomapa=1;
				break;
			}
	}

}



void Modelo_AutomataCelular::Evaluar_Individuo_BW(int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{
	if(paso_simulacion==0){return;}

	//memset(h_mapa_aux, 0.0, sizeof(float)*ROWS*COLS);

	//indice_historicos--;

	//printf("BW indice: %d, cant celdas: %d paso simulacion: %d \n", indice_historicos, (int)h_mapas_historicos[indice_historicos].size(), paso_simulacion);
	//Recuperar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos-1,paso_simulacion);
	//print_vector(h_mapas_historicos,indice_historicos);
	//CpyHostToDevice(d_terrain, h_mapa_aux, ROWS, COLS);
	//CpyHostToDevice(d_terrainnew, h_mapa_aux, ROWS, COLS);

	Histo.step_BW(paso_simulacion,h_mapa_aux,d_terrainnew,d_terrain);


	nuevomapa=1;
}

void Modelo_AutomataCelular::Evaluar_Individuo_A_CERO(int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{

	//while (true) {

    		if(paso_simulacion==0){return;}
        	//paso_simulacion--;

			//verificar_propagacion = (paso_simulacion % FUEGO_PROPAGA_NUMERO) == 0;

			// verifico si hay que almacenar el paso en historicos
			//if (paso_simulacion % paso_historicos == 0 || paso_simulacion==0) {

				memset(h_mapa_aux, 0.0, sizeof(float)*ROWS*COLS);

				indice_historicos--;
				while(indice_historicos>=0)
				{
					h_mapas_historicos[indice_historicos].clear();
					indice_historicos--;
				}
				paso_simulacion=h_historicos_paso[0];
				//Recuperar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);

				CpyHostToDevice(d_terrain, h_mapa_aux, ROWS, COLS);

				//printf("indice: %d, cant celdas: %d paso simulacion: %d \n", indice_historicos, (int)h_mapas_historicos[indice_historicos].size(), paso_simulacion);

				nuevomapa=1;
				//break;

			//}


			/*if (verificar_propagacion) {
				HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));
			}*/

	//}

}





void Modelo_AutomataCelular::Evaluar_Individuo(int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{

	/* intero con un individuo y propago en todo el mapa */
	int paso_simulacion = 0;
	int verificar_propagacion = 1;
	char opcion;

	/* flag para parar de iterar si el fuego no propaga */
	/*int *h_fuego_propaga = (int*)malloc(sizeof(int));
	int *d_fuego_propaga;
	HANDLE_ERROR(cudaMalloc((void**)&d_fuego_propaga, sizeof(int)));*/

	// esto es para almacenar los historicos
	float *h_mapa_aux = (float*)malloc(sizeof(float) * ROWS * COLS);


	if (!h_fuego_propaga || !d_fuego_propaga || !h_mapa_aux) {

		print_debug("Error con fuego propaga \n");
		exit (-1);
	}

  ///nuevo agregado para que vuelva a inicializar el mapa fuera del loop en main, sino propagaba del ultimo mapa y no dan bien las sims
/*	if(flag_imprimir_mejor_mapa){
	 int x = XIGNI;
	 int y = YIGNI;
	 Init_Initial_Map(&x,&y);
	}*/

	if (verificar_propagacion) {
		h_fuego_propaga[0] = 1;
		HANDLE_ERROR(cudaMemcpy(d_fuego_propaga, h_fuego_propaga, sizeof(int), cudaMemcpyHostToDevice));
	}

	indice_historicos = -1; // para incrementar o decrementar y luego trabajar
	paso_historicos =  1;  //es la cantidad de simulaciones entre cada almacenamiento de los historicos
	// modificar paso_historicos!!! le pongo 10 por poner algo

	while ((paso_simulacion < T_RUN) && (h_fuego_propaga[0]) ) {

        	paso_simulacion++;

			verificar_propagacion = (paso_simulacion % FUEGO_PROPAGA_NUMERO) == 0;

			/*El primer arg es para que no sean iguales las sims*/
			Fire_Propagation(paso_simulacion, h_fuego_propaga, d_fuego_propaga, verificar_propagacion);

			// verifico si hay que almacenar el paso en historicos
			if (paso_simulacion % paso_historicos == 0) {

				// interaccion con el ususario

				scanf("%c", &opcion);
				fflush(stdin);
				getchar(); // para absorver el enter sino lee 2 char!

				print_debug("paso simulacion: %d opcion: %c \n", paso_simulacion, opcion);

				memset(h_mapa_aux, 0.0, sizeof(float)*ROWS*COLS);

				switch (opcion) {
					case 'n': //NEXT
							indice_historicos++;

							CpyDeviceToHost(h_mapa_aux, d_terrainnew, ROWS, COLS);
							Almacenar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);



							print_debug("indice: %d, cant celdas: %d paso simulacion: %d \n", indice_historicos, (int)h_mapas_historicos[indice_historicos].size(), paso_simulacion);

							CpyDeviceToHost(h_terrain, d_terrainnew, ROWS, COLS);
							SaveMap((char*)"mapa_historico.map",h_terrain, ROWS, COLS);
							break;

					case 'p':  // PREVIOUS
							indice_historicos--;
							Recuperar_Historico_Indice(h_mapa_aux, ROWS, COLS, h_mapas_historicos, indice_historicos,paso_simulacion);

							//CpyHostToDevice(d_terrainnew, h_mapa_aux, ROWS, COLS);
							CpyHostToDevice(d_terrain, h_mapa_aux, ROWS, COLS);

							//paso_simulacion = paso_simulacion-100;
							print_debug("indice: %d, cant celdas: %d paso simulacion: %d \n", indice_historicos, (int)h_mapas_historicos[indice_historicos].size(), paso_simulacion);

							//CpyDeviceToHost(h_terrain, d_terrainnew, ROWS, COLS);
    						SaveMap((char*)"mapa_historico.map",h_mapa_aux, ROWS, COLS);

    						//paso_simulacion = paso_simulacion - paso_historicos;

							break;
					default:
							print_debug("salgo opcion: %c \n", opcion);
							exit(-1);
							break;
				}
			}


			if (verificar_propagacion) {
				HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));
			}

	}


	if (flag_imprimir_mejor_mapa) {
		CpyDeviceToHost(h_terrain, d_terrain, ROWS, COLS);
    	SaveMap((char*)"mapa_final.map",h_terrain, ROWS, COLS);
	}

	/*prueba de que queda en historicos */
	for (int caca = 0; caca < indice_historicos; caca++) {
		print_debug("cantidad de historicos: %d y mapa %d tiene %d celdas \n", indice_historicos, caca, (int)h_mapas_historicos[caca].size());


	}

	free(h_fuego_propaga);
	free(h_mapa_aux);
	cudaFree(d_fuego_propaga);

}






/* Almacena desde un mapa un vector con las celdas quemadas */
int Modelo_AutomataCelular::Almacenar_Historico_Indice(float *h_mapa_aux, int rows, int cols, vector< pair<int,int> > h_mapas_historicos[1000], int indice_historicos,int paso) {

	/*h_mapas_historicos[indice_historicos].clear();

	for (int i = 0; i < rows*cols; i++)
		if ((h_mapa_aux[i] == 1.0f) || (h_mapa_aux[i] == -1.0f)) {
			h_mapas_historicos[indice_historicos].push_back(i);
		}
	h_historicos_paso[indice_historicos]=paso;*/


	return 0;
}





/* Recupera desde un mapa historico, arma el mapa de ROWS x COLS */
int Modelo_AutomataCelular::Recuperar_Historico_Indice(float *h_mapa_aux, int rows, int cols, vector< pair <int,int> > h_mapas_historicos[1000], int indice_historicos, int& paso) {

	int i, celda;
	//printf("I=%d\n",indice_historicos);
	for (i = 0; i < h_mapas_historicos[indice_historicos].size(); i++) {

		celda = h_mapas_historicos[indice_historicos][i].first;
		h_mapa_aux[celda] = (float)h_mapas_historicos[indice_historicos][i].second;
	}
	paso=h_historicos_paso[indice_historicos];
	h_mapas_historicos[indice_historicos+1].clear();
	return 0;
}

void mifree(float * ptr)
{
	if(ptr!=NULL){free(ptr);ptr=NULL;}
}

void micudafree(float * ptr)
{
	if(ptr!=NULL){cudaFree(ptr);ptr=NULL;}
}

void print_vector(vector< pair<int,int> > h_mapas_historicos[1000], int indice_historicos)
{
	int i;
	//int celda;
	for(int q=0;q<=indice_historicos;q++)
 	{
 		printf("%d %lu\n",q,h_mapas_historicos[q].size());
		for (i = 0; i < h_mapas_historicos[q].size(); i++) {
			//celda = h_mapas_historicos[indice_historicos][i];
		}
	}
	printf("-------------\n");
}

//Inicio el fuego en UN punto incendiable hveg=1 o hveg=2 lo mas cercano posible al definido en Parameters.h como (XIGNI,YIGNI)
void inicializar_Mapa_Determinista(float *h_terrain, float *h_vege, int filas, int cols,int *x,int *y)
{
	int f,c;
	// elijo valores al azar hasta caer en una celda quemable

	c = Singleton::getInstance()->XIGNI;
	f = Singleton::getInstance()->YIGNI;

	//en caso en que la celda no sea quemable me muevo en diagonal hasta encontrar una quemable
  	while (h_vege[f*cols+c] == 0.0) {
		f = f+1;
		c = c+1;
	}//modificar esto para que no propague y avise al usuario

	for (int fi = 0; fi < filas; fi++) {
		for(int col = 0; col < cols; col++){
			h_terrain[fi * cols + col] = ((fi == f) && (col == c)) ;
		}
	}
	*x = c;
	*y = f;
}


// inicializa el mapa con una celda quemada aleatoria PERO dentro del area quemada del mapa de referencia (sea real o simulada)

int inicializar_Mapa_Random(float *h_terrain, float *h_vege, int filas, int cols, std::vector<int> &h_indicesfuegoRef,int *x, int *y)
{
	srand(time(NULL));
	// elijo valores al azar hasta caer en una celda quemable PERO dentro del area quemada en el mapa de referencia.
	int unodelosquemados=h_indicesfuegoRef[rand() % h_indicesfuegoRef.size()];
	while (h_vege[unodelosquemados] == 0.0)
	{
		unodelosquemados = h_indicesfuegoRef[rand() % h_indicesfuegoRef.size()];
	}

	for (int fi = 0; fi < filas; fi++){
		for(int col = 0; col < cols; col++){
			h_terrain[fi * cols + col] = (fi * cols + col==unodelosquemados) ? 1.0: 0.0;
			// seteo los valores de la celda de inicio ya que los necesito fuera

			if(fi * cols + col==unodelosquemados)
			{
				*x = fi;
				*y = col;
			}
		}
	}
	return 1;
}

// esta funcion se llama cuando se lee del archivo el fuego inicial, entonces lo proceso
// para obtener las coordenadas del fuego.

int Obtener_coordenadas_fuego_inicial(float *h_terrain, int filas, int cols, int *filaIni, int *colIni)
{
	int f, c;
	int flag = 1;

	for (f=0; f < filas; f++) {
		for(c=0; c < cols; c++)	{
			if (h_terrain[f*cols + c] == 1.0)
			{
				*filaIni = f;
				*colIni = c;
				flag = 0;
				break;
			}
		}
		if (!flag)
			break;  // sale si salimos del for anterior por !flag
	}

	return 0;
}


void Mapa_Diferencia(float * d_mapaA,float * d_mapaB,float * d_mapaAmB,int ROWS, int COLS)
{
	dim3 dimBlock(16,16);
	dim3 dimGrid((COLS + dimBlock.x-1) / dimBlock.x, (ROWS + dimBlock.y -1) / dimBlock.y,1);
	/* envio puntero a la poblacion e indice del individuo */
	//printf("%d %d %d %d %d\n",d_mapaA,d_mapaB,d_mapaAmB,ROWS,COLS);
	KERNEL_Diferencia<<<dimGrid, dimBlock>>>(d_mapaA,d_mapaB, d_mapaAmB,ROWS,COLS);
	checkCUDAError("Launch failure: 1 ");
	cudaDeviceSynchronize();
}

void Modelo_AutomataCelular::liberar_compartidas()
{
	print_debug("AutomataCelular liberando compartidas..");fflush(stdout);
	int e=1;

    micudafree(d_wind);print_debug("%d,",e);fflush(stdout);e++;
    micudafree(d_aspect);print_debug("%d,",e);fflush(stdout);e++;
    micudafree(d_slope);print_debug("%d,",e);fflush(stdout);e++;
    micudafree(d_forest);print_debug("%d,",e);fflush(stdout);e++;
    micudafree(d_alti);print_debug("%d,",e);fflush(stdout);e++;
    micudafree(d_vege);print_debug("%d,",e);fflush(stdout);e++;
    micudafree((float*)d_state);print_debug("%d,",e);fflush(stdout);e++;

    mifree(h_wind);print_debug("%d,",e);fflush(stdout);e++;
    mifree(h_aspect);print_debug("%d,",e);fflush(stdout);e++;
    mifree(h_slope);print_debug("%d,",e);fflush(stdout);e++;
    mifree(h_forest);print_debug("%d,",e);fflush(stdout);e++;
    mifree(h_alti);print_debug("%d,",e);fflush(stdout);e++;
    mifree(h_vege);print_debug("%d,",e);fflush(stdout);e++;
}


// Destructor
Modelo_AutomataCelular::~Modelo_AutomataCelular()
{
	print_debug("LLAMADO AL DESTRUCTOR DE Modelo_AutomataCelular %d\n",indice_matriz);
	print_debug("liberando..");fflush(stdout);
	int e=1;

	micudafree(d_terrain);print_debug("%d,",e);fflush(stdout);e++;
	micudafree(d_terrainnew);print_debug("%d,",e);fflush(stdout);e++;
	micudafree((float*)d_fuego_propaga);print_debug("%d,",e);fflush(stdout);e++;

	mifree(h_terrain);print_debug("%d,",e);fflush(stdout);e++;
	mifree((float*)h_fuego_propaga);print_debug("%d,",e);fflush(stdout);e++;

	Histo.destruir();

    print_debug("Listo.\n");
}
