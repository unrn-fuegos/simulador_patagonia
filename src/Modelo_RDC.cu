//#pragma once
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <math.h>   /*math functions stl library*/
#include <algorithm>  /* std::min_element, std::max_element*/
#include <curand.h>
#include <curand_kernel.h>

//#include "Parameters.h"
#include "Modelo_RDC.h"


//#pragma once
//#include "Individuo.h"
#include <vector>
#include <thrust/device_vector.h>
#include "Parameters.h"


/*#include <thrust/swap.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>*/
#include "curso.h"


#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

//#include "cortafuego.h"

extern int verificar_propagacion;
extern char opcion;

extern int paso_historicos;


#define MAX 100

#define WITHIN_BOUNDS( x ) ( x > -1 ) // por como está la función get_index, si no esta entre filas y cols retrona -1


#include "Singleton.h"

#define SEED 1332277027LLU   /*semilla para Philox*/

#define gl_debug 0

GLuint Modelo_RDC::createTextureAttachment(int width, int height, int nro) {
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height,
		0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+nro, texture, 0);
	return texture;
}

GLuint Modelo_RDC::createDepthTextureAttachment(int width, int height) {
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height,
		0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
	return texture;
}

GLuint Modelo_RDC::createDepthBufferAttachment(int width, int height) {
	GLuint depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	return depthBuffer;
}

void Modelo_RDC::generate_fbo(void)
{
	glGenFramebuffers(1, &myFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, myFBO);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	mytexture = createTextureAttachment(cols, filas,0);
	//img2d.refldepthtexture = createDepthTextureAttachment(water_resolution, water_resolution);


	glGenFramebuffers(1, &myFBO2);
	glBindFramebuffer(GL_FRAMEBUFFER, myFBO2);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	mytexture2 = createTextureAttachment(cols, filas,0);
}

void Modelo_RDC::gl_error(void)
{
	if (gl_debug)
	{
		int ret = glGetError();
		switch (ret)
		{
		case GL_NO_ERROR: break;
		case GL_INVALID_ENUM: printf("GL_INVALID_ENUM"); break;
		case GL_INVALID_VALUE: printf("GL_INVALID_VALUE"); break;
		case GL_INVALID_OPERATION: printf("GL_INVALID_OPERATION"); break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: printf("GL_INVALID_FRAMEBUFFER_OPERATION"); break;
		case GL_OUT_OF_MEMORY: printf("GL_OUT_OF_MEMORY"); break;
		case GL_STACK_UNDERFLOW: printf("GL_STACK_UNDERFLOW"); break;
		case GL_STACK_OVERFLOW: printf("GL_STACK_OVERFLOW"); break;
		default:printf("Otro error!!!"); break;
		}
		if (ret != GL_NO_ERROR) {
			printf("\n");
		}
	}
}

Modelo_RDC::Modelo_RDC()
{
    
}

void Modelo_RDC::atras(void){
	if(Singleton::getInstance()->Simulador.paso_simulacion==0)return;

	Histo.step_BW(Singleton::getInstance()->Simulador.paso_simulacion,texIS,NULL,d_terrainnew4);
	memcpy(floattex1,texIS,sizeof(float)*4*COLS*ROWS);
	Update_texIS_map();
}

void Modelo_RDC::paso_adelante(void){


	HANDLE_ERROR(cudaMemcpy(d_terrainnew4, texIS, sizeof(float)*COLS*ROWS*4, cudaMemcpyHostToDevice));
	Histo.save_new_map(d_terrainnew4,Singleton::getInstance()->Simulador.paso_simulacion);

	for (int u = 0;u < 50;u++)
	{
		Singleton::getInstance()->Simulador.paso_simulacion++;
		glBindTexture(GL_TEXTURE_2D, 0);//To make sure the texture isn't bound
		glBindFramebuffer(GL_FRAMEBUFFER, myFBO);
		glViewport(0, 0, cols, filas);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);

		//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mytexture2, 0);

		//Acá dibujar al framebuffer

		glUseProgram(mkariprogram.programID);
		glUniform1i(glGetUniformLocation(mkariprogram.programID, "tex0"), 0);
		glUniform1i(glGetUniformLocation(mkariprogram.programID, "tex1"), 1);
		glUniform1i(glGetUniformLocation(mkariprogram.programID, "tex2"), 2);
		glUniform1i(glGetUniformLocation(mkariprogram.programID, "tex3"), 3);

		gl_error();
		glBindVertexArray(Gvao);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GTtID);
		gl_error();
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GtexIS);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, GtexSlAsWiFo);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, GtexAlReVe);

		gl_error();
		glDrawElements(GL_TRIANGLES, g_icant, GL_UNSIGNED_INT, 0);
		gl_error();

		/*if(Singleton::gI()->save_folder_name!=NULL)
		{
			SaveTextureMaps();
		}*/




		glBindTexture(GL_TEXTURE_2D, 0);//To make sure the texture isn't bound
		glBindFramebuffer(GL_FRAMEBUFFER, myFBO2);
		glViewport(0, 0, cols, filas);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);

		//Acá dibujar al framebuffer para integrar

		glUseProgram(mkariintegralprogram.programID);
		glUniform1i(glGetUniformLocation(mkariintegralprogram.programID, "tex0"), 0);
		glUniform1i(glGetUniformLocation(mkariintegralprogram.programID, "tex1"), 1);
		glUniform1f(glGetUniformLocation(mkariintegralprogram.programID, "dt"), dt);

		gl_error();
		glBindVertexArray(Gvao);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mytexture);


		glActiveTexture(GL_TEXTURE1);
		gl_error();
		glBindTexture(GL_TEXTURE_2D, GtexIS);
		gl_error();
		glDrawElements(GL_TRIANGLES, g_icant, GL_UNSIGNED_INT, 0); //mytexture2=GtexIS+dt*mytexture
		gl_error();


		//glBindTexture(GL_TEXTURE_2D, mytexture);
		//glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mytextu);
		/*
		int p = 0;
		for (int i = 0; i < COLS*ROWS; i++)
		{
			texIS[p + 0] = (unsigned char)((float)dt*mytextu[p + 0]+(float)texIS[p + 0]);
			texIS[p + 1] = (unsigned char)((float)dt*mytextu[p + 1]+(float)texIS[p + 1]);
			//texIS[p + 0] = 200;
			texIS[p + 3] = 255;
			p += 4;
		}*/
		glBindTexture(GL_TEXTURE_2D, GtexIS);

		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, cols, filas);

	}
	

	/*glBindTexture(GL_TEXTURE_2D, mytexture2);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, mytextu);//Forma lenta de copiar la textura pasando por CPU.
	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 801, 801, 0, GL_RGBA, GL_UNSIGNED_BYTE, (const void*)mytextu);
	*/


	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, floattex1);//COPIAR LA TEXTURA A RAM DE CPU

	//ACÁ PODRIAMOS COPIAR la textura GtexIS a cuda, o darle el puntero para acceso

	memcpy(texIS,floattex1,sizeof(float)*4*cols*filas);




	gl_error();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, Singleton::gI()->window_width, Singleton::gI()->window_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	gl_error();

}

int Modelo_RDC::celda_quemable(float *h_vege, int celdax, int celday)
{
	return (h_vege[celday * COLS + celdax] == 1.0);
}

unsigned char* Modelo_RDC::loadBMP_custom(const char * imagepath, int* w,int* h){
	// Data read from the header of the BMP file
	unsigned char header[54]; // Each BMP file begins by a 54-bytes header
	unsigned int dataPos;     // Position in the file where the actual data begins
	unsigned int width, height;
	unsigned int imageSize;   // = width*height*3
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath,"rb");
	if (!file){print_debug("Image %s could not be opened\n",imagepath); return 0;}

	if ( fread(header, 1, 54, file)!=54 ){ // If not 54 bytes read : problem
		print_debug("Not a correct BMP file\n");
		return NULL;
	}

	if ( header[0]!='B' || header[1]!='M' ){
		print_debug("Not a correct BMP file\n");
		return NULL;
	}

	// Read ints from the byte array
	dataPos    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos==0)      dataPos=54; // The BMP header is done that way
	// Create a buffer
	data=(unsigned char*)malloc(imageSize*sizeof(unsigned char));

	// Read the actual data from the file into the buffer
	fread(data,1,imageSize,file);

	//Everything is in memory now, the file can be closed
	fclose(file);
	*w=width;
	*h=height;

	return data;

}


void Modelo_RDC::Update_texIS_map(void){
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texIS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void Modelo_RDC::Update_vege_map(float * newmap){
	//Falta poner código acá

	for(int i=0;i<cols*filas;i++)
	{
		/*texSlAsWiFo[i*4+0]=h_slope[i];
		texSlAsWiFo[i*4+1]=h_aspect[i];
		texSlAsWiFo[i*4+2]=h_wind[i];
		texSlAsWiFo[i*4+3]=h_forest[i];*/

		//texAlReVe[i*4+0]=h_alti[i];
		//texAlReVe[i*4+1]=0.0f;//M.h_real_fire[i];//El mapa de fuego real todav�a no est� alocado
		texAlReVe[i*4+2]=newmap[i];
		//texAlReVe[i*4+3]=0.0f;

		//printf("%.01f ",M.h_vege[i]);

		//if(texAlReVe[i*4+2]<=2.5f)texIS[4 * i + 1]=0.0f;
		//texIS[4 * i + 0] = 0; //INCENDIANDOSE
		//texIS[4 * i + 1] = 1.0f; //SUCEPTIBLE
		
		//texIS[4 * i + 2] = 0;
		//texIS[4 * i + 3] = 0;
	}


	/*glActiveTexture(GL_TEXTURE1);//SI DA ALGUN ERROR DESCOMENTAR ESTE BLOQUE DE TEXIS
	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 801, 801, 0, GL_RGBA, GL_FLOAT, (const void*)texIS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);*/
	

	/*glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, GtexSlAsWiFo);// mapas de Slope Aspect Wind Forest
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 801, 801, 0, GL_RGBA, GL_FLOAT, (const void*)texSlAsWiFo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);*/


	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, GtexAlReVe);//Mapas de Altitud (Incendio)Real Vegetacion
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texAlReVe);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}


void Modelo_RDC::Set_Pixel(int celda)
{
	if(texAlReVe[celda]<=2.0f)return;
	texIS[4 * celda + 0] = 1.0f; //INCENDIANDOSE, pone el punto de ignicion

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texIS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}


void Modelo_RDC::loadTESTOBJECT(void) {
	float points[] = {
	-1,  1, 0,1,//superior IZQ
	1,  1,0,1,//superior DER
	1, -1,0,1,// Inferior DER
	-1, -1,0,1,//Inferior IZQ
	};

	float uvs[] = {
	0,  1,
	1,  1,
	1, 0,
	0, 0,
	};

	int idxs[] = {
	0,1,2,
	2,3,0
	};

	gl_error();
	glGenVertexArrays(1, &Gvao);
	glBindVertexArray(Gvao);
	gl_error();
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	gl_error();
	glGenBuffers(1, &Gvbo);
	glBindBuffer(GL_ARRAY_BUFFER, Gvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &Guvbo);
	glBindBuffer(GL_ARRAY_BUFFER, Guvbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(uvs), uvs, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glGenBuffers(1, &Gibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Gibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(idxs), idxs, GL_STATIC_DRAW);

	
}

void Modelo_RDC::inicializar(void)
{
	dt = 0.05f;
    g_icant=6;
    modulo = T_RUN / CANT_MAPAS_PELI;
    XIGNI=Singleton::getInstance()->XIGNI;
    YIGNI=Singleton::getInstance()->YIGNI;
    print_debug("XIGNI=%d YIGNI=%d \n",XIGNI,YIGNI);
    ROWS=Singleton::getInstance()->vert_gis_y;
    COLS=Singleton::getInstance()->vert_gis_x;
    filas=ROWS;
    cols=COLS;

    print_debug("Inicializar modelo RDC con %d, %d\n",cols, filas);

    floattex1=(float*)malloc(filas*cols*4*sizeof(float));

    //while(true){printf("f=%d c=%d\n",filas,cols);}


    h_terrain = (float *)malloc(sizeof(float) * filas * cols );
	d_terrain = (float *)malloc(sizeof(float) * filas * cols);
	d_terrainnew = (float *)malloc(sizeof(float) * filas * cols);

	cudaMalloc((void**)&d_terrainnew4, sizeof(float) * filas * cols*4);

	d_terrain_aux = (float *)malloc(sizeof(float) * filas * cols);
	d_terrainnew_aux = (float *)malloc(sizeof(float) * filas * cols);
	h_wind = (float*)malloc(sizeof(float) * filas * cols);
	h_aspect = (float *)malloc(sizeof(float) * filas * cols );
	h_slope = (float*) malloc(sizeof(float) * filas * cols);
	h_forest = (float *)malloc(sizeof(float) * filas * cols );   // ojo que hay un mapa "bosque" y un mapa "vegetacion"
	h_alti = (float *)malloc(sizeof(float) * filas * cols );
	h_vege = (float *)malloc(sizeof(float) * filas * cols );
	h_reference_map = (float *)malloc(sizeof(float) * filas * cols );
	
	// mapas en device
	/*cudaMalloc((void**)&d_terrain, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_wind, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_aspect, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_slope, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_forest, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_alti, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_vege, sizeof(float) * filas * cols);
	cudaMalloc((void**)&d_reference_map, sizeof(float) * filas * cols);
		
	// solo en device, hace de mapa auxiliar para la actualización
	cudaMalloc((void**)&d_terrainnew, sizeof(float) * filas * cols);*/

	//cudaMalloc((void**)&d_terrain_aux, sizeof(float) * filas * cols);
	//cudaMalloc((void**)&d_terrainnew_aux, sizeof(float) * filas * cols);
	/*d_terrain_aux = (float*)malloc(sizeof(float) * filas * cols);
	d_terrainnew_aux = (float*)malloc(sizeof(float) * filas * cols);*/

	program.load_and_link_shaders("shaders/ShaderCodev.glsl", "shaders/ShaderCodef.glsl");
	waterprogram.load_and_link_shaders("shaders/ShaderWaterv.glsl", "shaders/ShaderWaterf.glsl");
	waterQprogram.load_and_link_shaders("shaders/ShaderWaterQv.glsl", "shaders/ShaderWaterQf.glsl");
	d2program.load_and_link_shaders("shaders/Shader2dv.glsl", "shaders/Shader2df.glsl");
	carprogram.load_and_link_shaders("shaders/CarShaderCodev.glsl", "shaders/CarShaderCodef.glsl");
	particleprogram.load_and_link_shaders("shaders/Particlev.glsl", "shaders/Particleg.glsl", "shaders/Particlef.glsl");
	vegetacionprogram.load_and_link_shaders("shaders/VegetacionV.glsl", "shaders/VegetacionF.glsl");

	mkariprogram.load_and_link_shaders("shaders/kariV.glsl", "shaders/kariF.glsl");
	mkariintegralprogram.load_and_link_shaders("shaders/kariintegralV.glsl", "shaders/kariintegralF.glsl");
	//AL INCLUIR ESTAS 2 líneas del shader da fallo de segmentacion, posiblemente porque ejecuta el código luego.



	loadTESTOBJECT();

	mytextu = (unsigned char*)malloc(filas*cols*4*sizeof(unsigned char));

	int texw, texh;
	char filename[256];
	sprintf(filename, "./grass_texture_seamless.bmp");
	tex = loadBMP_custom(filename, &texw, &texh);
	gl_error();
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &GTtID);
	gl_error();
	//printf("%d\n",i);
	glBindTexture(GL_TEXTURE_2D, GTtID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texw, texh, 0, GL_BGR, GL_UNSIGNED_BYTE, (const void*)tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glGenerateMipmap(GL_TEXTURE_2D);
	gl_error();

	generate_fbo();

	/* coordenadas del fuego inicial */
	int x, y;

	/* 1 entero como flag para determinar cuando no propaga un incendio y parar de simular*/
	//int *h_fuego_propaga, *d_fuego_propaga;
	//h_fuego_propaga = (int*)malloc(sizeof(int));
	//d_fuego_propaga = (int*)malloc(sizeof(int));
	//cudaMalloc((void**)&d_fuego_propaga, sizeof(int));

	/* Inicializa mapas topograficos y de viento */
	Init_All_Maps(Singleton::getInstance()->proyectow.ROWS, Singleton::getInstance()->proyectow.COLS, Singleton::getInstance()->proyectow.WIND_MAP_P, Singleton::getInstance()->proyectow.ELEV_MAP_P, Singleton::getInstance()->proyectow.SLOPE_MAP_P, Singleton::getInstance()->proyectow.REAL_FIRE_MAP_P, Singleton::getInstance()->proyectow.ALTI_MAP_P, Singleton::getInstance()->proyectow.ASPECT_MAP_P, Singleton::getInstance()->proyectow.VEGE_MAP_P, Singleton::getInstance()->proyectow.FOREST_MAP_P);

	texSlAsWiFo = (float *)malloc(filas*cols*4*sizeof(float));
	texAlReVe = (float *)malloc(filas*cols*4*sizeof(float));

	for(int i=0;i<filas*cols;i++)
	{
		texSlAsWiFo[i*4+0]=h_slope[i];
		texSlAsWiFo[i*4+1]=h_aspect[i];
		texSlAsWiFo[i*4+2]=h_wind[i];
		texSlAsWiFo[i*4+3]=h_forest[i];

		texAlReVe[i*4+0]=h_alti[i];
		texAlReVe[i*4+1]=0.0f;//M.h_real_fire[i];//El mapa de fuego real todav�a no est� alocado
		texAlReVe[i*4+2]=h_vege[i];
		texAlReVe[i*4+3]=0.0f;

		//printf("%.01f ",M.h_vege[i]);
	}

	texIS = (float *)malloc(filas*cols*4*sizeof(float));
	for (int i = 0; i < filas*cols; i++)
	{
		texIS[4 * i + 0] = 0; //INCENDIANDOSE
		texIS[4 * i + 1] = 1.0f; //SUCEPTIBLE
		if(texAlReVe[i*4+2]<=2.5f)texIS[4 * i + 1]=0.0f;
		texIS[4 * i + 2] = 0;
		texIS[4 * i + 3] = 1.0f; //El alpha
	}

	//texIS[4 * (300+801*400) + 0] = 1.0f;//punto de incendio ignicion

	glActiveTexture(GL_TEXTURE1);
	glGenTextures(1, &GtexIS);
	gl_error();
	//printf("%d\n",i);
	glBindTexture(GL_TEXTURE_2D, GtexIS);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texIS);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glGenerateMipmap(GL_TEXTURE_2D);


	//free(tex);



	/* Se realiza 1 sola simulacion */

	// cada simulaci�n comienza con el mapa h_terrain con 1 sola celda quemada en el punto (x,y) */
	int t = 1;
	Init_Initial_Map(&x, &y, t);

	print_debug("Initial map: %d %d \n", x, y);



	/* por cada simulacion arranco desde 0 y asumo que el fuego propaga (para entrar al while)*/
	Singleton::getInstance()->Simulador.paso_simulacion = 0;
	//h_fuego_propaga[0] = 1;
	//cudaMemcpy(d_fuego_propaga, h_fuego_propaga, sizeof(int), cudaMemcpyHostToDevice);

	/* Propago hasta llegar a T_RUN o hasta que el fuego no propaga mas */





	glActiveTexture(GL_TEXTURE2);
	glGenTextures(1, &GtexSlAsWiFo);
	gl_error();
	//printf("%d\n",i);
	glBindTexture(GL_TEXTURE_2D, GtexSlAsWiFo);// mapas de Slope Aspect Wind Forest
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texSlAsWiFo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


	glActiveTexture(GL_TEXTURE3);
	glGenTextures(1, &GtexAlReVe);
	gl_error();
	//printf("%d\n",i);
	glBindTexture(GL_TEXTURE_2D, GtexAlReVe);//Mapas de Altitud (Incendio)Real Vegetacion
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, cols, filas, 0, GL_RGBA, GL_FLOAT, (const void*)texAlReVe);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}


//Inicio el fuego en UN punto incendiable hveg=1 o hveg=2 lo mas cercano posible al definido en Parameters.h como (XIGNI,YIGNI) 
void Modelo_RDC::inicializar_Mapa_Determinista(float *h_terrain, float *h_vege, int filas, int cols,int *x,int *y)
{

	int f,c;

	// elijo valores al azar hasta caer en una celda quemable
	c = XIGNI;
	f = YIGNI;

	//en caso en que la celda no sea quemable me muevo en diagonal hasta encontrar una quemable	
  	while (h_vege[f*cols+c] == 0.0) 
  	{
		f=f+1;
		c=c+1;
	}
  
	for (int fi = 0; fi < filas; fi++) {
		for(int col = 0; col < cols; col++){
			h_terrain[fi * cols + col] = ((fi == f) && (col == c)) ? 1.0: 0.0;	
		}
	} 

	if(IMPRIMIR) {
		print_debug("PTO DE IGNICION: %d %d, terrain=%f \n", f, c,h_terrain[f * cols + c] );
	}

  *x=f;
  *y=c;
}


// Inicializa todos los mapas necesarios y el mapa inicial. 
void Modelo_RDC::Init_All_Maps(int filas, int cols, char *WIND_MAP_P, char *ELEV_MAP_P, char *SLOPE_MAP_P, char *REAL_FIRE_MAP_P, char *ALTI_MAP_P, char *ASPECT_MAP_P, char *VEGE_MAP_P, char *FOREST_MAP_P)
{
  
	// leo de disco todos los mapas que describen el medio ambiente
	GetMap(VEGE_MAP_P, h_vege, filas, cols);
	//CpyHostToDevice(d_vege, h_vege, filas, cols);

	//while(true){printf("Q\n");}
      
       // leo mapas en cpu y paso a gpu MAPA de VIENTO
	GetMap(WIND_MAP_P, h_wind, filas, cols);
	//CpyHostToDevice(d_wind, h_wind, filas, cols);
	
	// leo mapas en cpu y paso a gpu MAPA de COMBUSTIBLE
	GetMap(FOREST_MAP_P, h_forest, filas, cols);
	CpyHostToDevice(d_forest, h_forest, filas, cols);
	
	// leo mapas en cpu y paso a gpu MAPA de ORIENTACION DE LA PENDIENTE
	GetMap(ASPECT_MAP_P, h_aspect, filas, cols);
	//CpyHostToDevice(d_aspect, h_aspect, filas, cols);
		
	// leo mapas en cpu y paso a gpu MAPA de PENDIENTE
	GetMap(SLOPE_MAP_P, h_slope, filas, cols);
	//CpyHostToDevice(d_slope, h_slope, filas, cols);
		
	// leo mapas en cpu y paso a gpu MAPA de ALTITUD
	GetMap(ALTI_MAP_P, h_alti, filas, cols);
	//CpyHostToDevice(d_alti, h_alti, filas, cols);

	/*dim3 dimBlock(NTHREADS_X, NTHREADS_Y);
	dim3 dimGrid(ROWS / NTHREADS_X, COLS / NTHREADS_Y);
	
	KERNEL_calculate_psi_value<<<dimGrid, dimBlock>>>(d_aspect, d_slope,filas,cols); 
	*/
	
}
	
/*void Modelo_RDC::Inicializar_datos_celda(DatosCelda *h_mapa_datos)
{

	for (int fi = 0; fi < ROWS; fi++)
	{
	 for(int col = 0; col < COLS; col++)
	 {
          h_mapa_datos[fi * COLS + col].timeFireOn = 0.0; //inicializo todos los tiempos en cero

	 }
	}		  
}*/

void Modelo_RDC::Init_Initial_Map(int *x, int *y,int seed_) //Esta funcion se refiere a la inicializacion de las SIMULACIONES
{
	// MAPA INICIAL EN h_terrain HAY DOS POSIBILIDADES: son excluyentes!
	//las simulaciones comienzan desde un punto fijo determinado (XIGNI,YIGNI) o comienzan desde un punto elegido aleatoriamente dentro del area quemada 
	

	inicializar_Mapa_Determinista(h_terrain, h_vege, ROWS, COLS,x,y);

	//parametros.init(seed_); //inicializo parametros al azar ver Parameters.h
 	//Inicializar_datos_celda(h_mapa_datos);
    
    //cudaMemcpy(d_mapa_datos,h_mapa_datos,sizeof(DatosCelda)*filas*cols, cudaMemcpyHostToDevice);
 	
	//CpyHostToDevice(d_terrain, h_terrain, ROWS, COLS);

    int Nsites=ROWS*COLS;
    int index = XIGNI + YIGNI*ROWS;

	for (int i = 0; i < Nsites; i++)
	{
		d_terrain[i] = 0.0f;
		d_terrainnew[i] = 1.0f;
	}
    
    //thrust::host_vector<float> h_Inc(Nsites,0.0);
    //thrust::host_vector<float> h_Sus(Nsites,1.0);
	d_terrain[index]=1.0;
    
   // thrust::device_ptr<float> d_Inc(d_terrain);
    //thrust::device_ptr<float> d_Sus(d_terrainnew);
    
    //thrust::copy(h_Inc.begin(),h_Inc.end(),d_Inc);
    //thrust::copy(h_Sus.begin(),h_Sus.end(),d_Sus);
}

// quema una celda para iniciar la quema del mapa sintetico de referencia
void Modelo_RDC::Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y)
{
	if (IGNICION_REFERENCIA_DETERMINISTA)
	{
		// inicializar el punto de ignición usando XIGNI YIGNI
		inicializar_Mapa_Determinista(h_terrain, h_vege, ROWS, COLS,x,y);
	
	}
	else {
		// random x e y y le pongo 1.0 en esa celda
		int celdax, celday;

		srand(time(NULL));
		celdax = rand() % COLS;
		celday = rand() % ROWS;

		while (!celda_quemable(h_vege, celdax, celday)) {
			celdax = rand() % COLS;
			celday = rand() % ROWS;
		}


		for (int fi = 0; fi < ROWS; fi++)
		{
			for(int col = 0; col < COLS; col++){
				h_terrain[fi * COLS + col] = ((celday == fi) && (celdax == col)) ? 1.0: 0.0;
			}
		}
		
		// seteo los valores de la celda de inicio ya que los necesito fuera
		*x = celdax;
		*y = celday;
	}
}




// Copy Functions

/* transfer from CPU to GPU memory */
void Modelo_RDC::CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols )
{
     //cudaMemcpy(d_mapa,h_mapa, sizeof(float)*filas*cols, cudaMemcpyHostToDevice);
}


/* transfer from GPU to CPU memory */
void Modelo_RDC::CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols )
{
    //cudaMemcpy(h_mapa, d_mapa, sizeof(float)*filas*cols, cudaMemcpyDeviceToHost);
}



/* ENTRADA Y SALIDA DE MAPAS */
void Modelo_RDC::GetMap(const char * nombre, float *mapa, int filas, int cols)
{
	FILE *input = fopen(nombre,"r");

	if (input == NULL)
	{
		print_debug("Get Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j, dato;

	char line[MAX];
	for(i=0; i < 6; i++)
	{
		fgets(line, MAX, input);
	}

	for(i = 0; i < filas; i++)
	{
		for(j = 0; j < cols; j++)
		{
			fscanf(input, "%d", &dato);
			mapa[i*cols + j] = (float)dato;
		}
		
	}
	fclose(input);

}


Modelo_RDC::~Modelo_RDC(){

	print_debug("LLAMADO AL DESTRUCTOR DE MODELO_RDC\n");
	print_debug("liberando..");fflush(stdout);
	int e=1;

	free(floattex1);floattex1=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_terrain);h_terrain=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(d_terrain);d_terrain=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(d_terrainnew);d_terrainnew=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(d_terrain_aux);d_terrain_aux=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(d_terrainnew_aux);d_terrainnew_aux=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_wind);h_wind=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_aspect);h_aspect=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_slope);h_slope=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_forest);h_forest=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_alti);h_alti=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_vege);h_vege=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(h_reference_map);h_reference_map=NULL;print_debug("%d,",e);fflush(stdout);e++;

	free(mytextu);mytextu=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(texSlAsWiFo);texSlAsWiFo=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(texAlReVe);texAlReVe=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(texIS);texIS=NULL;print_debug("%d,",e);fflush(stdout);e++;
	free(tex);tex=NULL;print_debug("%d,",e);fflush(stdout);e++;


	glDeleteTextures(1, &GTtID);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteTextures(1, &GtexIS);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteTextures(1, &GtexSlAsWiFo);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteTextures(1, &GtexAlReVe);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteTextures(1, &mytexture);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteTextures(1, &mytexture2);print_debug("%d,",e);fflush(stdout);e++;

	glDeleteFramebuffers(1, &myFBO);print_debug("%d,",e);fflush(stdout);e++;
	glDeleteFramebuffers(1, &myFBO2);print_debug("%d,",e);fflush(stdout);e++;

	glDeleteVertexArrays(1,&Gvao);
	glDeleteBuffers(1,&Gvbo);
	glDeleteBuffers(1,&Guvbo);
	glDeleteBuffers(1,&Gibo);

	program.deletee();
	waterprogram.deletee();
	waterQprogram.deletee();
	d2program.deletee();
	carprogram.deletee();
	particleprogram.deletee();
	vegetacionprogram.deletee();

	mkariprogram.deletee();
	mkariintegralprogram.deletee();


	print_debug("Listo.\n");
}

//INCREMENTO DE USO DE MEMORIA AL SWICHEAR MODELOS
/*
159	556
180	568
371	976
446	900
631	1052
714	980
906	1132
990	1060
1200	1212



prueba2:
180	576
694	984
744	828
981	980
1011	828
1100	980
1100	828
1100	980
1100	828

prueba3:
180	580
345	988
378	832
379	984
379	832
398	984
398	832
423	984
423	832
448	984
448	832
473	984
473	832
498	984
509	832



*/