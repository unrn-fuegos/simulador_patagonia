#include "Historicos.h"
#include "curso.h"

#define MIN_CELDA_DIF 5000

/*
Prueba 2 con el nuevo sistema de realocado
5000 	paso 5100 265.4 MB
50000 	paso 5100 308 MB
*/

void Mapa_Diferencia(float * d_mapaA,float * d_mapaB,float * d_mapaAmB,int ROWS, int COLS);

void Historicos::init(int cols,int rows,int numfloats){

	this->cols=cols;
	this->rows=rows;
	this->numfloats=numfloats;
	act_HEntry=-1;
	HANDLE_ERROR(cudaMalloc((void**)&d_difmap, numfloats*cols*rows*sizeof(float)));
	h_difmap=(float*)malloc(numfloats*cols*rows*sizeof(float));
	memset(h_difmap, 0.0f, sizeof(float)*rows*cols*numfloats);

	h_initialmap=(float*)malloc(numfloats*cols*rows*sizeof(float));
	memset(h_initialmap, 0.0f, sizeof(float)*rows*cols*numfloats);

	HANDLE_ERROR(cudaMalloc((void**)&d_actmap, numfloats*cols*rows*sizeof(float)));
	h_actmap=(float*)malloc(numfloats*cols*rows*sizeof(float));
	memset(h_actmap, 0.0f, sizeof(float)*rows*cols*numfloats);

	last_celda_dif=0;
	derivada_celda_dif=0;
}

/*void Historicos::add_pixel(int x, int y){
	//if(act_HEntry<2)return;
	HEntry[act_HEntry]->cant_celdas_dif++;
	HEntry[act_HEntry]->cant_celdas_tot++;
	//printf("celdas_diff=%d\n",HEntry[act_HEntry]->celdas_dif);
	if(HEntry[act_HEntry]->celdas_dif==NULL)
	{
		HEntry[act_HEntry]->celdas_dif=(int*)realloc(HEntry[act_HEntry]->celdas_dif,MAX_CELDA_DIF*sizeof(int));
		HEntry[act_HEntry]->celdas_dif_V=(int*)realloc(HEntry[act_HEntry]->celdas_dif,MAX_CELDA_DIF*sizeof(int));
		for(int j=0;j<MAX_CELDA_DIF;j++){HEntry[act_HEntry]->celdas_dif[j]=-1;}
		for(int j=0;j<MAX_CELDA_DIF;j++){HEntry[act_HEntry]->celdas_dif_V[j]=-1;}
	}
	
	HEntry[act_HEntry]->celdas_dif[HEntry[act_HEntry]->cant_celdas_dif]=x+y*cols;
	HEntry[act_HEntry]->celdas_dif_V[HEntry[act_HEntry]->cant_celdas_dif]=this->paso;

}*/

int* Historicos::count_ones_store_array(float*h_map,int max,int&numones,int&ovf,float**vecV,int estim)
{
	numones=0;
	ovf=0;
	int estimado=estim;
	int*vec=(int*)malloc(estimado*sizeof(int));


	float * vecVp;
	vecVp=(float*)malloc(estimado*sizeof(float)*numfloats);

	//memset(vec,0,MAX_CELDA_DIF);
	for(int j=0;j<estimado;j++){vec[j]=-1;}
	int sti=0;
	for(int i=0;i<max && !ovf;i++)
	{
		bool haydif=0;
		if(numfloats>=1){if(abs(h_map[i*numfloats+0])>1.0e-6f){haydif=1;}}//PARECE QUE EL MEMSET PONE UN NRO LIGERAMENTE DISTINTO DE CERO.
		if(numfloats>=2){if(abs(h_map[i*numfloats+1])>1.0e-6f){haydif=1;}}
		if(numfloats>=3){if(abs(h_map[i*numfloats+2])>1.0e-6f){haydif=1;}}
		if(numfloats>=4){if(abs(h_map[i*numfloats+3])>1.0e-6f){haydif=1;}}

		if(haydif)
		{
			if(sti>(estimado-2))
			{
				estimado=(estimado*13)/10;
				vec=(int*)realloc(vec,estimado*sizeof(int));
				vecVp=(float*)realloc(vecVp,estimado*sizeof(float)*numfloats);
				*vecV=vecVp;
				print_debug("Realocado\n");
			}

			numones++;
			vec[sti]=i;
			if(numfloats>=1){vecVp[sti*numfloats+0]=h_map[i*numfloats+0];}
			if(numfloats>=2){vecVp[sti*numfloats+1]=h_map[i*numfloats+1];}
			if(numfloats>=3){vecVp[sti*numfloats+2]=h_map[i*numfloats+2];}
			if(numfloats>=4){vecVp[sti*numfloats+3]=h_map[i*numfloats+3];}
			//printf("D=%e\n",h_map[i*numfloats+0]);
			sti++;
		}
	}
	*vecV=vecVp;
	return vec;
}

void count_map_numbers(float * textura, int largo,int numfloats)
{
	int ceros[4],unos[4];
	ceros[0]=0;ceros[1]=0;ceros[2]=0;ceros[3]=0;
	unos[0]=0;unos[1]=0;unos[2]=0;unos[3]=0;
	for(int i=0;i<largo;i++)
	{
		if(numfloats>=1 && textura[i*numfloats+0]==0.0f){ceros[0]++;}
		if(numfloats>=2 && textura[i*numfloats+1]==0.0f){ceros[1]++;}
		if(numfloats>=3 && textura[i*numfloats+2]==0.0f){ceros[2]++;}
		if(numfloats>=4 && textura[i*numfloats+3]==0.0f){ceros[3]++;}

		if(numfloats>=1 && textura[i*numfloats+0]==1.0f){unos[0]++;}
		if(numfloats>=2 && textura[i*numfloats+1]==1.0f){unos[1]++;}
		if(numfloats>=3 && textura[i*numfloats+2]==1.0f){unos[2]++;}
		if(numfloats>=4 && textura[i*numfloats+3]==1.0f){unos[3]++;}
	}

	print_debug("El mapa inicial referencia de historicos tiene:\n");
	print_debug("Componente 0: %d ceros y %d unos y %d otros\n",ceros[0],unos[0],largo-ceros[0]-unos[0]);
	print_debug("Componente 1: %d ceros y %d unos y %d otros\n",ceros[1],unos[1],largo-ceros[1]-unos[1]);
	print_debug("Componente 2: %d ceros y %d unos y %d otros\n",ceros[2],unos[2],largo-ceros[2]-unos[2]);
	print_debug("Componente 3: %d ceros y %d unos y %d otros\n",ceros[3],unos[3],largo-ceros[3]-unos[3]);
}

void Historicos::load_initial_map(float * textura){
	//HANDLE_ERROR(cudaMemcpy(d_terrainnew4, texIS, sizeof(float)*COLS*ROWS*4, cudaMemcpyHostToDevice));
	memcpy(h_initialmap,textura,sizeof(float)*cols*rows*numfloats);
	count_map_numbers(textura,rows*cols,numfloats); // PARA DIAGNOSTICO
}

void Historicos::save_new_map(float*d_map,int paso){
	print_debug("ENTRANDO A SAVE_NEW_MAP %d %d\n",rows,cols);
	//printf("%d %d %d %d %d\n",d_map,d_actmap,d_difmap,rows*numfloats,cols);
	if(act_HEntry<0){
		memcpy(h_actmap,h_initialmap,sizeof(float)*cols*rows*numfloats);
		HANDLE_ERROR(cudaMemcpy(d_actmap,h_actmap, sizeof(float)*rows*cols*numfloats, cudaMemcpyHostToDevice));
	}
	Mapa_Diferencia(d_map,d_actmap,d_difmap,rows*numfloats, cols);
	HANDLE_ERROR(cudaMemcpy(h_difmap,d_difmap, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToHost));
	int * vector;
	float * vectorV=NULL;
	int onescount,ovf;

	int estim_celda_dif=0;
	estim_celda_dif=(last_celda_dif*105)/100+derivada_celda_dif;
	if(estim_celda_dif<MIN_CELDA_DIF){estim_celda_dif=MIN_CELDA_DIF;}

	print_debug("estim_celda_dif=%d\n",estim_celda_dif);

	
	vector=count_ones_store_array(h_difmap,rows*cols,onescount,ovf,&vectorV,estim_celda_dif); //ESTO ES RE LENTO!! con el !ovf mejoro
	//printf("ONES: %d OVF%d ",onescount,ovf);
	if(ovf)
	{
		print_debug("%d HISTORICOS CON DEMASIADAS CELDAS QUE CAMBIAN************************\n",onescount);
		exit(-1);
	}

	act_HEntry++;
	Historico_Entrada * entrada=new Historico_Entrada;
	entrada->paso=paso;
	entrada->cant_celdas_tot=0;	


	entrada->cant_celdas_dif=onescount;
	entrada->celdas_dif=vector;
	entrada->celdas_dif_V=vectorV;

	print_debug("NUM CELDAS DIFERENTES=%d\n",onescount);
	derivada_celda_dif=onescount-last_celda_dif;
	last_celda_dif=onescount;


	HANDLE_ERROR(cudaMemcpy(d_actmap,d_map, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToDevice));
	HANDLE_ERROR(cudaMemcpy(h_actmap,d_actmap, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToHost));

	//count_map_numbers(h_actmap,rows*cols,numfloats); // PARA DIAGNOSTICO


	HEntry[act_HEntry]=entrada;

	//printf("act_KeyFrame=%d act_HEntry=%d\n",act_KeyFrame,act_HEntry);*/
}


void Historicos::step_BW(int&paso,float*h_mapa_aux,float*d_tn,float*d_t)
{
	HANDLE_ERROR(cudaMemcpy(d_t,d_actmap, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToDevice));
	HANDLE_ERROR(cudaMemcpy(h_mapa_aux,d_t, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToHost));//ESTO ES LENTO
	if(d_tn!=NULL){HANDLE_ERROR(cudaMemcpy(d_tn,d_t, sizeof(float)*rows*cols*numfloats, cudaMemcpyDeviceToDevice));}

	

	if(HEntry[act_HEntry]->celdas_dif!=NULL)
	{
		for(int i=0;i<HEntry[act_HEntry]->cant_celdas_dif;i++)
		{
			int celdadif=HEntry[act_HEntry]->celdas_dif[i];
			//h_mapa_aux[HEntry[act_HEntry]->celdas_dif[i]]=(float)h_actmap[HEntry[act_HEntry]->celdas_dif[i]]-HEntry[act_HEntry]->celdas_dif_V[i];
			if(numfloats>=1){h_actmap[celdadif*numfloats+0]=h_actmap[celdadif*numfloats+0]-HEntry[act_HEntry]->celdas_dif_V[i*numfloats+0];}
			if(numfloats>=2){h_actmap[celdadif*numfloats+1]=h_actmap[celdadif*numfloats+1]-HEntry[act_HEntry]->celdas_dif_V[i*numfloats+1];}
			if(numfloats>=3){h_actmap[celdadif*numfloats+2]=h_actmap[celdadif*numfloats+2]-HEntry[act_HEntry]->celdas_dif_V[i*numfloats+2];}
			if(numfloats>=4){h_actmap[celdadif*numfloats+3]=h_actmap[celdadif*numfloats+3]-HEntry[act_HEntry]->celdas_dif_V[i*numfloats+3];}
		}
	}

	paso=HEntry[act_HEntry]->paso;

	/*HANDLE_ERROR(cudaMemcpy(d_t,h_mapa_aux, sizeof(float)*rows*cols, cudaMemcpyHostToDevice));//ESTO ES LENTO
	HANDLE_ERROR(cudaMemcpy(d_tn,d_t, sizeof(float)*rows*cols, cudaMemcpyDeviceToDevice));

	HANDLE_ERROR(cudaMemcpy(d_actmap,d_t, sizeof(float)*rows*cols, cudaMemcpyDeviceToDevice));*/

	HANDLE_ERROR(cudaMemcpy(d_actmap,h_actmap, sizeof(float)*rows*cols*numfloats, cudaMemcpyHostToDevice));


	free(HEntry[act_HEntry]->celdas_dif);
	free(HEntry[act_HEntry]->celdas_dif_V);

	free(HEntry[act_HEntry]);
	act_HEntry--;

	

}

void mifree(float * ptr);
void micudafree(float * ptr);


void Historicos::destruir(void){
	print_debug("Llamado destruir historicos\n");

	micudafree(d_difmap);
	mifree(h_difmap);

	for(int i=0;i<act_HEntry+1;i++)
	{
		if(HEntry[i]->celdas_dif!=NULL)
		{
			mifree((float*)HEntry[i]->celdas_dif_V);
			mifree((float*)HEntry[i]->celdas_dif);
		}
		free(HEntry[i]);
	}
}

Historicos::~Historicos(){
	print_debug("Llamado destructor historicos\n");
}