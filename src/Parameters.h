#pragma once

#include <stdio.h>
// PARAMETERS




#ifndef T_RUN
#define	T_RUN 5000	/* Running time o numero de pasos de simulacion OJO T_RUN / CANT_MAPAS_PELI debe ser entero*/
#endif


#ifndef IMPRIMIR
#define	IMPRIMIR 0		/* 1 para imprimir por pantalla o no imprimir (0) */
#endif


#ifndef NTHREADS_X
#define NTHREADS_X 16
#endif

#ifndef NTHREADS_Y
#define NTHREADS_Y 16
#endif

// ARMAR MAPA DE DATOS DEL AVANCE DEL FUEGO
#ifndef MAPA_DATOS_IGN
#define MAPA_DATOS_IGN 0 
#endif

// FUNCION DE FITNESS 
#ifndef CODIGO_FUNCION_FITNESS
#define CODIGO_FUNCION_FITNESS 2  //(1=fitness pajaro, 2=fitness Monica con atomic adds, 3=fitness Monica en paralelo con reduce)
#endif


// Para transferir bandera de propagacion del fuego desde device a host las iteraciones iteracion % FUEGO_PROPAGA_NUMERO
#ifndef FUEGO_PROPAGA_NUMERO
#define FUEGO_PROPAGA_NUMERO 1  //
#endif

// ARMAR MAPA DE DATOS DEL AVANCE DEL FUEGO
#ifndef PRINT_MAP
#define PRINT_MAP   /*=0 NO imprimo mapas*/
#endif



/*  BORRAR
#ifndef PRUEBA_LECTURA_hterrain
#define PRUEBA_LECTURA_hterrain "mapas_originales/leo_hterrain_prueba.map"
#endif
*/ 




#ifndef IGNICION_SIMS_DETERMINISTA
#define	IGNICION_SIMS_DETERMINISTA 1	/* Los mapas de las simulaciones tienen punto de ignicion conocido(XIGNI,YIGNI)=1 , ALEATORIO dentro del area quemada de referencia=0*/
#endif

#ifndef IGNICION_REFERENCIA_DETERMINISTA
#define	IGNICION_REFERENCIA_DETERMINISTA 1	/* El mapa de referencia tiene punto de Ignicion conocido(XIGNI,YIGNI)=1, ALEATORIO dentro de TODA la grilla=0 */
#endif


#ifndef POBLACION_INICIAL_X_Y_ALEATORIOS
#define POBLACION_INICIAL_X_Y_ALEATORIOS 1  // 0 PARA COMPIAR XIGNI Y IGNI EN LA POBLACION 1 para que sean aleatorios dentro del fuego inicial
#endif



#ifndef DIR_FIRE_PROGRESS
#define DIR_FIRE_PROGRESS "mapas_generados/"
#endif

#ifndef CANT_MAPAS_PELI
#define	CANT_MAPAS_PELI 50		/* si se imprime con PRINT_MAP esta es la cantidad de mapas que componen la peli*/
#endif

#ifndef CANT_MAPAS_REFERENCIA
#define	CANT_MAPAS_REFERENCIA 6		/* promedio sobre PROMEDIOS_REFMAP Mapas iniciales de referencia*/
#endif




#include<stdlib.h>
using namespace std;


// TIEMPOS DE ENCENDIDO DEL FUEGO
#ifndef TIEMPO_MATORRAL
#define TIEMPO_MATORRAL 1  //Cantidad de tiempos de simulacion que queda prendida una celda con matorral
#endif

#ifndef TIEMPO_BOSQUE
#define TIEMPO_BOSQUE 1  //Cantidad de tiempos de simulacion que queda prendida una celda con bosque
#endif

/*Parametros del modelo, los mejores encontrados por Morales et. al. con MCMC*/
#define beta_cero_best -5.0
//-3.914
#define beta_forest_best -10.0
//-10.739
#define beta_aspect_best 5.0
//5.076
#define beta_wind_best 18.1
//18.267
#define beta_slope_best -5.043
// IMPORTANTE: cuando beta slope es positivo 1.043 se quema todo si necesidad de agregar los demas parámetros!!!!!!
#ifndef DISTRIB_PRIOR
#define DISTRIB_PRIOR 5  //Distribucion de los valores de los parametros de las simulaciones (1=UniformeCentrada, 
//2=UnifRandom,  3=Gausiana, 4=Exponencial) 5= best params sin random
#endif

