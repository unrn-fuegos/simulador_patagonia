//#include "superinclude.h"

#include "Singleton.h"

#include "cortafuego.h"
#ifdef WIN32
#include "direntW.h"
#include <direct.h>
#else
#include "dirent.h"
#endif
#include "imgui/imgui.h" 

#include "Simulador.h"


extern bool imguicontrol;
extern bool imguicontrol2;

using namespace glm;


#define MAX_LN 128

int strtodelim(char* str,char delim);
void regenerar_textura_final(void);

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}


void RasterMap::Update_GPU_Map(void){
	if(!is_in_cpu)return;
	if(!is_in_gpu){ 
		print_debug("FT x=%d y=%d\n",x,y);
   		gpuErrchk(cudaMalloc((void**)&d_map, sizeof(int) * x*y));
   		is_in_gpu=1;
	}
    if(!is_gpu_utd){
    	gpuErrchk(cudaMemcpy(d_map, h_map, sizeof(int) * x*y, cudaMemcpyHostToDevice));
    	is_gpu_utd=1;
    	print_debug("UD x=%d y=%d\n",x,y);
    }
}

RasterMap::RasterMap(void){
	is_in_gpu=0;
	is_in_cpu=0;
	is_gpu_utd=0;
	h_map=NULL;
	d_map=NULL;
	map_a=0.8f;
	sinpaleta=0;
	has_tooltip=0;
	this->paleta=NULL;
}

RasterMap::RasterMap(bool sinpaleta){
	is_in_gpu=0;
	is_in_cpu=0;
	is_gpu_utd=0;
	h_map=NULL;
	d_map=NULL;
	map_a=0.8f;
	this->sinpaleta=sinpaleta;
	has_tooltip=0;
	this->paleta=NULL;
}

void RasterMap::reset(void){
	if(is_in_cpu)
	{
		memset(h_map, 0, sizeof(int)*x*y);
		if(is_in_gpu)
		{
			is_gpu_utd=0;
			Update_GPU_Map();
		}
	}
}

void RasterMap::SinPaleta(bool sinpaleta){
	is_in_gpu=0;
	is_in_cpu=0;
	is_gpu_utd=0;
	h_map=NULL;
	d_map=NULL;
	map_a=0.8f;
	this->sinpaleta=sinpaleta;
	//this->paleta=NULL;
	has_tooltip=0;
}

void RasterMap::set_size(int x,int y){
	this->x=x;
	this->y=y;
}

void RasterMap::Load_Paleta(vec3*paleta, int size){
	//printf("LP1 %x %d %d\n",this->paleta,size,paleta_size);
	if(size!=paleta_size)this->paleta=(vec3*)realloc(this->paleta,sizeof(vec3)*size);
	memcpy(this->paleta,paleta,sizeof(vec3)*size);
	paleta_size=size;
}

void IgnitionPoint::set(int step,int x, int y,bool enabled){

	this->step=step;
	this->x=x;
	this->y=y;
	this->enabled=enabled;
}

SimuScript::SimuScript(void){
	ignitionpoint=NULL;
	cant_ignitionpoint=0;
	oldstep=-1;
}

void SimuScript::load_ignitionpoint_file(char*filename){
	FILE * pFile;
	char str[128];
	print_debug("load_ignitionpoint_file()...for proj %s\n",Singleton::getInstance()->PROJECT_FOLDER_P);
	sprintf(str,"%s%s",Singleton::getInstance()->PROJECT_FOLDER_P,filename);
	pFile = fopen (str,"r");
	if (pFile!=NULL)
	{
		char line[128];
		while(fgets(line, MAX_LN, pFile))
		{
			if(strlen(line)<4)continue;
			int stpos=0;
			char ext[64];
			int extlen;
			if(line[0]=='#')continue;

			extlen=strtodelim(&line[stpos],'\t');
			memcpy(ext,&line[stpos],extlen);
			ext[extlen]=0;
			stpos+=extlen+1;

			int paso,px,py,ena;

			sscanf(ext,"%d",&paso);

			extlen=strtodelim(&line[stpos],'\t');
			memcpy(ext,&line[stpos],extlen);
			ext[extlen]=0;
			stpos+=extlen+1;
			sscanf(ext,"%d",&px);

			extlen=strtodelim(&line[stpos],'\t');
			memcpy(ext,&line[stpos],extlen);
			ext[extlen]=0;
			stpos+=extlen+1;
			sscanf(ext,"%d",&py);

			extlen=strtodelim(&line[stpos],'\t');
			memcpy(ext,&line[stpos],extlen);
			ext[extlen]=0;
			stpos+=extlen+1;
			sscanf(ext,"%d",&ena);
			add_ignitionpoint(paso,px,py,ena);
    	}
		fclose (pFile);
	}
	else
	{
		print_debug("No se puede abrir %s\n",str);
	}
	print_debug("OK\n");
}

void SimuScript::save_ignitionpoint_file(char*filename){
	FILE * pFile;
	char str[128];
	sprintf(str,"%s%s",Singleton::getInstance()->PROJECT_FOLDER_P,filename);
	pFile = fopen (str,"w");
	if (pFile!=NULL)
	{
		char str[64];
		sprintf(str,"#PASO\tX\tY\tENABLED(0/1)\r\n");
		fputs(str,pFile);
		for(int i=0;i<cant_ignitionpoint;i++)
		{
			sprintf(str,"%d\t%d\t%d\t%d\r\n",ignitionpoint[i].step,ignitionpoint[i].x,ignitionpoint[i].y,ignitionpoint[i].enabled);
			fputs(str,pFile);
		}
		fclose (pFile);
	}

}

void SimuScript::add_ignitionpoint(int step,int x, int y,bool enabled){
	cant_ignitionpoint++;

	this->ignitionpoint=(IgnitionPoint*)realloc(this->ignitionpoint,sizeof(IgnitionPoint)*cant_ignitionpoint);

	this->ignitionpoint[cant_ignitionpoint-1].set(step,x,y,enabled);
}

bool esta_entre(int valor, int a, int b){
	if(a>b)
	{
		if(valor>=b && valor<=a)return 1;
	}
	else
	{
		if(valor<=b && valor>=a)return 1;
	}
	return 0;
}


void SimuScript::evaluar_ingnitionpoints(int newstep,bool force){
	if(newstep==oldstep && !force)return;
	int numset=0;
	for(int i=0;i<cant_ignitionpoint;i++)
	{
		if(!ignitionpoint[i].enabled)continue;
		int step=ignitionpoint[i].step;
		if(esta_entre(step,newstep,oldstep) && step<=newstep && step>=oldstep)
		{
			Singleton::getInstance()->Simulador.SetPixelForAll(ignitionpoint[i].x,ignitionpoint[i].y);
			numset++;
		}
	}
	oldstep=newstep;
	if(numset>0){
		Singleton::getInstance()->Simulador.recorrer_floats();
        regenerar_textura_final();
	}
}

void SimuScript::delete_ignitionpoint(int i){
	if(i>=cant_ignitionpoint) return;

	for(int q=i;q<cant_ignitionpoint-1;q++)
	{
		ignitionpoint[q]=ignitionpoint[q+1];
	}

	cant_ignitionpoint--;
	this->ignitionpoint=(IgnitionPoint*)realloc(this->ignitionpoint,sizeof(IgnitionPoint)*cant_ignitionpoint);
}

void SimuScript::delete_all(void){
	free(this->ignitionpoint);
	this->ignitionpoint=NULL;
	cant_ignitionpoint=0;

}