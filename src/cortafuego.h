#pragma once
//#include "superinclude.h"


using namespace glm;


struct CORTAFUEGOS_LINEA{
	
public:
	int x0,y0,x1,y1;
	CORTAFUEGOS_LINEA(int x0,int y0, int x1, int y1);
	CORTAFUEGOS_LINEA();
	void get_pos(int* x0,int *y0, int *x1, int *y1);
	void set_pos(int x0,int y0, int x1, int y1);

};

struct CORTAFUEGOS_ENTRADA{
public:	
	int cant_lineas;
	CORTAFUEGOS_LINEA* LINEA;
	char nombre[32];
	bool visible;

	bool linea_Narb;
	void add_new(int x0,int y0, int x1, int y1);
	void remove();
	CORTAFUEGOS_ENTRADA();
};



struct CORTAFUEGOS_LISTA{
	
public:
	int cant_corf;
	bool algun_cambio,algun_cambio_t;
	CORTAFUEGOS_ENTRADA *ENTRADA;
	char **listbox_items;
	int *listbox_visible;
	int selected;

	char save_filename[32];
	int pers_filename;
	int show_save,show_open,show_map_open;
	char **listbox_file_list;
	int listbox_file_cant;
	bool show_prob_window;
	int ni,nj;

	int nueva_entrada(bool linea_Narb);// retorna ID
	void borrar_entrada(int id);
	void actualizar_mapa(int* mapa,int* mapat);
	CORTAFUEGOS_LISTA();
	void borrar_todo(void);
	void cargar_archivo(char* filename);
	void guardar_archivo(char* filename);
	void show_cortafuego_saveas(void);
	void show_cortafuego_open(void);
	void recalcular_filelist(char* proyecto,const char*subd,const char* exten);
	void show_map_openf(void);

};
