#pragma once

#include "Parameters.h"

#include <vector>

#include "Historicos.h"
#include <curand.h>
#include <curand_kernel.h>



#define Kdebug 0
//#include "Poblacion.h"


      struct DatosCelda {
		int step;
		int timeFireOn; //tiempo en el que se inicia el fuego en una celda.
		float intensity;
		int fireOrigin; // celda vecina desde donde vino el fuego
	};


/* Declaración de la clase Mapa */
class Modelo_AutomataCelular
{

	public:
	Historicos Histo;


	// esto es para almacenar los historicos
	float *h_mapa_aux;
	int nuevomapa;
	int paso_simulacion;
	int indice_matriz;

	int indice_historicos; // para incrementar o decrementar y luego trabajar
	float *d_terrain, *d_terrainnew;

	float beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind;

    private:

	std::vector<int> h_indicesfuegoRef;
	std::vector< pair<int,int> > h_mapas_historicos[1000];
	int h_historicos_paso[1000];




	static int ROWS;
	static int COLS;
	static int map_size;

	// lista donde se mantienen los N mejores individuos
//	Lista_Campeones campeones;
//	Fitness fitness;

	// float array to hold fire map
	float *h_terrain;

 	// float array to hold environmental features
	static float *h_slope, *h_aspect, *h_wind, *h_forest, *h_alti, *h_vege;
	static float *d_slope, *d_aspect, *d_wind, *d_forest, *d_alti, *d_vege;
	static curandState *d_state;

	float *h_real_fire;
	float *d_real_fire;

	float *d_reference_map;  // mapas de referencia para calcular el fitness (en cpu y gpu para hacerlo secuencial o paralelo)
	float **h_reference_map; //cada puntero apunta a una mapa de referencia
	//Propagation Probabilities Map
	float *d_pmap, *h_pmap;

	int *h_fuego_propaga;
	int *d_fuego_propaga;

        // private functions
	void CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols );
	void CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols );

	//void GetMap(const char * nombre, float *mapa, int nx, int ny);
	void GetMap(char * nombre, float *mapa, int nx, int ny);
	void SaveMap(char * nombre, float *mapa, int filas, int cols);

	int celda_quemable(float *h_vege, int celdax, int celday);

	void Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y,int nref);  //funcion privada
	void Obtener_coordenadas_fuego_referencia(int nref);
	//int inicializar_Mapa_Random(float *h_terrain, float *h_vege, int filas, int cols, std::vector<int> &h_indicesfuegoRef,int *x, int *y);
    public:


	 /*  typedef struct {
          int id;
          float fitness;
          float beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind;
          int x,y;
       }Individuo_T;
	*/


	// mapa con datos sobre la ignición del mapa
	DatosCelda *h_mapa_datos, *d_mapa_datos;
	Modelo_AutomataCelular();
	Modelo_AutomataCelular(int nx, int ny);
	~Modelo_AutomataCelular();

	void size(int nx, int ny);
	void alocar_compartidas(void);
	void liberar_compartidas(void);
	void alocar(void);
	void Update_vege_map(float * newmap);

	void Initmaps(int filas, int cols, int seed_, int *filaIni, int *colIni, std::vector<int> &h_indicesfuegoRef );

	//void Init_Reference_Map(int *x, int *y, int modulo, int nada,int nreferencia);
	void  Init_Reference_Map(int *x, int *y, int modulo, int nada, int nreferencia, vector<int> &h_indices_referencia);

	void Init_All_Maps(int filas, int cols, char *WIND_MAP_P, char *ELEV_MAP_P, char *SLOPE_MAP_P, char *REAL_FIRE_MAP_P, char *ALTI_MAP_P, char *ASPECT_MAP_P, char *VEGE_MAP_P, char *FOREST_MAP_P);
	void Init_Initial_Map(int *x, int *y);
	void InitFire();


	/* Funciones que popagan el fuego llamando a un kernel. Saco los datos de distintos lugares por eso son dos.  */
	void Fire_Propagation(int i, int *h_fuego_propaga, int *d_fuego_propaga, int verificar_propagacio);
	void Fire_Propagation_Fuego_de_Referencia(int semilla);


	void print_fire_progress(int iteration);
	void print_probability_progress(int iteration);


	void Inicializar_datos_celda(DatosCelda *h_mapa_datos);
//	float calcular_fitness_secuencial(int filas, int cols);
//	float calcular_fitness_paralelo(int filas, int cols);
	void PrintList(char * nombre, float fitness);
	void PrintParamsRef(char * nombre, int x, int y);
	void PrintParamsSims(char * nombre, int x, int y);
	// agrega a campeones sii el fitness es mayor al peor fitness de la lista
//	void agregar_a_campeones(float fitness, int x, int y);
//	params_t parametros;
	void ImprimirCampeonesEnArchivo();
	void FireProbabilityMap(int tcurrent_run);

	/* nuevos para el Algoritmo Genetico */
	//void Evaluar_Individuo(Individuo_T individuo);
	void Evaluar_Individuo(int indice, int flag_imprimir_mejor_mapa, int numero_generacion);  // este ultimo parametro es para simular con el mejor individuo
	void Evaluar_Individuo_FW(int indice, int flag_imprimir_mejor_mapa, int numero_generacion);  // este ultimo parametro es para simular con el mejor individuo
	void Evaluar_Individuo_FW_noh(int indice, int flag_imprimir_mejor_mapa, int numero_generacion, int paso_final);  // este ultimo parametro es para simular con el mejor individuo
	void Evaluar_Individuo_BW(int indice, int flag_imprimir_mejor_mapa, int numero_generacion);  // este ultimo parametro es para simular con el mejor individuo
	void Evaluar_Individuo_A_CERO(int indice, int flag_imprimir_mejor_mapa, int numero_generacion);
	void Evaluar_Individuo_INIT(int indice, int flag_imprimir_mejor_mapa, int numero_generacion);  // este ultimo parametro es para simular con el mejor individuo
	//int Evaluar_Poblacion(Individuo_T *d_poblacion, Fitness_T *h_fitnesses, int numero_generacion);

	/* Almacena desde un mapa un vector con las celdas quemadas */
	int Almacenar_Historico_Indice(float *h_mapa_aux, int rows, int cols, vector< pair<int,int> > h_mapas_historicos[100], int indice_historicos,int paso);

	/* Recupera desde un mapa historico, arma el mapa de ROWS x COLS */
	int Recuperar_Historico_Indice(float *h_mapa_aux, int rows, int cols, vector< pair<int,int> > h_mapas_historicos[100], int indice_historicos,int & paso);
	void Set_Pixel(int celda);
};

void mifree(float * ptr);
void micudafree(float * ptr);
void print_vector(vector< pair<int,int> > h_mapas_historicos[1000], int indice_historicos);
void inicializar_Mapa_Determinista(float *h_terrain, float *h_vege, int filas, int cols,int *x,int *y);
int inicializar_Mapa_Random(float *h_terrain, float *h_vege, int filas, int cols, std::vector<int> &h_indicesfuegoRef,int *x, int *y);
int Obtener_coordenadas_fuego_inicial(float *h_terrain, int filas, int cols, int *filaIni, int *colIni);
void Mapa_Diferencia(float * d_mapaA,float * d_mapaB,float * d_mapaAmB,int ROWS, int COLS);
