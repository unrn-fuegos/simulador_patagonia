#pragma once
/* Datos propios del proyecto */
#include "manejo_archivos.h"


// carga los datos del proyecto pasado por parametro
//int Cargar_Datos_Proyecto(char *proyecto, int *ROWS_P, int *COLS_P, char *WIND_MAP_P, char *WINDI_MAP_P, char *ELEV_MAP_P, char *SLOPE_MAP_P, char *REAL_FIRE_MAP_P, char *ALTI_MAP_P, char *ASPECT_MAP_P, char *VEGE_MAP_P, char *VEGE_PAL_P, char *FOREST_MAP_P, int* XIGNI_P, int* YIGNI_P, int* celdas_prom) ;

class Proyecto{
public:
	char proyecto[100];
	int ROWS,COLS;
	char WIND_MAP_P[100];
	char WINDI_MAP_P[100];
	char ELEV_MAP_P[100];
	char SLOPE_MAP_P[100];
	char REAL_FIRE_MAP_P[100];
	char ALTI_MAP_P[100];
	char ASPECT_MAP_P[100];
	char VEGE_MAP_P[100];
	char VEGE_PAL_P[100];
	char FOREST_MAP_P[100];
	char PALETA_F[100];
	int XIGNI,YIGNI;
	int celdas_prom;

	char * fuelnames[100];
	int vegeR[100],vegeG[100],vegeB[100];
	int paleta_conversion_vege[100];
	int start_fullscreen;

	int marcar;

	FWI_OUT fwi;

	int Cargar_config(char* proyecto);
	int Cargar_paleta(void);
	void mover_paleta_a_grafico(void);
	void mover_grafico_a_paleta(void);

};