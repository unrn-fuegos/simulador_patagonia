#pragma once
//#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <math.h>
#include <cmath>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp" 
#include "glm/gtx/quaternion.hpp"
#include <fstream>
#include <sys/stat.h>
#ifdef WIN32 
	#include <windows.h>
#endif
#include <thread>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
//#include <crtdbg.h>
#include <time.h>
#pragma comment(lib, "glew32s.lib")

#include "GL/glew.h"
#include "GL/glu.h"
#include "GL/glut.h"

#include "GL/freeglut.h"

class Shader
{
public:
	Shader();
	~Shader();
	void load_and_link_shaders(const char*vertex, const char*geometry, const char*fragment);
	void load_and_link_shaders(const char*vertex, const char*fragment);
	void getUniformLocations();
	void deletee();

	GLuint programID;
};

