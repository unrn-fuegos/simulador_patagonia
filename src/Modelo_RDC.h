#pragma once

#include "Parameters.h"

#include <vector>

#include "Historicos.h"
#include <curand.h>
#include <curand_kernel.h>

#include "Shader.h"






/* Declaración de la clase Modelo_RDC */
class Modelo_RDC
{
public:
	Shader mkariprogram,mkariintegralprogram,program, waterprogram, waterQprogram, d2program, carprogram, particleprogram, vegetacionprogram;
	GLuint GtexIS,GtexSlAsWiFo,GtexAlReVe;
	GLuint myFBO,myFBO2,mytexture,mytexture2;
	GLuint Gvao;
	GLuint Gvbo;
	GLuint Gibo;
	GLuint Gnbo;
	GLuint Gmbo;
	GLuint Guvbo;
	GLuint GtID;
	GLuint GTtID;

	GLint modelTransformMatrixUniformLocation;
	GLint modelTraslationMatrixUniformLocation;
	GLint modelRotationMatrixUniformLocation;
	GLint domiColorUniformLocation;
	GLint ambientLightUniformLocation;
	GLint ambientLightColorUniformLocation;
	GLint enablebackfacecullingUniformLocation;
	GLint cameraPositionUniformLocation;
	GLint skyColorUniformLocation;
	GLint ClipPlaneUniformLocation;

	GLuint textureID;

	float *h_terrain;
	float *d_terrain, *d_terrainnew, *d_terrainnew4;
    
	float *d_terrain_aux, *d_terrainnew_aux;
    
    // float array to hold environmental features
	float *h_slope, *h_aspect, *h_wind, *h_forest, *h_alti, *h_real_fire, *h_vege;
	float *d_slope, *d_aspect, *d_wind, *d_forest, *d_alti, *d_real_fire, *d_vege;
	float *h_reference_map; 

	int modulo = 1;  // para imprimir los mapas y ver el avance del fuego imprime cada 2

	unsigned char *mytextu;
	unsigned char* tex;

	int XIGNI,YIGNI;
	int ROWS,COLS;

	float *floattex1;

	Historicos Histo;

	//DatosCelda *h_mapa_datos, *d_mapa_datos;

	int g_icant;
	float * texIS,*texSlAsWiFo,*texAlReVe;
	float dt;
	Modelo_RDC();
	~Modelo_RDC();
	void paso_adelante(void);
	void atras(void);
	void gl_error(void);
	void inicializar(void);
	GLuint createTextureAttachment(int width, int height, int nro);
	GLuint createDepthTextureAttachment(int width, int height);
	GLuint createDepthBufferAttachment(int width, int height);
	void loadTESTOBJECT(void);
	unsigned char* loadBMP_custom(const char * imagepath, int* w,int* h);
	void generate_fbo(void);
	void Init_All_Maps(int filas, int cols, char *WIND_MAP_P, char *ELEV_MAP_P, char *SLOPE_MAP_P, char *REAL_FIRE_MAP_P, char *ALTI_MAP_P, char *ASPECT_MAP_P, char *VEGE_MAP_P, char *FOREST_MAP_P);
	void Init_Initial_Map(int *x, int *y, int t);
	void inicializar_Mapa_Determinista(float *h_terrain, float *h_vege, int filas, int cols,int *x,int *y);
	void Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y);  //funcion privada
	int celda_quemable(float *h_vege, int celdax, int celday);
	void Update_vege_map(float * newmap);
	void Update_texIS_map(void);
	void Set_Pixel(int celda);

private:
        // private functions
	void CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols );
	void CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols );
	void GetMap(const char * nombre, float *mapa, int nx, int ny);
	void SaveMap(char * nombre, float *mapa, int filas, int cols);
	int filas,cols;

};

