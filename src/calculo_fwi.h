
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include "manejo_archivos.h"
#include "constantes.h"
//#include "estadosVegetacion.h"



   

// ecuaciones del FWI                     
FLOAT calculaFFMC(FLOAT HH, FLOAT TT, FLOAT WW, FLOAT Ro, FLOAT Fo);
FLOAT calculaDMC(FLOAT HH, FLOAT TT, FLOAT Ro, FLOAT Po, int MM);
FLOAT calculaDC(FLOAT TT, FLOAT Ro, FLOAT Do, int MM);
FLOAT calculaISI(FLOAT F, FLOAT WW);
FLOAT calculaBUI(FLOAT P, FLOAT D);
FLOAT calculaFWI(FLOAT U, FLOAT R);
void peligrosidadXtipo(FLOAT isi, FLOAT bui, int GSecPorc, int *pelig);

