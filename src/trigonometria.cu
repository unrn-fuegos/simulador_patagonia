//#include "superinclude.h"
#include "Singleton.h"
#include "trigonometria.h"
#include <stdio.h>


using namespace glm;


int intersection_calc(int *ni,int *nj){
    vec3 ray_WCS;
    *ni=-1;
    *nj=-1;
    raycast_calc(&ray_WCS);

    vec3 v0=vec3(0,0,0);
    vec3 v1=vec3(5,0,0);
    vec3 v2=vec3(5,0,5);
    vec3 origen=Singleton::getInstance()->MainCam.position;
    int i,j;
    int idxptr=0,triangidx=0;
    float nearest_dist=999999.0f;
    float dist;
    vec3 v[3];
    int vert_x=Singleton::getInstance()->vert_x;
    int vert_y=Singleton::getInstance()->vert_y;
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;
    //printf("vert_x=%d vert_y=%d\n",vert_x,vert_y);
    //printf("vert_gis_x=%d vert_gis_y=%d\n",vert_gis_x,vert_gis_y);

    vec3* verticesPosicion=&Singleton::getInstance()->verticesPosicion[0];
    int* verticesIndex=&Singleton::getInstance()->verticesIndex[0];
    
    for(j=0;j<vert_y-1;j++)
    {
        for(i=0;i<vert_x-1;i++)
        {
            vec3 v0=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            vec3 v1=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            vec3 v2=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            vec3 origen=Singleton::getInstance()->MainCam.position;
            dist=triangle_intersection(&origen,&ray_WCS,&v0,&v1,&v2);
            if(dist>0.0f)
            {
                if(dist<nearest_dist)
                {
                    nearest_dist=dist;
                    *ni=i;*nj=j;
                    v[0]=v0;
                    v[1]=v1;
                    v[2]=v2;
                }
            }

            triangidx++;

            v0=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            v1=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            v2=verticesPosicion[verticesIndex[idxptr]];idxptr++;
            dist=triangle_intersection(&origen,&ray_WCS,&v0,&v1,&v2);
            if(dist>0.0f)
            {
                if(dist<nearest_dist)
                {
                    nearest_dist=dist;
                    *ni=i;*nj=j;
                    v[0]=v0;
                    v[1]=v1;
                    v[2]=v2;
                }
            }

            triangidx++;
        }

    }
    if((*ni)>=0)//si intersecta
    {
        vec3 intersec=(origen)+(ray_WCS)*nearest_dist;
        //calcular a que distancia del vertice intersecta y segun eso obtener X e Y reales.
        int x,y;
        //float correcx=(float)vert_gis_x/(vert_gis_x-vert_gis_x%Singleton::getInstance()->celdas_prom);//423.0f/420.0f;
        //float correcy=(float)vert_gis_y/(vert_gis_y-vert_gis_y%Singleton::getInstance()->celdas_prom);//423.0f/420.0f;

        float correcx=1.0f;//423.0f/420.0f;
        float correcy=1.0f;//423.0f/420.0f;


        x=(int)floor((float)correcx*intersec.x/0.0125f);//revisar esta parte para que el mouse marque bien
        y=(int)floor((float)correcy*intersec.z/0.0125f);
        //423*intersec.x/(0.0125f*420)
        if(x>(vert_gis_x-1) || y>(vert_gis_y-1)) {
            *ni=-1;
            *nj=-1;
            return 0;
        }


        *ni=x;
        *nj=y;

        //printf("X=%d Y=%d\n",x,y);
        return 1;

    }
    return 0;
}

void printvec3(const char* txt, vec3* vec){
    printf("%sx=%.06f y=%.06f z=%.06f\n",txt,vec->x,vec->y,vec->z);
}



float triangle_intersection(vec3* orig,
                      vec3* dir,
                      vec3* v0,
                      vec3* v1,
                      vec3* v2) {
    vec3 e1 = (*v1) - (*v0);
    vec3 e2 = (*v2) - (*v0);
    // Calculate planes normal vector
    vec3 pvec = cross(*dir, e2);
    float det = dot(e1, pvec);

    // Ray is parallel to plane
    if (det < 1e-8 && det > -1e-8) {
        return 0;
    }

    float inv_det = 1 / det;
    vec3 tvec = (*orig) - (*v0);
    float u = dot(tvec, pvec) * inv_det;
    if (u < 0 || u > 1) {
        return 0;
    }

    vec3 qvec = cross(tvec, e1);
    float v = dot(*dir, qvec) * inv_det;
    if (v < 0 || u + v > 1) {
        return 0;
    }
    return dot(e2, qvec) * inv_det;
}

void raycast_calc(vec3* ray_WCS){
    float x = (2.0f * Singleton::getInstance()->mouse_x) / Singleton::getInstance()->window_width - 1.0f;
    float y = 1.0f - (2.0f * Singleton::getInstance()->mouse_y) / Singleton::getInstance()->window_height;
    float z = -1.0f;

    vec3 ray_nds = vec3(x, y, z);

    vec4 ray_clip = vec4(ray_nds, 1.0);
    vec4 ray_eye = glm::inverse(Singleton::getInstance()->projectionMatrix) * ray_clip;
    ray_eye = vec4(ray_eye.x, ray_eye.y,  -1.0, 0.0);
    vec4 aux_vec4=inverse(Singleton::getInstance()->matrixWTV) * ray_eye;
    vec3 ray_wor = vec3(aux_vec4);
    ray_wor = normalize(ray_wor);
    *ray_WCS=ray_wor;
    //printf("x=%.02f y=%.02f z=%.02f\n",ray_wor.x,ray_wor.y,ray_wor.z);
}

void si_intersecta(int x,int y)
{
    printf("Intersecta con X=%d \tY=%d\n",x,y);
}
