#pragma once
////////////////////////////////////////////////////////////////////////////////
// declaration, forward


#include "Simulador.h"


bool runTest(int argc, char **argv, char *ref_file);
void cleanup();

// GL functionality
void createVBO(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags);
void createVBC(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags);
void deleteVBO(GLuint *vbo, struct cudaGraphicsResource *vbo_res);

// rendering callbacks
void display();
void timerEvent(int value);

// Cuda functionality
void runCuda(struct cudaGraphicsResource **vbo_resource);
void runAutoTest(int devID, char **argv, char *ref_file);
void checkResultCuda(int argc, char **argv, const GLuint &vbo);

void inicializar_mapa(struct cudaGraphicsResource **vbo_resource,struct cudaGraphicsResource **vbc_resource);
//vec3 get_color(int mapval);
void GenerarVertices(void);
void calc_camera_front(void);
float promedio_vecinas(float* map,int x, int y);
float* promediar_mapa(float* map);
void promediar_mapas(void);
void cargar_mapas_de_archivo(void);
int intersection_calc(int *ni,int *nj);
void ImGui_DrawAll(void);
void generar_textura(float* map,unsigned char** texdata,vec3 * paleta);
void regenerar_textura_final(void);
int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);

int Imprimir_Datos_Placa_Grafica();
int main_simulador(void);

void CP_1(void);
void CP_2(void);



