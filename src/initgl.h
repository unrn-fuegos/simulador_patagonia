#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>


#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#ifdef _WIN32
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#endif



#include "GL/glew.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <GL/freeglut.h>

// includes, cuda
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "Singleton.h"
#include "initgl.h"

#include "main.h"

#include "entrada.h"


// CUDA helper functions
#include "cuda_helpers/helper_cuda.h"         // helper functions for CUDA error check
#include "cuda_helpers/helper_cuda_gl.h"      // helper functions for CUDA/GL interop
//#include "superinclude.h"

void deleteVBO(GLuint *vbo, struct cudaGraphicsResource *vbo_res);
void createVBC(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags);
void createVBO(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags);
void initGLBUF(void);
void initGLUT(char * proyecto);
void installShaders(void);
char* readShaderCode(const char* fileName);
bool checkProgramStatus(GLuint programID);
bool checkShaderStatus(GLuint shaderID);
int findGraphicsGPU(char *name);
bool checkHW(char *name, const char *gpuType, int dev);
void launch_kernel(float4 *pos, unsigned int mesh_width,
                   unsigned int mesh_height, float time);

void ImGui_DrawAll(void);
void reset_camera(void);
void generar_textura_final(unsigned char** textura_final,float*h_mapavege,int*h_mapaquemado,int*h_mapaselcorf);
void generar_textura_aspect(unsigned char** textura_aspect,float*h_mapaaspect,float*h_mapaaslope);
void generar_textura_pelig(unsigned char** textura_pelig, int*h_mapavege);
void generar_textura_windI(unsigned char** textura_windi,float*h_mapawindINT);
void sendHUDUniformsToOpenGL(GLuint programID);
void sendHUD2UniformsToOpenGL(GLuint programID);
void sendUniformsToOpenGL(GLuint programID);
void sendLINESUniformsToOpenGL(GLuint programID);
void generate_fbo(void);
