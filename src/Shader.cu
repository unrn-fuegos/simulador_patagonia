
#include "Shader.h"

#pragma comment(lib, "glew32s.lib")

#include "initgl.h"


Shader::Shader()
{
}


Shader::~Shader()
{
}

/*bool checkProgramStatus(GLuint programID) {
	GLint linkStatus;
	glGetProgramiv(programID, GL_LINK_STATUS, &linkStatus);
	if (linkStatus != GL_TRUE) {
		GLint infoLogLength;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		glGetProgramInfoLog(programID, infoLogLength, &bufferSize, buffer);

		printf("%s\n", buffer);

		delete[] buffer;
		return false;
	}
	return true;
}

bool checkShaderStatus(GLuint shaderID) {
	GLint compileStatus;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compileStatus);
	if (compileStatus != GL_TRUE) {
		GLint infoLogLength;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		glGetShaderInfoLog(shaderID, infoLogLength, &bufferSize, buffer);

		printf("%s\n", buffer);

		delete[] buffer;
		return false;
	}
	return true;
}

char* readShaderCode(const char* fileName) {
	FILE *input = fopen(fileName, "r");

	if (input == NULL)
	{
		printf("Error abriendo archivo %s \n", fileName);
		exit(-1);
	}

	int i, ptr = -1;
	char* buffer;
	char lbuf[256];



	buffer = (char*)malloc(10000);
	if (buffer)
	{
		while (!feof(input))
		{
			if (!fgets(lbuf, 256, input)) {break; }
		//	fgets(lbuf, 256, input);
			i = 0;
			while (lbuf[i] != '\n')
			{
				ptr++;
				buffer[ptr] = lbuf[i];
				i++;
			}
			ptr++;
			buffer[ptr] = '\n';
		}
		ptr++;
		buffer[ptr] = '\0';
	}

	fclose(input);

	//printf("%sFIN",buffer);
	return buffer;
}*/

void Shader::load_and_link_shaders(const char*vertex, const char*fragment)
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	print_debug("Comenzando carga de %s %s \n",vertex,fragment);

	const char* adapter[1];
	char* str = readShaderCode(vertex);
	adapter[0] = str;
	glShaderSource(vertexShaderID, 1, adapter, 0);
	str = readShaderCode(fragment);
	adapter[0] = str;
	glShaderSource(fragmentShaderID, 1, adapter, 0);
	free(str);

	glCompileShader(vertexShaderID);
	glCompileShader(fragmentShaderID);

	if (!checkShaderStatus(vertexShaderID)) {
		print_debug("En el archivo %s\n",vertex);
		exit(-1);
	}

	if (!checkShaderStatus(fragmentShaderID)) {
		print_debug("En el archivo %s\n", fragment);
		exit(-1);
	}

	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);

	glLinkProgram(programID);

	if (!checkProgramStatus(programID)) {
		print_debug("En el programa de archivos %s %s\n", vertex,fragment);
		exit(-1);
	}
}



void Shader::load_and_link_shaders(const char*vertex, const char*geometry, const char*fragment)
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint geometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);
	GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	const char* adapter[1];
	char* str = readShaderCode(vertex);
	adapter[0] = str;
	glShaderSource(vertexShaderID, 1, adapter, 0);
	str = readShaderCode(fragment);
	adapter[0] = str;
	glShaderSource(fragmentShaderID, 1, adapter, 0);
	str = readShaderCode(geometry);
	adapter[0] = str;
	glShaderSource(geometryShaderID, 1, adapter, 0);

	free(str);

	glCompileShader(vertexShaderID);
	glCompileShader(geometryShaderID);
	glCompileShader(fragmentShaderID);


	if (!checkShaderStatus(geometryShaderID)) {
		print_debug("En el archivo %s\n", geometry);
		exit(-1);
	}

	if (!checkShaderStatus(vertexShaderID)) {
		print_debug("En el archivo %s\n", vertex);
		exit(-1);
	}

	if (!checkShaderStatus(fragmentShaderID)) {
		print_debug("En el archivo %s\n", fragment);
		exit(-1);
	}



	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);

	glLinkProgram(programID);

	if (!checkProgramStatus(programID)) {
		print_debug("En el programa de archivos %s %s %s\n", vertex, geometry, fragment);
		exit(-1);
	}
}

void delete_shader_and_program(GLuint*programID);

void Shader::deletee()
{
	delete_shader_and_program(&programID);
}


void Shader::getUniformLocations()
{
	Singleton::getInstance()->Simulador.RDC->domiColorUniformLocation = glGetUniformLocation(programID, "domiColor");
	Singleton::getInstance()->Simulador.RDC->ambientLightUniformLocation = glGetUniformLocation(programID, "ambientLight");
	//eyePositionWorldUniformLocation = glGetUniformLocation(programID, "eyePositionWorld");
	//fullTransfWTPUniformLocation = glGetUniformLocation(programID, "fullTransfWTP");
	//cameraDirectionUniformLocation = glGetUniformLocation(programID, "cameraDirection");
	Singleton::getInstance()->Simulador.RDC->modelTransformMatrixUniformLocation = glGetUniformLocation(programID, "modelTransformMatrix");
	Singleton::getInstance()->Simulador.RDC->enablebackfacecullingUniformLocation = glGetUniformLocation(programID, "bfcEnabled");
	Singleton::getInstance()->Simulador.RDC->cameraPositionUniformLocation = glGetUniformLocation(programID, "cameraPosition");
	Singleton::getInstance()->Simulador.RDC->skyColorUniformLocation = glGetUniformLocation(programID, "skyColor");
	Singleton::getInstance()->Simulador.RDC->ClipPlaneUniformLocation = glGetUniformLocation(programID, "ClipPlane");
}
