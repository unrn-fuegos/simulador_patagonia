#include "initgl.h"



#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

extern const char *sSDKsample;


using namespace glm;

void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v );

//*******************************************************************************************INITGL
///////////////////////////////////////////////////////////////////////////////
//! Simple kernel to modify vertex positions in sine wave pattern
//! @param data  data in global memory
///////////////////////////////////////////////////////////////////////////////
//__global__ void simple_vbo_kernel(float4 *pos, unsigned int width, unsigned int height, float time)
//{
    //unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    //unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

/*
    // calculate uv coordinates
    float u = x / (float) width;
    float v = y / (float) height;
    u = u*2.0f - 1.0f;
    v = v*2.0f - 1.0f;

    // calculate simple sine wave pattern
    float freq = 4.0f;
    float w = sinf(u*freq + time) * cosf(v*freq + time) * 0.5f;

    // write output vertex
    pos[y*width+x] = make_float4(u, w, v, 1.0f);
*/


//}


void launch_kernel(float4 *pos, unsigned int mesh_width,
                   unsigned int mesh_height, float time)
{
    // execute the kernel
    /*dim3 block(8, 8, 1);
    dim3 grid(mesh_width / block.x, mesh_height / block.y, 1);
    simple_vbo_kernel<<< grid, block>>>(pos, mesh_width, mesh_height, time);*/
}

bool checkHW(char *name, const char *gpuType, int dev)
{
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, dev);
    strcpy(name, deviceProp.name);

    if (!STRNCASECMP(deviceProp.name, gpuType, strlen(gpuType)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

int findGraphicsGPU(char *name)
{
    int nGraphicsGPU = 0;
    int deviceCount = 0;
    bool bFoundGraphics = false;
    char firstGraphicsName[256], temp[256];

    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

    if (error_id != cudaSuccess)
    {
        printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
        print_debug("> FAILED %s sample finished, exiting...\n", sSDKsample);
        exit(EXIT_FAILURE);
    }

    // This function call returns 0 if there are no CUDA capable devices.
    if (deviceCount == 0)
    {
        print_debug("> There are no device(s) supporting CUDA\n");
        return false;
    }
    else
    {
        print_debug("> Found %d CUDA Capable Device(s)\n", deviceCount);
    }

    for (int dev = 0; dev < deviceCount; ++dev)
    {
        bool bGraphics = !checkHW(temp, (const char *)"Tesla", dev);
        printf("> %s\t\tGPU %d: %s\n", (bGraphics ? "Graphics" : "Compute"), dev, temp);

        if (bGraphics)
        {
            if (!bFoundGraphics)
            {
                strcpy(firstGraphicsName, temp);
            }

            nGraphicsGPU++;
        }
    }

    if (nGraphicsGPU)
    {
        strcpy(name, firstGraphicsName);
    }
    else
    {
        strcpy(name, "this hardware");
    }

    return nGraphicsGPU;
}

bool checkShaderStatus(GLuint shaderID){
	GLint compileStatus;
	glGetShaderiv(shaderID,GL_COMPILE_STATUS,&compileStatus);
	if(compileStatus!=GL_TRUE){
		GLint infoLogLength;
		glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH,&infoLogLength);
		GLchar* buffer = new GLchar[infoLogLength];

		GLsizei bufferSize;
		glGetShaderInfoLog(shaderID,infoLogLength,&bufferSize,buffer);

		print_debug("%s\n",buffer);

		delete [] buffer;
		return false;
	}
	return true;
}

char* readShaderCode(const char* fileName){
	FILE *input = fopen(fileName,"r");

	if (input == NULL)
	{
		print_debug("Error abriendo archivo %s \n", fileName);
		exit(-1);
	}

	int i,ptr=-1;
	char* buffer;
	char lbuf[256];



	buffer = (char*)malloc(10000);
	if(buffer)
	{
		while(fgets(lbuf, 256, input))
		{
			i=0;
			while(lbuf[i]!='\n')
			{
				ptr++;
				buffer[ptr]=lbuf[i];
				i++;
			}
			ptr++;
			buffer[ptr]='\n';
		}
		ptr++;
		buffer[ptr]='\0';
	}

	fclose(input);

	//printf("%sFIN",buffer);
	return buffer;
}

bool checkProgramStatus(GLuint programID){
    GLint linkStatus;
    glGetProgramiv(programID,GL_LINK_STATUS,&linkStatus);
    if(linkStatus!=GL_TRUE){
        GLint infoLogLength;
        glGetProgramiv(programID,GL_INFO_LOG_LENGTH,&infoLogLength);
        GLchar* buffer = new GLchar[infoLogLength];

        GLsizei bufferSize;
        glGetProgramInfoLog(programID,infoLogLength,&bufferSize,buffer);

        print_debug("%s\n",buffer);

        delete [] buffer;
        return false;
    }
    return true;
}

void load_and_link_shaders(GLuint*programID, char*vertex ,char*fragment)
{
    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);


    const char* adapter[1];
    const char* adapter1[1];
    char* str = readShaderCode(vertex);
    adapter[0] = str;
    glShaderSource(vertexShaderID, 1, adapter, 0);
    str = readShaderCode(fragment);
    adapter1[0] = str;
    glShaderSource(fragmentShaderID, 1, adapter1, 0);

    free((void*)adapter[0]);
    free((void*)adapter1[0]);

    glCompileShader(vertexShaderID);
    glCompileShader(fragmentShaderID);

    if (!checkShaderStatus(vertexShaderID)) {
        print_debug("Shader %s compilation error.\n",vertex);
        exit(-1);
    }

    if (!checkShaderStatus(fragmentShaderID)) {
        print_debug("Shader %s compilation error.\n",fragment);
        exit(-1);
    }

    *programID = glCreateProgram();
    glAttachShader(*programID, vertexShaderID);
    glAttachShader(*programID, fragmentShaderID);

    glLinkProgram(*programID);

    if (!checkProgramStatus(*programID)) {
        print_debug("Shader %s %s link error.\n",vertex,fragment);
        exit(-1);
    }
}

void load_and_link_shaders(GLuint*programID, char*vertex, char*geometry, char*fragment)
{
    GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint geometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);
    GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    const char* adapter[1];
    const char* adapter1[1];
    const char* adapter2[1];
    char* str = readShaderCode(vertex);
    adapter[0] = str;
    glShaderSource(vertexShaderID, 1, adapter, 0);
    str = readShaderCode(fragment);
    adapter1[0] = str;
    glShaderSource(fragmentShaderID, 1, adapter1, 0);
    str = readShaderCode(geometry);
    adapter2[0] = str;
    glShaderSource(geometryShaderID, 1, adapter2, 0);

    free((void*)adapter[0]);
    free((void*)adapter1[0]);
    free((void*)adapter2[0]);

    glCompileShader(vertexShaderID);
    glCompileShader(geometryShaderID);
    glCompileShader(fragmentShaderID);


    if (!checkShaderStatus(vertexShaderID)) {
        print_debug("Shader %s compilation error.\n",vertex);
        exit(-1);
    }

    if (!checkShaderStatus(geometryShaderID)) {
        print_debug("Shader %s compilation error.\n",geometry);
        exit(-1);
    }

    if (!checkShaderStatus(fragmentShaderID)) {
        print_debug("Shader %s compilation error.\n",fragment);
        exit(-1);
    }


    *programID = glCreateProgram();
    glAttachShader(*programID, vertexShaderID);
    glAttachShader(*programID, geometryShaderID);
    glAttachShader(*programID, fragmentShaderID);

    glLinkProgram(*programID);

    if (!checkProgramStatus(*programID)) {
        print_debug("Shader %s %s %s link error.\n",vertex,geometry,fragment);
        exit(-1);
    }
}

void delete_shader_and_program(GLuint*programID){

    GLsizei cnt=0;
    GLuint shaders[3];
    glGetAttachedShaders(*programID,3,&cnt,shaders);
    for(int i=0;i<cnt;i++)
    {
        glDeleteShader(shaders[i]);
    }

    glDeleteProgram(*programID);
}


void installShaders(void){
    load_and_link_shaders(&Singleton::getInstance()->programID, (char*)"shaders/vertexShaderCode.glsl", (char*)"shaders/fragmentShaderCode.glsl");
	load_and_link_shaders(&Singleton::getInstance()->program2dID, (char*)"shaders/Shader2dv.glsl", (char*)"shaders/Shader2df.glsl");
    load_and_link_shaders(&Singleton::getInstance()->programHUDID, (char*)"shaders/vHUD.glsl", (char*)"shaders/fHUD.glsl");
    load_and_link_shaders(&Singleton::getInstance()->programLINESID, (char*)"shaders/vLINES.glsl", (char*)"shaders/fLINES.glsl");

    glUseProgram(Singleton::getInstance()->programID);
}

////////////////////////////////////////////////////////////////////////////////
//! Initialize GL
////////////////////////////////////////////////////////////////////////////////

void initGLUT(char * proyecto){
	int c=1;
	char **v;
	v=(char**)malloc(sizeof(char*)*2);
	v[0]=(char*)malloc(10);
	sprintf(v[0],"proban");
	v[1]=NULL;

    glutInit(&c, v);//no se por que GLUT requiere esto....
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(Singleton::getInstance()->window_width, Singleton::getInstance()->window_height);

    char title[256];
    //sprintf(title,"Simulador de Incendios Forestales. - %s",proyecto);
    sprintf(title,"Simulador de Incendios");

    glutCreateWindow(title);

    char* arry;
    int minv, majv;
    arry = (char*)glGetString(GL_VERSION);
    glGetIntegerv(GL_MAJOR_VERSION, &majv);
    glGetIntegerv(GL_MINOR_VERSION, &minv);
    print_debug("OpenGL version=%s %d.%d\n", arry, majv, minv);

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard_dn);
    glutKeyboardUpFunc(keyboard_up);
    glutSpecialFunc(special_keyboard_dn);
    glutSpecialUpFunc(special_keyboard_up);
    glutMotionFunc(motion);
    glutPassiveMotionFunc(motion);
    glutMouseFunc(mouse);
    glutReshapeFunc(reshape);
    //glutPassiveMotionFunc(mouseMotion_noButtonPressed);
    glutTimerFunc(REFRESH_DELAY, timerEvent,0);
    glEnable(GL_DEPTH_TEST);

    glClearColor(0.0, 0.0f, 0.0, 1.0);
    //glEnable(GL_DEPTH_TEST);

    // viewport
    glViewport(0, 0, Singleton::getInstance()->window_width, Singleton::getInstance()->window_height);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}



unsigned char pack_float01_as_byte(float valor)
{
    unsigned char val2;
    float aux;
    aux=valor*127.0f+127.0f;
    val2=(unsigned char)aux;
    if(aux<0.0f || aux>255.0f){printf("%.02f ",aux);}
    return val2;
}

void generar_textura_windI(unsigned char** textura_windi,float*h_mapawindINT){
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    int colormap_width=Singleton::getInstance()->colormap_width;
    float windi_max=Singleton::getInstance()->windi_max;
    float windi_min=Singleton::getInstance()->windi_min;
    unsigned char * colormap_bmp=Singleton::getInstance()->colormap_bmp;
    if((*textura_windi)==NULL){(*textura_windi)=(unsigned char*)realloc(*textura_windi,sizeof(unsigned char)*tex_x*tex_y*4);}
    //printf("++++++  %d \n",h_mapawindINT);
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            //vec3 aux=get_color(map[x+y*tex_x]);
            int pos=x+y*tex_x;
            int ppos=20;
            vec3 aspect;
            /*aspect.x=-sin(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.y=cos(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.z=(90.0f-h_mapaslope[pos])*255.0f/90.0f;*/
            //printf("/ %d %d %d \n",x,y,pos);
            if(h_mapawindINT[pos]<0.0f){
                float r=1.0f,g=0.0f,b=1.0f;

                textura_windi[0][(x+y*tex_x)*4+0]=(unsigned char)((float)r*255.0f);
                textura_windi[0][(x+y*tex_x)*4+1]=(unsigned char)((float)g*255.0f);
                textura_windi[0][(x+y*tex_x)*4+2]=(unsigned char)((float)b*255.0f);
                textura_windi[0][(x+y*tex_x)*4+3]=255;
            }else{
                if(h_mapawindINT[pos]<=windi_min){ppos=colormap_width-1;}
                else if(h_mapawindINT[pos]>=windi_max){ppos=0;}
                else{
                    float pwrperc=1.0f-((h_mapawindINT[pos]-windi_min)/(windi_max-windi_min));
                    float fpix=(float)(colormap_width-1)*pwrperc;
                    ppos=int(fpix);
                }


                textura_windi[0][(x+y*tex_x)*4+0]=colormap_bmp[ppos*3];
                textura_windi[0][(x+y*tex_x)*4+1]=colormap_bmp[ppos*3+1];;
                textura_windi[0][(x+y*tex_x)*4+2]=colormap_bmp[ppos*3+2];;
                textura_windi[0][(x+y*tex_x)*4+3]=255;
            }


        }
    }

}

void generar_textura_aspect(unsigned char** textura_aspect,float*h_mapaaspect,float*h_mapaaslope){
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    if((*textura_aspect)==NULL){(*textura_aspect)=(unsigned char*)realloc(*textura_aspect,sizeof(unsigned char)*tex_x*tex_y*4);}
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            //vec3 aux=get_color(map[x+y*tex_x]);
            int pos=x+y*tex_x;
            vec3 aspect;
            /*aspect.x=-sin(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.y=cos(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.z=(90.0f-h_mapaslope[pos])*255.0f/90.0f;*/
            float r,g,b;
            float p_grade=(float)100.0f*sinf((float)3.1415f*Singleton::getInstance()->h_mapaslope[pos]/180.0f);
            float sat;
            if(p_grade>40){sat=1.0f;}
            else if(p_grade>20){sat=0.8f;}
            else if(p_grade>5){sat=0.6f;}
            else {sat=0.4f;}
            HSVtoRGB(&r,&g,&b,h_mapaaspect[pos],sat,1.0f);
            /*textura_aspect[0][(x+y*tex_x)*4+0]=(unsigned char)((float)h_mapaaspect[pos]*255.0f/360.0f);
            textura_aspect[0][(x+y*tex_x)*4+1]=0;
            textura_aspect[0][(x+y*tex_x)*4+2]=(unsigned char)((float)h_mapaslope[pos]*255.0f/90.0f);
            textura_aspect[0][(x+y*tex_x)*4+3]=255;*/

            textura_aspect[0][(x+y*tex_x)*4+0]=(unsigned char)((float)r*255.0f);
            textura_aspect[0][(x+y*tex_x)*4+1]=(unsigned char)((float)g*255.0f);
            textura_aspect[0][(x+y*tex_x)*4+2]=(unsigned char)((float)b*255.0f);
            textura_aspect[0][(x+y*tex_x)*4+3]=255;

            /*textura_aspect[0][(x+y*tex_x)*4+0]=(unsigned char)((float)aspect.x*255.0f);
            textura_aspect[0][(x+y*tex_x)*4+1]=0;
            textura_aspect[0][(x+y*tex_x)*4+2]=(unsigned char)((float)aspect.y*255.0f);
            textura_aspect[0][(x+y*tex_x)*4+3]=255;*/
        }
    }

}

void generar_textura_pelig(unsigned char** textura_pelig, int*h_mapavege){
    //if(visualizar_pelig_indice<0)return;
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    if((*textura_pelig)==NULL){(*textura_pelig)=(unsigned char*)realloc(*textura_pelig,sizeof(unsigned char)*tex_x*tex_y*4);}
    int peli[5];
    int idx_to_vis=Singleton::getInstance()->Get_visualizar_pelig_indice();
    if(idx_to_vis>-1)
    {
        peli[0]=Singleton::getInstance()->FWI_T.fwi[idx_to_vis].pelig[0];
        peli[1]=Singleton::getInstance()->FWI_T.fwi[idx_to_vis].pelig[1];
        peli[2]=Singleton::getInstance()->FWI_T.fwi[idx_to_vis].pelig[2];
        peli[3]=Singleton::getInstance()->FWI_T.fwi[idx_to_vis].pelig[3];
        peli[4]=Singleton::getInstance()->FWI_T.fwi[idx_to_vis].pelig[4];
        sprintf(Singleton::getInstance()->peligmap.tooltiptext,"%d/%02d/%02d",Singleton::getInstance()->FWI_T.fwi[idx_to_vis].met.aa,Singleton::getInstance()->FWI_T.fwi[idx_to_vis].met.mm,Singleton::getInstance()->FWI_T.fwi[idx_to_vis].met.dd);
    }
    vec3* peli_col=&Singleton::getInstance()->peli_col[0];

    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {

            //vec3 aux=get_color(map[x+y*tex_x]);
            int pos=x+y*tex_x;
            vec3 rescol=vec3(0,0,0);
            if(h_mapavege[pos]>=3 && h_mapavege[pos]<=7 && idx_to_vis>-1)
            {
                int val;

                if(h_mapavege[pos]==3){val=2;}//TABLA DE CORRESPONDENCIA ENTRE COMBUSTIBLES DEL RASTER Y INDICE DE PELIGROSIDAD DEL EXCEL
                if(h_mapavege[pos]==4){val=3;}
                if(h_mapavege[pos]==5){val=4;}//ESTA TABLA HABRIA QUE GENERARLA AL INICIAR EL PROGRAMA, Y QUE HAGA UN STRCMP con los nombres
                if(h_mapavege[pos]==6){val=0;}//DE LOS COMBUSTIBLES, Y LO QUE NO ES UNO DE LOS 5 TIPOS RECONOCIDOS, PINTE DE NEGRO.
                if(h_mapavege[pos]==7){val=1;}
                rescol=peli_col[peli[val]];
            }

            textura_pelig[0][pos*4+0]=(unsigned char)(rescol.r);
            textura_pelig[0][pos*4+1]=(unsigned char)(rescol.g);
            textura_pelig[0][pos*4+2]=(unsigned char)(rescol.b);
            textura_pelig[0][pos*4+3]=255;
            Singleton::getInstance()->peligmap.h_map[pos]=256*256*((int)rescol.r)+256*((int)rescol.g)+((int)rescol.b);
            Singleton::getInstance()->peligmap.is_in_cpu=1;Singleton::getInstance()->peligmap.is_gpu_utd=0;
            Singleton::getInstance()->peligmap.nodata_value=0;
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
        }
    }
}

void generar_textura_alti(unsigned char** textura_alti,float*h_mapaaspect,float*h_mapaaslope,float*h_mapaalti){
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    if((*textura_alti)==NULL){(*textura_alti)=(unsigned char*)realloc(*textura_alti,sizeof(unsigned char)*tex_x*tex_y*4);}
    float delta_alti=Singleton::getInstance()->altitud_max-Singleton::getInstance()->altitud_min;
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            //vec3 aux=get_color(map[x+y*tex_x]);
            int pos=x+y*tex_x;
            //vec3 aspect;
            /*aspect.x=-sin(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.y=cos(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.z=(90.0f-h_mapaslope[pos])*255.0f/90.0f;*/
            float r,g,b;
            //HSVtoRGB(&r,&g,&b,20.0f,0.5f+0.5*(float)(h_mapaalti[pos]-Singleton::getInstance()->altitud_min)/delta_alti,0.7f);
            float hval=(float)(h_mapaalti[pos]-Singleton::getInstance()->altitud_min)/delta_alti;

            /*if(hval<0.25f){hval=hval*2.0f;}
            else if(hval>0.75f){hval=0.75f+(hval-0.75f)*2.0f;}
            else{hval=0.5f;}*/


            HSVtoRGB(&r,&g,&b,260.0f-260.0f*hval,1.0f,1.0f);

            textura_alti[0][(x+y*tex_x)*4+0]=(unsigned char)((float)r*255.0f);
            textura_alti[0][(x+y*tex_x)*4+1]=(unsigned char)((float)g*255.0f);
            textura_alti[0][(x+y*tex_x)*4+2]=(unsigned char)((float)b*255.0f);
            textura_alti[0][(x+y*tex_x)*4+3]=255;
        }
    }
}

void generar_textura_Calti(unsigned char** textura_alti,float*h_mapaaspect,float*h_mapaaslope,float*h_mapaalti){
    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;
    if((*textura_alti)==NULL){(*textura_alti)=(unsigned char*)realloc(*textura_alti,sizeof(unsigned char)*tex_x*tex_y*4);}
    float delta_alti=Singleton::getInstance()->altitud_max-Singleton::getInstance()->altitud_min;
    for(int y=0;y<tex_y;y++)
    {
        for(int x=0;x<tex_x;x++)
        {
            //vec3 aux=get_color(map[x+y*tex_x]);
            int pos=x+y*tex_x;
            //vec3 aspect;
            /*aspect.x=-sin(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.y=cos(h_mapaaspect[pos]*3.1415f/180.0f);
            aspect.z=(90.0f-h_mapaslope[pos])*255.0f/90.0f;*/

            float alti_frac=(float)(h_mapaalti[pos]-Singleton::getInstance()->altitud_min)/delta_alti;
            float sr=cos(3.1415f*alti_frac*Singleton::getInstance()->mapa_Calti_cant);
            float delta_picos=(float)1.0f/Singleton::getInstance()->mapa_Calti_cant;
            float colorstep=floor((float)alti_frac/delta_picos);
            sr=sr*sr;

            float val=pow(sr,Singleton::getInstance()->mapa_Calti_pow);

            float r,g,b;

            if(val<0.5f)
            {
                HSVtoRGB(&r,&g,&b,25.0f,0.5f+0.5f*(float)colorstep/Singleton::getInstance()->mapa_Calti_cant,1.0f);
                Singleton::getInstance()->levelmap.h_map[(x+y*tex_x)]=0;
            }
            else
            {
                val=1.0f;
                HSVtoRGB(&r,&g,&b,25.0f,0.5f+0.5f,1.0f-val);
                Singleton::getInstance()->levelmap.h_map[(x+y*tex_x)]=2;
            }




            textura_alti[0][(x+y*tex_x)*4+0]=(unsigned char)((float)r*255.0f);
            textura_alti[0][(x+y*tex_x)*4+1]=(unsigned char)((float)g*255.0f);
            textura_alti[0][(x+y*tex_x)*4+2]=(unsigned char)((float)b*255.0f);
            textura_alti[0][(x+y*tex_x)*4+3]=255;
        }
    }
    Singleton::getInstance()->levelmap.is_gpu_utd=0;
}

__constant__ float const_paleta_mapan[3*100];


__device__ vec4 alpha_tr(vec4 src,vec4 dst)
{
    if(src.a==0.0f)
    {
        return dst;
    }
    vec4 tret;

    tret.a=src.a+dst.a*(1-src.a);

    tret = (tret.a==0.0f)? vec4(0,0,0,tret.a):vec4((vec3(src)*src.a+vec3(dst)*dst.a*(1-src.a))/tret.a,tret.a)   ;

    return tret;
}

__device__ vec3 color_resultante(
        int mapani,float mapan_a,int mapan_nodata_value,
        vec4 mapavv)
{
    vec3 ret=vec3(0,0,0);
    vec4 mapanv,outv;

    if(mapani==mapan_nodata_value || mapani<0 || mapani>99){mapanv.a=0.0f;}
    else{
      mapanv=vec4(const_paleta_mapan[mapani*3+0],const_paleta_mapan[mapani*3+1],const_paleta_mapan[mapani*3+2],mapan_a);
      //mapanv=vec4(0.4,0,0,1);
    }

    outv=alpha_tr(mapanv,mapavv);
    //outv=vec4(0.0,0.0,0.5,0.5);

    ret=vec3(outv);
    return ret;
}



// kernel
__global__ void kernel_generar_textura_final(int tex_x,int tex_y, unsigned char* textura_final,
        int *mapan,float mapan_a,int mapan_nodata_value,
        unsigned char *mapav)
{

            int y = (blockIdx.y * blockDim.y) + threadIdx.y ;
            int x =  (blockIdx.x * blockDim.x) + threadIdx.x ;
            if(x>=tex_x || y>=tex_y) return;

            int pos=x+y*tex_x;
            vec4 mapav_aux;
            if(mapav==NULL){mapav_aux=vec4(0,0,0,0);}else{mapav_aux=vec4(mapav[pos*4+0],mapav[pos*4+1],mapav[pos*4+2],mapav[pos*4+3])/255.0f;}

            //pos=0;
            vec3 aux=color_resultante(
                mapan[pos],mapan_a,mapan_nodata_value,
                mapav_aux
            );
            //aux=vec3(0,0,0);
            //vec3 aux;
            //if(y>100){aux=vec3(1,0,0);}
            //printf("%d %d *",x,y);
            textura_final[(x+y*tex_x)*4+0]=(unsigned char)(aux.x*255);
            textura_final[(x+y*tex_x)*4+1]=(unsigned char)(aux.y*255);
            textura_final[(x+y*tex_x)*4+2]=(unsigned char)(aux.z*255);
            textura_final[(x+y*tex_x)*4+3]=255;

}

__device__ vec3 color_resultante_SP(
        int mapani,float mapan_a,int mapan_nodata_value,
        vec4 mapavv)
{
    vec3 ret=vec3(0,0,0);
    vec4 mapanv,outv;

    if(mapani==mapan_nodata_value || mapani<0){mapanv.a=0.0f;}
    else
    {
        int r=mapani/(256*256);
        int g=(mapani/256)-r*256;
        int b=mapani-(256*(mapani/256));

        float rc=(float)r/256.0f;
        float gc=(float)g/256.0f;
        float bc=(float)b/256.0f;


        mapanv=vec4(rc,gc,bc,mapan_a);
    }

    outv=alpha_tr(mapanv,mapavv);
    //outv=vec4(0.0,0.0,0.5,0.5);

    ret=vec3(outv);
    return ret;
}


// kernel
__global__ void kernel_generar_textura_final_SP(int tex_x,int tex_y, unsigned char* textura_final,
        int *mapan,float mapan_a,int mapan_nodata_value,
        unsigned char *mapav)
{

            int y = (blockIdx.y * blockDim.y) + threadIdx.y ;
            int x =  (blockIdx.x * blockDim.x) + threadIdx.x ;

            int pos=x+y*tex_x;
            vec4 mapav_aux;
            if(mapav==NULL){mapav_aux=vec4(0,0,0,0);}else{mapav_aux=vec4(mapav[pos*4+0],mapav[pos*4+1],mapav[pos*4+2],mapav[pos*4+3])/255.0f;}

            //pos=0;
            vec3 aux=color_resultante_SP(
                mapan[pos],mapan_a,mapan_nodata_value,
                mapav_aux
            );
            //vec3 aux;
            //if(y>100){aux=vec3(1,0,0);}
            textura_final[(x+y*tex_x)*4+0]=(unsigned char)(aux.x*255);
            textura_final[(x+y*tex_x)*4+1]=(unsigned char)(aux.y*255);
            textura_final[(x+y*tex_x)*4+2]=(unsigned char)(aux.z*255);
            textura_final[(x+y*tex_x)*4+3]=255;
}



void generar_textura_final(unsigned char** textura_final){

    int tex_x=Singleton::getInstance()->tex_x;
    int tex_y=Singleton::getInstance()->tex_y;


    if((*textura_final)==NULL){(*textura_final)=(unsigned char*)realloc(*textura_final,sizeof(unsigned char)*tex_x*tex_y*4);}


    dim3 dimBlock(16,16);//OJO CON PONER 32x32 porque cuda da errores raros!!!!!!
    dim3 dimGrid((tex_x + dimBlock.x-1) / dimBlock.x, (tex_y + dimBlock.y -1) / dimBlock.y,1);
    static unsigned char* d_textura_final,*d_textura_final2,*d_textura_final_aux;



    Singleton::getInstance()->vegetacion.Update_GPU_Map();
    Singleton::getInstance()->quemado.Update_GPU_Map();
    Singleton::getInstance()->selcorf.Update_GPU_Map();
    Singleton::getInstance()->mapaaux.Update_GPU_Map();
    Singleton::getInstance()->levelmap.Update_GPU_Map();
    Singleton::getInstance()->aspectmap.Update_GPU_Map();
    Singleton::getInstance()->altimap.Update_GPU_Map();
    Singleton::getInstance()->peligmap.Update_GPU_Map();
    Singleton::getInstance()->windImap.Update_GPU_Map();



    static bool ftcm=1;
    if(ftcm){
        gpuErrchk(cudaMalloc((void**)&d_textura_final, tex_x*tex_y*4*sizeof(unsigned char)));
        gpuErrchk(cudaMalloc((void**)&d_textura_final2, tex_x*tex_y*4*sizeof(unsigned char)));
        ftcm=0;
    }


    RasterMap* maps_v[MAX_NUM_LAYERS];
    for(int i=0;i<MAX_NUM_LAYERS;i++)
    {
        maps_v[i]=(RasterMap*)Singleton::getInstance()->layer_list[i].ptr_RasterMap;
    }

    d_textura_final_aux=NULL;



    //ENTRE ESTA LINEA*******************************************************************
    for(int i=MAX_NUM_LAYERS-1;i>=0;i--) //Estaba empezando desde i=MAX_NUM_LAYERS !!!!!
    {
        //print_debug("CICLO FOR i=%d\n",i);

        if(maps_v[i]==NULL)continue;
        if(!maps_v[i]->is_in_gpu)continue;
        if(maps_v[i]->map_a<0.01f || !Singleton::getInstance()->layer_list[i].enabled)continue;
        if(maps_v[i]->sinpaleta)
        {
            //print_debug("SPDRAW %d alpha=%.02f NDV=%d\n",i,maps_v[i]->map_a,maps_v[i]->nodata_value);
            kernel_generar_textura_final_SP<<<dimGrid, dimBlock>>>(tex_x,tex_y,d_textura_final,
                maps_v[i]->d_map,
                maps_v[i]->map_a,
                maps_v[i]->nodata_value,
                d_textura_final_aux
            );
            gpuErrchk( cudaPeekAtLastError() );
            gpuErrchk( cudaDeviceSynchronize() );
        }
        else
        {
            if(maps_v[i]->paleta==NULL){
                print_debug("Error paleta nula, Paleta_size=%d\n",maps_v[i]->paleta_size/*,maps_v[i]->paleta*/);
                continue;
            }



            gpuErrchk(cudaMemcpyToSymbol(const_paleta_mapan, (float*)maps_v[i]->paleta, sizeof(vec3)*(maps_v[i]->paleta_size)));
            //print_debug("B %d %d %d %f %d %d\n",tex_x,(long int)d_textura_final,(long int)maps_v[i]->d_map,maps_v[i]->map_a,maps_v[i]->nodata_value,(long int)d_textura_final_aux);
            //print_debug("LNAME *%s*\n",Singleton::getInstance()->layer_list[i].name);
            kernel_generar_textura_final<<<dimGrid, dimBlock>>>(tex_x,tex_y,d_textura_final,
                maps_v[i]->d_map,
                maps_v[i]->map_a,
                maps_v[i]->nodata_value,
                d_textura_final_aux
            );

            //if(debug)printf("A %d %d %d %f %d %d\n",tex_x,(long int)d_textura_final,(long int)maps_v[i]->d_map,maps_v[i]->map_a,maps_v[i]->nodata_value,(long int)d_textura_final_aux);
            gpuErrchk( cudaPeekAtLastError() );
            gpuErrchk( cudaDeviceSynchronize() );
        }
        //SWAP de punteros
        d_textura_final_aux=d_textura_final;
        d_textura_final=d_textura_final2;
        d_textura_final2=d_textura_final_aux;

    }
    //Y ESTA OTRA, SE PRODUCE EL ERROR*****************************************************


    cudaDeviceSynchronize();

    cudaMemcpy(*textura_final, d_textura_final2, sizeof(unsigned char) * tex_x*tex_y*4, cudaMemcpyDeviceToHost);

   //checkCUDAError("Launch failure: 1 ");
    //cudaFree(d_textura_final);
    //cudaFree(d_textura_final2);
}


vec3 RGBi(int r,int g,int b){
    vec3 ax;
    ax.x=(float)r/255.0f;
    ax.y=(float)g/255.0f;
    ax.z=(float)b/255.0f;
    return ax;
}



vec3 * generar_paleta_vegetacion(void)
{
	vec3* paleta_vegetacion;
    paleta_vegetacion=(vec3*)malloc(100*sizeof(vec3));
    /*paleta_vegetacion[0]=vec3(0,0,1);
    paleta_vegetacion[1]=vec3(0,1,0);
    paleta_vegetacion[2]=vec3(0,0.5f,0);*/

    paleta_vegetacion[0]=RGBi(0,50,0);
    paleta_vegetacion[1]=RGBi(0,200,10);
    paleta_vegetacion[2]=RGBi(150,163,20);
    paleta_vegetacion[3]=RGBi(0,133,30);
    paleta_vegetacion[4]=RGBi(0,103,40);
    paleta_vegetacion[5]=RGBi(0,74,50);//esta parece ser la mejor paleta!!!!
    paleta_vegetacion[6]=RGBi(0,48,60);
    paleta_vegetacion[7]=RGBi(254,18,70);
    paleta_vegetacion[8]=RGBi(232,0,80);
    paleta_vegetacion[9]=RGBi(209,0,90);
    paleta_vegetacion[10]=RGBi(136,0,100);
    paleta_vegetacion[11]=RGBi(255,218,110);
    paleta_vegetacion[12]=RGBi(255,189,120);
    paleta_vegetacion[13]=RGBi(255,163,130);
    paleta_vegetacion[14]=RGBi(255,133,140);
    paleta_vegetacion[15]=RGBi(255,103,150);
    paleta_vegetacion[16]=RGBi(255,74,160);//esta parece ser la mejor paleta!!!!
    paleta_vegetacion[17]=RGBi(255,48,170);
    paleta_vegetacion[18]=RGBi(254,18,180);
    paleta_vegetacion[19]=RGBi(232,0,190);
    paleta_vegetacion[20]=RGBi(209,0,200);
    paleta_vegetacion[99]=RGBi(136,0,255);

    Singleton::getInstance()->vegetacion.nodata_value=-1;
    Singleton::getInstance()->vegetacion.Load_Paleta(paleta_vegetacion,100);
    return paleta_vegetacion;
}


vec3 * generar_paleta_quemado(void)
{
	vec3* paleta_quemado;
    paleta_quemado=(vec3*)malloc(11*sizeof(vec3));
    /*paleta_quemado[0]=vec3(0.0,0,0);
    paleta_quemado[1]=vec3(0.3,0,0);
    paleta_quemado[2]=vec3(0.4,0,0);
    paleta_quemado[3]=vec3(0.5,0,0);
    paleta_quemado[4]=vec3(0.6,0,0);
    paleta_quemado[5]=vec3(0.65,0,0);
    paleta_quemado[6]=vec3(0.70,0,0);
    paleta_quemado[7]=vec3(0.75,0,0);
    paleta_quemado[8]=vec3(0.8,0,0);
    paleta_quemado[9]=vec3(0.9,0,0.9);
    paleta_quemado[10]=vec3(1.0,0,0);*/

    /*paleta_quemado[0]=vec3(0.0,0,0);
    paleta_quemado[1]=vec3(0.0,0,0);
    paleta_quemado[2]=vec3(0.0,0,0);
    paleta_quemado[3]=vec3(0.0,0,0);
    paleta_quemado[4]=vec3(0.0,0,0);
    paleta_quemado[5]=vec3(0.0,0,0);
    paleta_quemado[6]=vec3(0.5,0,0);
    paleta_quemado[7]=vec3(0.0,1,1);
    paleta_quemado[8]=vec3(1.0,0,1);
    paleta_quemado[9]=vec3(1.0,1,0.0);
    paleta_quemado[10]=vec3(1.0,0,0);*/

    /*paleta_quemado[0]=RGBi(130,33,26);
    paleta_quemado[1]=RGBi(130,33,26);
    paleta_quemado[2]=RGBi(130,33,26);
    paleta_quemado[3]=RGBi(211,209,0);
    paleta_quemado[4]=RGBi(246,234,12);
    paleta_quemado[5]=RGBi(246,187,5);
    paleta_quemado[6]=RGBi(252,137,10);
    paleta_quemado[7]=RGBi(243,105,16);
    paleta_quemado[8]=RGBi(226,73,16);
    paleta_quemado[9]=RGBi(163,52,41);
    paleta_quemado[10]=RGBi(130,33,26);*/

    paleta_quemado[0]=RGBi(255,218,0);
    paleta_quemado[1]=RGBi(255,189,0);
    paleta_quemado[2]=RGBi(255,163,0);
    paleta_quemado[3]=RGBi(255,133,0);
    paleta_quemado[4]=RGBi(255,103,0);
    paleta_quemado[5]=RGBi(255,74,0);//esta parece ser la mejor paleta!!!!
    paleta_quemado[6]=RGBi(255,48,0);
    paleta_quemado[7]=RGBi(254,18,0);
    paleta_quemado[8]=RGBi(232,0,0);
    paleta_quemado[9]=RGBi(209,0,0);
    paleta_quemado[10]=RGBi(136,0,0);



    //paleta_quemado[2]=vec3(0.5f,0.5f,0.5f);
    Singleton::getInstance()->quemado.nodata_value=0;
    Singleton::getInstance()->quemado.Load_Paleta(paleta_quemado,11);

    paleta_quemado[0]=vec3(0,0,0);
    paleta_quemado[1]=vec3(1,1,0);
    paleta_quemado[2]=vec3(0.5f,0.5f,0.5f);

    Singleton::getInstance()->mapaaux.Load_Paleta(paleta_quemado,3);
    return paleta_quemado;
}

vec3 * generar_paleta_selcorf(void)
{
	vec3* paleta_selcorf;
    paleta_selcorf=(vec3*)malloc(3*sizeof(vec3));
    paleta_selcorf[0]=vec3(0.5f,0.5f,0.5f);
    paleta_selcorf[1]=vec3(1,1,1);
    Singleton::getInstance()->selcorf.nodata_value=0;
    paleta_selcorf[2]=vec3(0,0,0);
    Singleton::getInstance()->selcorf.Load_Paleta(paleta_selcorf,3);
    return paleta_selcorf;
}

vec3 * generar_paleta_levelmap(void)
{
    vec3* paleta_selcorf;
    paleta_selcorf=(vec3*)malloc(3*sizeof(vec3));
    paleta_selcorf[0]=vec3(0.5f,0.5f,0.5f);
    paleta_selcorf[1]=vec3(1,1,1);
    Singleton::getInstance()->levelmap.nodata_value=0;
    paleta_selcorf[2]=vec3(0,0,0);
    Singleton::getInstance()->levelmap.Load_Paleta(paleta_selcorf,3);
    return paleta_selcorf;
}

void reset_camera(void){
    Singleton::getInstance()->MainCam.position=vec3(5.0f,10.0f,5.0f);
    Singleton::getInstance()->MainCam.position.x=(float)Singleton::getInstance()->vert_x*0.0125f*0.5f*Singleton::getInstance()->celdas_prom;//arreglar ac{a posicion inicial camara}
    Singleton::getInstance()->MainCam.position.z=(float)Singleton::getInstance()->vert_y*0.0125f*0.5f*Singleton::getInstance()->celdas_prom;
    Singleton::getInstance()->MainCam.position.y=Singleton::getInstance()->MainCam.position.x*2.0f+0.0125f*Singleton::getInstance()->altitud_min/30.0f;
    Singleton::getInstance()->MainCam.rotation=vec3(-90,0,0);
    calc_camera_front();
}


unsigned char* loadBMP_custom(const char * imagepath, int* w,int* h){
    // Data read from the header of the BMP file
    unsigned char header[54]; // Each BMP file begins by a 54-bytes header
    unsigned int dataPos;     // Position in the file where the actual data begins
    unsigned int width, height;
    unsigned int imageSize;   // = width*height*3
    // Actual RGB data
    unsigned char * data;

    // Open the file
    FILE * file = fopen(imagepath,"rb");
    if (!file){print_debug("Image %s could not be opened\n",imagepath); return 0;}

    if ( fread(header, 1, 54, file)!=54 ){ // If not 54 bytes read : problem
        print_debug("Not a correct BMP file\n");
        return NULL;
    }

    if ( header[0]!='B' || header[1]!='M' ){
        print_debug("Not a correct BMP file\n");
        return NULL;
    }

    // Read ints from the byte array
    dataPos    = *(int*)&(header[0x0A]);
    imageSize  = *(int*)&(header[0x22]);
    width      = *(int*)&(header[0x12]);
    height     = *(int*)&(header[0x16]);

    // Some BMP files are misformatted, guess missing information
    if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
    if (dataPos==0)      dataPos=54; // The BMP header is done that way
    // Create a buffer
    data=(unsigned char*)malloc(imageSize*sizeof(unsigned char));
    if(data==NULL){print_debug("Error alocando imagen\n");}
    // Read the actual data from the file into the buffer
    fread(data,1,imageSize,file);

    //Everything is in memory now, the file can be closed
    fclose(file);
    *w=width;
    *h=height;
    print_debug("cargado bmp %s de %d x %d.\n",imagepath,width,height);

    return data;

}

void initGLHUDBUF(void){
    vec3 vpos[4]={
        vec3(0.0,1.0,0.0),
        vec3(1.0,1.0,0.0),
        vec3(0.0,0.0,0.0),
        vec3(1.0,0.0,0.0)
    };
    vec3 vuv[4]={
        vec3(0,0,0),
        vec3(1,0,0),
        vec3(0,-1,0),
        vec3(1,-1,0)
    };

    int vidx[6]={0,1,2,2,1,3};

    glGenVertexArrays(1,&Singleton::getInstance()->VAO2DID);
    glBindVertexArray(Singleton::getInstance()->VAO2DID);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &Singleton::getInstance()->VBOP2DID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->VBOP2DID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*4, vpos, GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);
    //checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));
    //SDK_CHECK_ERROR_GL();

    glGenBuffers(1, &Singleton::getInstance()->VBOUV2DID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->VBOUV2DID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*4, vuv, GL_STATIC_DRAW);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,0);

    glGenBuffers(1, &Singleton::getInstance()->VBOI2DID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Singleton::getInstance()->VBOI2DID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*6, vidx, GL_STATIC_DRAW);


    vec3 vpos2[4]={
        /*vec3(-1.0,-1.0,0.0),
        vec3(1.0,0.0,0.0),
        vec3(0.0,1.0,0.0),
        vec3(1.0,1.0,0.0)*/

        vec3(-1.0,0.0,+1.0),
        vec3(1.0,0.0,+1.0),
        vec3(-1.0,0.0,-1.0),
        vec3(1.0,0.0,-1.0)
    };
    vec3 vuv2[4]={
        vec3(0,0,0),
        vec3(1,0,0),
        vec3(0,1,0),
        vec3(1,1,0)
    };

    int vidx2[6]={0,1,2,2,1,3};

    glGenVertexArrays(1,&Singleton::getInstance()->VAOHUDID);
    glBindVertexArray(Singleton::getInstance()->VAOHUDID);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glGenBuffers(1, &Singleton::getInstance()->VBOPHUDID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->VBOPHUDID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*4, vpos2, GL_STATIC_DRAW);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);
    //checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));
    //SDK_CHECK_ERROR_GL();

    glGenBuffers(1, &Singleton::getInstance()->VBOUVHUDID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->VBOUVHUDID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*4, vuv2, GL_STATIC_DRAW);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,0);

    glGenBuffers(1, &Singleton::getInstance()->VBOIHUDID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Singleton::getInstance()->VBOIHUDID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*6, vidx2, GL_STATIC_DRAW);

}

// r,g,b values are from 0 to 1
// h = [0,360], s = [0,1], v = [0,1]
//      if s == 0, then h = -1 (undefined)
/*void RGBtoHSV( float r, float g, float b, float *h, float *s, float *v )
{
    float min, max, delta;
    min = MIN( r, g, b );
    max = MAX( r, g, b );
    *v = max;               // v
    delta = max - min;
    if( max != 0 )
        *s = delta / max;       // s
    else {
        // r = g = b = 0        // s = 0, v is undefined
        *s = 0;
        *h = -1;
        return;
    }
    if( r == max )
        *h = ( g - b ) / delta;     // between yellow & magenta
    else if( g == max )
        *h = 2 + ( b - r ) / delta; // between cyan & yellow
    else
        *h = 4 + ( r - g ) / delta; // between magenta & cyan
    *h *= 60;               // degrees
    if( *h < 0 )
        *h += 360;
}*/


void HSVtoRGB( float *r, float *g, float *b, float h, float s, float v )
{
    int i;
    float f, p, q, t;
    if( s == 0 ) {
        // achromatic (grey)
        *r = *g = *b = v;
        return;
    }
    h /= 60;            // sector 0 to 5
    i = floor( h );
    f = h - i;          // factorial part of h
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );
    switch( i ) {
        case 0:
            *r = v;
            *g = t;
            *b = p;
            break;
        case 1:
            *r = q;
            *g = v;
            *b = p;
            break;
        case 2:
            *r = p;
            *g = v;
            *b = t;
            break;
        case 3:
            *r = p;
            *g = q;
            *b = v;
            break;
        case 4:
            *r = t;
            *g = p;
            *b = v;
            break;
        default:        // case 5:
            *r = v;
            *g = p;
            *b = q;
            break;
    }
}

void initGLBUF(void){
    //gluPerspective(60.0, (GLfloat)window_width / (GLfloat) window_height, 0.0001, 100.0);


    generate_fbo();
    promediar_mapas();


	int numvertices=Singleton::getInstance()->vert_x*Singleton::getInstance()->vert_y*4;
    int numindices=6*(Singleton::getInstance()->vert_x-1)*(Singleton::getInstance()->vert_y-1);

    //int numindices=6*4*(Singleton::getInstance()->vert_x-1)*(Singleton::getInstance()->vert_y-1);

    Singleton::getInstance()->numvertices=numvertices;
    Singleton::getInstance()->numindices=numindices;

    print_debug("NV=%d NI=%d\n",numvertices,numindices);


    Singleton::getInstance()->verticesPosicion=(vec3*)malloc(sizeof(vec3)*numvertices);
    Singleton::getInstance()->verticesNormals=(vec3*)malloc(sizeof(vec3)*numvertices);
    Singleton::getInstance()->verticesUV=(vec3*)malloc(sizeof(vec3)*numvertices);
    Singleton::getInstance()->verticesUVF=(vec3*)malloc(sizeof(vec3)*numvertices);
    Singleton::getInstance()->verticesIndex=(int*)malloc(sizeof(int)*numindices);

    GenerarVertices();

    //numindices=sizeof(indices)/sizeof(int);
    //numvertices=2;

    glGenVertexArrays(1,&Singleton::getInstance()->globalVAOID);
    glBindVertexArray(Singleton::getInstance()->globalVAOID);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);

	glGenBuffers(1, &Singleton::getInstance()->vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*numvertices, Singleton::getInstance()->verticesPosicion, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0,0);
    //checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));
    //SDK_CHECK_ERROR_GL();

	glGenBuffers(1, &Singleton::getInstance()->uvBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->uvBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*numvertices, Singleton::getInstance()->verticesUV, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,0);

    glGenBuffers(1, &Singleton::getInstance()->normalBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->normalBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*numvertices, Singleton::getInstance()->verticesNormals, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,0,0);

    glGenBuffers(1, &Singleton::getInstance()->uvfBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, Singleton::getInstance()->uvfBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vec3)*numvertices, Singleton::getInstance()->verticesUVF, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(3,3,GL_FLOAT,GL_FALSE,0,0);

	glGenBuffers(1, &Singleton::getInstance()->indexBufferID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Singleton::getInstance()->indexBufferID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*numindices, Singleton::getInstance()->verticesIndex, GL_DYNAMIC_DRAW);


    Singleton::getInstance()->paleta_vegetacion=generar_paleta_vegetacion();
    Singleton::getInstance()->proyectow.mover_paleta_a_grafico();
    Singleton::getInstance()->paleta_selcorf=generar_paleta_selcorf();
    Singleton::getInstance()->paleta_quemado=generar_paleta_quemado();
    generar_paleta_levelmap();



    /*generar_textura(h_mapavege,&textura_vegetacion,paleta_vegetacion);
    generar_textura(h_mapaselcorf,&textura_selcorf,paleta_selcorf);
    generar_textura(h_mapaquemado,&textura_quemado,paleta_quemado);*/

    Singleton::getInstance()->windImap.h_map=(int*)malloc(sizeof(int)*Singleton::getInstance()->tex_x*Singleton::getInstance()->tex_y);
    Singleton::getInstance()->aspectmap.h_map=(int*)malloc(sizeof(int)*Singleton::getInstance()->tex_x*Singleton::getInstance()->tex_y);
    Singleton::getInstance()->altimap.h_map=(int*)malloc(sizeof(int)*Singleton::getInstance()->tex_x*Singleton::getInstance()->tex_y);
    Singleton::getInstance()->peligmap.h_map=(int*)malloc(sizeof(int)*Singleton::getInstance()->tex_x*Singleton::getInstance()->tex_y);



    //FIN ERROR


    //Aca hay que cargar las texturas de las flechas para el viento, de 128x128 px, al TEXTURE UNIT 2
    //Las mismas deben ser calculadas previamente y rotadas segun la direccion y magnitud del viento en
    //cada celda "divx". Se dibuja una flecha cada divx pixeles.

    glActiveTexture(GL_TEXTURE2);
    glGenTextures(1, &Singleton::getInstance()->texture_flecha_ID);
    glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_flecha_ID);
    //aca hay que cargar el bmp de la flecha (flecha.bmp) de 64x64 px.
    int fw,fh;
    unsigned char* flecha_bmp=loadBMP_custom("resources/flecha.bmp",&fw,&fh);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, fw, fh, 0, GL_RGB, GL_UNSIGNED_BYTE, flecha_bmp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


    glActiveTexture(GL_TEXTURE3);
    glGenTextures(1, &Singleton::getInstance()->texture_colormap_ID);
    glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_colormap_ID);
    //aca hay que cargar el bmp de la flecha (flecha.bmp) de 64x64 px.
    int cw,ch;
    Singleton::getInstance()->colormap_bmp=loadBMP_custom("resources/colormap.bmp",&cw,&ch);//a futuro cargar el archivo colormap_list.bmp y seleccionar el
    //colormap en especifico a utilizar.
    Singleton::getInstance()->colormap_width=cw;


    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, cw, ch, 0, GL_RGB, GL_UNSIGNED_BYTE, Singleton::getInstance()->colormap_bmp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    generar_textura_aspect(&Singleton::getInstance()->textura_aspect,Singleton::getInstance()->h_mapaaspect,Singleton::getInstance()->h_mapaslope);
    generar_textura_windI(&Singleton::getInstance()->textura_windi,Singleton::getInstance()->h_mapawindINT);
    generar_textura_pelig(&Singleton::getInstance()->textura_pelig,Singleton::getInstance()->vegetacion.h_map);
    generar_textura_alti(&Singleton::getInstance()->textura_alti,Singleton::getInstance()->h_mapaaspect,Singleton::getInstance()->h_mapaslope,Singleton::getInstance()->h_mapaalti);
    generar_textura_Calti(&Singleton::getInstance()->textura_Calti,Singleton::getInstance()->h_mapaaspect,Singleton::getInstance()->h_mapaslope,Singleton::getInstance()->h_mapaalti);

    for(int i=0;i<Singleton::getInstance()->windImap.x*Singleton::getInstance()->windImap.y;i++)
    {
        Singleton::getInstance()->windImap.h_map[i]=256*256*((int)Singleton::getInstance()->textura_windi[i*4+0])+256*((int)Singleton::getInstance()->textura_windi[i*4+1])+((int)Singleton::getInstance()->textura_windi[i*4+2]);
    }
    Singleton::getInstance()->windImap.is_in_cpu=1;Singleton::getInstance()->windImap.is_gpu_utd=0;
    Singleton::getInstance()->windImap.nodata_value=-1;

    for(int i=0;i<Singleton::getInstance()->aspectmap.x*Singleton::getInstance()->aspectmap.y;i++)
    {
        Singleton::getInstance()->aspectmap.h_map[i]=256*256*((int)Singleton::getInstance()->textura_aspect[i*4+0])+256*((int)Singleton::getInstance()->textura_aspect[i*4+1])+((int)Singleton::getInstance()->textura_aspect[i*4+2]);
    }
    Singleton::getInstance()->aspectmap.is_in_cpu=1;Singleton::getInstance()->aspectmap.is_gpu_utd=0;
    Singleton::getInstance()->aspectmap.nodata_value=-1;

    for(int i=0;i<Singleton::getInstance()->altimap.x*Singleton::getInstance()->altimap.y;i++)
    {
        Singleton::getInstance()->altimap.h_map[i]=256*256*((int)Singleton::getInstance()->textura_alti[i*4+0])+256*((int)Singleton::getInstance()->textura_alti[i*4+1])+((int)Singleton::getInstance()->textura_alti[i*4+2]);
    }
    Singleton::getInstance()->altimap.is_in_cpu=1;Singleton::getInstance()->altimap.is_gpu_utd=0;
    Singleton::getInstance()->altimap.nodata_value=-1;

    generar_textura_final(&Singleton::getInstance()->textura_final); //ESTA LINEA HACE QUE EL CUDA MALLOC DE ERROR
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &Singleton::getInstance()->textureID);
    glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->textureID);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, Singleton::getInstance()->tex_x, Singleton::getInstance()->tex_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, Singleton::getInstance()->textura_final);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &Singleton::getInstance()->texture_flecha_norte_ID);
    glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->texture_flecha_norte_ID);

    unsigned char* flechanorte_bmp=loadBMP_custom("resources/flecha norte.bmp",&fw,&fh);

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, fw, fh, 0, GL_RGB, GL_UNSIGNED_BYTE, flechanorte_bmp);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	reset_camera();
    initGLHUDBUF();

}



void regenerar_textura_final(void)
{
	generar_textura_final(&Singleton::getInstance()->textura_final);


    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Singleton::getInstance()->textureID);

    if(Singleton::getInstance()->update_Calti){
        Singleton::getInstance()->update_Calti=0;
        generar_textura_Calti(&Singleton::getInstance()->textura_Calti,Singleton::getInstance()->h_mapaaspect,Singleton::getInstance()->h_mapaslope,Singleton::getInstance()->h_mapaalti);
    }

    //HECHO TODO: PARA HACER: Pasar todas estas texturas a clases RasterMap con su paleta correspondiente
    //HECHO Para asi poder ordenar las capas y ajustar su transparencia.

    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, Singleton::getInstance()->tex_x, Singleton::getInstance()->tex_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, Singleton::getInstance()->textura_final);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

}

GLuint createTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1,&texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
        0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,texture, 0);
    return texture;
}

GLuint createDepthTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height,
        0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);
    return texture;
}

GLuint createDepthBufferAttachment(int width, int height) {
    GLuint depthBuffer;
    glGenRenderbuffers(1,&depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width,height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,GL_RENDERBUFFER, depthBuffer);
    return depthBuffer;
}

void generate_fbo(void)
{
    glGenFramebuffers(1, &Singleton::getInstance()->flechaFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, Singleton::getInstance()->flechaFBO);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    Singleton::getInstance()->flechatexture = createTextureAttachment(256, 256);

    //img2d.depthbuffer = createDepthBufferAttachment(water_resolution, water_resolution);
    print_debug("Generate_fbo() OK\n");
}



////////////////////////////////////////////////////////////////////////////////
//! Create VBO
////////////////////////////////////////////////////////////////////////////////
void createVBO(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags)
{
    assert(vbo);

    // create buffer object
    glGenBuffers(1, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, *vbo);

    // initialize buffer object
    unsigned int size = sizeof(float4)*Singleton::getInstance()->verticesT;
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // register this buffer object with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));

    SDK_CHECK_ERROR_GL();
}

void createVBC(GLuint *vbo, struct cudaGraphicsResource **vbo_res,
               unsigned int vbo_res_flags)
{
    assert(vbo);

    // create buffer object
    glGenBuffers(1, vbo);
    glBindBuffer(GL_ARRAY_BUFFER, *vbo);

    // initialize buffer object
    unsigned int size = sizeof(float3)*Singleton::getInstance()->verticesT;
    glBufferData(GL_ARRAY_BUFFER, size, 0, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // register this buffer object with CUDA
    checkCudaErrors(cudaGraphicsGLRegisterBuffer(vbo_res, *vbo, vbo_res_flags));

    SDK_CHECK_ERROR_GL();
}

////////////////////////////////////////////////////////////////////////////////
//! Delete VBO
////////////////////////////////////////////////////////////////////////////////
void deleteVBO(GLuint *vbo, struct cudaGraphicsResource *vbo_res)
{

    // unregister this buffer object with CUDA
    checkCudaErrors(cudaGraphicsUnregisterResource(vbo_res));

    glBindBuffer(1, *vbo);
    glDeleteBuffers(1, vbo);

    *vbo = 0;
}
///*****************************************************************************

void sendUniformsToOpenGL(GLuint programID)
{
	mat4 worldToViewMatrixTRA=translate(mat4(),-Singleton::getInstance()->MainCam.position);
    mat4 worldToViewMatrixROT=rotate(mat4(),radians(-Singleton::getInstance()->MainCam.rotation.x),vec3(1,0,0));
    worldToViewMatrixROT=rotate(worldToViewMatrixROT,radians(-Singleton::getInstance()->MainCam.rotation.y),vec3(0,1,0));

    Singleton::getInstance()->projectionMatrix=perspective(radians(60.0f),(float)Singleton::getInstance()->window_width/Singleton::getInstance()->window_height,Singleton::getInstance()->near_plane,20.0f);

    static mat4 modelTraslationMatrix=translate(mat4(),vec3(0,0,0));
    mat4 modelRotationMatrix=rotate(mat4(),0.0f,vec3(1,0,0));


    Singleton::getInstance()->matrixVTP=Singleton::getInstance()->projectionMatrix;
    Singleton::getInstance()->matrixWTV=worldToViewMatrixROT * worldToViewMatrixTRA;
    mat4 fullTransfWTP=Singleton::getInstance()->matrixVTP * Singleton::getInstance()->matrixWTV;
    Singleton::getInstance()->g_fullTransfWTP=fullTransfWTP;

    mat4 modelTransformMatrix=modelTraslationMatrix*modelRotationMatrix;


    GLint modelTransformMatrixUniformLocation=glGetUniformLocation(programID, "modelTransformMatrix");
    GLint fullTransfWTPUniformLocation=glGetUniformLocation(programID, "fullTransfWTP");
    GLint cameraDirectionUniformLocation=glGetUniformLocation(programID, "cameraDirection");
    GLint eyePositionWorldUniformLocation=glGetUniformLocation(programID, "eyePositionWorld");

    glUniform3fv(eyePositionWorldUniformLocation,1,&Singleton::getInstance()->MainCam.position[0]);
    glUniform3fv(cameraDirectionUniformLocation,1,&Singleton::getInstance()->MainCam.front[0]);
    glUniformMatrix4fv(modelTransformMatrixUniformLocation,1,GL_FALSE,&modelTransformMatrix[0][0]);
    glUniformMatrix4fv(fullTransfWTPUniformLocation,1,GL_FALSE,&fullTransfWTP[0][0]);

    glUniform1i( glGetUniformLocation( programID, "tex0" ), 0);
    glUniform1i( glGetUniformLocation( programID, "tex1" ), 1);
    glUniform1i( glGetUniformLocation( programID, "tex2" ), 2);
    glUniform1i( glGetUniformLocation( programID, "tex3" ), 3);

    glUniform1f( glGetUniformLocation( programID, "wind_a" ), Singleton::getInstance()->wind_a);
    glUniform1f( glGetUniformLocation( programID, "wind_pot" ), Singleton::getInstance()->wind_pot);
    glUniform1f( glGetUniformLocation( programID, "ambient_mult" ), Singleton::getInstance()->ambient_mult);
    glUniform1f( glGetUniformLocation( programID, "specular_mult" ), Singleton::getInstance()->specular_mult);
    glUniform1f( glGetUniformLocation( programID, "diffuse_mult" ), Singleton::getInstance()->diffuse_mult);
    glUniform1f( glGetUniformLocation( programID, "diffuseC_mult" ), Singleton::getInstance()->diffuseC_mult);
}

void sendLINESUniformsToOpenGL(GLuint programID)
{
    mat4 worldToViewMatrixTRA=translate(mat4(),-Singleton::getInstance()->MainCam.position);
    mat4 worldToViewMatrixROT=rotate(mat4(),radians(-Singleton::getInstance()->MainCam.rotation.x),vec3(1,0,0));
    worldToViewMatrixROT=rotate(worldToViewMatrixROT,radians(-Singleton::getInstance()->MainCam.rotation.y),vec3(0,1,0));

    Singleton::getInstance()->projectionMatrix=perspective(radians(60.0f),(float)Singleton::getInstance()->window_width/Singleton::getInstance()->window_height,Singleton::getInstance()->near_plane,20.0f);

    static mat4 modelTraslationMatrix=translate(mat4(),vec3(0,0,0));
    mat4 modelRotationMatrix=rotate(mat4(),0.0f,vec3(1,0,0));


    Singleton::getInstance()->matrixVTP=Singleton::getInstance()->projectionMatrix;
    Singleton::getInstance()->matrixWTV=worldToViewMatrixROT * worldToViewMatrixTRA;
    mat4 fullTransfWTP=Singleton::getInstance()->matrixVTP * Singleton::getInstance()->matrixWTV;
    Singleton::getInstance()->g_fullTransfWTP=fullTransfWTP;

    mat4 modelTransformMatrix=modelTraslationMatrix*modelRotationMatrix;


    GLint modelTransformMatrixUniformLocation=glGetUniformLocation(programID, "modelTransformMatrix");
    GLint fullTransfWTPUniformLocation=glGetUniformLocation(programID, "fullTransfWTP");
    GLint cameraDirectionUniformLocation=glGetUniformLocation(programID, "cameraDirection");
    GLint eyePositionWorldUniformLocation=glGetUniformLocation(programID, "eyePositionWorld");

    glUniform3fv(eyePositionWorldUniformLocation,1,&Singleton::getInstance()->MainCam.position[0]);
    glUniform3fv(cameraDirectionUniformLocation,1,&Singleton::getInstance()->MainCam.front[0]);
    glUniformMatrix4fv(modelTransformMatrixUniformLocation,1,GL_FALSE,&modelTransformMatrix[0][0]);
    glUniformMatrix4fv(fullTransfWTPUniformLocation,1,GL_FALSE,&fullTransfWTP[0][0]);

    glUniform1i( glGetUniformLocation( programID, "tex0" ), 0);
    glUniform1i( glGetUniformLocation( programID, "tex1" ), 1);
    glUniform1i( glGetUniformLocation( programID, "tex2" ), 2);
    glUniform1i( glGetUniformLocation( programID, "tex3" ), 3);

    glUniform1f( glGetUniformLocation( programID, "wind_a" ), Singleton::getInstance()->wind_a);
    glUniform1f( glGetUniformLocation( programID, "wind_pot" ), Singleton::getInstance()->wind_pot);
    glUniform1f( glGetUniformLocation( programID, "ambient_mult" ), Singleton::getInstance()->ambient_mult);
    glUniform1f( glGetUniformLocation( programID, "specular_mult" ), Singleton::getInstance()->specular_mult);
    glUniform1f( glGetUniformLocation( programID, "diffuse_mult" ), Singleton::getInstance()->diffuse_mult);
    glUniform1f( glGetUniformLocation( programID, "diffuseC_mult" ), Singleton::getInstance()->diffuseC_mult);
}


void sendHUDUniformsToOpenGL(GLuint programID)
{
    mat4 worldToViewMatrixROT=rotate(mat4(),radians(-Singleton::getInstance()->MainCam.rotation.x),vec3(1,0,0));
    worldToViewMatrixROT=rotate(worldToViewMatrixROT,radians(-Singleton::getInstance()->MainCam.rotation.y),vec3(0,1,0));

    Singleton::getInstance()->projectionMatrix=perspective(radians(60.0f),(float)256/256,Singleton::getInstance()->near_plane,20.0f);

    static mat4 modelTraslationMatrix=translate(mat4(),vec3(0,0,-2));

    Singleton::getInstance()->matrixVTP=Singleton::getInstance()->projectionMatrix;
    Singleton::getInstance()->matrixWTV=modelTraslationMatrix * worldToViewMatrixROT;
    mat4 fullTransfMTP=Singleton::getInstance()->matrixVTP * Singleton::getInstance()->matrixWTV;


    GLint fullTransfMTPUniformLocation=glGetUniformLocation(programID, "fullTransfMTP");

    glUniformMatrix4fv(fullTransfMTPUniformLocation,1,GL_FALSE,&fullTransfMTP[0][0]);

    glUniform1i( glGetUniformLocation(programID, "tex0" ), 0);
}

void sendHUD2UniformsToOpenGL(GLuint programID)//HUD 2 !!!!!!!!!!!!!!!!!!!!
{
    //mat4 worldToViewMatrixTRA=translate(mat4(),MainCam.position/*-MainCam.front*near_plane*/);
    mat4 worldToViewMatrixROT=rotate(mat4(),radians(-Singleton::getInstance()->MainCam.rotation.x),vec3(1,0,0));
    worldToViewMatrixROT=rotate(worldToViewMatrixROT,radians(-Singleton::getInstance()->MainCam.rotation.y),vec3(0,1,0));

    Singleton::getInstance()->projectionMatrix=perspective(radians(60.0f),(float)Singleton::getInstance()->window_width/Singleton::getInstance()->window_height,Singleton::getInstance()->near_plane,20.0f);

    //static mat4 modelTraslationMatrix=translate(mat4(),vec3(0.0f,0.5f,-0.021f));
    static mat4 modelTraslationMatrix=translate(mat4(),vec3(0.0f,4.7f,-10));//Esto aca habria que ver
    //de calcularlo como corresponde utilizando transformaciones.

    //printf("%.02f %.02f %.02f \n",pmio.x,pmio.y,pmio.z);

    //static mat4 modelTraslationMatrix=translate(mat4(),vec3(pmio));

    Singleton::getInstance()->matrixVTP=Singleton::getInstance()->projectionMatrix;
    Singleton::getInstance()->matrixWTV=modelTraslationMatrix * worldToViewMatrixROT;
    mat4 fullTransfMTP=Singleton::getInstance()->matrixVTP * Singleton::getInstance()->matrixWTV;


    GLint fullTransfMTPUniformLocation=glGetUniformLocation(programID, "fullTransfMTP");

    glUniformMatrix4fv(fullTransfMTPUniformLocation,1,GL_FALSE,&fullTransfMTP[0][0]);

    glUniform1i( glGetUniformLocation(programID, "tex0" ), 0);
}
