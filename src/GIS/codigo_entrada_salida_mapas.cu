#include <stdio.h>
#include <stdlib.h>

#define MAX_LN 100

float* GetMap(const char * nombre, int *filas, int *cols);
void SaveMap(const char * nombre, float *mapa, int filas, int cols);
/*int main() {

  char nombre_in[] = "grani_alti.asc";
  char nombre_out[] = "salida.map";

  int filas, cols;
  filas = cols = 801;

  float *mapa;
  mapa = (float*) malloc (sizeof(float) * filas*cols);
  GetMap(nombre_in, mapa, filas, cols);
  SaveMap(nombre_out, mapa, filas, cols);

  return 0;
}  */

  /* ENTRADA Y SALIDA DE MAPAS */
float* GetMapwz(const char * nombre, int & filas, int & cols,int & nodata,bool & error, char * errorstr)
{
	FILE *input = fopen(nombre,"r");
	float *mapa;
	char auxestr[2048];

	//printf("FNAME=%s\n",nombre);

	if (!strcmp(nombre,"."))
	{
		memcpy(auxestr,errorstr,2048);
		sprintf(errorstr,"%s\nEl archivo %s no se encuentra. \n",auxestr, nombre);
		error=1;
		return NULL;
	}

	if (input == NULL)
	{
		memcpy(auxestr,errorstr,2048);
		sprintf(errorstr,"%s\nEl archivo %s no se encuentra. \n",auxestr, nombre);
		error=1;
		return NULL;
	}

	int i,j;
	float dato;

	char line[MAX_LN];
	char identif[24];
	int rfilas,rcols;

	fscanf(input,"%s%d",identif,&rcols);
	fscanf(input,"%s%d",identif,&rfilas);

	
	/*if(rfilas!=filas || rcols!=cols)
	{
		printf("Get Map error: el archivo %s es de otro tamaño diferente a (%d,%d) es de (%d,%d) \n", nombre,cols,filas,rcols,rfilas);
		exit(-1);
	}
	else
	{
		//printf("Cargado archivo GIS %s con ALTO=%d ANCHO=%d\n",nombre,rfilas,rcols);
	}*/

	filas=rfilas;
	cols=rcols;

	printf("FILAS=%d COLS=%d \n",filas,cols);


	if (filas<1|| cols<1 || filas>3000|| cols>3000)
	{
		memcpy(auxestr,errorstr,2048);
		sprintf(errorstr,"%s\n Archivo %s :filas y/o columnas no válidas. filas=%d cols=%d \n",auxestr, nombre,filas,cols);
		error=1;
		return NULL;
	}

	mapa = (float*)malloc(sizeof(float)*filas*cols);

    

	for(i=0; i < 4; i++) { //aca deberia leer solo 4 lineas, pero si le pongo 4 lee mal (revisar)
		fgets(line, MAX_LN, input);
		//printf("%s",line);
	}
	nodata=-9999;
	float auxnd;
	fscanf(input,"%s%f",identif,&auxnd);
	nodata=(int)auxnd;
	printf("NODATA=%d\n",nodata);

	for(i = 0; i < (filas); i++)
	{
		for(j = 0; j < (cols); j++)
		{
			fscanf(input, "%f", &dato);
			//if((float)dato==nodata){dato=0;}
			mapa[i*(cols) + j] = (float)dato;
			//mapa[i*(cols) + j] = 0.0f;
			//printf("%.01f ",(float)dato);
		}
		
	}
	fclose(input);
	return mapa;
}

/* ENTRADA Y SALIDA DE MAPAS */
float* GetMap(const char * nombre, int filas, int cols,int *nodata)
{
	FILE *input = fopen(nombre,"r");
	float *mapa;

	if (input == NULL)
	{
		printf("Get Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j;
	float dato;

	char line[MAX_LN];
	char identif[24];
	int rfilas,rcols;

	fscanf(input,"%s%d",identif,&rcols);
	fscanf(input,"%s%d",identif,&rfilas);

	
	if(rfilas!=filas || rcols!=cols)
	{
		printf("Get Map error: el archivo %s es de otro tamaño diferente a (%d,%d) es de (%d,%d) \n", nombre,cols,filas,rcols,rfilas);
		exit(-1);
	}
	else
	{
		//printf("Cargado archivo GIS %s con ALTO=%d ANCHO=%d\n",nombre,rfilas,rcols);
	}

	mapa = (float*)malloc(sizeof(float)*filas*cols);

    

	for(i=0; i < 4; i++) { //aca deberia leer solo 4 lineas, pero si le pongo 4 lee mal (revisar)
		fgets(line, MAX_LN, input);
		//printf("%s",line);
	}
	*nodata=-9999;
	float auxnd;
	fscanf(input,"%s%f",identif,&auxnd);
	*nodata=(int)auxnd;
	//printf("NODATA=%d\n",nodata);

	for(i = 0; i < (filas); i++)
	{
		for(j = 0; j < (cols); j++)
		{
			fscanf(input, "%f", &dato);
			//if((float)dato==*nodata){dato=0;}
			mapa[i*(cols) + j] = (float)dato;
			//mapa[i*(cols) + j] = 0.0f;
			//printf("%.01f ",(float)dato);
		}
		
	}
	fclose(input);
	return mapa;
}

int* GetMapi(const char * nombre, int filas, int cols,int *nodata)
{
	FILE *input = fopen(nombre,"r");
	int *mapa;

	if (input == NULL)
	{
		printf("Get Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j;
	float dato;

	char line[MAX_LN];
	char identif[24];
	int rfilas,rcols;

	fscanf(input,"%s%d",identif,&rcols);
	fscanf(input,"%s%d",identif,&rfilas);

	
	if(rfilas!=filas || rcols!=cols)
	{
		printf("Get Map error: el archivo %s es de otro tamaño diferente a (%d,%d) es de (%d,%d) \n", nombre,cols,filas,rcols,rfilas);
		exit(-1);
	}
	else
	{
		//printf("Cargado archivo GIS %s con ALTO=%d ANCHO=%d\n",nombre,rfilas,rcols);
	}

	mapa = (int*)malloc(sizeof(int)*filas*cols);

    

	for(i=0; i < 4; i++) { //aca deberia leer solo 4 lineas, pero si le pongo 4 lee mal (revisar)
		fgets(line, MAX_LN, input);
		//printf("%s",line);
	}
	*nodata=-9999;
	float auxnd;
	fscanf(input,"%s%f",identif,&auxnd);
	*nodata=(int)auxnd;
	//printf("NODATA=%d\n",nodata);

	for(i = 0; i < (filas); i++)
	{
		for(j = 0; j < (cols); j++)
		{
			fscanf(input, "%f", &dato);
			//if((float)dato==*nodata){dato=0;}
			mapa[i*(cols) + j] = (int)dato;
			//mapa[i*(cols) + j] = 0.0f;
			//printf("%.01f ",(float)dato);
		}
		
	}
	fclose(input);
	return mapa;
}


void SaveMap(const char * nombre, float *mapa, int filas, int cols)
{
	FILE *output = fopen(nombre,"w");

	if (output == NULL)
	{
		printf("Save Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	
	int i,j;
	float dato;
	for(i = 0; i < filas; i++) {
		for(j = 0; j < cols; j++) {
			dato = mapa[i*cols + j];
			fprintf(output,"%.2f ",dato); 
			//"%.0f ", dato);
		}
		fprintf(output, "\n");
	}
	fclose(output);

}
