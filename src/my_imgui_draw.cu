#include "Simulador.h"
#include "manejo_archivos.h"
#include "my_imgui_draw.h"

#include "imgui/imgui.h"    
#include "imgui/imgui_draw.cpp"  
#include "imgui/imgui.cpp"  
#include "imgui/imgui_demo.cpp"
#include "imgui_sig.c"



#include "Singleton.h"
#include "initgl.h"

#ifdef WIN32
#include "direntW.h"
#include <direct.h>
#else
#include "dirent.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include "ImGuiFileDialog.h"

#define PROMEDIO_MOUSE 7
#define PARAL 10


bool imguicontrol=0;
bool imguicontrol2=0;

float windpow_at_mouse=0.0f;
float height_at_mouse=0.0f;
float slope_at_mouse=0.0f;
float aspect_at_mouse=0.0f;

int ptr_mouse=0;
float windpowP[PROMEDIO_MOUSE];
float heightP[PROMEDIO_MOUSE];
float slopeP[PROMEDIO_MOUSE];
float aspectP[PROMEDIO_MOUSE];

bool show_barrapaso_window=0;
bool show_mapeoscomb_window=0;
bool show_fwit_window=0;
bool show_editmeteor_window=0;
bool show_fwit_ref_window=0;
bool show_fwilm_ref_window=0;
bool show_editar_combustible=0;
bool show_windreference_window=0;
bool show_terrainreference_window=0;
bool show_load_project_window=0;
bool show_load_project_window_runonce=0;
bool show_load_project_window_1erinicio=0;
bool show_new_project_window=0;
bool show_new_project_window_runonce=0;
bool mostrar_cargar_real=0;
bool mostrar_guardar_cortafuego=0;
bool mostrar_cargar_cortafuego=0;

extern int nuevomapa;
extern float* nuevomapaptr;

extern int ni,nj;
extern bool linea_temp;

extern int mouse_y;
extern int mouse_x;
extern int paso_simulacion;
extern int automode;
extern int frame_cnt;

float* GetMapwz(const char * nombre, int & filas, int & cols,int & nodata,bool & error, char * errorstr);

float promediar_nuevo_valor_mouse(float*vector,float nvalor)
{
    float suma=0.0f;
    vector[ptr_mouse]=nvalor;
    for(int i=0;i<PROMEDIO_MOUSE;i++)
    {
        suma+=vector[i];
    }
    return (float)suma/PROMEDIO_MOUSE;
}


void export_fire_map(char*str,int stepNbin){
    char strF[128];
    sprintf(strF,"%s%s",Singleton::getInstance()->PROJECT_FOLDER_P,str);
    FILE *output = fopen(strF,"w");

    if (output == NULL)
    {
        print_debug("Error al exportar: no abre archivo %s \n", strF);
        return;
        //exit(-1);
    }
    /*
    ncols       801
    nrows       801
    xllcorner   268962
    yllcorner   5401414
    cellsize    30
    NODATA_value  -1
    */

    fprintf(output,"ncols %d\n",Singleton::getInstance()->vert_gis_x);
    fprintf(output,"nrows %d\n",Singleton::getInstance()->vert_gis_y);
    fprintf(output,"xllcorner 012\n");
    fprintf(output,"yllcorner 012\n");
    fprintf(output,"cellsize 30\n");
    fprintf(output,"NODATA_value -1\n");
    int vert_gis_x=Singleton::getInstance()->vert_gis_x;
    int vert_gis_y=Singleton::getInstance()->vert_gis_y;

    if(stepNbin){//EXPORTADO PASO QUEMADO
        float * map=Singleton::getInstance()->Simulador.export_fire_map();
        for(int y=0;y<vert_gis_y;y++)
        {
            for(int x=0;x<vert_gis_x;x++)
            {
                fprintf(output,"%d ",(int)map[y*vert_gis_x+x]);
            }
            fprintf(output,"\r\n");
        }
    }else{//EXPORTADO BINARIO (QUEMADO / NO-QUEMADO)
        int * map=Singleton::getInstance()->quemado.h_map;
        for(int y=0;y<vert_gis_y;y++)
        {
            for(int x=0;x<vert_gis_x;x++)
            {
                fprintf(output,"%d ",map[y*vert_gis_x+x]);
            }
            fprintf(output,"\r\n");
        }
    }

    fclose(output);
}




void paso_atras_simulador(void)
{
    Singleton::getInstance()->Simulador.atras();
}

void volver_a_cero_simulador(void)
{
    Singleton::getInstance()->Simulador.volver_a_cero();
}

void paso_adelante_simulador(void)
{
    Singleton::getInstance()->Simulador.adelante();
}

static void ShowMenuFile()
{
    mostrar_cargar_real=0;
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    if (ImGui::MenuItem("New...", "Ctrl+N",false,!Singleton::getInstance()->proyecto_cargado)) {show_new_project_window=1;show_new_project_window_runonce=1;}
    if (ImGui::MenuItem("Open project...", "Ctrl+O",false,!Singleton::getInstance()->proyecto_cargado)) {show_load_project_window=1;show_load_project_window_runonce=1;}
    /*if (ImGui::BeginMenu("Open Recent"))
    {
        ImGui::MenuItem("fish_hat.c");
        ImGui::MenuItem("fish_hat.inl");
        ImGui::MenuItem("fish_hat.h");
        if (ImGui::BeginMenu("More.."))
        {
            ImGui::MenuItem("Hello");
            ImGui::MenuItem("Sailor");
            if (ImGui::BeginMenu("Recurse.."))
            {
                ShowExampleMenuFile();
                ImGui::EndMenu();
            }
            ImGui::EndMenu();
        }
        ImGui::EndMenu();
    }*/
    //if (ImGui::MenuItem("Save", "Ctrl+S")) {}
    if (ImGui::MenuItem("Guardar mapa quemado(por paso)..","",false,Singleton::getInstance()->proyecto_cargado)) {export_fire_map((char*)"output_fire_step.asc",1);}
    if (ImGui::MenuItem("Guardar mapa quemado(parche binario)..","",false,Singleton::getInstance()->proyecto_cargado)) {export_fire_map((char*)"output_fire_fbin.asc",0);}

    ImGui::Separator();
    if(ImGui::MenuItem("Cargar mapa fuego real..","",false,Singleton::getInstance()->proyecto_cargado))
    {
        //CORTAFUEGOS.guardar_archivo("./AAAprueba.cfu");
        mostrar_cargar_real=1;
    }
    ImGui::Separator();
    /*if (ImGui::BeginMenu("Options"))
    {
        static bool enabled = true;
        ImGui::MenuItem("Enabled", "", &enabled);
        ImGui::BeginChild("child", ImVec2(0, 60), true);
        for (int i = 0; i < 10; i++)
            ImGui::Text("Scrolling Text %d", i);
        ImGui::EndChild();
        static float f = 0.5f;
        static int n = 0;
        ImGui::SliderFloat("Value", &f, 0.0f, 1.0f);
        ImGui::InputFloat("Input", &f, 0.1f);
        ImGui::Combo("Combo", &n, "Yes\0No\0Maybe\0\0");
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Disabled", false)) // Disabled
    {
        IM_ASSERT(0);
    }*/
    //if (ImGui::MenuItem("Checked", NULL, true)) {}
    if (ImGui::MenuItem("Quit", "Alt+F4")) {exit(-1);}
}

static void ShowBarraPaso(bool* p_open){
    ImGui::SetNextWindowSize(ImVec2(550,70), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Barra Paso", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }

    
    ImGui::BeginChild("Test2", ImVec2(550, 60), true);
    //p = ImGui::GetCursorScreenPos();
    ImVec2 p = ImGui::GetCursorScreenPos();
    ImGui::Image((void*) NULL/*ImGui::GetIO().Fonts->TexID*/, ImVec2(550,50));
    ImGui::EndChild();
    //ImGui::Text("Aspect");
    ImGui::BeginChild("Test2");

    int max_paso=600;
    if(Singleton::getInstance()->Simulador.paso_simulacion>(max_paso-100))max_paso=Singleton::getInstance()->Simulador.paso_simulacion+100;
    float max_pasof=(float)max_paso;
    float relsize=500.0f/max_pasof;
    

    for(int i=0;i<Singleton::getInstance()->SIMUSCRIPT.cant_ignitionpoint;i++)
    {
        int step=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].step;
        if(Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].enabled){
            ImGui::GetWindowDrawList()->AddLine(p+ImVec2(step*relsize,0),p+ImVec2(step*relsize, 50), IM_COL32(255, 0, 0, 255), 3.0f);
        }else{
            ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(step*relsize,50),5.0f, IM_COL32(160, 160, 160, 255),20);
        }
    }
    ImGui::GetWindowDrawList()->AddLine(p+ImVec2(Singleton::getInstance()->Simulador.paso_simulacion*relsize,25),p+ImVec2(Singleton::getInstance()->Simulador.paso_simulacion*relsize, 50), IM_COL32(255, 255, 0, 255), 3.0f);
    ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(Singleton::getInstance()->Simulador.paso_simulacion*relsize,25),6.0f, IM_COL32(255, 255, 0, 255),20);

    if(Singleton::getInstance()->SIMUSCRIPT.selected_ip>-1 && Singleton::getInstance()->SIMUSCRIPT.selected_ip<Singleton::getInstance()->SIMUSCRIPT.cant_ignitionpoint)
    {
        int step=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[Singleton::getInstance()->SIMUSCRIPT.selected_ip].step;
        ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(step*relsize,0),4.0f, IM_COL32(0, 255, 0, 255),20);
    }
    //ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(50,50), 6.0f, IM_COL32(255, 0, 255, 255));
    
    ImGui::EndChild();

    ImGui::End();   
}

static void ShowTerrainReference(bool* p_open){
    char str[128];
    ImGui::SetNextWindowSize(ImVec2(450,270), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Ref. Terreno", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }

    //float tex_w = 1208;
    //float tex_h = 52;
    //ImTextureID tex_id = (void*)texture_colormap_ID;
    //ImGui::Text("%.0fx%.0f", tex_w, tex_h);
    //ImGui::Image(tex_id, ImVec2(tex_w/5,tex_h), ImVec2(1,1), ImVec2(0,0), ImColor(255,255,255,255), ImColor(255,255,255,128));
    //ImGui::PushItemWidth(tex_w/5);
    //ImGui::SliderFloat("", &windpow_at_mouse, windi_min, windi_max);
    
    

    ImGui::Columns(3, "mycolumns"); // 4-ways, with border
    ImGui::Separator();
    ImGui::SetColumnOffset(0,0);
    ImGui::Text("ELEVACION"); ImGui::NextColumn();
    ImGui::SetColumnOffset(0,0);
    ImGui::Text("SLOPE"); ImGui::NextColumn();
    ImGui::SetColumnOffset(1,150);
    ImGui::Text("ASPECT"); ImGui::NextColumn();
    ImGui::SetColumnOffset(2,295);
    //ImGui::Text("Flags"); ImGui::NextColumn();
    ImGui::Separator();

    ImGui::VSliderFloat("##v", ImVec2(18,200), &height_at_mouse, Singleton::getInstance()->altitud_min, Singleton::getInstance()->altitud_max, "");
    sprintf(str,"%.02f\t",height_at_mouse);
    ImGui::SameLine();
    ImGui::PushItemWidth(150.0f);
    ImGui::Text(str);

    ImGui::NextColumn();

    //ImGui::SameLine();
    ImGui::BeginChild("Test", ImVec2(120, 120), true);
    ImVec2 p = ImGui::GetCursorScreenPos();
    ImGui::Image((void*) NULL/*ImGui::GetIO().Fonts->TexID*/, ImVec2(100,100));
    ImGui::EndChild();
    ImGui::Text("Slope");
    ImGui::BeginChild("Test");

    ImVec2 lin_xy;
    float radio;
    radio=100.0f;
    lin_xy.x=radio*cos(radians(slope_at_mouse));
    lin_xy.y=radio*sin(radians(slope_at_mouse));
    //printf("%.02f %.02f\n",lin_xy.x,lin_xy.y);

    ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x +100 - lin_xy.x, p.y +100 - lin_xy.y),ImVec2(p.x + 100, p.y + 100), IM_COL32(255, 0, 0, 255), 3.0f);
    ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(100,100), 6.0f, IM_COL32(255, 0, 255, 255));
    ImGui::EndChild();
    sprintf(str,"%.02f",slope_at_mouse);
    ImGui::Text(str);

    ImGui::NextColumn();

    ImGui::BeginChild("Test2", ImVec2(120, 120), true);
    p = ImGui::GetCursorScreenPos();
    ImGui::Image((void*) NULL/*ImGui::GetIO().Fonts->TexID*/, ImVec2(100,100));
    ImGui::EndChild();
    ImGui::Text("Aspect");
    ImGui::BeginChild("Test2");

    //ImVec2 lin_xy;
    //float radio, angulo;
    //angulo=aspect_at_mouse;
    radio=50.0f;
    lin_xy.x=-radio*sin(radians(aspect_at_mouse));
    lin_xy.y=radio*cos(radians(aspect_at_mouse));
    //printf("%.02f %.02f\n",lin_xy.x,lin_xy.y);

    ImGui::GetWindowDrawList()->AddLine(ImVec2(p.x +50 - lin_xy.x, p.y +50 - lin_xy.y),ImVec2(p.x + 50, p.y + 50), IM_COL32(255, 0, 0, 255), 3.0f);
    ImGui::GetWindowDrawList()->AddCircleFilled(p+ImVec2(50,50), 6.0f, IM_COL32(255, 0, 255, 255));
    ImGui::EndChild();

    sprintf(str,"%.02f\t",aspect_at_mouse);
    ImGui::Text(str);
    float aspect_val[8]={22.5f,67.5f,112.5f,157.5f,202.5f,247.5f,292.5f,337.5};
    const char* text[]={"N","NE","E","SE","S","SO","O","NO"};
    int i=-1,selaspect=0;
    for(i=0;i<7;i++)
    {
        if(aspect_at_mouse>aspect_val[i]&&aspect_at_mouse<aspect_val[i+1])
        {
            selaspect=i+1;break;
        }
    }

    sprintf(str,"%s",text[selaspect]);
    ImGui::Text(str);

    ImGui::End();   
}

static void ShowWindReference(bool* p_open){
    char str[128];
    ImGui::SetNextWindowSize(ImVec2(300,150), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Ref. Viento", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }

    float tex_w = 1208;
    float tex_h = 52;
    ImTextureID tex_id = (void*)Singleton::getInstance()->texture_colormap_ID;
    sprintf(str,"VIENTO");
    ImGui::Text(str);
    //ImGui::Text("%.0fx%.0f", tex_w, tex_h);
    ImGui::Image(tex_id, ImVec2(tex_w/5,tex_h), ImVec2(1,1), ImVec2(0,0), ImColor(255,255,255,255), ImColor(255,255,255,128));
    ImGui::PushItemWidth(tex_w/5);
    ImGui::SliderFloat("", &windpow_at_mouse, Singleton::getInstance()->windi_min, Singleton::getInstance()->windi_max);

    ImGui::End();   
}

static void ShowFWIReference(bool* p_open){
    ImGui::SetNextWindowSize(ImVec2(420,230), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("FWI Ref.", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }
    
    ImVec4 color;
    int iidx=1;
    vec3*peli_col=&Singleton::getInstance()->peli_col[0];

    iidx=1;color = ImColor(peli_col[iidx].r/256.0f, peli_col[iidx].g/256.0f, peli_col[iidx].b/256.0f, 1.0f);
    ImGui::ColorButton(color);ImGui::SameLine();ImGui::Text("BAJO");

    iidx=2;color = ImColor(peli_col[iidx].r/256.0f, peli_col[iidx].g/256.0f, peli_col[iidx].b/256.0f, 1.0f);
    ImGui::ColorButton(color);ImGui::SameLine();ImGui::Text("MEDIO");

    iidx=3;color = ImColor(peli_col[iidx].r/256.0f, peli_col[iidx].g/256.0f, peli_col[iidx].b/256.0f, 1.0f);
    ImGui::ColorButton(color);ImGui::SameLine();ImGui::Text("ALTO");

    iidx=4;color = ImColor(peli_col[iidx].r/256.0f, peli_col[iidx].g/256.0f, peli_col[iidx].b/256.0f, 1.0f);
    ImGui::ColorButton(color);ImGui::SameLine();ImGui::Text("MUY ALTO");

    iidx=5;color = ImColor(peli_col[iidx].r/256.0f, peli_col[iidx].g/256.0f, peli_col[iidx].b/256.0f, 1.0f);
    ImGui::ColorButton(color);ImGui::SameLine();ImGui::Text("EXTREMO");

    /*ImVec2 pos = ImGui::GetCursorScreenPos();
    ImGui::SetCursorPosX(190.0f);
    ImGui::SetCursorPosY(0.0f);
    float wrap_width=1.0f;
    ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + wrap_width);
    ImGui::Text("Pastizal");
    ImGui::PopTextWrapPos();

    ImGui::SetCursorPosX(240.0f);
    ImGui::SetCursorPosY(0.0f);
    ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + wrap_width);
    ImGui::Text("Arbustal");
    ImGui::PopTextWrapPos();

    ImGui::SetCursorPosX(290.0f);
    ImGui::SetCursorPosY(0.0f);
    ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + wrap_width);
    ImGui::Text("Bosque.A");
    ImGui::PopTextWrapPos();

    ImGui::SetCursorPosX(340.0f);
    ImGui::SetCursorPosY(0.0f);
    ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + wrap_width);
    ImGui::Text("Bosque.B");
    ImGui::PopTextWrapPos();

    ImGui::SetCursorPosX(390.0f);
    ImGui::SetCursorPosY(0.0f);
    ImGui::PushTextWrapPos(ImGui::GetCursorPos().x + wrap_width);
    ImGui::Text("Bosque.I");
    ImGui::PopTextWrapPos();*/
    

    ImGui::End();   
}

static void ShowFWITable(bool* p_open){
    char str[128];
    ImGui::SetNextWindowSize(ImVec2(1410,500), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    bool prevstate=&p_open;
    if(!ImGui::Begin("FWI", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoScrollbar))
    {
        ImGui::End();
        return;
    }
    //DATE       TEMP     RH    WIND(S)  WIND(V) RAIN    FFMC    DMC     DC  ISI     BUI     FWI     DSR 

    int co=0,iidx=1;

    ImGui::BeginChild("header", ImVec2(1400, 40), false);
        ImGui::Columns(14, "mycolumns2"); // 4-ways, with border
        ImGui::Separator();
        ImGui::Text("DATE"); ImGui::NextColumn();
        
        ImGui::Text("TEMP"); ImGui::NextColumn();
        
        ImGui::Text("RH"); ImGui::NextColumn();
        
        ImGui::Text("W(S)"); ImGui::NextColumn();
        ImGui::Text("W(V)"); ImGui::NextColumn();
        ImGui::Text("RAIN"); ImGui::NextColumn();
        ImGui::Text("FFMC"); ImGui::NextColumn();
        ImGui::Text("DMC"); ImGui::NextColumn();
        ImGui::Text("DC"); ImGui::NextColumn();
        ImGui::Text("ISI"); ImGui::NextColumn();
        ImGui::Text("BUI"); ImGui::NextColumn();
        ImGui::Text("FWI"); ImGui::NextColumn();
        ImGui::Text("DSR"); ImGui::NextColumn();
        ImGui::Text("Pa Ar BA BB BI"); ImGui::NextColumn();
        ImGui::Separator();
        

        co+=160;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=70;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=70;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=90;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=95;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=95;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
        co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    ImGui::EndChild();

    ImGui::BeginChild("tablac", ImVec2(1400, 400), false);
    ImGui::Columns(14, "mycolumns"); // 4-ways, with border
    /*ImGui::Separator();
    ImGui::Text("DATE"); ImGui::NextColumn();
    
    ImGui::Text("TEMP"); ImGui::NextColumn();
    
    ImGui::Text("RH"); ImGui::NextColumn();
    
    ImGui::Text("W(S)"); ImGui::NextColumn();
    ImGui::Text("W(V)"); ImGui::NextColumn();
    ImGui::Text("RAIN"); ImGui::NextColumn();
    ImGui::Text("FFMC"); ImGui::NextColumn();
    ImGui::Text("DMC"); ImGui::NextColumn();
    ImGui::Text("DC"); ImGui::NextColumn();
    ImGui::Text("ISI"); ImGui::NextColumn();
    ImGui::Text("BUI"); ImGui::NextColumn();
    ImGui::Text("FWI"); ImGui::NextColumn();
    ImGui::Text("DSR"); ImGui::NextColumn();
    ImGui::Text("PELIG"); ImGui::NextColumn();*/

    co=0;
    iidx=1;

    co+=160;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=70;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=70;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=90;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=95;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=95;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    co+=80;ImGui::SetColumnOffset(iidx,co);iidx++;
    //ImGui::SetColumnOffset(13,1400);
    //ImGui::Text("Flags"); ImGui::NextColumn();
    ImGui::Separator();
    //const char* names[3] = { "One", "Two", "Three" };
    //const char* paths[3] = { "/path/one", "/path/two", "/path/three" };
    char letras[] = {'.','B', 'M', 'A', 'X', 'E'};
    char* nombres[5]={(char*)"Pastizal",(char*)"Arbustal",(char*)"Bosque A",(char*)"Bosque B",(char*)"Bosque I"};
    static int selected = -1;
    for (int i = 0; i < Singleton::getInstance()->FWI_T.cant_fwi; i++)
    {
        if(Singleton::getInstance()->FWI_T.fwi[i].valid==0)continue;
        //ImGui::NextColumn();
        /*ImGui::Text(names[i]); ImGui::NextColumn();
        ImGui::Text(paths[i]); ImGui::NextColumn();
        ImGui::Text("...."); ImGui::NextColumn();*/
        /*ImVec4 color = ImColor(paleta_vegetacion[i].r, paleta_vegetacion[i].g, paleta_vegetacion[i].b, 1.0f);
        sprintf(str,"%d",i);
        ImGui::PushItemWidth(100);*/
        //ImGui::Text(str);
        

        //ImGui::NextColumn();
        sprintf(str,"%d/%02d/%02d",Singleton::getInstance()->FWI_T.fwi[i].met.aa,Singleton::getInstance()->FWI_T.fwi[i].met.mm,Singleton::getInstance()->FWI_T.fwi[i].met.dd);
        //ImGui::Text(str);ImGui::NextColumn();
        if (ImGui::Selectable(str, selected == i, ImGuiSelectableFlags_SpanAllColumns))
            selected = i;
        /*ImGui::ColorButton(color);*/
        ImGui::NextColumn();


        sprintf(str,"%.01f",Singleton::getInstance()->FWI_T.fwi[i].met.temp);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].met.rh);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%s",Singleton::getInstance()->FWI_T.fwi[i].met.ws_str);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].met.wv);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].met.rain);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].ffmc);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].dmc);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].dc);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].isi);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].bui);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].fwi);
        ImGui::Text(str);ImGui::NextColumn();

        sprintf(str,"%.02f",Singleton::getInstance()->FWI_T.fwi[i].dsr);
        ImGui::Text(str);ImGui::NextColumn();



        /*sprintf(str,"%d%d%d%d%d",FWI_T.fwi[i].pelig[0],FWI_T.fwi[i].pelig[1],FWI_T.fwi[i].pelig[2],FWI_T.fwi[i].pelig[3],FWI_T.fwi[i].pelig[4]);
        ImGui::Text(str);ImGui::NextColumn();*/

        vec3* peli_col=&Singleton::getInstance()->peli_col[0];
        for (int m = 0; m < 5; m++)
        {
            if (m > 0) ImGui::SameLine();
            int pelm=Singleton::getInstance()->FWI_T.fwi[i].pelig[m];
            ImColor color=ImColor(peli_col[pelm].r/256.0f, peli_col[pelm].g/256.0f, peli_col[pelm].b/256.0f, 1.0f);
            ImGui::PushID(i*5+m);
            ImGui::PushStyleColor(ImGuiCol_Button, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
            ImGui::PushStyleColor(ImGuiCol_Text, ImColor(0.0f,0.0f,0.0f,1.0f));
            
            sprintf(str,"%c ",letras[pelm]);
            ImGui::Button(str);
            
            ImGui::PopStyleColor(4);
            ImGui::PopID();
            if(ImGui::IsItemHovered())
            {
                ImGui::BeginTooltip();
                ImGui::Text("%s\n",nombres[m]);
                ImGui::EndTooltip();
            }
        }

        //sprintf(str,"%c%c%c%c%c",letras[FWI_T.fwi[i].pelig[0]],letras[FWI_T.fwi[i].pelig[1]],letras[FWI_T.fwi[i].pelig[2]],letras[FWI_T.fwi[i].pelig[3]],letras[FWI_T.fwi[i].pelig[4]]);
        /*ImGui::Text(str);*/ImGui::NextColumn();
        
    }

    bool vpi_cha=Singleton::getInstance()->Set_visualizar_pelig_indice(selected);
    if(vpi_cha)
    {
        print_debug("PELIG IDX: %d\n",Singleton::getInstance()->Get_visualizar_pelig_indice());
        generar_textura_pelig(&Singleton::getInstance()->textura_pelig,Singleton::getInstance()->vegetacion.h_map);
        /*glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture_pelig_ID);
        glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, tex_x, tex_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, textura_pelig);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        regenerar_textura_final();*/
        Singleton::gI()->proyectow.fwi=Singleton::getInstance()->FWI_T.fwi[selected];
    }

    ImGui::Columns(1);
    ImGui::Separator();

    /*ImGuiIO& io = ImGui::GetIO();

    for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++)
    {
        if (ImGui::IsMouseDoubleClicked(i) && selected>=0)
        {
            show_editar_combustible=1;
        }
    }*/


    //sprintf(str,"SEl:%d",selected);
    //ImGui::Text(str);

    /*for(int i=0;i<100;i++)
    {
        ImVec4 color = ImColor(paleta_vegetacion[i].r, paleta_vegetacion[i].g, paleta_vegetacion[i].b, 1.0f);
        sprintf(str,"%d\t",i);
        ImGui::Text(str);
        ImGui::SameLine();
        ImGui::ColorButton(color);
        sprintf(str,"\t%d\t",paleta_conversion_vege[i]);
        ImGui::SameLine();
        ImGui::Text(str);

    }*/
    ImGui::EndChild();
    ImGui::End(); 

    if(prevstate==1 && *p_open==0){show_fwit_ref_window=0;}  
}

static void ShowMapeoCombustibles(bool* p_open){
    char str[128];
    ImGui::SetNextWindowSize(ImVec2(800,400), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Mapeo combustibles", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }

    ImGui::Columns(4, "mycolumns"); // 4-ways, with border
    ImGui::Separator();
    ImGui::SetColumnOffset(0,0);
    ImGui::Text("MAPA"); ImGui::NextColumn();
    ImGui::SetColumnOffset(0,0);
    ImGui::Text("SIMU"); ImGui::NextColumn();
    ImGui::SetColumnOffset(1,100);
    ImGui::Text("COLOR"); ImGui::NextColumn();
    ImGui::SetColumnOffset(2,200);
    ImGui::Text("NOMBRE"); ImGui::NextColumn();
    ImGui::SetColumnOffset(3,300);
    //ImGui::Text("Flags"); ImGui::NextColumn();
    ImGui::Separator();
    //const char* names[3] = { "One", "Two", "Three" };
    //const char* paths[3] = { "/path/one", "/path/two", "/path/three" };
    static int selected = -1;
    vec3*paleta_vegetacion=&Singleton::getInstance()->paleta_vegetacion[0];
    for (int i = 0; i < 100; i++)
    {
        if(Singleton::getInstance()->proyectow.fuelnames[i]==NULL)continue;
        //ImGui::NextColumn();
        /*ImGui::Text(names[i]); ImGui::NextColumn();
        ImGui::Text(paths[i]); ImGui::NextColumn();
        ImGui::Text("...."); ImGui::NextColumn();*/
        ImVec4 color = ImColor(paleta_vegetacion[i].r, paleta_vegetacion[i].g, paleta_vegetacion[i].b, 1.0f);
        sprintf(str,"%d",i);
        ImGui::PushItemWidth(100);
        //ImGui::Text(str);
        if (ImGui::Selectable(str, selected == i, ImGuiSelectableFlags_SpanAllColumns))
            selected = i;

        ImGui::NextColumn();
        sprintf(str,"%d",Singleton::getInstance()->proyectow.paleta_conversion_vege[i]);
        ImGui::Text(str);ImGui::NextColumn();
        ImGui::ColorButton(color);ImGui::NextColumn();
        sprintf(str,"%s",Singleton::getInstance()->proyectow.fuelnames[i]);
        ImGui::Text(str);ImGui::NextColumn();
        Singleton::getInstance()->proyectow.marcar=selected;
        
    }
    ImGui::Columns(1);
    ImGui::Separator();

    ImGuiIO& io = ImGui::GetIO();

    for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++)
    {
        if (ImGui::IsMouseDoubleClicked(i) && selected>=0)
        {
            show_editar_combustible=1;
            //*p_open=0;//aca se pondria el codigo para editar la asignacion de colores, etc.

        }
    }


    //sprintf(str,"SEl:%d",selected);
    //ImGui::Text(str);

    /*for(int i=0;i<100;i++)
    {
        ImVec4 color = ImColor(paleta_vegetacion[i].r, paleta_vegetacion[i].g, paleta_vegetacion[i].b, 1.0f);
        sprintf(str,"%d\t",i);
        ImGui::Text(str);
        ImGui::SameLine();
        ImGui::ColorButton(color);
        sprintf(str,"\t%d\t",paleta_conversion_vege[i]);
        ImGui::SameLine();
        ImGui::Text(str);

    }*/

    if (ImGui::Button("Guardar Cambios en archivo"))
    {
        *p_open=0;
        FILE *output = fopen(Singleton::getInstance()->proyectow.PALETA_F,"w");

        if (output == NULL)
        {
            printf("Error al guardar paleta: no abre archivo %s \n", Singleton::getInstance()->proyectow.PALETA_F);
            //exit(-1);
        }

        fprintf(output,"###### El formato para el mapeo es:  VALOR RASTER=VALOR SIMULADOR #####\n");
        for(int i=0;i<100;i++)
        {
            if(Singleton::getInstance()->proyectow.paleta_conversion_vege[i]==99)continue;
            fprintf(output,"R%d=%d\n",i,Singleton::getInstance()->proyectow.paleta_conversion_vege[i]);
        }

        fprintf(output,"\n###### Se define el nombre y color RGB para cada valor del raster #####\n");
        for(int i=0;i<100;i++)
        {
            if(Singleton::getInstance()->proyectow.paleta_conversion_vege[i]==99)continue;
            fprintf(output,"I%d=\"%s\",%d,%d,%d\n",i,Singleton::getInstance()->proyectow.fuelnames[i],Singleton::getInstance()->proyectow.vegeR[i],Singleton::getInstance()->proyectow.vegeG[i],Singleton::getInstance()->proyectow.vegeB[i]);
        }
        fclose(output);
    }
    ImGui::End();   
}

static void ShowEditMeteor(bool *p_open)
{
    
    //char str[128];
    ImGui::SetNextWindowSize(ImVec2(800,500), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Edición parámetros meteorologicos", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoResize))
    {
        //ImGui::End();
        return;
    }

    int cursor_pos_x=100;

    //ImGui::PushItemWidth(150);
    
    ImGui::Text("Temp:");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##1", &Singleton::gI()->proyectow.fwi.met.temp, -10.0f, 60.0f);ImGui::SameLine();
    ImGui::InputFloat("##2", &Singleton::gI()->proyectow.fwi.met.temp, 1.0f,0.0f,1);

    ImGui::Text("RH:");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##3", &Singleton::gI()->proyectow.fwi.met.rh,0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##4", &Singleton::gI()->proyectow.fwi.met.rh, 1.0f,0.0f,1);

    /*ImGui::Text("Temp:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SliderFloat("  ", &Singleton::gI()->proyectow.fwi.met.temp, -10.0f, 60.0f);ImGui::SameLine();
    ImGui::InputFloat("", &Singleton::gI()->proyectow.fwi.met.temp, 1.0f);*/

    ImGui::Text("W(v):\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##5", &Singleton::gI()->proyectow.fwi.met.wv, 0.0f, 150.0f);ImGui::SameLine();
    ImGui::InputFloat("##6", &Singleton::gI()->proyectow.fwi.met.wv, 1.0f,0.0f,1);

    ImGui::Text("Rain:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##7", &Singleton::gI()->proyectow.fwi.met.rain, 0.0f, 1000.0f);ImGui::SameLine();
    ImGui::InputFloat("##8", &Singleton::gI()->proyectow.fwi.met.rain, 1.0f,0.0f,1);

    ImGui::Separator();

    ImGui::Text("FFMC:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##9", &Singleton::gI()->proyectow.fwi.ffmc, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##10", &Singleton::gI()->proyectow.fwi.ffmc, 1.0f,0.0f,1);

    ImGui::Text("DMC:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##11", &Singleton::gI()->proyectow.fwi.dmc, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##12", &Singleton::gI()->proyectow.fwi.dmc, 1.0f,0.0f,1);

    ImGui::Text("DC:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##13", &Singleton::gI()->proyectow.fwi.dc, 0.0f, 300.0f);ImGui::SameLine();
    ImGui::InputFloat("##14", &Singleton::gI()->proyectow.fwi.dc, 1.0f,0.0f,1);

    ImGui::Text("ISI:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##15", &Singleton::gI()->proyectow.fwi.isi, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##16", &Singleton::gI()->proyectow.fwi.isi, 1.0f,0.0f,1);

    ImGui::Text("BUI:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##17", &Singleton::gI()->proyectow.fwi.bui, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##18", &Singleton::gI()->proyectow.fwi.bui, 1.0f,0.0f,1);

    ImGui::Text("FWI:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##19", &Singleton::gI()->proyectow.fwi.fwi, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##20", &Singleton::gI()->proyectow.fwi.fwi, 1.0f,0.0f,1);

    ImGui::Text("DSR:\t");ImGui::SameLine();ImGui::PushItemWidth(200);
    ImGui::SetCursorPosX(cursor_pos_x);ImGui::SliderFloat("  ##21", &Singleton::gI()->proyectow.fwi.dsr, 0.0f, 100.0f);ImGui::SameLine();
    ImGui::InputFloat("##22", &Singleton::gI()->proyectow.fwi.dsr, 1.0f,0.0f,1);

    /*float temp,rh,ws,wv,rain;

    float ffmc,dmc,dc,isi,bui,fwi,dsr;*/

    /*if(Singleton::getInstance()->proyectow.marcar<0){*p_open=0;ImGui::End();return;}
    int selected=Singleton::getInstance()->proyectow.marcar;
    ImGui::InputText("Name", Singleton::getInstance()->proyectow.fuelnames[selected], 48);
    sprintf(str,"Mapear valor %d del raster al simulador:",selected);
    ImGui::InputInt(str, &Singleton::getInstance()->proyectow.paleta_conversion_vege[selected]);
    ImVec4 color = ImColor(Singleton::getInstance()->paleta_vegetacion[selected].r, Singleton::getInstance()->paleta_vegetacion[selected].g, Singleton::getInstance()->paleta_vegetacion[selected].b, 1.0f);
    ImGui::ColorButton(color);
    ImGui::BeginChild("child", ImVec2(0, 160), true);
    ImGui::SliderFloat("R", &Singleton::getInstance()->paleta_vegetacion[selected].r, 0.0f, 1.0f, "%.3f");
    ImGui::SliderFloat("G", &Singleton::getInstance()->paleta_vegetacion[selected].g, 0.0f, 1.0f, "%.3f");
    ImGui::SliderFloat("B", &Singleton::getInstance()->paleta_vegetacion[selected].b, 0.0f, 1.0f, "%.3f");
    ImGui::EndChild();
    Singleton::getInstance()->vegetacion.Load_Paleta(Singleton::getInstance()->paleta_vegetacion,100);
    Singleton::getInstance()->proyectow.mover_grafico_a_paleta();
    regenerar_textura_final();
    if (ImGui::Button("Cerrar"))
    {
        *p_open=0;
    }*/
    ImGui::End();
    return;
}

static void ShowEditarCombustible(bool *p_open)
{
    
    char str[128];
    ImGui::SetNextWindowSize(ImVec2(600,400), ImGuiSetCond_Always);
    //ImGui::SetNextWindowPos(ImVec2(window_width-400,0),ImGuiSetCond_Always);
    if(!ImGui::Begin("Editar combustible", p_open,/*ImGuiWindowFlags_NoMove|*/ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }

    if(Singleton::getInstance()->proyectow.marcar<0){*p_open=0;ImGui::End();return;}
    int selected=Singleton::getInstance()->proyectow.marcar;
    ImGui::InputText("Name", Singleton::getInstance()->proyectow.fuelnames[selected], 48);
    sprintf(str,"Mapear valor %d del raster al simulador:",selected);
    ImGui::InputInt(str, &Singleton::getInstance()->proyectow.paleta_conversion_vege[selected]);
    ImVec4 color = ImColor(Singleton::getInstance()->paleta_vegetacion[selected].r, Singleton::getInstance()->paleta_vegetacion[selected].g, Singleton::getInstance()->paleta_vegetacion[selected].b, 1.0f);
    ImGui::ColorButton(color);
    ImGui::BeginChild("child", ImVec2(0, 160), true);
    ImGui::SliderFloat("R", &Singleton::getInstance()->paleta_vegetacion[selected].r, 0.0f, 1.0f, "%.3f");
    ImGui::SliderFloat("G", &Singleton::getInstance()->paleta_vegetacion[selected].g, 0.0f, 1.0f, "%.3f");
    ImGui::SliderFloat("B", &Singleton::getInstance()->paleta_vegetacion[selected].b, 0.0f, 1.0f, "%.3f");
    ImGui::EndChild();
    Singleton::getInstance()->vegetacion.Load_Paleta(Singleton::getInstance()->paleta_vegetacion,100);
    Singleton::getInstance()->proyectow.mover_grafico_a_paleta();
    regenerar_textura_final();
    if (ImGui::Button("Cerrar"))
    {
        *p_open=0;
    }
    ImGui::End();
    return;
}

static void ShowMenuOptions()
{
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    
    if (ImGui::MenuItem("Open", "Ctrl+O")) {}
    if (ImGui::BeginMenu("Open Recent"))
    {
        ImGui::MenuItem("fish_hat.c");
        ImGui::MenuItem("fish_hat.inl");
        ImGui::MenuItem("fish_hat.h");
        if (ImGui::BeginMenu("More.."))
        {
            ImGui::MenuItem("Hello");
            ImGui::MenuItem("Sailor");
            ImGui::EndMenu();
        }
        ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Save", "Ctrl+S")) {}
    if (ImGui::MenuItem("Save As..")) {}
    ImGui::Separator();
    if (ImGui::BeginMenu("Options"))
    {
        static bool enabled = true;
        ImGui::MenuItem("Enabled", "", &enabled);
        ImGui::BeginChild("child", ImVec2(0, 60), true);
        for (int i = 0; i < 10; i++)
            ImGui::Text("Scrolling Text %d", i);
        ImGui::EndChild();
        static float f = 0.5f;
        static int n = 0;
        ImGui::SliderFloat("Value", &f, 0.0f, 1.0f);
        ImGui::InputFloat("Input", &f, 0.1f);
        ImGui::Combo("Combo", &n, "Yes\0No\0Maybe\0\0");
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Colors"))
    {
        for (int i = 0; i < ImGuiCol_COUNT; i++)
            ImGui::MenuItem(ImGui::GetStyleColName((ImGuiCol)i));
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Disabled", false)) // Disabled
    {
        IM_ASSERT(0);
    }
    if (ImGui::MenuItem("Checked", NULL, true)) {}
    if (ImGui::MenuItem("Quit", "Alt+F4")) {}
}

static void ShowMenuCortafuego()
{
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    if (ImGui::MenuItem("Descargar todo")) 
    {
        Singleton::getInstance()->CORTAFUEGOS.borrar_todo();
        Singleton::getInstance()->CORTAFUEGOS.selected=-1;
        Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
    }
    if (ImGui::MenuItem("Cargar...")) {mostrar_cargar_cortafuego=1;}
    if (ImGui::MenuItem("Guardar como...", "Ctrl+G")) {mostrar_guardar_cortafuego=1;}
    ImGui::Separator();
    if (ImGui::MenuItem("Poner cortafuego",NULL,Singleton::getInstance()->btnS_poner_cortafuego)) {Singleton::getInstance()->btnS_poner_cortafuego =!Singleton::getInstance()->btnS_poner_cortafuego; Singleton::getInstance()->draw_line_state=0;linea_temp=0;}
    if (ImGui::MenuItem("Multilinea",NULL,Singleton::getInstance()->btnS_poner_cortafuego_multilinea)) {Singleton::getInstance()->btnS_poner_cortafuego_multilinea =!Singleton::getInstance()->btnS_poner_cortafuego_multilinea; Singleton::getInstance()->draw_line_state=0;linea_temp=0;}
}

static void ShowMenuFWI()
{
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    
    if (ImGui::MenuItem("Tabla FWI", NULL, show_fwit_window)) {show_fwit_window=!show_fwit_window;show_fwit_ref_window=show_fwit_window;}
    if (ImGui::MenuItem("Referencia FWI", NULL, show_fwit_ref_window)) {show_fwit_ref_window=!show_fwit_ref_window;}
    if (ImGui::MenuItem("Cargar archivo meteorologico...")) {show_fwilm_ref_window=1;}

}

static void ShowMenuSimulador()
{
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    //if (ImGui::MenuItem("Tabla FWI", NULL, show_fwit_window)) {show_fwit_window=!show_fwit_window;show_fwit_ref_window=show_fwit_window;}
    if (ImGui::MenuItem("Mapeo combustibles a simulador","",false,Singleton::getInstance()->proyecto_cargado)) {show_mapeoscomb_window=1;}
    if (ImGui::BeginMenu("Edición de parámetros"))
    {
        if (ImGui::MenuItem("Meteorologicos")) {show_editmeteor_window=1;}
        if (ImGui::MenuItem("Constantes internas")) {;}
        /*static bool enabled = true;
        ImGui::MenuItem("Enabled", "", &enabled);
        ImGui::BeginChild("child", ImVec2(0, 60), true);
        for (int i = 0; i < 10; i++)
            ImGui::Text("Scrolling Text %d", i);
        ImGui::EndChild();
        static float f = 0.5f;
        static int n = 0;
        ImGui::SliderFloat("Value", &f, 0.0f, 1.0f);
        ImGui::InputFloat("Input", &f, 0.1f);
        ImGui::Combo("Combo", &n, "Yes\0No\0Maybe\0\0");
        ImGui::EndMenu();*/
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Modelo"))
    {
        //if(ImGui::MenuItem("Enabled", NULL, visualizar_Calti_map)){visualizar_Calti_map=!visualizar_Calti_map;CORTAFUEGOS.algun_cambio=1;}
        if (ImGui::MenuItem("Ninguno", NULL, Singleton::getInstance()->Simulador.modelo==0)) {Singleton::getInstance()->Simulador.inicializar(0);}
        if (ImGui::MenuItem("Autómata Celular", NULL, Singleton::getInstance()->Simulador.modelo==1)) {Singleton::getInstance()->Simulador.inicializar(1);}
        if (ImGui::MenuItem("RDC", NULL, Singleton::getInstance()->Simulador.modelo==2)) {Singleton::getInstance()->Simulador.inicializar(2);}
        ImGui::EndMenu();
    }
}


static void ShowMenuView()
{
    //ImGui::MenuItem("(dummy menu)", NULL, false, false);
    if (ImGui::MenuItem("Pantalla completa", NULL, Singleton::getInstance()->fullscreen)) {glutFullScreenToggle();Singleton::getInstance()->fullscreen=!Singleton::getInstance()->fullscreen;}
    if (ImGui::MenuItem("Reset Camera")) {reset_camera();}
    if (ImGui::BeginMenu("Curvas nivel"))
    {
        //if(ImGui::MenuItem("Enabled", NULL, visualizar_Calti_map)){visualizar_Calti_map=!visualizar_Calti_map;CORTAFUEGOS.algun_cambio=1;}
        float old_mapa_Calti_cant=Singleton::getInstance()->mapa_Calti_cant,old_mapa_Calti_pow=Singleton::getInstance()->mapa_Calti_pow;
        ImGui::SliderInt("Cant", &Singleton::getInstance()->mapa_Calti_cant, 1, 10);
        ImGui::SliderInt("Pow", &Singleton::getInstance()->mapa_Calti_pow, 1, 100);

        if(old_mapa_Calti_cant!=Singleton::getInstance()->mapa_Calti_cant || old_mapa_Calti_pow!=Singleton::getInstance()->mapa_Calti_pow)
        {
            Singleton::getInstance()->update_Calti=1;
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
        }
        ImGui::EndMenu();
    }

    if (ImGui::MenuItem("Referencia viento", NULL, show_windreference_window)) {show_windreference_window=!show_windreference_window;}
    if (ImGui::MenuItem("Referencia terreno", NULL, show_terrainreference_window)) {show_terrainreference_window=!show_terrainreference_window;}
    if (ImGui::MenuItem("Barra Paso", NULL, show_barrapaso_window)) {show_barrapaso_window=!show_barrapaso_window;}
    if (ImGui::BeginMenu("Iluminacion"))
    {
        ImGui::PushItemWidth(100);
        static bool iluminac_terreno=1.0f;
        //ImGui::SliderFloat("Iluminac.", &iluminar_terreno, 0.0f, 1.0f, "%.2f");
        bool old_iluminac_terreno=iluminac_terreno;
        ImGui::Checkbox("Activar Iluminacion", &iluminac_terreno);
        if(iluminac_terreno!=old_iluminac_terreno)
        {
            if(iluminac_terreno)
            {
                Singleton::getInstance()->specular_mult=0.2f;
                Singleton::getInstance()->diffuse_mult=0.2f;
                Singleton::getInstance()->diffuseC_mult=0.1f;
            }
            else
            {
                Singleton::getInstance()->ambient_mult=0.0f;
                Singleton::getInstance()->specular_mult=0.0f;
                Singleton::getInstance()->diffuse_mult=0.0f;
                Singleton::getInstance()->diffuseC_mult=0.0f;
            }
        }
        ImGui::SliderFloat("Ambient", &Singleton::getInstance()->ambient_mult, 0.0f, 1.0f, "%.2f");
        ImGui::SliderFloat("Specular", &Singleton::getInstance()->specular_mult, 0.0f, 1.0f, "%.2f");
        ImGui::SliderFloat("Diffuse", &Singleton::getInstance()->diffuse_mult, 0.0f, 1.0f, "%.2f");
        ImGui::SliderFloat("DiffuseC", &Singleton::getInstance()->diffuseC_mult, 0.0f, 1.0f, "%.2f");
        ImGui::EndMenu();
    }
    /*if (ImGui::MenuItem("Open", "Ctrl+O")) {}
    if (ImGui::BeginMenu("Open Recent"))
    {
        ImGui::MenuItem("fish_hat.c");
        ImGui::MenuItem("fish_hat.inl");
        ImGui::MenuItem("fish_hat.h");
        if (ImGui::BeginMenu("More.."))
        {
            ImGui::MenuItem("Hello");
            ImGui::MenuItem("Sailor");
            ImGui::EndMenu();
        }
        ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Save", "Ctrl+S")) {}
    if (ImGui::MenuItem("Save As..")) {}
    ImGui::Separator();
    if (ImGui::BeginMenu("Options"))
    {
        static bool enabled = true;
        ImGui::MenuItem("Enabled", "", &enabled);
        ImGui::BeginChild("child", ImVec2(0, 60), true);
        for (int i = 0; i < 10; i++)
            ImGui::Text("Scrolling Text %d", i);
        ImGui::EndChild();
        static float f = 0.5f;
        static int n = 0;
        ImGui::SliderFloat("Value", &f, 0.0f, 1.0f);
        ImGui::InputFloat("Input", &f, 0.1f);
        ImGui::Combo("Combo", &n, "Yes\0No\0Maybe\0\0");
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Colors"))
    {
        for (int i = 0; i < ImGuiCol_COUNT; i++)
            ImGui::MenuItem(ImGui::GetStyleColName((ImGuiCol)i));
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Disabled", false)) // Disabled
    {
        IM_ASSERT(0);
    }
    if (ImGui::MenuItem("Checked", NULL, true)) {}
    if (ImGui::MenuItem("Quit", "Alt+F4")) {}*/
}

static void ShowMainMenuBar()
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            ShowMenuFile();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit"))
        {
            if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
            ImGui::Separator();
            if (ImGui::MenuItem("Cut", "CTRL+X")) {}
            if (ImGui::MenuItem("Copy", "CTRL+C")) {}
            if (ImGui::MenuItem("Paste", "CTRL+V")) {}
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Ver"))
        {
            ShowMenuView();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Options"))
        {
            ShowMenuOptions();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Cortafuego",Singleton::getInstance()->proyecto_cargado))
        {
            ShowMenuCortafuego();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("FWI",Singleton::getInstance()->proyecto_cargado))
        {
            ShowMenuFWI();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Simulador",Singleton::getInstance()->proyecto_cargado))
        {
            ShowMenuSimulador();
            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void eliminar_ceros_layer_list(int init=0){
    int i=init;
    LAYER *layer_list=&Singleton::getInstance()->layer_list[0];
    while(layer_list[i].show_on_panel!=0){i++; if(i>=MAX_NUM_LAYERS-2)return;}
    int q=i;
    while(true)
    {
        if(q+1>MAX_NUM_LAYERS-2)break;
        layer_list[q]=layer_list[q+1];
        q++;
    }
    if(i>=MAX_NUM_LAYERS-2){return;}else{eliminar_ceros_layer_list(init+1);}
}

void ImGui_DrawAll(void){
	

    ImVec4 clear_color = ImColor(114, 144, 154);
    ImGui_ImplGlfwGL3_NewFrame();
    char auxchar[64];
    /*static bool show_another_windowd = true;
    ImGui::Begin("Another Window", &show_another_windowd, ImVec2(200, 200));
    ImGui::Text("Hello");
    ImGui::End();*/
    
    static bool first_time=true;
    mostrar_cargar_real=0;
    mostrar_guardar_cortafuego=0;
    mostrar_cargar_cortafuego=0;

    ShowMainMenuBar();

    /*static bool testOpen = true;
    ImGui::ShowTestWindow(&testOpen); //VENTANA DE PRUEBA DE IMGUI
    */

    // 1. Show a simple window
    // Tip: if we don't call ImGui::Begin()/ImGui::End() the widgets appears in a window automatically called "Debug"
    /*    static float f = 0.0f;
        ImGui::Text("Hello, world!");
        ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
        ImGui::ColorEdit3("clear color", (float*)&clear_color);
        ImGui::Button("Test Window");
        ImGui::Button("Another Window");
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);*/
    
    if(show_barrapaso_window)ShowBarraPaso(&show_barrapaso_window);
    if(show_mapeoscomb_window)ShowMapeoCombustibles(&show_mapeoscomb_window);
    if(show_fwit_ref_window)ShowFWIReference(&show_fwit_ref_window);
    if(show_editmeteor_window)ShowEditMeteor(&show_editmeteor_window);
    if(show_new_project_window)
    {
        //ImGui::OpenPopup("New Project...");
        if(show_new_project_window_runonce)
        {
            //LoadProjectRecalcularFilelist();
            show_new_project_window_runonce=0;
        }
        NewProjectWindow();
    }
    if(show_fwit_window)ShowFWITable(&show_fwit_window);
    if(!show_fwit_window){Singleton::getInstance()->Set_visualizar_pelig_indice(-1);}
    if(show_windreference_window)ShowWindReference(&show_windreference_window);
    if(show_terrainreference_window)ShowTerrainReference(&show_terrainreference_window);
    if(show_editar_combustible)ShowEditarCombustible(&show_editar_combustible);
    if(mostrar_cargar_real){
        ImGui::OpenPopup("Cargar mapa fuego real...");
        Singleton::getInstance()->CORTAFUEGOS.recalcular_filelist(Singleton::getInstance()->PROJECT_FOLDER_P,"","asc");
        Singleton::getInstance()->CORTAFUEGOS.show_save=0;
        Singleton::getInstance()->CORTAFUEGOS.show_open=0;
        Singleton::getInstance()->CORTAFUEGOS.show_map_open=1;
    }
    if(show_fwilm_ref_window){
        ImGui::OpenPopup("Cargar archivo datos meteorologicos...");
    }

    if(show_load_project_window)
    {
        if(show_load_project_window_runonce)
        {
            LoadProjectRecalcularFilelist();
            show_load_project_window_runonce=0;
            show_load_project_window_1erinicio=1;
        }
        ImGui::OpenPopup("Load Project...");
        
    }




    bool show_another_window=1;

    ImFontAtlas* atlas = ImGui::GetIO().Fonts;
    for (int i = 0; i < atlas->Fonts.Size; i++)
    {
        ImFont* font = atlas->Fonts[i];
        font->Scale=2.0f;
    }


    bool imguicontrol2=0;

    sprintf(auxchar,"Controles:");

    ImGui::SetNextWindowSize(ImVec2(400,Singleton::getInstance()->window_height), ImGuiSetCond_Always);
    ImGui::SetNextWindowPos(ImVec2(Singleton::getInstance()->window_width-400,0),ImGuiSetCond_Always);
    ImGui::Begin(auxchar, &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
    ImGui::PushItemWidth(-400);
    ImGui::Spacing();
    if ((ImGui::CollapsingHeader("Propiedades de Celda:")||first_time) && Singleton::getInstance()->proyecto_cargado)
    {
        
        sprintf(auxchar,"Celda: X=%d\t Y=%d\nElevacion: %.01f metros",ni,nj,Singleton::getInstance()->h_mapaalti[ni+nj*Singleton::getInstance()->vert_gis_x]);
        ImGui::Text(auxchar);
        if(ni>-1 && nj>-1)
        {
            Singleton::getInstance()->CORTAFUEGOS.ni=ni;
            Singleton::getInstance()->CORTAFUEGOS.nj=nj;
            Singleton::getInstance()->CORTAFUEGOS.show_prob_window=1;
            //CORTAFUEGOS.prob_window_value=(float)quemado.h_map[ni+nj*vert_gis_x]*10.0f;
            sprintf(auxchar,"Probabilidad de quemado %.00f",(float)Singleton::getInstance()->quemado.h_map[ni+nj*Singleton::getInstance()->Singleton::getInstance()->vert_gis_x]*10.0f);
            ImGui::Text(auxchar);

        }
        else
        {
            Singleton::getInstance()->CORTAFUEGOS.show_prob_window=0;
            sprintf(auxchar,"Probabilidad de quemado 0");
            ImGui::Text(auxchar);
        }


        
        if (ImGui::Button("<")) {paso_atras_simulador();}
        ImGui::SameLine();
        if (ImGui::Button(">")) {paso_adelante_simulador();}
        if (ImGui::Button("Auto")) {
            if(!automode){
                automode=1; /*for(int u=0;u<PARAL;u++){M[u].paso_simulacion=0;}*/frame_cnt=0;
                paso_adelante_simulador();
            }
            else
            {
                automode=0; /*for(int u=0;u<PARAL;u++){M[u].paso_simulacion=0;}*/frame_cnt=0;
                paso_adelante_simulador();
            }
        }
        sprintf(auxchar,"Paso: %d",Singleton::getInstance()->Simulador.paso_simulacion);
        ImGui::Text(auxchar);

        sprintf(auxchar,"\t%.0f FPS",Singleton::getInstance()->avgFPS);
        ImGui::SameLine();//DESCOMENTAR PARA VER LOS FPS
        ImGui::Text(auxchar);


        if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
    }

    
    //ImGui::End();

    /*ImGui::SetNextWindowSize(ImVec2(400,400), ImGuiSetCond_Always);
    ImGui::SetNextWindowPos(ImVec2(window_width-400,400),ImGuiSetCond_Always);
    ImGui::Begin("Cortafuegos:", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);*/

    if(/*ImGui::Button("Guardar como...")*/mostrar_guardar_cortafuego)
    {

        //CORTAFUEGOS.guardar_archivo("./AAAprueba.cfu");
        ImGui::OpenPopup("Guardar cortafuego como...");
        Singleton::getInstance()->CORTAFUEGOS.recalcular_filelist(Singleton::getInstance()->PROJECT_FOLDER_P,"cortafuego/","cfu");
        Singleton::getInstance()->CORTAFUEGOS.show_save=1;
        Singleton::getInstance()->CORTAFUEGOS.show_open=0;
        mostrar_guardar_cortafuego=0;
        
    }
    Singleton::getInstance()->CORTAFUEGOS.show_cortafuego_saveas();
    //ImGui::SameLine();


    if(/*ImGui::Button("Cargar...")*/mostrar_cargar_cortafuego)
    {
        //CORTAFUEGOS.guardar_archivo("./AAAprueba.cfu");
        ImGui::OpenPopup("Abrir cortafuego...");
        Singleton::getInstance()->CORTAFUEGOS.recalcular_filelist(Singleton::getInstance()->PROJECT_FOLDER_P,"cortafuego/","cfu");
        Singleton::getInstance()->CORTAFUEGOS.show_save=0;
        Singleton::getInstance()->CORTAFUEGOS.show_open=1;
        mostrar_cargar_cortafuego=0;
    }
    Singleton::getInstance()->CORTAFUEGOS.show_cortafuego_open();

    if (ImGui::CollapsingHeader("Cortafuegos:") && Singleton::getInstance()->proyecto_cargado)
    {
        if (ImGui::Button("Poner Cortafuego")) {Singleton::getInstance()->btnS_poner_cortafuego =!Singleton::getInstance()->btnS_poner_cortafuego; Singleton::getInstance()->draw_line_state=0;linea_temp=0;}
        if (Singleton::getInstance()->btnS_poner_cortafuego)
        {
            Singleton::getInstance()->btnS_poner_cortafuego_multilinea=0;
            ImGui::SameLine();
            switch(Singleton::getInstance()->draw_line_state)
            {
                case 0:ImGui::Text("ACTIVO");break;
                case 1:ImGui::Text("ACTIVO");break;
            }
        }
        if (ImGui::Button("Multilinea")) {Singleton::getInstance()->btnS_poner_cortafuego_multilinea =!Singleton::getInstance()->btnS_poner_cortafuego_multilinea; Singleton::getInstance()->draw_line_state=0;/*printf("CLK\n");*/linea_temp=0;}
        if (Singleton::getInstance()->btnS_poner_cortafuego_multilinea)
        {
            Singleton::getInstance()->btnS_poner_cortafuego=0;
            ImGui::SameLine();
            switch(Singleton::getInstance()->draw_line_state)
            {
                case 0:ImGui::Text("ACTIVO");break;
                case 1:ImGui::Text("DIBUJO");break;
            }
        }
        

        if(ImGui::Button("Descargar todo"))
        {
            Singleton::getInstance()->CORTAFUEGOS.borrar_todo();
            Singleton::getInstance()->CORTAFUEGOS.selected=-1;
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
        }

        ImGui::PushItemWidth(350);

        if (ImGui::TreeNode("Lista:"))
        {
            static int selected=-1;
            int selected_prev;
            static bool anyhovered=0;
            bool anyhovered_prev=0;

            char stri[32];
            char stria[32];

            anyhovered_prev=anyhovered;
            selected_prev=selected;
            bool anyho=0;

            for(int i=0;i<Singleton::getInstance()->CORTAFUEGOS.cant_corf;i++)
            {
                sprintf(stria,"##%d",i);
                bool prev=Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].visible;
                ImGui::Checkbox(stria, &Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].visible); ImGui::SameLine();
                if(prev!=Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].visible){Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;}
                sprintf(stri,"%s##%d",Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].nombre,i);
                bool selectedax=0;
                ImGui::Selectable(stri, &selectedax);
                if(ImGui::IsItemHovered())
                {
                    selected=i;
                    anyho=1;
                }
                    //if (ImGui::IsMouseDoubleClicked(0))listbox_item_current=i;
                if (ImGui::BeginPopupContextItem(stria))
                {
                    print_debug("Q\n");
                    ImGui::InputText("Name", Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].nombre, 32);
                    ImGui::Text("Edit position");

                    //ImGui::ColorEdit3("##edit", (float*)&color);
                    ImGui::InputInt("x0", &Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].LINEA[0].x0);
                    ImGui::InputInt("y0", &Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].LINEA[0].y0);
                    ImGui::InputInt("x1", &Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].LINEA[0].x1);
                    ImGui::InputInt("y1", &Singleton::getInstance()->CORTAFUEGOS.ENTRADA[i].LINEA[0].y1);
                    //printf("ci=%d\n",listbox_item_current);
                    if (ImGui::Button("Borrar"))
                    {
                        Singleton::getInstance()->CORTAFUEGOS.borrar_entrada(i);
                        Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
                        ImGui::CloseCurrentPopup();
                    }
                    ImGui::EndPopup();
                }

            }
            ImGui::TreePop();
            anyhovered=anyho;
            if(!anyhovered){selected=-1;}
            if(anyhovered!=anyhovered_prev || selected!=selected_prev)
            {
                Singleton::getInstance()->CORTAFUEGOS.selected=selected;
                Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
            }
        }
        if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
    }

    static bool vertodos_pigni=false;
    static int selected_ip=-1;
    static bool anyhovered_ip=0;
    static int selected_ip_3d=-1; 
    Singleton::getInstance()->SIMUSCRIPT.selected_ip=-1;
    if (ImGui::CollapsingHeader("Ptos Ignicion:") && Singleton::getInstance()->proyecto_cargado)
    {
        if (ImGui::Button("Nuevo P IGNI")) {Singleton::getInstance()->btnS_IGNI =!Singleton::getInstance()->btnS_IGNI;}
        if (Singleton::getInstance()->btnS_IGNI)
        {
            ImGui::SameLine();
            ImGui::Text("ACTIVO");
        }
        if(Singleton::getInstance()->btnS_IGNI_redef>-1)
        {
            ImGui::SameLine();
            ImGui::Text("ACT REDEF");
        }

        if(ImGui::Button("Recargar"))
        {
            Singleton::getInstance()->SIMUSCRIPT.delete_all();
            Singleton::getInstance()->SIMUSCRIPT.load_ignitionpoint_file((char*)"puntos_ignicion.txt");
            //simuscript recargar
            
        }
        ImGui::SameLine();

        if(ImGui::Button("Guardar"))
        {
            Singleton::getInstance()->SIMUSCRIPT.save_ignitionpoint_file((char*)"puntos_ignicion.txt");
            //simuscript guardar cambios
        }

        ImGui::SameLine(); ImGui::Checkbox("Ver todos", &vertodos_pigni); 

        ImGui::PushItemWidth(350);

        if (ImGui::TreeNode("Lista:##02"))
        {

            int selected_prev;
            
            bool anyhovered_prev=0;

            char stri[32];
            char stria[32];

            anyhovered_prev=anyhovered_ip;
            selected_prev=selected_ip;
            bool anyho=0;

            for(int i=0;i<Singleton::getInstance()->SIMUSCRIPT.cant_ignitionpoint;i++)
            {
                sprintf(stria,"##%d",i);
                bool prev=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].enabled;
                ImGui::Checkbox(stria, &Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].enabled); ImGui::SameLine();
                if(prev!=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].enabled){Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;}
                sprintf(stri,"%02d - %03d (%03d,%03d)##%d",i,Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].step,Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].x,Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].y,i);
                bool selectedax=0;
                if(selected_ip_3d==i)selectedax=1;
                ImGui::Selectable(stri, &selectedax);
                if(ImGui::IsItemHovered() || selected_ip_3d==i)
                {
                    selected_ip=i;
                    anyho=1;
                    Singleton::getInstance()->SIMUSCRIPT.selected_ip=i;
                }
                    //if (ImGui::IsMouseDoubleClicked(0))listbox_item_current=i;
                ImGui::PushItemWidth(150);
                if (ImGui::BeginPopupContextItem(stria))
                {
                    ImGui::PushItemWidth(150);
                    ImGui::Text("Edit");

                    //ImGui::ColorEdit3("##edit", (float*)&color);
                    ImGui::InputInt("x", &Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].x);
                    ImGui::InputInt("y", &Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].y);
                    ImGui::InputInt("step", &Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].step);
                    //printf("ci=%d\n",listbox_item_current);
                    if (ImGui::Button("Borrar"))
                    {
                        //simuscript borrar entrada
                        Singleton::getInstance()->SIMUSCRIPT.delete_ignitionpoint(i);
                        ImGui::CloseCurrentPopup();
                    }
                    ImGui::SameLine();
                    if (ImGui::Button("Redef POS"))
                    {
                        Singleton::getInstance()->btnS_IGNI_redef=i;
                        //simuscript redefinir solo X Y de la entrada
                        ImGui::CloseCurrentPopup();
                    }
                    ImGui::EndPopup();
                }

            }
            ImGui::TreePop();
            anyhovered_ip=anyho;
            if(!anyhovered_ip){selected_ip=-1;}
            if(anyhovered_ip!=anyhovered_prev || selected_ip!=selected_prev)
            {
                //redibujar marcando con color el actual
            }
        }
        if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
    }
    //ImGui::End();




    /*ImGui::SetNextWindowSize(ImVec2(400,200), ImGuiSetCond_Always);
    ImGui::SetNextWindowPos(ImVec2(window_width-400,800),ImGuiSetCond_Always);
    ImGui::Begin("Camara:", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);*/

    /*if (ImGui::CollapsingHeader("Camara:")||first_time)
    {
        if (ImGui::Button("Reset")) {
            reset_camera();
        }
    }*/

    if ((ImGui::CollapsingHeader("Capas:")||first_time)&&Singleton::getInstance()->proyecto_cargado)
    {
        float old_selcorf_a=Singleton::getInstance()->selcorf.map_a;
        float old_quemado_a=Singleton::getInstance()->quemado.map_a;
        float old_vegetacion_a=Singleton::getInstance()->vegetacion.map_a;
        float old_mapaaux_a=Singleton::getInstance()->mapaaux.map_a;
        float old_levelmap_a=Singleton::getInstance()->levelmap.map_a;
        float old_aspectmap_a=Singleton::getInstance()->aspectmap.map_a;
        float old_altimap_a=Singleton::getInstance()->altimap.map_a;
        float old_peligmap_a=Singleton::getInstance()->peligmap.map_a;
        
        //bool enabled_capa;
        ImGui::BeginChild("child2", ImVec2(0, 360), true);
        char str[24];
        static int move_item=-1;static int move_to=-1;
        static int last_frame_dragstate;


        static bool wind_enabled=1;
        ImGui::Checkbox("##WINDCH", &wind_enabled);//Esto lo creo aca afuera para que no se le pueda hacer drag and drop
        //porque como el viento se procesa de manera diferente en el shader, siempre estara por encima de todo
    
        ImGui::SameLine();
        ImGui::PushItemWidth(100);
        static float wind_a_aux=1.0f;
        
        ImGui::SliderFloat("Viento Flechas", &wind_a_aux, 0.0f, 1.0f, "%.2f");
        if(wind_enabled){Singleton::getInstance()->wind_a=wind_a_aux;}else{Singleton::getInstance()->wind_a=0.0f;}


        for(int i=0;i<MAX_NUM_LAYERS;i++)
        {
            if(!Singleton::getInstance()->layer_list[i].show_on_panel)break;
            sprintf(str,"##CH%d",i);
            bool oldvalch=Singleton::getInstance()->layer_list[i].enabled;
            ImGui::Checkbox(str, &(Singleton::getInstance()->layer_list[i].enabled));
            if(oldvalch!=Singleton::getInstance()->layer_list[i].enabled){Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;}
            ImGui::SameLine();
            ImGui::PushItemWidth(100);
            //ImGui::SliderFloat(layer_list[i].name, layer_list[i].alpha, 0.0f, 1.0f, "%.2f");
            sprintf(str,"##SLA%d",i);
            float alpha_tmp=*(Singleton::getInstance()->layer_list[i].alpha);
            ImGui::SliderFloat(str, &alpha_tmp, 0.0f, 1.0f, "%.2f");
            if(Singleton::getInstance()->layer_list[i].SetAlpha(alpha_tmp)){Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;}

            if (((RasterMap*)Singleton::getInstance()->layer_list[i].ptr_RasterMap)->has_tooltip && ImGui::IsItemHovered())
            {ImGui::SetTooltip(((RasterMap*)Singleton::getInstance()->layer_list[i].ptr_RasterMap)->tooltiptext);}

            ImGui::SameLine();
            ImGui::Button(Singleton::getInstance()->layer_list[i].name);
            ImVec2 act_slider = ImGui::GetCursorScreenPos();
            if (((RasterMap*)Singleton::getInstance()->layer_list[i].ptr_RasterMap)->has_tooltip && ImGui::IsItemHovered())
            {ImGui::SetTooltip(((RasterMap*)Singleton::getInstance()->layer_list[i].ptr_RasterMap)->tooltiptext);}
            if (ImGui::IsItemActive())
            {
                ImDrawList* draw_list = ImGui::GetWindowDrawList();
                draw_list->PushClipRectFullScreen();
                draw_list->AddLine(ImGui::CalcItemRectClosestPoint(ImGui::GetIO().MousePos, true, -2.0f), ImGui::GetIO().MousePos, ImColor(ImGui::GetStyle().Colors[ImGuiCol_Button]), 4.0f);
                ImVec2 droppos;
                droppos=ImGui::GetIO().MousePos;

                //usar este sistema para detectar donde se lo larga, al levantar boton mouse

                int lv=-i-1;int lock=0;

                while(!lock){
                    if(abs(droppos.y-(act_slider.y+lv*36.0f))<10){droppos.y=(act_slider.y+lv*36.0f);lock=1;break;}
                    lv++;
                    if(lv>=MAX_NUM_LAYERS+1-i)break;
                }
                int subir_o_bajar;
                if(lv<=0){subir_o_bajar=lv+1;}else{subir_o_bajar=lv+1;}
                if(lv==0||lv==-1){subir_o_bajar=0;}
                last_frame_dragstate=1;
                if(subir_o_bajar==0){lock=0;}

                if(lock)
                {
                    move_item=i;
                    move_to=i+subir_o_bajar;
                }
                if(subir_o_bajar==0 || lock==0){last_frame_dragstate=0;}


                //HECHO Pero como sabemos la posicion de los elementos que todavia no dibujamos???
                //HECHO Quiza sea mejor obtener la posicion del elemento actual, y sabiendo la distancia entre ellos calculas todas por intervalos.
                

                draw_list->AddLine(droppos+ImVec2(150.0f,0.0f), droppos, ImColor(ImGui::GetStyle().Colors[ImGuiCol_Button]), 4.0f);
                draw_list->PopClipRect();

                


                /*ImVec2 value_raw = ImGui::GetMouseDragDelta(0, 0.0f);
                ImVec2 value_with_lock_threshold = ImGui::GetMouseDragDelta(0);
                ImVec2 mouse_delta = ImGui::GetIO().MouseDelta;*/
                //ImGui::SameLine(); ImGui::Text("Raw (%.1f, %.1f), WithLockThresold (%.1f, %.1f), MouseDelta (%.1f, %.1f)", value_raw.x, value_raw.y, value_with_lock_threshold.x, value_with_lock_threshold.y, mouse_delta.x, mouse_delta.y);
            }
        }


        





        if(ImGui::IsMouseReleased(0) && last_frame_dragstate==1 && move_to<MAX_NUM_LAYERS)// ¿Hay que hacer drag and drop de algun elemento?
        {
            last_frame_dragstate=0;
            print_debug("RELEASED DRAG AND DROP %d > %d \n",move_item,move_to);
            LAYER auxI=Singleton::getInstance()->layer_list[move_item];
            Singleton::getInstance()->layer_list[move_item].show_on_panel=0;
            int i=MAX_NUM_LAYERS-2;
            while(true)
            {
                Singleton::getInstance()->layer_list[i+1]=Singleton::getInstance()->layer_list[i];
                if(i<=move_to)break;
                i--;
            }
            Singleton::getInstance()->layer_list[move_to]=auxI;
            eliminar_ceros_layer_list();
            Singleton::getInstance()->CORTAFUEGOS.algun_cambio=1;
        }
        

        ImGui::EndChild();
            //ImGui::TreePop();
        //}

        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));

        ImGui::PopStyleVar();

        

        
        /*if(ImGui::Button("Cargar mapa fuego real.."))
        {

            //CORTAFUEGOS.guardar_archivo("./AAAprueba.cfu");
            ImGui::OpenPopup("Cargar mapa fuego real...");
            CORTAFUEGOS.recalcular_filelist(PROJECT_FOLDER_P,"","asc");
            CORTAFUEGOS.show_save=0;
            CORTAFUEGOS.show_open=0;
            CORTAFUEGOS.show_map_open=1;
        }
        CORTAFUEGOS.show_map_openf();*/
    }


    ImGui::End();


    if(Singleton::getInstance()->CORTAFUEGOS.show_map_open || Singleton::getInstance()->CORTAFUEGOS.show_open || Singleton::getInstance()->CORTAFUEGOS.show_save || show_fwilm_ref_window || show_load_project_window || show_new_project_window)
    {
        Singleton::getInstance()->CORTAFUEGOS.show_prob_window=0;
    }

    

    if(Singleton::getInstance()->CORTAFUEGOS.show_prob_window && !ImGui::IsMouseHoveringAnyWindow() && Singleton::getInstance()->proyecto_cargado)
    {
        ImGui::SetNextWindowSize(ImVec2(300,280), ImGuiSetCond_Always);
        ImGui::SetNextWindowPos(ImVec2(Singleton::getInstance()->mouse_x+10,Singleton::getInstance()->mouse_y+10), ImGuiSetCond_Always);
        if(!ImGui::Begin("Info inc:", &Singleton::getInstance()->CORTAFUEGOS.show_prob_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoScrollbar))
        {
            ImGui::End();
            //printf("N\n");
        }
        else
        {
            int nij=Singleton::getInstance()->CORTAFUEGOS.ni+Singleton::getInstance()->CORTAFUEGOS.nj*Singleton::getInstance()->vert_gis_x;
            int fuel=Singleton::getInstance()->vegetacion.h_map[nij];
            if(fuel<0 || fuel>99)fuel=0;
            sprintf(auxchar,"(%03d ; %03d)",ni,nj);
            ImGui::Text(auxchar);
            sprintf(auxchar,"Quemado: %.00f",(float)Singleton::getInstance()->quemado.h_map[nij]*10.0f);
            ImGui::Text(auxchar);
            sprintf(auxchar,"SIM: %02d   MAP: %02d",Singleton::getInstance()->vegetacionSIM.h_map[nij],Singleton::getInstance()->vegetacion.h_map[nij]);
            ImGui::Text(auxchar);
            sprintf(auxchar,"%s",Singleton::getInstance()->proyectow.fuelnames[fuel]);
            ImGui::Text(auxchar);
            sprintf(auxchar,"ALTITUD: %.02f",Singleton::getInstance()->h_mapaalti[nij]);
            ImGui::Text(auxchar);
            sprintf(auxchar,"ASPECT: %.02f",Singleton::getInstance()->h_mapaaspect[nij]);
            ImGui::Text(auxchar);
            sprintf(auxchar,"SLOPE: %.02f",Singleton::getInstance()->h_mapaslope[nij]);
            ImGui::Text(auxchar);
            sprintf(auxchar,"WIND: %.02f < %.02f",Singleton::getInstance()->h_mapawindINT[nij],Singleton::getInstance()->h_mapawindANG[nij]);
            ImGui::Text(auxchar);
            if(Singleton::getInstance()->btnS_IGNI_redef>-1){
                sprintf(auxchar,"CLICK PARA REDEFINIR EL PUNTO DE IGNICION");
                ImGui::Text(auxchar);
            }

            ImGui::End();
            /*windpow_at_mouse=h_mapawindINT[nij];
            height_at_mouse=h_mapaalti[nij];
            aspect_at_mouse=h_mapaaspect[nij];
            slope_at_mouse=h_mapaslope[nij];*/

            windpow_at_mouse=promediar_nuevo_valor_mouse(windpowP,Singleton::getInstance()->h_mapawindINT[nij]);
            height_at_mouse=promediar_nuevo_valor_mouse(heightP,Singleton::getInstance()->h_mapaalti[nij]);
            aspect_at_mouse=promediar_nuevo_valor_mouse(aspectP,Singleton::getInstance()->h_mapaaspect[nij]);
            slope_at_mouse=promediar_nuevo_valor_mouse(slopeP,Singleton::getInstance()->h_mapaslope[nij]);
            ptr_mouse++;
            if(ptr_mouse>=PROMEDIO_MOUSE)ptr_mouse=0;
            //printf("S\n");
        }   
    }
    selected_ip_3d=-1;
    if(Singleton::getInstance()->proyecto_cargado){
        for(int i=0;i<Singleton::getInstance()->SIMUSCRIPT.cant_ignitionpoint;i++){
            if(!vertodos_pigni&&i!=selected_ip)continue;
            int pi=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].x;
            int pj=Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].y;
            vec4 vert_pos_W,vert_pos_S;
            vert_pos_W=vec4(0.0125f*pi,(Singleton::getInstance()->h_mapaalti[pi+pj*Singleton::getInstance()->vert_gis_x]-Singleton::getInstance()->altitud_min)*0.0125f/30.0f,0.0125f*pj,1.0f);
            vert_pos_S=Singleton::getInstance()->g_fullTransfWTP*vert_pos_W;
            vert_pos_S=vert_pos_S/vert_pos_S.z;
            if(vert_pos_S.x>1.0f || vert_pos_S.x<-1.0f) continue;
            if(vert_pos_S.y>1.0f || vert_pos_S.y<-1.0f) continue;
            float Sx,Sy;
            Sx=(vert_pos_S.x+1.0f)*0.5f*Singleton::getInstance()->window_width;
            Sy=(-vert_pos_S.y+1.0f)*0.5f*Singleton::getInstance()->window_height;
            int sx=(int)Sx;
            int sy=(int)Sy;

            ImGui::SetNextWindowSize(ImVec2(260,40), ImGuiSetCond_Always);
            ImGui::SetNextWindowPos(ImVec2(sx+5,sy+5), ImGuiSetCond_Always);
            bool show_igni_window=true;
            char stria[32];
            sprintf(stria,"Info igni:##%d",i);
            if(!ImGui::Begin(stria, &show_igni_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoCollapse|ImGuiWindowFlags_NoScrollbar))
            {
                ImGui::End();
                //printf("N\n");
            }
            else
            {   
                sprintf(auxchar,"P:%d (%03d ; %03d)",Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].step,pi,pj);
                ImVec4 color_ip=ImVec4(0.7,0.7,0.7,1);
                if(selected_ip==i){
                    color_ip=ImVec4(1,1,1,1);
                }

                if(!Singleton::getInstance()->SIMUSCRIPT.ignitionpoint[i].enabled)
                {
                    color_ip.x=0.5f*color_ip.x;
                    color_ip.y=0.5f*color_ip.y;
                    color_ip.z=0.5f*color_ip.z;
                }

                ImGui::TextColored(color_ip, auxchar);
                if (ImGui::IsItemHovered()){
                    selected_ip_3d=i;
                }
                 
                ImGui::End();
            } 

            //printf("%f %f %f\n",vert_pos_S.x,vert_pos_S.y,vert_pos_S.z);
            //printf("%d %d\n",sx,sy);
        }
    }

    if(Singleton::getInstance()->CORTAFUEGOS.show_map_open){Singleton::getInstance()->CORTAFUEGOS.show_map_openf();}
    if(show_fwilm_ref_window)
    {
        Singleton::getInstance()->FWI_T.show_load_mfile(&show_fwilm_ref_window);
    }

    if(show_load_project_window)
    {
        LoadProjectWindow();
    }

    if(show_new_project_window)
    {
        
    }

    if(ImGui::IsAnyItemActive()){imguicontrol2=1;}

    if(imguicontrol2==1)
    {
        imguicontrol=1;
    }
    else
    {
        if(imguicontrol==1)
        {
            if(!ImGui::IsMouseHoveringAnyWindow())
            {
                int click=0;
                ImGuiIO& io = ImGui::GetIO();
                for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++) {
                    if (ImGui::IsMouseClicked(i))click=1;
                }
                if(click)imguicontrol=0;
            }
        }
        else
        {
            if(ImGui::IsMouseHoveringAnyWindow())
            {
                int click=0;
                ImGuiIO& io = ImGui::GetIO();
                for (int i = 0; i < IM_ARRAYSIZE(io.MouseDown); i++) {
                    if (ImGui::IsMouseClicked(i))click=1;
                }
                if(click)imguicontrol=1;
            }
        }
    }

    

    //ImGui::Text("Hello");
    //ImGui::ShowTestWindow();
    ImGui::Render();

}

void LoadProjectRecalcularFilelist(void){
    print_debug("Recalcula LoadProjectRecalcularFilelist\n");
    for(int i=0;i<Singleton::gI()->projectfilelist.listbox_file_cant;i++)
    {
        free(Singleton::gI()->projectfilelist.listbox_file_list[i]);
        Singleton::gI()->projectfilelist.listbox_file_list[i]=NULL;
    }
    print_debug("1.0\n");
    if(Singleton::gI()->projectfilelist.listbox_file_list!=NULL)free(Singleton::gI()->projectfilelist.listbox_file_list);
    Singleton::gI()->projectfilelist.listbox_file_list=NULL;
    Singleton::gI()->projectfilelist.listbox_file_cant=0;
    //AGREGAR SUBDIRECTORIO CORTAFUEGOS****************************************************************
    DIR *d;
    struct dirent *dir;
    char str[256],str2[256];
    sprintf(str,"./");
    d=opendir(str);

    print_debug("2.0\n");

    if(d)
    { 
        while((dir=readdir(d))!=NULL){
            bool is_dir;
            struct stat stbuf;
            // stat follows symlinks, lstat doesn't.
            stat(dir->d_name, &stbuf);              // TODO: error check
            is_dir = S_ISDIR(stbuf.st_mode);
            //if(!strcmp(get_filename_ext(dir->d_name),exten)){
            if(is_dir)
            {
                sprintf(str2,"%s/config.txt",dir->d_name);
                FILE *fhnd = fopen(str2, "r");
                if ( fhnd != NULL ) {
                    fclose(fhnd);
                    Singleton::gI()->projectfilelist.listbox_file_list=(char**)realloc(Singleton::gI()->projectfilelist.listbox_file_list,(1+Singleton::gI()->projectfilelist.listbox_file_cant)*sizeof(char*));
                    Singleton::gI()->projectfilelist.listbox_file_list[Singleton::gI()->projectfilelist.listbox_file_cant]=(char*)malloc(32);
                    memcpy(Singleton::gI()->projectfilelist.listbox_file_list[Singleton::gI()->projectfilelist.listbox_file_cant],dir->d_name,1+strlen(dir->d_name));
                    Singleton::gI()->projectfilelist.listbox_file_cant++; 
                }
                
                
            }

        }
        closedir(d);

    }

    print_debug("Recalcula filelist OK\n");
}

void LoadProjectWindow(void)
{
    ImGui::SetNextWindowSize(ImVec2(500,600), ImGuiSetCond_Always);

    
    if (ImGui::BeginPopupModal("Load Project..."))
    {
        
        //ImGui::SetNextWindowPos(ImVec2(window_width-300,200),ImGuiSetCond_Always);
        //ImGui::Begin("Cargar mapa fuego real...", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
        
        //char *listbox_file_list[32];

        static int listbox_item_current = 1;
        int old_listbox_item_current = listbox_item_current;
        //printf("CANT: %d\n",Singleton::gI()->projectfilelist.listbox_file_cant);

        ImGui::ListBox("selecciona \nproyecto \npara \nabrir", &listbox_item_current, Singleton::gI()->projectfilelist.listbox_file_list, Singleton::gI()->projectfilelist.listbox_file_cant, 15);

        static char myfilename[64];
        if(listbox_item_current!=old_listbox_item_current || show_load_project_window_1erinicio)
        {
            Singleton::gI()->projectfilelist.pers_filename=0;
            show_load_project_window_1erinicio=0;
            memcpy(myfilename,Singleton::gI()->projectfilelist.listbox_file_list[listbox_item_current],1+strlen(Singleton::gI()->projectfilelist.listbox_file_list[listbox_item_current]));
        }
        ImGui::Text("Nombre:");
        ImGui::SameLine();

        if(ImGui::InputText("", myfilename, 32)){
            Singleton::gI()->projectfilelist.pers_filename=1;

        }
                

        if (ImGui::Button("Abrir")) {

            char str[128];

            sprintf(str,"%s/",myfilename);
            memcpy(Singleton::getInstance()->PROJECT_FOLDER_P,str,1+strlen(str));

            print_debug("Projsel %s \n",str);
            print_debug("Projsel2 %s \n",Singleton::getInstance()->PROJECT_FOLDER_P);

            Cargar_proyecto();
            //cargar_archivo(str);
            ImGui::CloseCurrentPopup();
            show_load_project_window=0;
        }
        ImGui::SameLine();
        if (ImGui::Button("Cancelar")) {
            ImGui::CloseCurrentPopup();
            show_load_project_window=0;
        }

        if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
        ImGui::EndPopup();
    }

}

int make_directory(const char* name)
{
#ifdef LINUX
    return mkdir(name, S_IRWXU | S_IRWXG | S_IRWXO); /* Or what parameter you need here ... */
#else
    return _mkdir(name);
#endif
}

#include <fstream>

void copy_file(const char* dest_file ,const char* srce_file)
{
    std::ifstream srce( srce_file, std::ios::binary ) ;
    std::ofstream dest( dest_file, std::ios::binary ) ;
    dest << srce.rdbuf() ;
}

int* GetMapi(const char * nombre, int filas, int cols,int *nodata);

void NewProjectWindow(void)
{
    ImVec2 ventana_size=ImVec2(1200,700);
    ImGui::SetNextWindowPos((ImVec2(Singleton::getInstance()->window_width,Singleton::getInstance()->window_height)-ventana_size)/2.0f,ImGuiSetCond_Always);
    ImGui::SetNextWindowSize(ventana_size, ImGuiSetCond_Always);

    static char project_name[128]="NUEVO_PROYECTO";
    static int filesel_pointer=0;
    static char project_ELEVACION[1024]=".";
    static char project_VEGETACION[1024]=".";
    static char project_WINDINT[1024]=".";
    static char project_WINDANG[1024]=".";
    static char project_SLOPE[1024]=".";
    static char project_ASPECT[1024]=".";
    static char project_METEOR[1024]=".";

    static char projectfn_ELEVACION[256]=".";
    static char projectfn_VEGETACION[256]=".";
    static char projectfn_WINDINT[256]=".";
    static char projectfn_WINDANG[256]=".";
    static char projectfn_SLOPE[256]=".";
    static char projectfn_ASPECT[256]=".";
    static char projectfn_METEOR[256]=".";

    //static bool show_mapeo_combustibles=false;

    static bool p_open=1;

    if(!ImGui::Begin("New Project...", &p_open,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize))
    {
        ImGui::End();
        return;
    }
    
    ImGui::TextWrapped("Un proyecto para el simulador se compone de un directorio de la siguiente estructura:");
    ImGui::Spacing();

    
    //ShowHelpMarker("Directorio raiz del proyecto");
    ImGui::SameLine(); 

    ImGui::SameLine();

    /*if (ImGui::TreeNode("Context menus"))
        {
            static float value = 0.5f;
            ImGui::Text("Value = %.3f (<-- right-click here)", value);
            if (ImGui::BeginPopupContextItem("item context menu"))
            {
                if (ImGui::Selectable("Set to zero")) value = 0.0f;
                if (ImGui::Selectable("Set to PI")) value = 3.1415f;
                ImGui::EndPopup();
            }

            static ImVec4 color = ImColor(0.8f, 0.5f, 1.0f, 1.0f);
            ImGui::ColorButton(color);
            if (ImGui::BeginPopupContextItem("color context menu"))
            {
                ImGui::Text("Edit color");
                ImGui::ColorEdit3("##edit", (float*)&color);
                if (ImGui::Button("Close"))
                    ImGui::CloseCurrentPopup();
                ImGui::EndPopup();
            }
            ImGui::SameLine(); ImGui::Text("(<-- right-click here)");

            ImGui::TreePop();
        }*/

    /*static float value = 0.5f;
    ImGui::Text("Value = %.3f (<-- right-click here)", value);
    if (ImGui::BeginPopupContextItem("item context menu"))
    {
        if (ImGui::Selectable("Set to zero")) value = 0.0f;
        if (ImGui::Selectable("Set to PI")) value = 3.1415f;
        ImGui::EndPopup();
    }*/

    
    
    ImGui::SameLine();
    ShowHelpMarker("Nombre del proyecto, click aquí con la ruedita del mouse para editar");
    if (ImGui::BeginPopupContextItem("EDITAR NOMBRE"))
    {
        // your popup code
        ImGui::InputText("", project_name, 64);
        ImGui::EndPopup();
    }
    ImGui::SameLine();

    bool opened = ImGui::TreeNodeEx(project_name,ImGuiTreeNodeFlags_DefaultOpen);
    
    
    
    
    if( opened )
    {
        
        static int selection_mask = (1 << 2); // Dumb representation of what may be user-side selection state. You may carry selection state inside or outside your objects in whatever format you see fit.
        int node_clicked = -1;                // Temporary storage of what node we have clicked to process selection at the end of the loop. May be a pointer to your own node type, etc.
        ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImGui::GetFontSize()*6); // Increase spacing to differentiate leaves from expanded contents.
        /*for (int i = 0; i < 6; i++)
        {
            // Disable the default open on single-click behavior and pass in Selected flag according to our selection state.
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << i)) ? ImGuiTreeNodeFlags_Selected : 0);
            if (i < 3)
            {
                // Node
                bool node_open = ImGui::TreeNodeEx((void*)(intptr_t)i, node_flags, "Selectable Node %d", i);
                if (ImGui::IsItemClicked()) 
                    node_clicked = i;
                if (node_open)
                {
                    ImGui::Text("Blah blah\nBlah Blah");
                    ImGui::TreePop();
                }
            }
            else
            {
                // Leaf: The only reason we have a TreeNode at all is to allow selection of the leaf. Otherwise we can use BulletText() or TreeAdvanceToLabelPos()+Text().
                ImGui::TreeNodeEx((void*)(intptr_t)i, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "Selectable Leaf %d", i);
                if (ImGui::IsItemClicked()) 
                    node_clicked = i;
            }
        }*/

        {
            ShowHelpMarker("Directorio donde se guardan los cortafuegos definidos por el usuario");
            ImGui::SameLine(); 
            ImGuiTreeNodeFlags node_flags = /*ImGuiTreeNodeFlags_OpenOnArrow |*/ ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << 0)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)0, node_flags /*| ImGuiTreeNodeFlags_Leaf */| ImGuiTreeNodeFlags_NoTreePushOnOpen, "cortafuego");
            if (ImGui::IsItemClicked()) 
                node_clicked = 0;
        }
        {
            ShowHelpMarker("Archivo de configuracion del proyecto, puede ser modificado por el usuario con un editor de texto");
            ImGui::SameLine(); 
            ImGuiTreeNodeFlags node_flags = /*ImGuiTreeNodeFlags_OpenOnArrow |*/ ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << 1)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)1, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "config.txt");
            if (ImGui::IsItemClicked()) 
                node_clicked = 1;
        }
        {
            ShowHelpMarker("Mapa raster con los valores de elevación.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << 2)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)2, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_ELEVACION");
            if (ImGui::IsItemClicked()) 
                node_clicked = 2;
            ImGui::SameLine();
            ImGui::PushID(2);
            if (ImGui::Button(project_ELEVACION)) {
                filesel_pointer=1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_ELEVACION);
            }
            ImGui::PopID();
        }
        {
            ShowHelpMarker("Mapa raster con los valores de vegatacion.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << 3)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)3, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_VEGETACION");
            if (ImGui::IsItemClicked()) 
                node_clicked = 3;
            ImGui::SameLine();
            ImGui::PushID(3);
            if (ImGui::Button(project_VEGETACION)) {
                filesel_pointer=2;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_VEGETACION);
            }
            ImGui::PopID();
        }
        {
            int ci=4;
            ShowHelpMarker("Mapa raster de la intensidad de viento.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << ci)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)ci, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_WINDINT");
            if (ImGui::IsItemClicked()) 
                node_clicked = ci;
            ImGui::SameLine();
            ImGui::PushID(ci);
            if (ImGui::Button(project_WINDINT)) {
                filesel_pointer=ci-1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_WINDINT);
            }
            ImGui::PopID();
        }
        {
            int ci=5;
            ShowHelpMarker("Mapa raster del ángulo del viento.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << ci)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)ci, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_WINDANG");
            if (ImGui::IsItemClicked()) 
                node_clicked = ci;
            ImGui::SameLine();
            ImGui::PushID(ci);
            if (ImGui::Button(project_WINDANG)) {
                filesel_pointer=ci-1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_WINDANG);
            }
            ImGui::PopID();
        }
        {
            int ci=6;
            ShowHelpMarker("Mapa raster de la pendiente.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << ci)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)ci, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_SLOPE");
            if (ImGui::IsItemClicked()) 
                node_clicked = ci;
            ImGui::SameLine();
            ImGui::PushID(ci);
            if (ImGui::Button(project_SLOPE)) {
                filesel_pointer=ci-1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_SLOPE);
            }
            ImGui::PopID();
        }
        {
            int ci=7;
            ShowHelpMarker("Mapa raster del aspect.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << ci)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)ci, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "MAPA_ASPECT");
            if (ImGui::IsItemClicked()) 
                node_clicked = ci;
            ImGui::SameLine();
            ImGui::PushID(ci);
            if (ImGui::Button(project_ASPECT)) {
                filesel_pointer=ci-1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_ASPECT);
            }
            ImGui::PopID();
        }
        {
            int ci=8;
            ShowHelpMarker("Datos meteorologicos día a día.");
            ImGui::SameLine();
            ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ((selection_mask & (1 << ci)) ? ImGuiTreeNodeFlags_Selected : 0);
            ImGui::TreeNodeEx((void*)(intptr_t)ci, node_flags | ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen, "DATOS_METEOROLOGICOS");
            if (ImGui::IsItemClicked()) 
                node_clicked = ci;
            ImGui::SameLine();
            ImGui::PushID(ci);
            if (ImGui::Button(project_METEOR)) {
                filesel_pointer=ci-1;
                ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", project_METEOR);
            }
            ImGui::PopID();
        }

        if (node_clicked != -1)
        {
            // Update selection state. Process outside of tree loop to avoid visual inconsistencies during the clicking-frame.
            if (ImGui::GetIO().KeyCtrl)
                selection_mask ^= (1 << node_clicked);          // CTRL+click to toggle
            else //if (!(selection_mask & (1 << node_clicked))) // Depending on selection behavior you want, this commented bit preserve selection when clicking on item that is part of the selection
                selection_mask = (1 << node_clicked);           // Click to single-select
        }
        ImGui::PopStyleVar();
        /*if (align_label_with_current_x_position)
            ImGui::Indent(ImGui::GetTreeNodeToLabelSpacing());*/
        ImGui::TreePop();
    }

    ImGui::Text("Índices iniciales para el archivo meteorológico:");
    static float FFMC=21.2f,DMC=0.1f,DC=0.6f;
    ImGui::InputFloat("FFMC",&FFMC,21.2f);//21.2    0.1 0.6
    ImGui::InputFloat("DMC",&DMC,0.1f);//21.2    0.1 0.6
    ImGui::InputFloat("DC",&DC,0.6f);//21.2    0.1 0.6

    static char errorstr[2048];        

    if (ImGui::Button("Siguiente")) {

        /*char str[128];

        sprintf(str,"%s/",myfilename);
        memcpy(Singleton::getInstance()->PROJECT_FOLDER_P,str,1+strlen(str));

        printf("Projsel %s \n",str);
        printf("Projsel2 %s \n",Singleton::getInstance()->PROJECT_FOLDER_P);

        Cargar_proyecto();*/
        //cargar_archivo(str);

        //float* GetMapwz(const char * nombre, int & filas, int & cols,int & nodata,bool & error)
        bool error=0;
        int nodata;
        int filas[6],cols[6];
        
        sprintf(errorstr," ");
        float* map;

        map = GetMapwz(project_ELEVACION, filas[0], cols[0],nodata, error,errorstr);
        if(map!=NULL)free(map);

        map= GetMapwz(project_VEGETACION, filas[1], cols[1],nodata, error,errorstr);
        if(map!=NULL)free(map);

        map= GetMapwz(project_WINDINT, filas[2], cols[2],nodata, error,errorstr);
        if(map!=NULL)free(map);

        map= GetMapwz(project_WINDANG, filas[3], cols[3],nodata, error,errorstr);
        if(map!=NULL)free(map);

        map= GetMapwz(project_SLOPE, filas[4], cols[4],nodata, error,errorstr);
        if(map!=NULL)free(map);

        map= GetMapwz(project_ASPECT, filas[5], cols[5],nodata, error,errorstr);
        if(map!=NULL)free(map);

        if(!error)
        {
            for(int i=0;i<6;i++)
            {
                if(filas[0]!=filas[i] || cols[0]!=cols[i])
                {
                    error=1;
                    char auxestr[2048];

                    memcpy(auxestr,errorstr,2048);
                    sprintf(errorstr,"%s\nEl número de filas y columnas no se corresponden entre archivos f0=%d c0=%d  f%d=%d c%d=%d. \n",auxestr, filas[0],cols[0],i,filas[i],i,cols[i]);
                }
            }
        }

        
        if(!error)
        {
            char project_directory[128];
            sprintf(project_directory,"./%s",project_name);
#ifdef LINUX
            int temp = umask(0);
#endif
            if (make_directory(project_directory) != 0)
            {
                error=1;
                char auxestr[2048];

                memcpy(auxestr,errorstr,2048);
                sprintf(errorstr,"%s\nError al crear el directorio %s \n",auxestr, project_directory);
            }

            sprintf(project_directory,"./%s/cortafuego",project_name);
            if (make_directory(project_directory) != 0)
            {
                error=1;
                char auxestr[2048];

                memcpy(auxestr,errorstr,2048);
                sprintf(errorstr,"%s\nError al crear el directorio %s \n",auxestr, project_directory);
            }
        }


        if(!error)
        {
            char destino[256];
            sprintf(destino,"./%s/%s",project_name,projectfn_ELEVACION);
            copy_file(destino,project_ELEVACION);
            
            sprintf(destino,"./%s/%s",project_name,projectfn_VEGETACION);
            copy_file(destino,project_VEGETACION);  

            sprintf(destino,"./%s/%s",project_name,projectfn_WINDINT);
            copy_file(destino,project_WINDINT); 

            sprintf(destino,"./%s/%s",project_name,projectfn_WINDANG);
            copy_file(destino,project_WINDANG); 

            sprintf(destino,"./%s/%s",project_name,projectfn_SLOPE);
            copy_file(destino,project_SLOPE);

            sprintf(destino,"./%s/%s",project_name,projectfn_ASPECT);
            copy_file(destino,project_ASPECT);

            sprintf(destino,"./%s/datos_meteorologicos.txt",project_name);
            copy_file(destino,project_METEOR);
        }

        if(!error)
        {
            char configfile[256];
            sprintf(configfile,"./%s/config.txt",project_name);

            FILE * pFile;
            pFile = fopen (configfile,"w");
            if (pFile!=NULL)
            {
                char str[128];

                sprintf(str,"ROWS=%d\r\nCOLS=%d\r\nfullscreen=1\r\nPALETA=paleta.txt\r\n",filas[0],cols[0]);
                fputs(str,pFile);

                sprintf(str,"ELEV_MAP=%s\r\n",projectfn_ELEVACION);
                fputs(str,pFile);

                sprintf(str,"ALTI_MAP=%s\r\n",projectfn_ELEVACION);
                fputs(str,pFile);

                sprintf(str,"VEGETATION_MAP=%s\r\n",projectfn_VEGETACION);
                fputs(str,pFile);

                sprintf(str,"FOREST_MAP=%s#este mapa pronto no se usará mas\r\n",projectfn_VEGETACION);
                fputs(str,pFile);

                

                sprintf(str,"INT_WIND=%s\r\n",projectfn_WINDINT);
                fputs(str,pFile);

                sprintf(str,"WIND_MAP=%s\r\n",projectfn_WINDANG);
                fputs(str,pFile);

                sprintf(str,"SLOPE_MAP=%s\r\n",projectfn_SLOPE);
                fputs(str,pFile);

                sprintf(str,"ASPECT_MAP=%s\r\n",projectfn_ASPECT);
                fputs(str,pFile);

                fclose (pFile);
            }
            int nodata_ocurr=0;
            int ocurr[256];
            int nodata_val;
            for (int i=0;i<256;i++)
            {
                ocurr[i]=0;
            }

            ocurr[0]=1;

            int *mapi=NULL;
            mapi=GetMapi(project_VEGETACION,filas[0],cols[0],&nodata_val);

            if(mapi==NULL){

                print_debug("ERROR DE GetMapi en el asistente para nuevo proyecto.\n");
            }

            for(int i=0;i<filas[0]*cols[0];i++)
            {
                int dato=mapi[i];
                if(dato==nodata_val){nodata_ocurr++;continue;}
                if(dato>=0 || dato<256){
                    ocurr[dato]++;
                }
            }
            free(mapi);


            char paletafile[256];
            sprintf(paletafile,"./%s/paleta.txt",project_name);

            //FILE * pFile;
            pFile = fopen (paletafile,"w");
            if (pFile!=NULL)
            {
                char str[128];



                sprintf(str,"###### El formato para el mapeo es:  VALOR RASTER=VALOR SIMULADOR #####\r\n");
                fputs(str,pFile);

                for(int i=0;i<256;i++)
                {
                    if(ocurr[i]>0){
                        sprintf(str,"R%d=2\r\n",i);
                        fputs(str,pFile);
                    }
                }


                sprintf(str,"###### Se define el nombre y color RGB para cada valor del raster #####\r\n");
                fputs(str,pFile);

                for(int i=0;i<256;i++)
                {
                    if(ocurr[i]>0){
                        sprintf(str,"I%d=\"Nombre %d\",128,0,0\r\n",i,i);
                        fputs(str,pFile);
                    }
                }


                fclose (pFile);
            }


            char indinicfile[256];
            sprintf(indinicfile,"./%s/indices_iniciales.txt",project_name);

            //FILE * pFile;
            pFile = fopen (indinicfile,"w");
            if (pFile!=NULL)
            {
                char str[128];



                sprintf(str,"#Indices correspondientes al 1er\r\n#dia del archivo datos_meteorologicos.txt\r\n#FFMC\tDMC\tDC\r\n");
                fputs(str,pFile);

                sprintf(str,"%.02f\t%.02f\t%.02f\r\n",FFMC,DMC,DC);
                fputs(str,pFile);

                fclose (pFile);
            }
        }


        if(error)
        {
            ImGui::OpenPopup("error");
        }

        //show_mapeoscomb_window=true;


        if(!error)
        {
            ImGui::CloseCurrentPopup();
            show_new_project_window=0;
            show_load_project_window=0;

            char str[128];

            sprintf(str,"%s/",project_name);
            memcpy(Singleton::getInstance()->PROJECT_FOLDER_P,str,1+strlen(str));

            print_debug("Projsel %s \n",str);
            print_debug("Projsel2 %s \n",Singleton::getInstance()->PROJECT_FOLDER_P);

            Cargar_proyecto();
            //cargar_archivo(str);
            //ImGui::CloseCurrentPopup();
            show_mapeoscomb_window=true;
        }

        /*ImGui::CloseCurrentPopup();
        show_new_project_window=0;*/
    }

    if (ImGui::BeginPopup("error"))
    {
        ImGui::Text("Listado de error/es:");
        ImGui::Text(errorstr);
        if (ImGui::Button("OK")) {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }

    ImGui::SameLine();
    if (ImGui::Button("Cancelar")) {
        ImGui::CloseCurrentPopup();
        show_new_project_window=0;
    }
    ImGui::SameLine();
    /*if (ImGui::Button("Open File Dialog"))
    {
        ImGuiFileDialog::Instance()->OpenDialog("ChooseFileDlgKey", "Choose File", ".asc\0.txt\0.dat\0\0", ".");
    }*/

    // display
    if (ImGuiFileDialog::Instance()->FileDialog("ChooseFileDlgKey")) 
    {

        // action if OK
        if (ImGuiFileDialog::Instance()->IsOk == true)
        {
          std::string filePathName = ImGuiFileDialog::Instance()->GetFilepathName();
          std::string filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
          std::string fileName = ImGuiFileDialog::Instance()->GetCurrentFileName();
          
          // action

            char *cstrfilePathName = new char[filePathName.length() + 1];
            strcpy(cstrfilePathName, filePathName.c_str());

            char *cstrfilePath = new char[filePath.length() + 1];
            strcpy(cstrfilePath, filePath.c_str());

            char *cstrfileName = new char[fileName.length() + 1];
            strcpy(cstrfileName, fileName.c_str());
            // do stuff
            

          if(filesel_pointer==1)
          {
            /*printf("%s\n",cstrfilePathName);
            printf("%s\n",cstrfilePath);
            printf("%s\n",cstrfileName);*/
            memcpy(project_ELEVACION,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_ELEVACION,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==2)
          {
            /*printf("%s\n",cstrfilePathName);
            printf("%s\n",cstrfilePath);
            printf("%s\n",cstrfileName);*/
            memcpy(project_VEGETACION,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_VEGETACION,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==3)
          {
            memcpy(project_WINDINT,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_WINDINT,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==4)
          {
            memcpy(project_WINDANG,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_WINDANG,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==5)
          {
            memcpy(project_SLOPE,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_SLOPE,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==6)
          {
            memcpy(project_ASPECT,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_ASPECT,cstrfileName,strlen(cstrfileName)+1);
          }
          else if(filesel_pointer==7)
          {
            memcpy(project_METEOR,cstrfilePathName,strlen(cstrfilePathName)+1);
            memcpy(projectfn_METEOR,cstrfileName,strlen(cstrfileName)+1);
          }


          delete [] cstrfilePathName;
          delete [] cstrfilePath;
          delete [] cstrfileName;
        }
        // close
        ImGuiFileDialog::Instance()->CloseDialog("ChooseFileDlgKey");
    }


    if(ImGui::IsAnyItemActive()){imguicontrol2=1;}

    ImGui::End();
}