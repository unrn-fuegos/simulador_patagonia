﻿
/***********************************************************************************************************************************
 *                                                                                                                                 *
 * Este programa IMPLEMENTA las ecuaciones definidas para el cálculo del Fire Wheather Index (FWI) definidas                       *                     
 * por el Canadian Forestry Service. Forestry Technical Report 35 (Ottawa 1987) y publicadas en múltiples                          * 
 * trabajos (C. E. Van Wagner et all). Se respetan completamente las ecuaciones definidas en "FWI_Equations_FORTRAN.pdf"           *
 * y "Cálculo_Bariloche_2017-2018.xlsx". NO HAY GARANTIA ALGUNA DE SUS RESULTADOS NI DE SU USO ESPECIFICO.                         *
 *                                                                                                                                 *
 * Esta obra es software libre; puede redistribuirse y/o modificarse de acuerdo con los términos de la                             *
 * Licencia Pública General GNU publicada por la Free Software Foundation, en la versión 2 de la licencia                          *
 * o cualquier otra posterior. Esta obra se distribuye con la esperanza de que pueda ser útil, pero SIN                            *
 * GARANTIA ALGUNA; ni siquiera la garantía implícita de COMERCIALIZACION o ADECUACIÓN A UNA FINALIDAD ESPECÍFICA.                 *
 * Véase la versión 2 y la versión 3 de la Licencia Pública General GNU para conocer más detalles (http://www.gnu.org/licenses/).  *
 *                                                                                                                                 *
 **********************************************************************************************************************************/


#include "manejo_archivos.h"
#include <stdlib.h>
#include <stdio.h>
#ifdef LINUX
#include <unistd.h> // for file exists
#endif
#ifdef WIN32
#include "direntW.h" //manejo de directorios
#endif
#include <string.h>
#include <math.h>
#include "calculo_fwi.h"
#ifdef LINUX
#include <dirent.h> //manejo de directorios
#endif
#include "imgui/imgui.h" 
#include "Singleton.h"

#define MAX 128
#define MAX_LN 128



//----------:------------------------------------------------------------------------//
/*int procesar_datos_desde_archivo() {

  mostrar_carpeta();
  char in[MAX],out[MAX];
  printf("Escriba el nombre del archivo a procesar: ");
  scanf("%s", in);
  printf("\n");
  

  if (access(in, F_OK) == -1) {
      printf("El archivo %s no existe, se sale de la aplicacion. \n", in);
      return (-1);
  }
  else  {

      printf("Escriba el nombre del archivo de salida:  ");
      scanf("%s", out);
      printf("\n");
      procesar_archivo(in, out);
 }
  return 0;  

}*/


extern bool imguicontrol2;

FWI_OUT::FWI_OUT()
{
  met.temp=18.3f;
  met.rh=35.0f;
  met.ws=0;
  met.wv=39.0f;
  met.rain=0.0f;
  ffmc=91.41f;
  dmc=46.11f;
  dc=151.1f;
  isi=37.07f;
  bui=54.88f;
  fwi=55.21f;
  dsr=32.95f;
  //printf("CONSTRUCTOR FWI_OUT()\n");

}



//--------------------------------------------------------------------
int recuperar_codigos_de_archivo(FLOAT *FFMC, FLOAT *DMC, FLOAT *DC) {

  //mostrar_carpeta();
  /*char in[MAX],out[MAX], line[256];
  printf("Escriba el nombre del archivo de donde obtener códigos previos: ");
  scanf("%s", in);
  printf("\n");
  

  if (access(in, F_OK) == -1) {
      printf("El archivo %s no existe, se sale de la aplicacion. \n", in);
      return (-1);
  }
  else  {

      FILE *fin=;
      if (!(fin = fopen(in, "r+"))) {printf("Error al abrir archivo de entrada\n"); return(-1); }

      //acceso secuencial a archivo de texto, voy a la ultima linea
      while( fgets(line, sizeof(line), fin)!=NULL );  

      int fecha;
      FLOAT temp, hum, vv, pp, isi, bui, fwi, dsr;
      char sectorV[MAX];
 
      // funcion para obtener desde un string los datos con formato
      sscanf(line, "%d\t%f\t%f\t%s\t%f%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f",&fecha, &temp, &hum, sectorV, &vv, &pp, FFMC, DMC, DC, &isi, &bui, &fwi, &dsr);
    
      
      fclose(fin);

  }*/
  return 0;
}





//-----------------------------------------------------------------------------------------//
int obtener_valores_de_entrada(FLOAT *FFMC, FLOAT *DMC, FLOAT *DC, FLOAT *hh, FLOAT *tt, FLOAT *ww, FLOAT *ro, int *dia, int *mes, int *anio) {

      char opcion[2];


      // datos meteorologicos y de la fecha se ingresan por teclado
      printf("\nIngrese datos meteorologicos: \n");
      printf("\n   Temperatura: ");
      scanf("%f", tt);
      printf("   Humedad relativa: ");
      scanf("%f", hh);
      printf("   Velocidad del viento: ");
      scanf("%f", ww);
      printf("   Precipitación: ");
      scanf("%f", ro);
 
      int ok = 0;
      while (!ok) {
          printf("   Ingresar la fecha (dd/mm/aaaa): ");
          scanf("%d/%d/%d", dia,mes,anio);
          ok = (*dia != 0) && (*mes != 0) && (*anio !=0 );
      }
      
      // los valores del FFMC, DMC y DC pueden venir de antes, se da la opcion de dejarlos por defecto
      printf("\n\nElija opción para los valores de FFMC, DMC y DC previos \n\n");
      printf("\n   a. Ingresarlos por teclado \n");
      printf("   b. Leerlos desde un archivo\n");
      printf("\n\nOpcion: ");
      
      
      scanf("%s", opcion);
     
      if (opcion[0] == 'a' || opcion[0] == 'A'){
          printf("\nIngrese los valores para cada codigo: \n");
          printf("\n   Valor de FFMC: ");
          scanf("%f", FFMC);
          printf("   Valor de DMC: ");
          scanf("%f", DMC);
          printf("   Valor de DC: ");
          scanf("%f", DC);
          printf("\n");
      }


      if (opcion[0] == 'b' || opcion[0] == 'B'){
          recuperar_codigos_de_archivo(FFMC, DMC, DC);
       //   printf("Los datos sacados desde el archivo son: %f %f %f \n ", *FFMC, *DMC, *DC);

      }

      return 0;
}



//-----------------------------------------------------------------------------------------//
int imprimir_valores_pantalla_archivo(int dia, int mes, int anio, FLOAT tt, FLOAT hh, FLOAT ww, FLOAT ro, FLOAT FFMC, FLOAT DMC, FLOAT DC, FLOAT ISI, FLOAT BUI, FLOAT FWI, FLOAT DSR) {

      // para rele
      /*char letra1 = (mes < 10) ? '0' : '\0';
      char letra2 = (dia < 10) ? '0' : '\0';

      printf("\n\n\n# FWI TEST for %d/%d/%d \n", dia, mes, anio);
      printf("# INITIAL VALUES FOR FFMC: %0.2f, DMC: %.2f, DC: %.2f \n\n", FFMC, DMC, DC);
      printf("#DATE\t\t TEMP\t  RH\tWIND(S)\t WIND(V) RAIN\t FFMC\t DMC\t DC\t ISI\t BUI\t FWI\t DSR \n");
    
      printf("%d%c%d%c%d\t %.2f\t %.2f\t --\t %.2f\t %.2f\t ", anio, letra1, mes, letra2, dia, tt, hh, ww, ro);
      printf("%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f \n", FFMC,DMC,DC,ISI,BUI,FWI, DSR);

      char opcion[2], nombre[256];
      printf("Desea agregar estos datos a un archivo de salida? (S/N): ");
      scanf("%s", opcion);

      if (opcion[0] == 's' || opcion[0] == 'S') {
          
          printf("Ingrese el nombre del arhivo: ");
          scanf("%s", nombre);

          if (access(nombre, F_OK) == -1) {
              printf("El archivo %s no existe, se sale de la aplicacion. \n", nombre);
              return (-1);
          }
          else  {
             FILE *archivo;
             // escribo en el archivo
             if (!(archivo = fopen(nombre, "a"))) {printf("Error al abrir archivo \n"); return(-1); }

            // armo la fecha en formato aaaammdd
             int date = dia + mes * 100 + anio * 10000;

             // se imprimen los datos en el archivo de salida  
             fprintf(archivo, "%d\t %.2f\t %.2f\t --\t %.2f\t %.2f\t ", date, tt, hh, ww, ro);

             // se imprimen los datos calculados
             fprintf(archivo, "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f \n", FFMC,DMC,DC,ISI,BUI,FWI, DSR);


             fclose(archivo);
  
             printf("Datos almacenados con exito en: %s \n", nombre);

          }

      }
*/
      return 0;

}


//-----------------------------------------------------------------------------------------//
int procesar_dato_diario() {

      FLOAT FFMC, DMC, DC, ISI, BUI, FWI, DSR;
      FLOAT hh,tt,ww,ro;

      int dia, anio, mes=9;
      //char opcion[2];
      //char car;

      // ingreso de datos de entrada para el calculo de los indices
      obtener_valores_de_entrada(&FFMC, &DMC,  &DC,  &hh,  &tt,  &ww,  &ro, &dia, &mes, &anio);


      // calculo de los indices       
      FFMC = calculaFFMC(hh,tt,ww,ro,FFMC);
      DMC = calculaDMC(hh,tt,ro,DMC,mes-1);  // usa mes como índice que comienza en 0
      DC = calculaDC(tt,ro,DC,mes-1);        // usa mes como índice que comienza en 0
      ISI = calculaISI(FFMC,ww);
      BUI = calculaBUI(DMC,DC);
      FWI = calculaFWI(BUI,ISI);
      DSR = 0.0272 * pow(FWI,1.77);                                   // (31)

      // salida de resultados
      imprimir_valores_pantalla_archivo(dia, mes, anio, tt, hh, ww, ro, FFMC, DMC, DC, ISI, BUI, FWI, DSR);
      
      return 0;
}

int strtodelim(char* str,char delim);

//---------------------------------------------------------------------------------------//
// la fecha viene con formato aaaammdd
int calcular_mes(int date) {
    int /*i,*/ unidad,decena/*, num*/;

    // descarto los ultimos 2 digitos por el dia
    date = date / 100;
    // proeceso los dos digitos que le siguen
    unidad = date % 10;
    date = date / 10;
    decena = date % 10;

    decena = decena == 1 ? 10 : 0;
    return (decena + unidad);
}

void FWI_TABLE::init(char* project_folder, char*filenamei,char*filenamem,char*filenameo){
  char auxstr[128];
  sprintf(auxstr,"%s%s",project_folder,filenamei);
  memcpy(indice_filename,auxstr,1+strlen(auxstr));

  sprintf(auxstr,"%s%s",project_folder,filenamem);
  memcpy(meteor_filename,auxstr,1+strlen(auxstr));

  sprintf(auxstr,"%s%s",project_folder,filenameo);
  memcpy(output_filename,auxstr,1+strlen(auxstr));

  cant_fwi=1;
  fwi=(FWI_OUT*)malloc(sizeof(FWI_OUT));
}

void FWI_TABLE::init(char* project_folder,char*filenamem,char*filenameo){
  char auxstr[128];
  /*sprintf(auxstr,"%s%s",project_folder,filenamei);
  memcpy(indice_filename,auxstr,1+strlen(auxstr));*/
  indice_filename[0]=0;

  sprintf(auxstr,"%s%s",project_folder,filenamem);
  memcpy(meteor_filename,auxstr,1+strlen(auxstr));

  sprintf(auxstr,"%s%s",project_folder,filenameo);
  memcpy(output_filename,auxstr,1+strlen(auxstr));

  cant_fwi=1;
  fwi=(FWI_OUT*)malloc(sizeof(FWI_OUT));
}

void FWI_TABLE::clear(void){
    if(cant_fwi==0) return;
    free(fwi);
}

FWI_TABLE::FWI_TABLE(){
  this->cant_fwi=0;
  indice_filename[0]=0;
  meteor_filename[0]=0;
  output_filename[0]=0;
}

void FWI_TABLE::load_files(float FFMC,float DMC,float DC){
  FILE *fin, *fout,*findices;
  char line[256];
  //char fecha[10];

  //bool debug=0;

  float /*FFMC=21.2, DMC=0.1, DC=0.6, */ISI, BUI, FWI, DSR;
  //double hh,tt,ww,ro=0;

  if(this->indice_filename[0]!=0){
    findices = fopen(this->indice_filename, "r");
    if (!findices) {print_debug("Error al abrir archivo de entrada de indices FWI\n"); return; }
    //char line[128];
    while(fgets(line, MAX_LN, findices))
    {
      if(strlen(line)<4)continue;
      int stpos=0;
      char ext[64];
      int extlen;
      if(line[0]=='#')continue;

      extlen=strtodelim(&line[stpos],'\t');
      memcpy(ext,&line[stpos],extlen);
      ext[extlen]=0;
      stpos+=extlen+1;


      sscanf(ext,"%f",&FFMC);

      extlen=strtodelim(&line[stpos],'\t');
      memcpy(ext,&line[stpos],extlen);
      ext[extlen]=0;
      stpos+=extlen+1;
      sscanf(ext,"%f",&DMC);

      extlen=strtodelim(&line[stpos],'\t');
      memcpy(ext,&line[stpos],extlen);
      ext[extlen]=0;
      stpos+=extlen+1;
      sscanf(ext,"%f",&DC);
    }
    fclose (findices);
  }   


  float  temp, rh, windV, rain;
  int date;
  char windS[8];  // Variable, N, S, E, O, ONO, NO, ESE, SE, etc

  // abro archivos de entrada y de salida
  fin = fopen(this->meteor_filename, "r+");
  fout = fopen(this->output_filename, "w+");

  if (!fin) {print_debug("Error al abrir archivo de entrada meteorologico\n"); return; }
  if (!fout) {print_debug("Error al abrir archivo de salida FWI\n"); return; }


  fprintf(fout, "# FWI TEST DATA USING FILE %s \n", this->meteor_filename);
  fprintf(fout, "# INITIAL VALUES FOR FFMC: %0.2f, DMC: %.2f, DC: %.2f \n\n", FFMC, DMC, DC);
  fprintf(fout, "#DATE\t\t TEMP\t  RH\tWIND(S)\t WIND(V) RAIN\t FFMC\t DMC\t DC\t ISI\t BUI\t FWI\t DSR \n");

  // el primer dia se comienzan con valores por defecto
  fgets(line, sizeof(line), fin);




  int i=0;



  print_debug("LL1\n");
  while (fgets(line, sizeof(line), fin)) {

        if(line[0]=='#' || strlen(line)<5)continue;
        print_debug("LL2\n");
        // se procesa line para obtener los datos en el formato deseado     
        obtener_datos(line, &date, &temp, &rh, windS, &windV, &rain);

        if(i!=0){
          cant_fwi++;
          fwi=(FWI_OUT*)realloc(fwi,cant_fwi*sizeof(FWI_OUT));
        }

        print_debug("LL3\n");
        // formateo para imprimir datos de entrada
        if (strcmp(windS, "Variable") == 0)
            strcpy(windS, "Var");
     
        // se imprimen los datos en el archivo de salida  
        fprintf(fout, "%d\t %.2f\t %.2f\t %s\t %.2f\t %.2f\t ", date, temp, rh, windS, windV, rain);

        fwi[i].ffmc=FFMC;
        fwi[i].dmc=DMC; //carga los valores iniciales a la 1er entrada
        fwi[i].dc=DC;
        fwi[i].met.temp=temp;
        fwi[i].met.rh=rh;
        fwi[i].met.rain=rain;
        fwi[i].met.wv=windV;
        fwi[i].met.ws=-1;
        memcpy(fwi[i].met.ws_str,windS,4);
        fwi[i].met.aa=date/10000;
        fwi[i].met.mm=date/100-fwi[i].met.aa*100;
        fwi[i].met.dd=date-100*(date/100);

        

        print_debug("LL4\n");
        // HH: humedad relativa, TT: temperatura, WW: velocidad viento, Ro: lluvia anterior, Fo: FFMC anterior: FFMC cero
        //double calculaFFMC(double HH, double TT, double WW, double Ro, double Fo)

        FFMC = calculaFFMC(rh, temp, windV, rain, FFMC); //calculaFFMC(hh,tt,ww,ro,FFMC);
        DMC = calculaDMC(rh, temp, rain, DMC, calcular_mes(date)-1);  // (hh,tt,ro,DMC,mes-1);  // usa mes como índice que comienza en 0
        DC = calculaDC(temp, rain, DC, calcular_mes(date)-1);//(tt,ro,DC,mes-1);        // usa mes como índice que comienza en 0
        ISI = calculaISI(FFMC, windV);//(FFMC,ww);
        BUI = calculaBUI(DMC,DC);
        FWI = calculaFWI(BUI,ISI);
        DSR = 0.0272 * pow(FWI,1.77);  

        print_debug("LL5\n");     


        // se imprimen los datos en la salida
        fprintf(fout, "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t", FFMC,DMC,DC,ISI,BUI,FWI, DSR);
        print_debug("LL6\n");
        fwi[i].isi=ISI;
        fwi[i].bui=BUI;
        fwi[i].fwi=FWI;
        fwi[i].dsr=DSR;
        fwi[i].valid=1;
        print_debug("LL7\n");
        peligrosidadXtipo(fwi[i].isi,fwi[i].bui,55,fwi[i].pelig);
        char letras[] = {'.','B', 'M', 'A', 'X', 'E'};
        //"%c%c%c%c%c",letras[FWI_T.fwi[i].pelig[0]],letras[FWI_T.fwi[i].pelig[1]],letras[FWI_T.fwi[i].pelig[2]],letras[FWI_T.fwi[i].pelig[3]],letras[FWI_T.fwi[i].pelig[4]]);
        fprintf(fout, "%c%c%c%c%c \n",letras[fwi[i].pelig[0]],letras[fwi[i].pelig[1]],letras[fwi[i].pelig[2]],letras[fwi[i].pelig[3]],letras[fwi[i].pelig[4]]);
        i++;
      
  }
  print_debug("LL8\n");
  fclose(fin);
  fclose(fout);
  //fclose(findices);
  print_debug("LL9\n");

  print_debug("Datos de FWI almacenados con exito en: %s \n", this->output_filename);
  print_debug("Datos de FWI almacenados con exito en: %s \n", this->output_filename);
}

const char *get_filename_ext(const char * filename);

void FWI_TABLE::show_load_mfile(bool*showw){
    ImGui::SetNextWindowSize(ImVec2(600,900), ImGuiSetCond_Always);

    if (ImGui::BeginPopupModal("Cargar archivo datos meteorologicos..."))
    {
    //ImGui::SetNextWindowPos(ImVec2(window_width-300,200),ImGuiSetCond_Always);
    //ImGui::Begin("Cargar mapa fuego real...", &show_another_window,ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoResize);
        static int listbox_file_cant=0;
        static char **listbox_file_list=NULL;
        
        static bool firstload=1;
        //static bool pers_filename=0;
        if(firstload)//leer archivos del directorio
        {
            listbox_file_list=NULL;
            //pers_filename=0;
            //printf("FIRSTLOAD\n");
            
            listbox_file_cant=0;
            //AGREGAR SUBDIRECTORIO CORTAFUEGOS****************************************************************
            DIR *d;
            struct dirent *dir;
            char str[256];
            sprintf(str,"%s",Singleton::getInstance()->PROJECT_FOLDER_P);
            //printf("%s\n",str);
            d=opendir(str);



            char exten[]="txt";

            if(d)
            {
              while((dir=readdir(d))!=NULL){
                if(!strcmp(get_filename_ext(dir->d_name),exten)){
                  listbox_file_list=(char**)realloc(listbox_file_list,(1+listbox_file_cant)*sizeof(char*));
                  listbox_file_list[listbox_file_cant]=(char*)malloc(1+strlen(dir->d_name));
                  memcpy(listbox_file_list[listbox_file_cant],dir->d_name,1+strlen(dir->d_name));
                  listbox_file_cant++;
                }

              }
              closedir(d);
            }

            firstload=0;
        }


    
    static int listbox_item_current = 1;
    int old_listbox_item_current = listbox_item_current;

    ImGui::ListBox("selecciona \narchivo \npara \nabrir", &listbox_item_current, listbox_file_list, listbox_file_cant, 15);
    //ImGui::SameLine();
    static float FFMC=21.2f,DMC=0.1f,DC=0.6f;
    ImGui::InputFloat("FFMC",&FFMC,21.2f);//21.2    0.1 0.6
    ImGui::InputFloat("DMC",&DMC,0.1f);//21.2    0.1 0.6
    ImGui::InputFloat("DC",&DC,0.6f);//21.2    0.1 0.6

    static char myfilename[32];
    if(listbox_item_current!=old_listbox_item_current)
    {
      //pers_filename=0;
      memcpy(myfilename,listbox_file_list[listbox_item_current],1+strlen(listbox_file_list[listbox_item_current]));
    }
    ImGui::Text("Nombre:");
    ImGui::SameLine();

    if(ImGui::InputText("", myfilename, 32)){
      //pers_filename=1;

    }
            

    if (ImGui::Button("Abrir")) {

        //char str[128];
        //sprintf(str,"%s%s",Singleton::getInstance()->PROJECT_FOLDER_P,myfilename);

        this->clear();
        char str2[256];
        sprintf(str2,"OUT_FWI_%s",myfilename);
        this->init(Singleton::getInstance()->PROJECT_FOLDER_P,myfilename,str2);
        this->load_files(FFMC,DMC,DC);//PROCESADO DATOS METEOROLOGICOS Y CALCULO FWI*/

      //cargar_archivo(str);
        ImGui::CloseCurrentPopup();
        for(int i=0;i<listbox_file_cant;i++)
        {
            free(listbox_file_list[i]);
            listbox_file_list[i]=NULL;
        }
        free(listbox_file_list);
        firstload=1;
        *showw=0;
    }
    ImGui::SameLine();

    if (ImGui::Button("Cancelar")) {
      ImGui::CloseCurrentPopup();
      for(int i=0;i<listbox_file_cant;i++)
        {
            free(listbox_file_list[i]);
            listbox_file_list[i]=NULL;
        }
        free(listbox_file_list);
      firstload=1;
      *showw=0;
    }

    //if(ImGui::IsAnyItemActive()){imguicontrol2=1;}
    ImGui::EndPopup();
  }

}


//----------:------------------------------------------------------------------------//
int procesar_archivo(char in[MAX], char out[MAX]) {

  FILE *fin, *fout;
  char line[256];
  //char fecha[10];

  double FFMC=21.2, DMC=0.1, DC=0.6, ISI, BUI, FWI, DSR;
  //double hh,tt,ww,ro=0;


  float  temp, rh, windV, rain;
  int date;
  char windS[8];  // Variable, N, S, E, O, ONO, NO, ESE, SE, etc

  // abro archivos de entrada y de salida
  fin = fopen(in, "r+");
  fout = fopen(out, "w+");
  if (!fin) {print_debug("Error al abrir archivo de entrada\n"); return(-1); }
  if (!fout) {print_debug("Error al abrir archivo de salida\n"); return(-1); }


  fprintf(fout, "# FWI TEST DATA USING FILE %s \n", in);
  fprintf(fout, "# INITIAL VALUES FOR FFMC: %0.2f, DMC: %.2f, DC: %.2f \n\n", FFMC, DMC, DC);
  fprintf(fout, "#DATE\t\t TEMP\t  RH\tWIND(S)\t WIND(V) RAIN\t FFMC\t DMC\t DC\t ISI\t BUI\t FWI\t DSR \n");

  // el primer dia se comienzan con valores por defecto
  fgets(line, sizeof(line), fin);


  while (fgets(line, sizeof(line), fin)) {

        // se procesa line para obtener los datos en el formato deseado     
        obtener_datos(line, &date, &temp, &rh, windS, &windV, &rain);

        // formateo para imprimir datos de entrada
        if (strcmp(windS, "Variable") == 0)
            strcpy(windS, "Var");
     
        // se imprimen los datos en el archivo de salida  
        fprintf(fout, "%d\t %.2f\t %.2f\t %s\t %.2f\t %.2f\t ", date, temp, rh, windS, windV, rain);


        // HH: humedad relativa, TT: temperatura, WW: velocidad viento, Ro: lluvia anterior, Fo: FFMC anterior: FFMC cero
        //double calculaFFMC(double HH, double TT, double WW, double Ro, double Fo)
        FFMC = calculaFFMC(rh, temp, windV, rain, FFMC); //calculaFFMC(hh,tt,ww,ro,FFMC);
        DMC = calculaDMC(rh, temp, rain, DMC, calcular_mes(date)-1);  // (hh,tt,ro,DMC,mes-1);  // usa mes como índice que comienza en 0
        DC = calculaDC(temp, rain, DC, calcular_mes(date)-1);//(tt,ro,DC,mes-1);        // usa mes como índice que comienza en 0
        ISI = calculaISI(FFMC, windV);//(FFMC,ww);
        BUI = calculaBUI(DMC,DC);
        FWI = calculaFWI(BUI,ISI);
        DSR = 0.0272 * pow(FWI,1.77);       

        // se imprimen los datos en la salida
        fprintf(fout, "%.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f\t %.2f \n", FFMC,DMC,DC,ISI,BUI,FWI, DSR);
     
      
  }

  fclose(fin);
  fclose(fout);

  print_debug("Datos almacenados con exito en: %s \n", out);

  return 0;  

}


void replace_semicolon(char num[256]) {
    int i = 0;
    int salir = 0;

    for (i=0; ((num[i]!= '\0') && (!salir)); i++)    {
     
        if (num[i] == ','){
           num[i] = '.';
           salir = 1;
        }
        
    }
  
}


// descuageringa la linea y obtiene dato por dato
int obtener_datos(char line[256], int *date, float *temp, float *rh, char windS[8], float *windV, float *rain) {

    const char delim[2] = "\t";
    char *token;
    //float dato;
   

    /* los datos tienen formato DATE TEMP HH WINDS WINDV RAIN */
    // se obtiene la fecha
    token = strtok(line, delim);
    *(date) = strtol(token, NULL,10);

    // se obtiene la temperatura
    token = strtok(NULL, delim);
    replace_semicolon(token);
    *(temp) = strtof(token, NULL);

    // se obtiene la humedad relativa
    token = strtok(NULL, delim);
    replace_semicolon(token);
    *(rh) = strtof(token, NULL);

    // se obtiene la direccion del viento 
    token = strtok(NULL, delim);
    strcpy(windS, token);

    // se obtiene la velocidad del viento
    token = strtok(NULL, delim);
    replace_semicolon(token);
    *(windV) = strtof(token, NULL);
    
    // se obtiene las precipitaciones
    token = strtok(NULL, delim);
    replace_semicolon(token);
    *(rain) = strtof(token, NULL);
    
    /*while (token != NULL) {
        dato = strtof(token, NULL);
        printf("%s (%f) \n",  token, dato);
        token = strtok(NULL, delim);
    }*/

    return 0;

}

