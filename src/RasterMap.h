#pragma once
//#include "superinclude.h"


using namespace glm;



class RasterMap{
public:
	int *h_map,*d_map;// Mapa en HOST, Mapa en DEVICE
	int x,y; //Tamaño del mapa
	bool sinpaleta; //Tiene o no tiene paleta? (Ojo, los indices pasan a ser colores en cada byte r*256*256+g*256+b)
	bool is_in_gpu,is_gpu_utd;//Esta ya en gpu el mapa?  Is GPU map Up To Date?
	bool is_in_cpu;//Esta en cpu?
	vec3 * paleta;//Puntero a los colores de paleta guardados como float de 0 a 1.0f
	int paleta_size;//Entradas de paleta
	int nodata_value;//NoData Value para zonas transparentes por ejemplo el fuego, solo pintar zona quemada
	float map_a;	//Valor del slidebar con la transparencia
	//__constant__ float const_paleta_abc[3*3]; 
	bool has_tooltip;
	char tooltiptext[48];

	void Update_GPU_Map(void);
	void Load_Paleta(vec3*paleta, int size);
	RasterMap(void);
	RasterMap(bool sinpaleta);
	void SinPaleta(bool sinpaleta);
	void set_size(int x,int y);
	void reset(void);
};




class IgnitionPoint{
public:
	int step,x,y;
	bool enabled;

	void set(int step,int x, int y,bool enabled);
};


class SimuScript{
public:
	IgnitionPoint * ignitionpoint;
	int cant_ignitionpoint;
	int oldstep;
	int selected_ip;

	SimuScript(void);
	void add_ignitionpoint(int step,int x, int y,bool enabled);
	void load_ignitionpoint_file(char*filename);
	void save_ignitionpoint_file(char*filename);
	void evaluar_ingnitionpoints(int newstep,bool force);
	void delete_ignitionpoint(int i);
	void delete_all(void);
};



