#include "Singleton.h"

#include <math.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

using namespace glm;

bool LAYER::SetAlpha(float newalpha){
	float old=*alpha;
	*alpha=newalpha;
	if(newalpha!=old){return 1;}
	return 0;
}

Singleton* Singleton::instance = NULL;

Singleton* Singleton::getInstance(){
	if(instance==NULL){instance=new Singleton;}
	return instance;
}

Singleton* Singleton::gI(){
	if(instance==NULL){instance=new Singleton;}
	return instance;
}

Singleton::Singleton()
{
	logfile_sigdbg=fopen("log.txt","a");

	visualizar_pelig_indice=-1;
	projectfilelist.listbox_file_cant=0;
	projectfilelist.listbox_file_list=NULL;

	drawwiremesh=0;
	proyecto_cargado=0;

	window_width  = 512;
	window_height = 512;

	near_plane=0.01f;

	celdas_prom=10;

	fullscreen=1;

	windi_max=1.0f;
	windi_min=2.0f;

	ambient_mult=0.0f;
	specular_mult=0.2f;
	diffuse_mult=0.2f;
	diffuseC_mult=0.1f;

	wind_a=1.0f;
	wind_pot=0.5f;
	camspd=0.1f;

	verticesT=0;

	click_si=0;
	click_x=0;
	click_y=0;

	altitud_min=0.0f;
	altitud_max=0.0f;


	mouse_buttons = 0;
	rotate_x = 90.0;
	rotate_y = 0.0;
	rotate_z = 0.0;
	translate_x = -1.0;
	translate_y = 2.0;
	translate_z = -1.0;

	// Auto-Verification Code
	fpsCount = 0;        // FPS count for averaging
	fpsLimit = 1;        // FPS limit for sampling
	g_Index = 0;
	avgFPS = 0.0f;
	frameCount = 0;
	g_TotalErrors = 0;
	g_bQAReadback = false;

	

	textura_vegetacion=NULL;
	textura_quemado=NULL;
	textura_final=NULL;
	textura_selcorf=NULL;
	textura_aspect=NULL;
	textura_pelig=NULL;
	textura_alti=NULL;
	textura_Calti=NULL;
	textura_windi=NULL;
	btnS_poner_cortafuego=false;
	btnS_poner_cortafuego_multilinea=false;

	pArgc = NULL;
	pArgv = NULL;

	update_Calti=0;

	mapa_Calti_cant=7;
	mapa_Calti_pow=70;

	btnS_IGNI=false;
	btnS_IGNI_redef=-1;

	for(int i=0;i<1024;i++){keys[i]=0;}
	arrowstate[0]=0;
	arrowstate[1]=0;

	aspectmap.SinPaleta(1);
	altimap.SinPaleta(1);
	peligmap.SinPaleta(1);
	windImap.SinPaleta(1);

}

int Singleton::Get_visualizar_pelig_indice(){
	return visualizar_pelig_indice;
}

bool Singleton::Set_visualizar_pelig_indice(int val){
	old_visualizar_pelig_indice=visualizar_pelig_indice;
	visualizar_pelig_indice=val;
	if(val!=old_visualizar_pelig_indice){return 1;}
	return 0;
}