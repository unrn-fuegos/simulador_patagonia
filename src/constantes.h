

#ifndef _CONSTANTES_H_
#define _CONSTANTES_H_





// para elegir rapidamente la precision
typedef float FLOAT;
//typedef double FLOAT;


// 0: south hemisphere, 1: north hemisphere
// effective day-lengths per month for DMC (Daylength factor Le)
#ifndef Hem
#define Hem 0
#endif 

#ifndef HEMISFERIOS
#define HEMISFERIOS 2
#endif

#ifndef MESES
#define MESES 12
#endif




#endif