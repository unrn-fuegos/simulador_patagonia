CC=nvcc
#CPPFLAGS= -D__STDC_CONSTANT_MACROS
#CFLAGS=-Xptxas -v
#************************MAKEFILE DEL SIMULADOR*************************

SRC_DIR := ./src
OBJ_DIR := ./obj
SRC_DIRDW := .\src
OBJ_DIRDW := .\obj
SRC_FILES := $(wildcard $(SRC_DIR)/*.cu)
OBJ_FILES := 
OB_EXTEN :=
detected_OS :=


detected_OS := OTRO_SISTEMA
ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    detected_OS := Windows
    #detected_OS := Otro1
else
    detected_OS := $(shell uname)  # same as "uname -s"
    detected_OS := Linux
endif

#VERV_FLAG := --verbose


#detected_OS := Linux
$(info $(detected_OS))


ifeq ($(detected_OS),Windows)
	CFLAGS += -DWIN32 --machine 64
	OBJ_FILES := $(patsubst $(SRC_DIR)/%.cu,$(OBJ_DIR)/%.obj,$(SRC_FILES))
	LDFLAGS := -I -L -O3 -DGLEW_STATIC -Isrc/ -std=c++14
	CXXFLAGS := -D__STRICT_ANSI__
	LIBRARIES := -lglew32s -lfreeglut -lopengl32
	BIN:=main.exe
	CLEANFLAG := del /Q $(OBJ_DIRDW)\*.obj
	CLEANFLAGB := del /Q $(OBJ_DIRDW)\*.d
	CLEANFLAGC := del /Q $(BIN)
	OB_EXTEN := .obj

endif

ifeq ($(detected_OS),Darwin)        # Mac OS X
    CFLAGS += -D OSX
endif

ifeq ($(detected_OS),Linux)
	OBJ_FILES := $(patsubst $(SRC_DIR)/%.cu,$(OBJ_DIR)/%.o,$(SRC_FILES))
	CFLAGS   += -DLINUX
	LDFLAGS := -I -L -g -DGLEW_STATIC -std=c++14
	CXXFLAGS := -D__STRICT_ANSI__ --expt-relaxed-constexpr
	LIBRARIES := -lGL -lGLU -lX11 -lglut -lGLEW
	BIN:=main
	CLEANFLAG := rm -f $(OBJ_DIR)/*.o
	CLEANFLAGB := rm -f $(OBJ_DIR)/*.d
	CLEANFLAGC := rm -f $(BIN)
	OB_EXTEN := .o
endif



$(BIN): $(OBJ_FILES)
	$(CC) $(VERV_FLAG) $(CFLAGS) $(LDFLAGS) $(CXXFLAGS) -o $@ $^ $(LIBRARIES)

$(OBJ_DIR)/%$(OB_EXTEN): $(SRC_DIR)/%.cu
	$(CC) $(VERV_FLAG) $(CFLAGS) $(LDFLAGS) $(CXXFLAGS) -c -o $@ $< $(LIBRARIES)



#-include .depend
#CXXFLAGS += 
-include $(OBJ_FILES:$(OB_EXTEN)=.d)

.depend: $(SRC_FILES) *.h
	$(CC) -M $(CPPFLAGS) $(SRC_FILES) > .depend

.PHONY: all clean

clean: 
	$(CLEANFLAG)
	$(CLEANFLAGB)
	$(CLEANFLAGC)