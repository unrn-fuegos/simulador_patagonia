#version 330
/*layout (binding=0) uniform BNTest
{
	mat4 MTWM;
};*/
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexNormalModel;
layout (location = 2) in vec3 vertexUV;
layout (location = 3) in vec4 vertexPrelit;
layout (location = 4) in vec4 vertexColor;
uniform vec3 domiColor;
uniform mat4 modelTransformMatrix;
uniform mat4 fullTransfWTP;
uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform vec3 ambientLight;
uniform vec3 skyColor;
uniform vec4 ClipPlane;

out vec4 color;
out vec4 normalWorld;
out vec3 vertexPositionWorld;
out vec4 uv;
out vec4 prelit;
out float visibility;

const float c_precision = 128.0;
const float c_precisionp1 = c_precision + 1.0;
 
float color2float(vec3 color) {
    color = clamp(color, 0.0, 1.0);
    return floor(color.r * c_precision + 0.5) 
        + floor(color.b * c_precision + 0.5) * c_precisionp1
        + floor(color.g * c_precision + 0.5) * c_precisionp1 * c_precisionp1;
}
 
vec3 float2color(float value) {
    vec3 color;
    color.r = mod(value, c_precisionp1) / c_precision;
    color.b = mod(floor(value / c_precisionp1), c_precisionp1) / c_precision;
    color.g = floor(value / (c_precisionp1 * c_precisionp1)) / c_precision;
    return color;
}

vec3 UnPackFloat(float src)
{
  vec3 tr;
  tr.r = fract(src);
  tr.g = fract(src * 256.0f);
  tr.b = fract(src * 65536.0f);
 
  //Unpack to the -1..1 range
  tr.r = (tr.r * 2.0f) - 1.0f;
  tr.g = (tr.g * 2.0f) - 1.0f;
  tr.b = (tr.b * 2.0f) - 1.0f;
  return tr;
}

const float density=0.001;
const float gradient=5.5;

void main()
{
	vec4 vpM=vec4(vertexPositionModel,1.0);
	vec4 vpW=modelTransformMatrix * vpM;
	vec3 totalLight;
	
	
	gl_Position=fullTransfWTP * vpW;
	
	normalWorld=modelTransformMatrix*vec4(vertexNormalModel,0);
	
	
	vertexPositionWorld=vec3(vpW);
	float distance=length(cameraPosition-vec3(vpW));
	visibility=exp(-pow(distance*density,gradient));
	visibility=clamp(visibility,0,1);
visibility=1;

	color=vertexColor;
	gl_ClipDistance[0]=dot(vpW,ClipPlane);
	
	int aux=int(floor(vertexUV.z+0.5));
	int sampler=aux/8192;
	int capa=aux-sampler*8192;
	//int capa=int(mod(aux,8192));
	//sampler=0;
	//capa=10;
	uv=vec4(vertexUV.r,vertexUV.g,float(capa),float(sampler));
	prelit=vertexPrelit;
	
}