#version 330
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexUV;
uniform vec3 cameraDirection;

out vec4 uv;

void main()
{
	gl_Position=vec4(vertexPositionModel,1.0);
	//float angle=asin(cameraDirection.z);
	//float disp=-angle/3.1415;
	//vec2 uv_t=vec2(vertexUV.r+disp,vertexUV.g+disp);
	
	//uv=vec4(uv_t.r,uv_t.g,0,0);
	uv=vec4(vertexUV.r,vertexUV.g,0,0);
}
