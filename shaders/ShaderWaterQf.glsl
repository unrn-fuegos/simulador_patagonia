#version 330
layout (location = 0) out vec4 daColor;

in vec4 vertexPositionWorld_o;
in vec3 skycolor;
in float visibility;
in vec4 clipSpace;
in vec3 vertexToCamera;

const float near=1.0;
const float far=4000;

void main()
{
	vec3 viewVector=normalize(vertexToCamera);
	if(visibility<0.3)discard;
	daColor=vec4(1,0,0,1);
}