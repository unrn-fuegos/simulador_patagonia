#version 330
layout (location = 0) out vec4 daColor;

in vec3 uv;

uniform sampler2D tex0;

void main()
{
	vec4 tex=texture(tex0,uv.xy).rgba;
	if(length(tex.rgb)<0.1)discard;
	daColor=tex;
}
