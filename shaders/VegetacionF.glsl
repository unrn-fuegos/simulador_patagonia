#version 330
layout (location = 0) out vec4 daColor;
in vec4 color;
in vec4 normalWorld;
in vec3 vertexPositionWorld;
in vec2 uv;
in vec4 prelit;
in vec3 skycolor;

uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform sampler2D tex0;

uniform vec3 ambientLightColor;
uniform vec3 skyColor;

void main()
{
	float val=abs(dot(cameraDirection,normalWorld.xyz));

	float brillo=dot(vec4(0,0,1,0),normalize(normalWorld));
	brillo=clamp(brillo,0,1);
	brillo=pow(brillo,30);
	vec4 diffuseLight=0.4*vec4(brillo,brillo,brillo,1);

	float brilloc=dot(normalize(vec4(0,1,0.1,0)),normalize(normalWorld));
	brilloc=clamp(brilloc,0,1);
	brillo=pow(brilloc,10);
	vec4 diffuseLightc=vec4(brilloc,brilloc,brilloc,1);
	
	vec4 reflectedLightVector=reflect(vec4(0,0,-1,0),normalWorld);
	vec4 eyeVectorWorld=normalize(vec4(cameraPosition,0)-vec4(vertexPositionWorld,0));
	float s=dot(reflectedLightVector,eyeVectorWorld);
	s=clamp(s,0,1);
	s=pow(s,20);
	//vec4 specularLight=vec4(s,s,s,1)*ambientLight;
	vec4 specularLight=vec4(s,s,s,1);

	
	//vec4 coloro=vec4(0.5,0,0,1);
	vec4 coloro=color;
	//daColor=coloro+clamp(specularLight,0,0.1)+clamp(diffuseLightc,0,0.1)+clamp(diffuseLight,0,1);
	//daColor=vec4(normalize(normalWorld.xyz),1);
	daColor=vec4(texture(tex0,uv.xy).rgb,1);
}

