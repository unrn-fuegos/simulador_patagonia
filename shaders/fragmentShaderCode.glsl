#version 330
layout (location = 0) out vec4 daColor;

in vec3 uv;
in vec3 uvf;
in vec4 normalWorld;
in vec3 vertexPositionWorld;

uniform vec3 cameraDirection;
uniform vec3 eyePositionWorld;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform float wind_a;
uniform float wind_pot;
uniform float ambient_mult;
uniform float specular_mult;
uniform float diffuse_mult;
uniform float diffuseC_mult;

void main()
{
	float brillo=dot(vec4(0,1,0,0),normalize(normalWorld));
	brillo=clamp(brillo,0,1);
	brillo=pow(brillo,10);
	vec4 diffuseLight=vec4(brillo,brillo,brillo,1);
	
	float brilloc=dot(-vec4(cameraDirection,0),normalize(normalWorld));
	brilloc=clamp(brilloc,0,1);
	brilloc=pow(brilloc,4);
	vec4 diffuseLightc=vec4(brilloc,brilloc,brilloc,1);
	
	vec4 reflectedLightVector=reflect(vec4(0,-1,0,0),normalWorld);
	vec4 eyeVectorWorld=normalize(vec4(eyePositionWorld,0)-vec4(vertexPositionWorld,0));
	float s=dot(reflectedLightVector,eyeVectorWorld);
	s=clamp(s,0,1);
	s=pow(s,10);
	vec4 specularLight=vec4(s,s,s,1);

	vec4 texf;
	texf=vec4(texture(tex2,uvf.xy).rgb,1.0); //aca estuve probando
	float wind_a_new=wind_a;

	
	
	if(texf.r<=0.2){
		wind_a_new=0.0;	
	}
	else
	{
		vec4 texcolormap=texture(tex3,vec2(uvf.z,0.5)).rgba;
		texf.rgb=texcolormap.rgb;
	}

	vec4 tex;
	tex=mix(vec4(texture(tex0,uv.xy).rgb,1.0),vec4(texture(tex1,uv.xy).rgb,1.0),0.0); //aca estuve probando
	float slope=texture(tex1,uv.xy).b;
	float a=0.4*texture(tex1,uv.xy).r;
	vec4 solL=vec4(slope,slope,slope,1);

	if(uv.x>1 || uv.y>1) discard;
	
	vec4 auxi=diffuse_mult*clamp(diffuseLight,0,1)+specular_mult*clamp(specularLight,0,1);
	daColor=mix(tex,texf,wind_a_new)+diffuseC_mult*clamp(diffuseLightc,0,1)+auxi+ambient_mult*vec4(1,1,1,1);
	//daColor=vec4(uv.xy,0,1);
}

