#version 330
layout (location = 0) out vec4 daColor;

in vec4 Vertex_UV;
in float ls;
in float texme;

uniform sampler2DArray textura;

void main()
{
	//daColor = vec4(1.0, 0.0, 0.0, 1-ls);
	vec4 col1=texture(textura,Vertex_UV.xyz).rgba;
	vec4 col2=texture(textura,Vertex_UV.xyw).rgba;
	daColor=mix(col1,col2,texme);
//daColor = vec4(1.0, 0.0, 0.0, 1);
	if(daColor.a < 0.05)discard;
	
}