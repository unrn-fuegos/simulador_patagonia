#version 330
layout (location = 0) out vec4 daColor;
in vec4 uv;

uniform sampler2D tex0;

float LinearizeDepth(in vec2 uv)
{
    float zNear = 1;    // TODO: Replace by the zNear of your perspective projection
    float zFar  = 4000.0; // TODO: Replace by the zFar  of your perspective projection
    float depth = texture(tex0, uv).x;
    return (2.0 * zNear) / (zFar + zNear - depth * (zFar - zNear));
}


void main()
{
	vec4 coloro=texture(tex0,uv.xy).rgba;
	if(length(coloro.rgb)<0.1)coloro.a=0.0;
	//coloro.a=1;
	//float c = LinearizeDepth(uv.xy);
    	//daColor = vec4(c, c, c, 1.0);
	daColor=coloro;
}
