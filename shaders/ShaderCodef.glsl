#version 330
layout (location = 0) out vec4 daColor;
in vec4 color;
in vec4 normalWorld;
in vec3 vertexPositionWorld;
in vec4 uv;
in vec4 prelit;
in vec3 skycolor;
in float visibility;

uniform vec3 cameraDirection;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform sampler2DArray tex0;
uniform sampler2DArray tex1;
uniform sampler2DArray tex2;
uniform sampler2DArray tex3;

uniform float headlightenabled;
uniform vec3 ambientLightColor;
uniform vec3 skyColor;

void main()
{
	vec4 coloro;
	int aux=int(floor(uv.a+0.5));

	if(aux==0){coloro=texture(tex0,uv.rgb).rgba;}
	else if(aux==1){coloro=texture(tex1,uv.rgb).rgba;}
	else if(aux==2){coloro=texture(tex2,uv.rgb).rgba;}
	else if(aux==3){coloro=texture(tex3,uv.rgb).rgba;}
	if(coloro.a < 0.1)discard;

	float luz=0.0f;
	if(headlightenabled==1.0f){
		float cos=dot(HeadLightDir,normalize(vertexPositionWorld-HeadLightPos));
		float obt=0;
		if(cos>0.4){
			obt=1;
			float ced=length(cross(HeadLightDir,vertexPositionWorld-HeadLightPos));
			float dist=dot(HeadLightDir,vertexPositionWorld-HeadLightPos);
			luz=obt*1/(1+ced*ced*ced/(dist));
		}
	}
	
	coloro.rgb=(ambientLight*ambientLightColor+mix(color.rgb,prelit.rgb,ambientLight)+luz)*coloro.rgb;
	coloro.a=mix(color.a,prelit.a,ambientLight)*coloro.a;
	
	daColor=mix(vec4(skyColor,1),coloro,visibility);
	//daColor=vec4(0,0.5,0,1);
}