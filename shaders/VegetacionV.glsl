#version 330
/*layout (binding=0) uniform BNTest
{
	mat4 MTWM;
};*/
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexNormalModel;
layout (location = 2) in vec3 vertexUV;

uniform vec3 domiColor;
uniform mat4 modelTransformMatrix;
uniform mat4 fullTransfWTP;
uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform vec3 ambientLight;
uniform vec3 skyColor;
uniform vec4 ClipPlane;

out vec4 color;
out vec4 normalWorld;
out vec3 vertexPositionWorld;
out vec2 uv;
out vec4 prelit;

// All components are in the range [0�1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
	vec4 vpM=vec4(vertexPositionModel,1);
	vec4 vpW=modelTransformMatrix * vpM;
	vec3 totalLight;
	
	
	gl_Position=fullTransfWTP * vpW;
	//gl_Position=vpM;

	normalWorld=modelTransformMatrix*vec4(vertexNormalModel,0);
	float min=0.0;
	float max=117.0;
	float hue=(vpW.b-min)/(max-min);
	//hue=0.5;
	//hue=clamp(hue,1,0);


	uv=vertexUV.xy;
	vec3 rgbcol=hsv2rgb(vec3(clamp(hue,0,1),1,1));
	color=vec4(rgbcol.rgb,1);
	vertexPositionWorld=vec3(vpW);
}

