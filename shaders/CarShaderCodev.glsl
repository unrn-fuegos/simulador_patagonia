#version 330
/*layout (binding=0) uniform BNTest
{
	mat4 MTWM;
};*/
layout (location = 0) in vec4 vertexPosition;
layout (location = 1) in vec2 vertexUV;

uniform vec3 domiColor;
uniform vec3 skyColor;

//out vec4 vertexPosition;
out vec2 uv;

// All components are in the range [0�1], including hue.
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
	//vertexPosition=vertexPositionModel;
	gl_Position=vertexPosition;
	uv=vertexUV;
}