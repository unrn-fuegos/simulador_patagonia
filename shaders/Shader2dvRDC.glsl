#version 330
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexNormalModel;
layout (location = 2) in vec3 vertexUV;
layout (location = 3) in vec4 vertexPrelit;
layout (location = 4) in vec4 vertexColor;
uniform vec3 domiColor;
uniform mat4 modelTransformMatrix;
uniform mat4 fullTransfWTP;
uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform vec3 ambientLight;
uniform vec3 skyColor;

out vec4 color;
out vec4 normalWorld;
out vec3 vertexPositionWorld;
out vec4 uv;
out vec4 prelit;
out float visibility;

const float density=0.001;
const float gradient=5.5;

void main()
{
	gl_Position=vec4(vertexPositionModel,1.0);;
	float angle=asin(cameraDirection.z);
	color=vertexColor;
	float disp=-angle/3.1415;
	vec2 uv_t=vec2(vertexUV.r+disp,vertexUV.g+disp);
	
	uv=vec4(uv_t.r,uv_t.g,0,0);
	prelit=vertexPrelit;	
}