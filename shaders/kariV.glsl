#version 330
/*layout (binding=0) uniform BNTest
{
	mat4 MTWM;
};*/
layout (location = 0) in vec4 vertexPosition;
layout (location = 1) in vec2 vertexUV;

uniform vec3 domiColor;
uniform vec3 skyColor;

out vec2 uv;


void main()
{
	//vertexPosition=vertexPositionModel;
	gl_Position=vertexPosition;
	uv=vertexUV;
}