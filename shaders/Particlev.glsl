#version 330
layout (location = 0) in vec4 pointPosition;
layout (location = 1) in vec4 pointAditional;
//para pasarle cuanto tiempo de vida le queda va a haber que usar otro layout con un solo float...


uniform mat4 matrixWTV;

out Vertex
{
  float particle_size;
  float rotation;
  float lifespan;
} vertex;


void main()
{
	gl_Position = matrixWTV*vec4(pointPosition.xyz, 1.0);
	vertex.particle_size=pointPosition.w;
	vertex.rotation=pointAditional.y;
	vertex.lifespan=pointAditional.x;
}