#version 330
layout (location = 0) in vec4 vertexPositionModel;
layout (location = 1) in vec3 vertexNormalModel;
layout (location = 2) in vec3 vertexUV;
layout (location = 3) in vec4 vertexPrelit;
layout (location = 4) in vec4 vertexColor;
uniform vec3 domiColor;
uniform mat4 fullTransfWTP;
uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform vec3 ambientLight;
uniform vec3 skyColor;

out vec3 vertexToCamera;
out vec4 clipSpace;
out vec4 color;
out vec4 normalWorld;
out vec3 vertexPositionWorld;
out vec2 uv;
out vec4 prelit;
out float visibility;

const float density=0.001;
const float gradient=5.5;

void main()
{
	clipSpace=fullTransfWTP * vertexPositionModel;
	vec3 totalLight;
	
	gl_Position=clipSpace;
	
	normalWorld=vec4(vertexNormalModel,0);
	
	
	vertexPositionWorld=vec3(vertexPositionModel);
	vertexToCamera=cameraPosition-vertexPositionWorld;
	float distance=length(vertexToCamera);
	visibility=exp(-pow(distance*density,gradient));
	visibility=clamp(visibility,0,1);
	vertexPositionWorld.xy+=vertexUV.xy;
	uv=vec2(vertexPositionWorld.x*2-1,vertexPositionWorld.y*2-1)*0.01;	
}