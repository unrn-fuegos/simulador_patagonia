#version 330
layout (location = 0) out vec4 daColor;
in vec4 color;
in vec4 normalWorld;
in vec3 vertexPositionWorld;
in vec4 uv;
in vec4 prelit;
in vec3 skycolor;
in float visibility;

uniform vec3 cameraDirection;
uniform vec3 eyePositionWorld;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform sampler2D tex0;
uniform sampler2D tex1;

uniform float headlightenabled;
uniform vec3 ambientLightColor;
uniform vec3 skyColor;

float LinearizeDepth(in vec2 uv)
{
    float zNear = 1;    // TODO: Replace by the zNear of your perspective projection
    float zFar  = 4000.0; // TODO: Replace by the zFar  of your perspective projection
    float depth = texture(tex0, uv).x;
    return (2.0 * zNear) / (zFar + zNear - depth * (zFar - zNear));
}


void main()
{
	vec4 coloro=texture(tex0,uv.xy).rgba;
	coloro.a=1;
	float c = LinearizeDepth(uv.xy);
    	daColor = vec4(c, c, c, 1.0);
	daColor=coloro;
}