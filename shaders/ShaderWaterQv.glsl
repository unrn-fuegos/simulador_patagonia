#version 330
layout (location = 0) in vec4 vertexPositionWorld;

uniform mat4 fullTransfWTP;
uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform vec3 ambientLight;
uniform vec3 skyColor;

out vec3 vertexToCamera;
out vec4 clipSpace;
out vec4 vertexPositionWorld_o;
out float visibility;

const float density=0.001;
const float gradient=5.5;

void main()
{
	clipSpace=fullTransfWTP * vertexPositionWorld;
	
	gl_Position=clipSpace;
	
	
	vertexPositionWorld_o=vertexPositionWorld;

	vertexToCamera=cameraPosition-vec3(vertexPositionWorld);
	float distance=length(vertexToCamera);
	visibility=exp(-pow(distance*density,gradient));
	visibility=clamp(visibility,0,1);
	//vertexPositionWorld.xy+=vertexUV.xy;	
}