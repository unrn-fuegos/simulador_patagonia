#version 330
layout (location = 0) out vec4 daColor;

//in vec4 vertexPosition;
in vec2 uv;

uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;

uniform vec3 ambientLightColor;
uniform vec3 skyColor;

void main()
{
	float vege=texture(tex3,uv).b/7; /* dividido el valor maximo posible para llevarlo entre 0 y 1 --> vegetacion va de 0..7 */
	//vege=1.0;//COMENTAR O DESCOMENTAR
	vec3 text0=texture(tex0,uv).rgb;   // aca es donde se mezclan los colores y se pintan vegetacion + I + S y se pintan
	//daColor=vec4(vec3(text0.r*vege!=0.0f?1.0f:0.0f,text0.g*vege<1.0f?0.0f:1.0f,text0.b),1);
	daColor=vec4(vec3(text0.r*vege,text0.g*vege,text0.b),1);
	//daColor=vec4(vec3(1.0,1.0,text0.b),1);
	//daColor=vec4(uv,0,1);
}



//hola
