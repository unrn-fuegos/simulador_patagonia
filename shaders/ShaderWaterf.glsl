#version 330
layout (location = 0) out vec4 daColor;
in vec4 color;
in vec4 normalWorld;
in vec3 vertexPositionWorld;
in vec2 uv;
in vec4 prelit;
in vec3 skycolor;
in float visibility;
in vec4 clipSpace;
in vec3 vertexToCamera;

uniform vec3 cameraDirection;
uniform vec3 eyePositionWorld;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform sampler2D tex0;//reflejado
uniform sampler2D tex1;//dudvMap
uniform sampler2D tex2;//refractado
uniform sampler2D tex3;//depthTexture

uniform float headlightenabled;
uniform vec3 ambientLightColor;
uniform vec3 skyColor;

const float near=1.0;
const float far=4000;

void main()
{
	vec3 viewVector=normalize(vertexToCamera);
	if(visibility<0.2)discard;
	
	vec2 distortion=(texture(tex1,vec2(uv.x,uv.y)).rg*2-1)*0.02;
	//distortion=vec2(0,0);
	vec3 waterNormal=normalize(vec3(distortion.x,distortion.y,5.0));
	float refractiveFactor=clamp(dot(waterNormal,viewVector),0,0.7);

	vec2 ndc=(clipSpace.xy/clipSpace.w)/2.0+0.5;
	vec2 reflectionCoord=vec2(ndc.x,-ndc.y);
	vec2 refractionCoord=vec2(ndc.x,ndc.y);
	float depth=texture(tex3,refractionCoord).r;
	float floorDistance=2*near*far/(far+near-(2*depth-1)*(far-near));

	depth = gl_FragCoord.z;
	float waterDistance=2*near*far/(far+near-(2*depth-1)*(far-near));

	float waterDepth=floorDistance-waterDistance;
	distortion=distortion*clamp(waterDepth/20.0,0,1);

	reflectionCoord+=distortion;
	reflectionCoord.x=clamp(reflectionCoord.x,0.001,0.999);
	reflectionCoord.y=clamp(reflectionCoord.y,-0.999,-0.001);
	vec4 reflColor=texture(tex0,reflectionCoord.xy).rgba;
	vec4 refrColor=texture(tex2,refractionCoord.xy).rgba;

	vec4 coloro=mix(reflColor,refrColor,refractiveFactor);
	float depthFactor=clamp(waterDepth/50.0,0.5,1);

	coloro=mix(coloro,vec4(0,0.3,0.5,1.0),0.2);

	daColor=mix(vec4(skyColor,1),coloro,visibility);
	daColor.a=depthFactor;
	//daColor=vec4(waterDepth/30.0,waterDepth/50.0,waterDepth/50.0,1.0);
}