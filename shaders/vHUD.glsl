#version 330
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexUV;
uniform mat4 fullTransfMTP;

out vec3 uv;

void main()
{
	vec4 vpM=vec4(vertexPositionModel,1.0);
	
	gl_Position=fullTransfMTP * vpM;
	uv=vertexUV;
}
