#version 330
layout (location = 0) in vec3 vertexPositionModel;
layout (location = 1) in vec3 vertexUV;
layout (location = 2) in vec3 vertexNormal;
layout (location = 3) in vec3 vertexUVF;
uniform mat4 modelTransformMatrix;
uniform vec3 cameraDirection;
uniform vec3 eyePositionWorld;
uniform mat4 fullTransfWTP;
uniform float wind_a;

out vec3 uv;
out vec3 uvf;
out vec4 normalWorld;
out vec3 vertexPositionWorld;

void main()
{
	vec4 vpM=vec4(vertexPositionModel,1.0);
	vec4 vpW=modelTransformMatrix * vpM;
	
	gl_Position=fullTransfWTP * vpW;
	normalWorld=modelTransformMatrix*vec4(vertexNormal,0);
	
	uvf=vertexUVF;
	uv=vertexUV;
	vertexPositionWorld=vec3(vpW);
	
}
