#version 330
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
uniform mat4 projectionMatrix;
uniform float numSprites;

in Vertex
{
  float particle_size;
  float rotation;
  float lifespan;
} vertex[];

out vec4 Vertex_UV;
out float ls;
out float texme;


void main()
{
  vec4 P = gl_in[0].gl_Position;
  float exc=vertex[0].lifespan*numSprites;
  float texz=floor(exc);
  float texw=texz+1;
  exc=exc-texz;

  clamp(texw,0,numSprites);
  

  // a: left-bottom 
  vec2 va = P.xy + vec2(-0.5, -0.5) * vertex[0].particle_size;
  gl_Position = projectionMatrix * vec4(va, P.zw);
  Vertex_UV = vec4(0.0, 0.0,texz,texw);
  ls=vertex[0].lifespan;
  texme=exc;
  //Vertex_Color = vertex[0].color;
  EmitVertex();  
  
  // b: left-top
  vec2 vb = P.xy + vec2(-0.5, 0.5) * vertex[0].particle_size;
  gl_Position = projectionMatrix * vec4(vb, P.zw);
  Vertex_UV = vec4(0.0, 1.0,texz,texw);
  ls=vertex[0].lifespan;
  texme=exc;
  //Vertex_Color = vertex[0].color;
  EmitVertex();  
  
  // d: right-bottom
  vec2 vd = P.xy + vec2(0.5, -0.5) * vertex[0].particle_size;
  gl_Position = projectionMatrix * vec4(vd, P.zw);
  Vertex_UV = vec4(1.0, 0.0,texz,texw);
  ls=vertex[0].lifespan;
  texme=exc;
  //Vertex_Color = vertex[0].color;
  EmitVertex();  

  // c: right-top
  vec2 vc = P.xy + vec2(0.5, 0.5) * vertex[0].particle_size;
  gl_Position = projectionMatrix * vec4(vc, P.zw);
  Vertex_UV = vec4(1.0, 1.0,texz,texw);
  ls=vertex[0].lifespan;
  texme=exc;
  //Vertex_Color = vertex[0].color;
  EmitVertex();  

  EndPrimitive();  
}