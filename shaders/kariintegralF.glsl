#version 330
layout (location = 0) out vec4 daColor;

//in vec4 vertexPosition;
in vec2 uv;

uniform vec3 cameraDirection;
uniform vec3 cameraPosition;
uniform float ambientLight;
uniform vec3 HeadLightPos;
uniform vec3 HeadLightDir;

uniform float dt;

uniform sampler2D tex0;
uniform sampler2D tex1;

uniform vec3 ambientLightColor;
uniform vec3 skyColor;

void main()
{
	vec4 outc;
	//outI=dIdt*dt+I;
	//outI=dSdt*dt+S;
	outc.r=texture(tex0,uv).r*dt+texture(tex1,uv).r;

	if(outc.r<0.0f)outc.r=0.0f;//PARA IMPEDIR INCENDIANDOSE MENORES QUE CERO

	outc.g=texture(tex0,uv).g*dt+texture(tex1,uv).g;
	/*outc.r=clamp(texture(tex0,uv).r*dt+texture(tex1,uv).r,-100.0,100.0);
	outc.g=clamp(texture(tex0,uv).g*dt+texture(tex1,uv).g,-100.0,100.0);*/
	outc.b=max(texture(tex0,uv).b,texture(tex1,uv).b);
	//outc.b=0.0;

	if(texture(tex0,uv).b==0.0f)
	{
		//outc.r=0.0;
		//outc.g=0.0;
	}
	outc.a=1;
	
	//daColor=vec4(texture(tex1,uv).rgb,1);
	//daColor=vec4(uv,0,1);
	daColor=outc;
	//daColor=vec4(uv.r*1.1,0,0,1);
	//daColor=texture(tex0,centro).rgba;

	
}

