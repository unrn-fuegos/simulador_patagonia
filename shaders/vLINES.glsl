#version 330
layout (location = 0) in vec3 vertexPositionModel;
uniform mat4 modelTransformMatrix;
uniform vec3 cameraDirection;
uniform vec3 eyePositionWorld;
uniform mat4 fullTransfWTP;
uniform float wind_a;

out vec3 vertexPositionWorld;

void main()
{
	vec4 vpM=vec4(vertexPositionModel,1.0);
	vec4 vpW=modelTransformMatrix * vpM;
	
	gl_Position=fullTransfWTP * vpW;
	
	vertexPositionWorld=vec3(vpW);
	
}
